<div align="center">

![Magma wallet logo](README-Assets/magma.png "Magma wallet logo")

# Magma iOS

</div>

<br/>

Magma is a secure native wallet app for the Tezos blockchain written in Swift, designed and developed by [camlCase Inc.](https://camlcase.io/). Magma leverages [camlKit](https://gitlab.com/camlcase-dev/camlkit) to create wallets and communicate with the Tezos blockchain. Magma allows you to store and access your XTZ, view your transaction history and exchange XTZ for FA1.2 tokens by using the [Dexter decentralized exchange](http://dexter.camlcase.io/).

Magma uses the [CoinGecko](https://www.coingecko.com/en) API to display XTZ prices in your local currency. Magma will calculate prices for FA1.2 tokens, by calculating its Dexter exchange rate, and multiplying by the xtz local currency price.

Magma is completely open source to allow users to verify its contents. If you encounter an issue with Magma, please contact [support@camlcase.io](mailto:supprt@camlcase.io).





<br/>
<br/>

## Dependencies

Library | License | Use
--- | --- | ---
[camlKit](https://gitlab.com/camlcase-dev/camlkit) | [MIT License](https://gitlab.com/camlcase-dev/camlkit/-/blob/master/LICENSE) | Creating wallets, communicating with the Tezos node and Baking bad services
[KeychainSwift](https://github.com/evgenyneu/keychain-swift) | [MIT License](https://github.com/evgenyneu/keychain-swift/blob/master/LICENSE) | Helper functions for storing data in the keychain
[Sentry](https://github.com/getsentry/sentry-cocoa) | [MIT License](https://github.com/getsentry/sentry-cocoa/blob/master/LICENSE.md) | Error reporting
[Kingfisher](https://github.com/onevcat/Kingfisher) | [MIT License](https://github.com/onevcat/Kingfisher/blob/master/LICENSE) | Downloading remote images, such as Baker logos
[Toast-Swift](https://github.com/scalessec/Toast-Swift) | [MIT License](https://github.com/scalessec/Toast-Swift/blob/master/LICENSE) | Displaying toast's in the app to alert the user to actions being completed
 |  | 
**API** | **License** | **Use**
[Baking Bad](https://baking-bad.org/docs/api/) | [Terms of use](https://baking-bad.org/terms/) | Fetching a list of active bakers
[CoinGecko](https://www.coingecko.com/en/api) | [Terms of use](https://www.coingecko.com/en/api_terms) | XTZ local currency values






<br/>
<br/>

## Running locally

### Dependencies
Magma uses Cocoapods to manage its dependencies. If you don't have cocoapods already installed. Open terminal and run:

```bash
sudo gem install cocoapods
```

Then clone the magma repo, open terminal, cd to the repo and run the command

```bash
pod install
```


### Secrets
Magma also makes use of [Cocoapods-Keys](https://github.com/orta/cocoapods-keys) to store and scramble API keys and manage other environment variables. After running `pod install` you will be asked to supply credentials for the following:



- **sentryDSN**  
Magma uses [Sentry.io](https://sentry.io/) for crash and error reporting to ensure the quality of the app. A DSN is needed to connect to sentry on startup.

- **messariAPIKey**  
An API key from Messari to access USD rates for XTZ.

- **tezosNodeURL**  
The URL to the mainnet Tezos node used by the app.

- **tezosNodeURL_parse**  
The URL to the mainnet Tezos node used by the app to verify remote forges were done correctly. For security reasons this should be a different node to the previous. Ideally on a different network.

- **tezosNodeURL_credentials**  
Base64 encoded string containing the basic auth credentials needed to access the Tezos Node. blank string if not needed.

- **tezosNodeURL_testnet**  
The URL to the testnet Tezos node used by the app.

- **tezosNodeURL_testnet_parse**  
The URL to the testnet Tezos node used by the app to verify remote forges were done correctly. For security reasons this should be a different node to the previous. Ideally on a different network.

- **tezosNodeURL_testnet_credentials**  
Base64 encoded string containing the basic auth credentials needed to access the Tezos Node. blank string if not needed.

- **tzktURL**  
The URL to the mainnet TZKT indexer. Info can be found [here](https://api.tzkt.io/)

- **tzktURL_testnet**  
The URL to the testnet TZKT indexer. Info can be found [here](https://api.tzkt.io/)

- **uitest_onboardingMnemonic**  
A mnemonic belonging to an existing testnet wallet, used by automated UI tests to test various steps and flows of the onboarding process. Can be a blank string if you don't plan to run UI tests.

- **uitest_transactionMnemonic1**  
A mnemonic belonging to an existing testnet wallet, used by automated UI tests to test various tests involving sending or exchanging XTZ or FA1.2 tokens. This account needs to be funded. Can be a blank string if you don't plan to run UI tests.

- **uitest_transactionAddress1**  
The tz1 address belonging to the previous mnemonic. Can be a blank string if you don't plan to run UI tests.

- **uitest_transactionMnemonic2**  
A mnemonic belonging to an existing testnet wallet, used by automated UI tests to test various tests involving sending or exchanging XTZ or FA1.2 tokens. This account does not need to be funded. Can be a blank string if you don't plan to run UI tests.

- **uitest_transactionAddress2**  
The tz1 address belonging to the previous mnemonic. Can be a blank string if you don't plan to run UI tests.





### API Keys and URLS
- Sentry and Messari both offer limited free accounts that you can setup to get API keys for testing.
- There are public public Tezos nodes available from [Tezos Giga Node](https://tezos.giganode.io/) and [Nautilus Cloud](https://nautilus.cloud/).
- For TZKT, you can use the [publicly available nodes](https://api.tzkt.io/) for experimenting. Deploying an app like this to production would require hosting your own nodes.


### Running the app
Open the MagmaWallet.xcworkspace in Xcode. There are 2 Schemes:

- **MagmaWallet**  
Used for running locally with Debug (pointing to Testnet), and archiving with Release (pointing to mainnet)

- **MagmaWallet Testnet**  
Used for archiving a version of the app pointing to Testnet. So it can be installed and tested on non-development devices.

Sharing 3 configurations:

- **Debug**  
Intended for running locally for development purposes.

- **Release**  
Intended for production releases with app pointing to Mainnet.

- **Testnet**  
Intended for internal releases, for testing, pointing to Testnet.





<br/>
<br/>

## Project structure and architecture

Magma is a native Swift app, built using Storyboards and a custom light weight MVVM implementation relying on generics and protocols. MVVM components will be refined and released separately in the future.

The app is broken up into the following main folders:

- **Modules**  
Using the mini storyboard approach, the app is broken up into logical modules comprising of a storyboard, view controllers, view models and cells. Navigation is managed via segues, with some minor exceptions implemented in code. The app flow starts with the LaunchScreen. This screen is then duplicated as the initial viewController inside Launch.storyboard. This screen will act as the entry point deciding whether to go to onboarding, login or to home. This approach gives us multiple benefits, such as a single place where all the navigation logic resides for when the app dies and returns, implemented in a manner native to the apple SDK's avoiding the need for additional custom layers such as co-ordinators, and also gives a place to add some startup animations and effects for first time launch.
  
  ```mermaid
  graph LR;
  LaunchScreen-->Launch.Storyboard;
  Launch.Storyboard-->Onboarding.Storyboard;
  Launch.Storyboard-->Login.Storyboard;
  Launch.Storyboard-->Home.Storyboard;
  ```

- **Controls**  
Classes and subclasses needed to reuse logic across the app. Such as validators for textfield input or UIButton / UILabel subclasses with IBDesignable's to enable styling reuse across storyboards.

- **Shared Cells**  
UITableViewCell's that are shared across multiple screens.

- **Models**  
Models shared throughout the app.

- **Extensions**  
Extensions to create helper functions, such as UIStoryboard helpers to find and instantiate a ViewController for reuse, or UINavigationController extension to pop back to a given viewController.

- **MVVM**  
Protocols, Models and Extensions that serve as a light weight template to leverage MVVM. Without forcing a complete paradigm shift from the standard Apple libraries and patterns.

- **Services**  
Service classes are responsible for networking, caching and filtering data from various sources. As well as holding on to app state for its usecase, as the state is needed across many screens and flows. These classes are managed by a singleton `DependencyManager` which instantiates the service classes with a `URLSession` or a `URLSessionMock` object depending on whether it is running inside `XCTest` or not. ViewModels and ViewControllers will use these classes to fetch data and also to pass data (textfield input, dropdown choices) between each other. This facilitates transferring data across long flows without ViewControllers having to act as intermediaries for data that is irrelevant to them or again introducing custom layers such as co-ordinators. This helps us keep the interaction points between classes as simple as possible for readability and debugability. The `DependencyManager` also holds onto clients and config files needed for [camlKit](https://gitlab.com/camlcase-dev/camlkit/), so that classes can make use of them too, where needed.

  Most services follow a similar pattern / structure with the exception of the `HomeService` which is in charge of fetching many separate pieces of data (e.g. XTZ balance, balance for each FA1.2 token, Dexter exchange rates, CoinGecko prices etc.) all required for the wallet home screen and shared with some others. This class has implemented a Delegate array, allowing multiple classes to feed from the same data and respond to the same events. This approach should feel more familiar to iOS developers and apple SDK users, rather than introducing large third party dependencies, such as RxSwift, requiring a steep learning curve to integrate.

  ```mermaid
  graph TD;
    DependencyManager-->URLSession;
    DependencyManager-->URLSessionMock;
    URLSession-->HomeService;
    URLSessionMock-->HomeService;
    URLSession-->TokenPriceService;
    URLSessionMock-->TokenPriceService;
    URLSession-->AccountService;
    URLSessionMock-->AccountService;
  ```
