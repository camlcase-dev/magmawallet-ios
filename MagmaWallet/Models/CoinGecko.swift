//
//  CoinGeckoExchangeRate.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 09/12/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation


// MARK: - Exhcnage Rates

struct CoinGeckoExchangeRateResponse: Codable {
	let rates: [String: CoinGeckoExchangeRate]
}

struct CoinGeckoExchangeRate: Codable {
	let name: String
	let unit: String
	let value: Decimal
	let type: String
}


// MARK: - Current Price

struct CoinGeckoCurrentPrice: Codable {
	let tezos: [String: Decimal]
	
	func price() -> Decimal {
		guard let firstKey = tezos.keys.first, let price = tezos[firstKey] else {
			return 0
		}
		
		return price
	}
}
