//
//  MoonPay.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 10/12/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation

struct MoonPayCountry: Codable {
	
	let alpha2: String
	let alpha3: String
	let isAllowed: Bool
	let isBuyAllowed: Bool
	let isSellAllowed: Bool
	let name: String
}

struct MoonPayCurrency: Codable {
	let name: String
	let code: String
	let isSupportedInUS: Bool?
}
