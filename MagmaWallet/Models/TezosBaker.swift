//
//  TezosBaker.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 14/04/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import camlKit
import os.log

struct TezosBaker: Codable {
	
	var address: String
	let name: String
	let logo: String
	let fee: Decimal			// 0 -> 1 as a percentage
	let minDelegation: Decimal?
	let openForDelegation: Bool
	let estimatedRoi: Decimal	// 0 -> 1 as a percentage
	let serviceHealth: String	// only "active"
	let payoutTiming: String	// only "stable"
	let payoutAccuracy: String	// only "precise"
	
	var logoURL: URL? {
		get {
			return URL(string: logo)
		}
	}
	
	func passesFilter() -> Bool {
		return (fee < 0.16 && estimatedRoi >= 0.05 && openForDelegation)
	}
	
	func minDelegationAsXTZ() -> XTZAmount {
		return XTZAmount(fromNormalisedAmount: self.minDelegation ?? 0)
	}
}
