//
//  TezosContact.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 03/09/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import Contacts

struct TezosContact: Codable {
	
	public static let systemContactLabel = "Tezos Address"
	
	enum NetworkProtocol: String, Codable {
		// mainnet is not a procotol, but will be used to denote Mainnet vs Testnet addresses
		case mainnet
		
		// Testnet
		case carthagenet
		case unknown // will assume latest Testnet
	}
	
	let contactIdentifier: String
	let fullName: String
	let tezosAddress: String
	let networkProtocol: NetworkProtocol
	let containsOnlyMagmaData: Bool
	
	
	init?(withContact contact: CNContact) {
		self.contactIdentifier = contact.identifier
		
		let nameString = TezosContact.fullName(fromContact: contact)
		let tezosData = TezosContact.existingTezosAddress(contact)
		
		guard let name = nameString, let data = tezosData else {
			// Doesn't contain the correct data
			return nil
		}
		
		
		let components = data.username.components(separatedBy: "//")
		let proto = components[0]
		let address = components[1]
		
		
		self.fullName = name
		self.networkProtocol = NetworkProtocol(rawValue: proto) ?? .unknown
		self.tezosAddress = address
		
		// Check to see is there any other data attached to contact. Store the result of the check and ignore the data itself
		self.containsOnlyMagmaData =
			(
				contact.phoneNumbers.count == 0 &&
				contact.emailAddresses.count == 0 &&
				contact.urlAddresses.count == 0 &&
				contact.postalAddresses.count == 0 &&
				contact.socialProfiles.count == 0
			)
	}
	
	static func fullName(fromContact contact: CNContact) -> String? {
		return CNContactFormatter.string(from: contact, style: .fullName)
	}
	
	static func systemContactPrefix() -> String {
		#if TESTNET
		return NetworkProtocol.carthagenet.rawValue
		#else
		return NetworkProtocol.mainnet.rawValue
		#endif
	}
	
	static func createSystemContactAddress(_ address: String) -> String {
		return "\(TezosContact.systemContactPrefix())//\(address)"
	}
	
	static func existingTezosAddress(_ contact: CNContact) -> (instantMessageIndex: Int, username: String)? {
		var tezosDataIndex: Int? = nil
		var tezosData: String? = nil
		
		for (index, instantMessage) in contact.instantMessageAddresses.enumerated() {
			if instantMessage.label == TezosContact.systemContactLabel {
				
				let username = instantMessage.value.username
				let proto = username.components(separatedBy: "//").first
				
				// Only return data, if it is using the same protocol
				if let p = proto, p == TezosContact.systemContactPrefix() {
					tezosDataIndex = index
					tezosData = instantMessage.value.username
					break
				}
			}
		}
		
		if let index = tezosDataIndex, let data = tezosData {
			return (instantMessageIndex: index, username: data)
		}
		
		return nil
	}
}
