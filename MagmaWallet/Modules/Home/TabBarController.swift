//
//  TabBarController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 28/04/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
	
	static let walletIndex = 0
	static let exchangeIndex = 1
	static let contactsIndex = 2
	static let activityIndex = 3
	static let settingsIndex = 4
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.navigationItem.largeTitleDisplayMode = .never
		self.navigationItem.hidesBackButton = true
		
		self.tabBar.clipsToBounds = true
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		if let items = tabBar.items {
			for (index, barItem) in items.enumerated() {
				
				if index == TabBarController.walletIndex {
					barItem.image = UIImage(named: "Wallet-Inactive")?.withRenderingMode(.alwaysOriginal)
					barItem.selectedImage = UIImage(named: "Wallet-Active")?.withRenderingMode(.alwaysOriginal)
					
				} else if index == TabBarController.exchangeIndex {
					barItem.image = UIImage(named: "Exchange-Inactive")?.withRenderingMode(.alwaysOriginal)
					barItem.selectedImage = UIImage(named: "Exchange-Active")?.withRenderingMode(.alwaysOriginal)
					
				} else if index == TabBarController.contactsIndex {
					barItem.image = UIImage(named: "Contact-Inactive")?.withRenderingMode(.alwaysOriginal)
					barItem.selectedImage = UIImage(named: "Contact-Active")?.withRenderingMode(.alwaysOriginal)
								   
				} else if index == TabBarController.activityIndex {
					barItem.image = UIImage(named: "History-Inactive")?.withRenderingMode(.alwaysOriginal)
					barItem.selectedImage = UIImage(named: "History-Active")?.withRenderingMode(.alwaysOriginal)
					
				} else if index == TabBarController.settingsIndex {
					barItem.image = UIImage(named: "Settings-Inactive")?.withRenderingMode(.alwaysOriginal)
					barItem.selectedImage = UIImage(named: "Settings-Active")?.withRenderingMode(.alwaysOriginal)
				}
			}
		}
	}
}
