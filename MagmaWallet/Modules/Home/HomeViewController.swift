//
//  HomeViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 17/01/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import camlKit
import MagmaWalletKit
import Toast_Swift
import os.log

class HomeViewContoller: UIViewController {
	
	@IBOutlet weak var totalCurrencyAmount: ThemeLabel?
	@IBOutlet weak var currencyShortNameLabel: ThemeLabel?
	@IBOutlet weak var moonPayButton: ThemeButton?
	@IBOutlet weak var sendButton: ThemeButton?
	@IBOutlet weak var recieveButton: ThemeButton?
	@IBOutlet weak var tableView: UITableView?
	
	private let viewModel = HomeViewModel()
	private let refreshControl = UIRefreshControl()
	private var isVisible = false
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.refreshControl.tintColor = UIColor.white
		self.refreshControl.tintColorDidChange()
		self.refreshControl.addTarget(self, action: #selector(pullToRefresh), for: UIControl.Event.valueChanged)
		
		// Hack: UIRefreshControl doesn't pick up tint colour changes until *after* the first appearance.
		// Need to trigger change by opening control at the begining. viewDidLoad will kick off network requests anyway, which will cause it to animate
		let contentOffset = CGPoint(x: 0, y: -refreshControl.frame.height)
		tableView?.contentOffset = contentOffset
		
		self.tableView?.delegate = self
		self.tableView?.rowHeight = UITableView.automaticDimension
		self.tableView?.addSubview(refreshControl)
		
		// Add shared cells
		self.tableView?.register(UINib(nibName: TokenAmountAndValueCellModel.staticIdentifier, bundle: nil), forCellReuseIdentifier: TokenAmountAndValueCellModel.staticIdentifier)
		
		// Set initial placeholders, until we get network data
		totalCurrencyAmount?.text = " "
		currencyShortNameLabel?.text = " "
		currencyShortNameLabel?.text = " "
		moonPayButton?.isHidden = true

		// Load network data
		pullToRefresh()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.navigationController?.setNavigationBarHidden(true, animated: false)
		self.navigationItem.hidesBackButton = true
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		isVisible = true
		
		if DependencyManager.shared.homeService.isRefreshingAnything() {
			showRefreshControl()
			
		} else {
			refreshUI()
		}
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		isVisible = false
		DependencyManager.shared.homeService.removeDelegate(self)
		
		hideRefreshControl()
	}
	
	@objc func pullToRefresh() {
		DependencyManager.shared.homeService.addDelegate(self)
		DependencyManager.shared.homeService.refreshAllNetworkData(forceRefresh: true)
	}
	
	func refreshUI() {
		viewModel.update { [weak self] (response) in
			if response.success {
				if let isEmpty = self?.viewModel.isWalletEmpty, !isEmpty {
					self?.sendButton?.isEnabled = true
				}
				
				let tokenPriceService = DependencyManager.shared.tokenPriceService
				
				self?.totalCurrencyAmount?.text = self?.viewModel.headerData.totalBalance
				self?.currencyShortNameLabel?.text = "wlt_total_currency_value".localized(tokenPriceService.selectedCurrency?.uppercased() ?? "")
				
				self?.tableView?.tableViewData = self?.viewModel.tableViewData
				self?.tableView?.reloadData()
				self?.hideRefreshControl()
				self?.moonPayButton?.isHidden = !(self?.viewModel.buyButtonEnabled ?? false)
				
			} else {
				if let errorString = response.userDisplayString {
					self?.alert(errorWithMessage: errorString)
				}
				
				self?.tableView?.tableViewData = self?.viewModel.errorCell()
				self?.tableView?.reloadData()
				self?.hideRefreshControl()
			}
		}
	}
	
	@objc func moveToTezTokenDetails() {
		self.performSegue(withIdentifier: "tokenDetailsSegue", sender: self.viewModel.tezHeaderDataAsCellModel())
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "tokenDetailsSegue", let vc = segue.destination as? TokenDetailsViewController {
			vc.cellModel = sender as? TokenAmountAndValueCellModel
		}
	}
}

extension HomeViewContoller: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		guard let cellModel = self.viewModel.tableViewData[indexPath.section].cellModels[indexPath.row] as? TokenAmountAndValueCellModel else {
			return
		}
		
		self.performSegue(withIdentifier: "tokenDetailsSegue", sender: cellModel)
	}
	
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: WalletHeaderCellModel.staticIdentifier) as? WalletHeaderCell else {
			os_log(.error, log: .viewLifeCycle, "Unable to fetch cell: %@", WalletHeaderCellModel.staticIdentifier)
			return nil
		}
		
		cell.setup(withModel: WalletHeaderCellModel(tokenAmount: viewModel.headerData.tezAmount, conversion: viewModel.headerData.tezConversionString, localCurrency: viewModel.headerData.tezCurrencyAmount))
		
		if viewModel.headerData.hasData {
			cell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(moveToTezTokenDetails)))
		}
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return WalletHeaderCellModel(tokenAmount: "", conversion: "", localCurrency: "").estimatedRowHeight
	}
	
	
	// MARK: - Hide tableviewCells under header
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		guard let tableview = tableView, !DependencyManager.shared.homeService.isRefreshingAnything(), isVisible else { return }
		
		for cell in tableview.visibleCells {
			let hiddenFrameHeight = scrollView.contentOffset.y + (tableview.frame.origin.y - 8) - cell.frame.origin.y
			if (hiddenFrameHeight >= 0 || hiddenFrameHeight <= cell.frame.size.height) {
				maskCell(cell: cell, margin: Float(hiddenFrameHeight))
			}
		}
	}

	func maskCell(cell: UITableViewCell, margin: Float) {
		cell.layer.mask = visibilityMaskForCell(cell: cell, location: (margin / Float(cell.frame.size.height) ))
		cell.layer.masksToBounds = true
	}

	func visibilityMaskForCell(cell: UITableViewCell, location: Float) -> CAGradientLayer {
		let mask = CAGradientLayer()
		mask.frame = cell.bounds
		mask.colors = [UIColor(white: 1, alpha: 0).cgColor, UIColor(white: 1, alpha: 1).cgColor]
		mask.locations = [NSNumber(value: location), NSNumber(value: location+0.1)]
		return mask;
	}
	
	
	// MARK: - Custom refresh logic
	func showRefreshControl() {
		if refreshControl.isRefreshing {
			return
		}
		
		// Offset tableview to show refresh control
		let contentOffset = CGPoint(x: 0, y: -refreshControl.frame.height)
		tableView?.setContentOffset(contentOffset, animated: true)
		//tableView?.contentOffset = contentOffset
		
		refreshControl.beginRefreshing()
	}
	
	func hideRefreshControl() {
		refreshControl.endRefreshing()
	}
}

extension HomeViewContoller: HomeServiceDelegate {
	
	func didBeginRefreshingData(balancesAndPricesOnly: Bool) {
		self.sendButton?.isEnabled = false
		showRefreshControl()
	}
	
	func didEndRefreshingData(balancesAndPricesOnly: Bool, error: ErrorResponse?) {
		if let err = error {
			self.tableView?.tableViewData = self.viewModel.errorCell()
			self.tableView?.reloadData()
			hideRefreshControl()
			
			let errorDetails = ErrorMessageService.errorTitleAndMessage(for: err)
			self.view.makeToast(errorDetails.message)
		} else {
			refreshUI()
		}
	}
}
