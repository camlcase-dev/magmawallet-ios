//
//  HomeViewModel.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 17/01/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import camlKit
import os.log

struct HomeHeaderData {
	var totalBalance: String = " "
	var tezAmount: String = " "
	var tezConversionString: String = " "
	var tezCurrencyAmount: String = " "
	var hasData = false
}

class HomeViewModel: ViewModel {
	
	var headerData = HomeHeaderData()
	var isWalletEmpty = false
	var buyButtonEnabled = false
	var tableViewData: [UITableViewSectionModel] = []
	
	private var tokens: [(amount: String, symbol: String, conversionString: String, dollarPrice: Decimal, currencyString: String, token: Token)] = []
	
	func update(withInput input: [String : Any]? = [:], completion: @escaping (ViewModelResponse) -> Void) {
		
		tokens = []
		DependencyManager.shared.transactionService.resetStoredData() // When homepage is refreshed, reset any lingering data
		let accountService = DependencyManager.shared.accountService
		let tokenPriceService = DependencyManager.shared.tokenPriceService
		
		buyButtonEnabled = DependencyManager.shared.moonPayService.isMoonPayAvailable()
		
		var totalWalletCurrencyAmount = Decimal(0.0)
		
		for token in DependencyManager.shared.balances {
			let tokenAmountString = tokenPriceService.format(decimal: token.balance.toNormalisedDecimal() ?? 0, numberStyle: .decimal, maximumFractionDigits: token.decimalPlaces)
			let tokenCurrencyAmountDecimal = token.balance * token.localCurrencyRate
			let tokenCurrencyAmountString = tokenPriceService.format(decimal: tokenCurrencyAmountDecimal, numberStyle: .currency, maximumFractionDigits: 2)
			
			if token.tokenType == .xtz {
				self.headerData.tezAmount = tokenAmountString + " \(token.symbol.uppercased())"
				self.headerData.tezCurrencyAmount = tokenCurrencyAmountString
				self.headerData.tezConversionString = tokenPriceService.format(decimal: token.localCurrencyRate, numberStyle: .currency, maximumFractionDigits: 2)
				
			} else if token.balance > TokenAmount.zeroBalance(decimalPlaces: 0) {
				let localValue = tokenPriceService.format(decimal: token.localCurrencyRate, numberStyle: .currency, maximumFractionDigits: (token.localCurrencyRate > 100) ? 2 : 6)
				self.tokens.append((amount: tokenAmountString, symbol: token.symbol, conversionString: localValue, dollarPrice: token.localCurrencyRate, currencyString: tokenCurrencyAmountString, token: token))
			}
			
			totalWalletCurrencyAmount += tokenCurrencyAmountDecimal
		}
		
		self.isWalletEmpty = accountService.isWalletEmpty()
		self.headerData.totalBalance = tokenPriceService.format(decimal: totalWalletCurrencyAmount, numberStyle: .currency)
		self.headerData.hasData = true
		
		self.tableViewData = self.createTableViewData()
		completion(ViewModelResponse(success: true, responseCode: nil, userDisplayString: nil, errorResponse: nil))
	}
	
	
	// MARK: Cells
	func loadingCell() -> [UITableViewSectionModel] {
		self.tableViewData = [UITableViewSectionModel(withHeader: nil, footer: nil, andCellModels: [LoadingCellModel(estimatedRowHeight: (UIScreen.main.bounds.height - 300), loadingMessage: "")] )]
		
		return self.tableViewData
	}
	
	func errorCell() -> [UITableViewSectionModel] {
		self.tableViewData = [UITableViewSectionModel(withHeader: nil, footer: nil, andCellModels: [ErrorCellModel(estimatedRowHeight: (UIScreen.main.bounds.height - 300), errorMessage: "error_wallet_balance".localized)] )]
		
		return self.tableViewData
	}
	
	func tezHeaderDataAsCellModel() -> TokenAmountAndValueCellModel {
		return TokenAmountAndValueCellModel(amount: headerData.tezAmount, symbol: "XTZ", conversionString: headerData.tezConversionString, currencyString: headerData.tezCurrencyAmount, token: DependencyManager.shared.balances[0], hideCurrency: false)
	}
	
	
	// MARK: Private Cell functions
	private func createTableViewData() -> [UITableViewSectionModel] {
		var tokenCells: [UITableViewCellModel] = []
		
		if isWalletEmpty {
			tokenCells = [EmptyWalletCellModel()]
		} else {
			tokenCells = createTokenListCells()
		}
		
		return [UITableViewSectionModel(withHeader: nil, footer: nil, andCellModels: tokenCells)]
	}
	
	private func createTokenListCells() -> [UITableViewCellModel] {
		var tokenCells: [UITableViewCellModel] = []
		
		for token in tokens {
			tokenCells.append(TokenAmountAndValueCellModel(amount: token.amount, symbol: token.symbol, conversionString: token.conversionString, currencyString: token.currencyString, token: token.token, hideCurrency: token.dollarPrice == 0))
		}
		
		return tokenCells
	}
}
