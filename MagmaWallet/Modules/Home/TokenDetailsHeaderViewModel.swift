//
//  TokenDetailsHeaderViewModel.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 04/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import camlKit

class TokenDetailsHeaderViewModel: ViewModel {
	
	enum TokenDetailsHeaderViewModelInputKeys: String {
		case token
	}
	
	var token: Token?
	
	var tokenAmountString: String = ""
	var tokenSymbolString: String = ""
	var tokenConversionRateString: String = ""
	var tokenCurrencyAmountString: String = ""
	var isWalletEmpty = false

	func update(withInput input: [String : Any]? = [:], completion: @escaping (ViewModelResponse) -> Void) {
		guard let token = input?[TokenDetailsHeaderViewModelInputKeys.token.rawValue] as? Token else {
			completion(ViewModelResponse(success: false, responseCode: nil, userDisplayString: nil, errorResponse: nil))
			return
		}
		
		self.token = token
		let tokenPriceService = DependencyManager.shared.tokenPriceService
		
		tokenAmountString = tokenPriceService.format(decimal: token.balance.toNormalisedDecimal() ?? 0, numberStyle: .decimal, maximumFractionDigits: token.decimalPlaces)
		tokenSymbolString = token.symbol
		
		tokenConversionRateString = tokenPriceService.format(decimal: token.localCurrencyRate, numberStyle: .currency, maximumFractionDigits: (token.localCurrencyRate > 100) ? 2 : 6)
		tokenCurrencyAmountString = tokenPriceService.format(decimal: token.balance * token.localCurrencyRate, numberStyle: .currency, maximumFractionDigits: 2)
		isWalletEmpty = DependencyManager.shared.accountService.isWalletEmpty()
		
		completion(ViewModelResponse(success: true, responseCode: nil, userDisplayString: nil, errorResponse: nil))
	}
	
	func setupExchangeDetails() {
		if let t = token {
			DependencyManager.shared.transactionService.transactionType = .exchange
			DependencyManager.shared.transactionService.exchangeData.selectedPrimaryTokenIndex = DependencyManager.shared.balances.firstIndex(where: { $0.symbol == t.symbol }) ?? 0
			
			if t.tokenType == .xtz {
				DependencyManager.shared.transactionService.exchangeData.selectedSecondaryTokenIndex = 1
			} else {
				DependencyManager.shared.transactionService.exchangeData.selectedSecondaryTokenIndex = 0
			}
		}
	}
}
