//
//  EmptyWalletCell.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 28/04/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import os.log

struct EmptyWalletCellModel: UITableViewCellModel {
	
	static let staticIdentifier = "emptyWalletCell"
	let cellId: String = EmptyWalletCellModel.staticIdentifier
	let estimatedRowHeight: CGFloat = 110
}

class EmptyWalletCell: UITableViewCell, UITableViewCellMVVM {
	
	func setup(withModel model: UITableViewCellModel) {
		guard let _ = model as? EmptyWalletCellModel else {
			os_log(.error, log: .mvvm, "Cell model passed in to 'EmptyWalletCell' did not match required type 'EmptyWalletCellModel', instead found '%@'", String(describing: model.self))
			return
		}
	}
}
