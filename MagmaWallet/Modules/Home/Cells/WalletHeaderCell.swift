//
//  WalletHeaderCell.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 06/08/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import MagmaWalletKit
import os.log

struct WalletHeaderCellModel: UITableViewCellModel {
	
	static let staticIdentifier = "walletHeaderCell"
	let cellId: String = WalletHeaderCellModel.staticIdentifier
	let estimatedRowHeight: CGFloat = 131
	
	let tokenAmount: String
	let conversion: String
	let localCurrency: String
}

class WalletHeaderCell: UITableViewCell, UITableViewCellMVVM {
	
	@IBOutlet weak var tokenAmountLabel: ThemeLabel!
	@IBOutlet weak var conversionRateButton: ThemeButton!
	@IBOutlet weak var localCurrencyLabel: ThemeLabel!
	
	
	func setup(withModel model: UITableViewCellModel) {
		guard let cellModel = model as? WalletHeaderCellModel else {
			os_log(.error, log: .mvvm, "Cell model passed in to 'WalletHeaderCell' did not match required type 'WalletHeaderCellModel', instead found '%@'", String(describing: model.self))
			return
		}
		
		tokenAmountLabel.text = cellModel.tokenAmount
		conversionRateButton.setTitle(cellModel.conversion, for: .normal)
		localCurrencyLabel.text = cellModel.localCurrency
	}
}
