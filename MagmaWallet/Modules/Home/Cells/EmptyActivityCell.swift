//
//  EmptyActivityCell.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 29/04/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import os.log

struct EmptyActivityCellModel: UITableViewCellModel {
	
	static let staticIdentifier = "emptyActivityCell"
	let cellId: String = EmptyActivityCellModel.staticIdentifier
	let estimatedRowHeight: CGFloat = 84
}

class EmptyActivityCell: UITableViewCell, UITableViewCellMVVM {
	
	func setup(withModel model: UITableViewCellModel) {
		guard let _ = model as? EmptyActivityCellModel else {
			os_log(.error, log: .mvvm, "Cell model passed in to 'EmptyActivityCell' did not match required type 'EmptyActivityCellModel', instead found '%@'", String(describing: model.self))
			return
		}
	}
}
