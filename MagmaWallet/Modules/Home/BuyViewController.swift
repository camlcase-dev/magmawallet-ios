//
//  BuyViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 08/12/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import camlKit
import SafariServices


class BuyViewController: UIViewController {
	
	@IBOutlet weak var containerView: UIView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
	}
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		containerView.roundCorners(corners: [.topLeft, .topRight], radius: 12)
	}
	
	@IBAction func closeButtonTapped(_ sender: Any) {
		self.dismiss(animated: true, completion: nil)
	}
	
	@IBAction func continueButtonTapped(_ sender: Any) {
		guard let moonPayURL = DependencyManager.shared.moonPayURL(forWalletAddress: DependencyManager.shared.accountService.currentAddress()) else {
			return
		}
		
		#if TESTNET
			UIPasteboard.general.string = "0x87dc2e57a2b3d072e1658c3a400b748b15ddf7e9"
		#else
			UIPasteboard.general.string = DependencyManager.shared.accountService.currentAddress()
		#endif
		
		
		let vc = SFSafariViewController(url: moonPayURL)
		vc.modalPresentationStyle = .pageSheet
		
		
		// If on a screen like token detials, embedded in a UINavigation controller, handle it slightly differently to UITabBar
		if let nav = self.presentingViewController as? UINavigationController {
			self.dismiss(animated: true) {
				nav.viewControllers.last?.present(vc, animated: true, completion: {
					
					// Delay displaying message to make it more prominent
					DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
						vc.view.makeToast("buy_copy_info".localized)
					}
				})
			}
			
		} else {
			self.presentingViewController?.present(vc, animated: true, completion: {
				
				// Delay displaying message to make it more prominent
				DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
					vc.view.makeToast("buy_copy_info".localized)
				}
				
				self.dismiss(animated: true, completion: nil)
			})
		}
	}
}
