//
//  TokenDetailsViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 29/04/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import camlKit
import MagmaWalletKit

class TokenDetailsViewController: UIViewController {
	
	@IBOutlet weak var tokenTotalLabel: ThemeLabel!
	@IBOutlet weak var tokenSymbolLabel: ThemeLabel!
	@IBOutlet weak var tokenConversionRateButton: ThemeButton!
	@IBOutlet weak var tokenCurrencyLabel: ThemeLabel!
	@IBOutlet weak var exchangeButton: UIButton!
	@IBOutlet weak var buyButton: ThemeButton!
	@IBOutlet weak var sendButton: ThemeButton!
	@IBOutlet weak var subheadingLabel: ThemeLabel!
	@IBOutlet weak var tableView: UITableView!
	
	var cellModel: TokenAmountAndValueCellModel? = nil
	
	private var headerViewModel = TokenDetailsHeaderViewModel()
	private var activityViewModel = ActivityViewModel()
	private let refreshControl = UIRefreshControl()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.navigationItem.hidesBackButton = false
		
		self.refreshControl.tintColor = UIColor.white
		self.refreshControl.addTarget(self, action: #selector(pullToRefresh), for: UIControl.Event.valueChanged)
		
		self.subheadingLabel.text = "tok_subheading".localized.uppercased()
		self.tableView.rowHeight = UITableView.automaticDimension
		self.tableView.addSubview(refreshControl)
		self.tableView.delegate = self
		
		// Header area
		tokenTotalLabel.text = ""
		tokenSymbolLabel.text = ""
		tokenConversionRateButton.setTitle("", for: .normal)
		tokenCurrencyLabel.text = ""
		buyButton.isHidden = true
		
		// Add shared cells
		self.tableView.register(UINib(nibName: TransactionHistoryCellModel.staticIdentifier, bundle: nil), forCellReuseIdentifier: TransactionHistoryCellModel.staticIdentifier)
		self.tableView.register(UINib(nibName: TransactionHistoryExchangeCellModel.staticIdentifier, bundle: nil), forCellReuseIdentifier: TransactionHistoryExchangeCellModel.staticIdentifier)
		self.tableView.register(UINib(nibName: TransactionHistoryFailedCellModel.staticIdentifier, bundle: nil), forCellReuseIdentifier: TransactionHistoryFailedCellModel.staticIdentifier)
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		DependencyManager.shared.homeService.addDelegate(self)
		
		self.navigationController?.setNavigationBarHidden(false, animated: false)
		self.navigationItem.title = cellModel?.token.name.localized
		
		if DependencyManager.shared.homeService.isRefreshingAnything() {
			showRefreshControl()
			
		} else {
			refreshUI()
		}
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		DependencyManager.shared.homeService.removeDelegate(self)
		
		hideRefreshControl()
	}
	
	@objc func pullToRefresh() {
		DependencyManager.shared.homeService.addDelegate(self)
		DependencyManager.shared.homeService.refreshAllNetworkData()
	}
	
	func refreshUI() {
		guard let token = cellModel?.token else {
			return
		}
		
		if token.tokenType == .xtz {
			buyButton.isHidden = !DependencyManager.shared.moonPayService.isMoonPayAvailable()
		} else {
			tokenConversionRateButton.isHidden = true
			tokenCurrencyLabel.isHidden = true
			exchangeButton.isHidden = true
		}
		
		headerViewModel.update(withInput: [ActivityViewModel.ActivityViewModelInputKeys.token.rawValue: token], completion: { [weak self] (response) in
			self?.tokenTotalLabel.text = self?.headerViewModel.tokenAmountString
			self?.tokenSymbolLabel.text = self?.headerViewModel.tokenSymbolString
			self?.tokenConversionRateButton.setTitle(self?.headerViewModel.tokenConversionRateString, for: .normal)
			self?.tokenCurrencyLabel.text = self?.headerViewModel.tokenCurrencyAmountString
			
			if let isEmpty = self?.headerViewModel.isWalletEmpty, !isEmpty {
				self?.sendButton?.isEnabled = true
			}
		})
		
		activityViewModel.update(withInput: [ActivityViewModel.ActivityViewModelInputKeys.token.rawValue: token], completion: { [weak self] (response) in
			self?.tableView.tableViewData = self?.activityViewModel.tableViewData
			self?.tableView.reloadData()
			self?.hideRefreshControl()
		})
	}
	
	func showRefreshControl() {
		if refreshControl.isRefreshing {
			return
		}
		
		// Offset tableview to show refresh control
		let contentOffset = CGPoint(x: 0, y: -refreshControl.frame.height)
		tableView?.setContentOffset(contentOffset, animated: true)
		
		refreshControl.beginRefreshing()
	}
	
	func hideRefreshControl() {
		refreshControl.endRefreshing()
	}
	
	@IBAction func exchangeButtonTapped(_ sender: Any) {
		let currentIndex = currentTokenIndex()
		if currentIndex != 0 {
			// If we are looking at the token details of an FA token. Default the exchange screen to <FA token> -> XTZ
			DependencyManager.shared.transactionService.exchangeData.selectedPrimaryTokenIndex = currentIndex
			DependencyManager.shared.transactionService.exchangeData.selectedSecondaryTokenIndex = 0
		}
		
		headerViewModel.setupExchangeDetails()
		self.navigationController?.popToExchange()
	}
	
	@IBAction func sendButtonTapped(_ sender: Any) {
		DependencyManager.shared.transactionService.sendData.selectedTokenIndex = currentTokenIndex()
		
		let vc = UIStoryboard.sendViewController()
		self.navigationController?.pushViewController(vc, animated: true)
	}
	
	@IBAction func receiveButtonTapped(_ sender: Any) {
		let vc = UIStoryboard.receiveViewController()
		vc.token = cellModel?.token
		
		self.navigationController?.present(vc, animated: true, completion: nil)
	}
	
	func currentTokenIndex() -> Int {
		return DependencyManager.shared.balances.firstIndex(where: { $0.symbol == cellModel?.symbol }) ?? 0
	}
}

extension TokenDetailsViewController: HomeServiceDelegate {
	
	func didBeginRefreshingData(balancesAndPricesOnly: Bool) {
		
	}
	
	func didEndRefreshingData(balancesAndPricesOnly: Bool, error: ErrorResponse?) {
		if let err = error {
			self.tableView.tableViewData = self.activityViewModel.errorCell()
			self.tableView.reloadData()
			self.hideRefreshControl()
			
			let errorDetails = ErrorMessageService.errorTitleAndMessage(for: err)
			self.view.makeToast(errorDetails.message)
		} else {
			refreshUI()
		}
	}
}

extension TokenDetailsViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 20
	}
	
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		if self.activityViewModel.tableViewData.count == 0 || section > self.activityViewModel.tableViewData.count-1 {
			return nil
		}
		
		let containerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 20))
		let sectionHeaderLabel = UILabel(frame: CGRect(x: 24, y: 0, width: tableView.frame.width, height: 20))
		sectionHeaderLabel.text = self.activityViewModel.tableViewData[section].header
		sectionHeaderLabel.textColor = UIColor(named: "SemiTransparentWhiteText")
		sectionHeaderLabel.font = UIFont.barlowFont(.semiBold, withSize: 13)
		
		containerView.addSubview(sectionHeaderLabel)
		
		return containerView
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		self.activityViewModel.openBlockExplorer(forIndexPath: indexPath)
	}
}
