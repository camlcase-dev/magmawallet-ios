//
//  ActivityViewModel.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 28/04/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import camlKit
import os.log

class ActivityViewModel: ViewModel {
	
	enum ActivityViewModelInputKeys: String {
		case token
		case address
	}
	
	private var contactService = DependencyManager.shared.contactService
	private var tokenPriceService = DependencyManager.shared.tokenPriceService
	
	var tableViewData: [UITableViewSectionModel] = []
	
	func update(withInput input: [String : Any]? = [:], completion: @escaping (ViewModelResponse) -> Void) {
		
		contactService = DependencyManager.shared.contactService
		
		let filterToken = input?[ActivityViewModelInputKeys.token.rawValue] as? Token
		let filterAddress = input?[ActivityViewModelInputKeys.address.rawValue] as? String
		tableViewData = createTransactionHistoryCells(filterByToken: filterToken, filterByAddress: filterAddress)
		
		completion(ViewModelResponse(success: true, responseCode: nil, userDisplayString: nil, errorResponse: nil))
	}
	
	
	// MARK: Helpers
	func openBlockExplorer(forIndexPath indexPath: IndexPath) {
		guard let cellModel = tableViewData[indexPath.section].cellModels[indexPath.row] as? TransactionHistoryCellModelProtocol,
			let url = URL(string: "\(DependencyManager.shared.blockExplorerBaseURL)\(cellModel.operationHash)") else {
				os_log(.error, log: .models, "Can't find model for indexPath: ", "\(indexPath)")
				return
		}
		
		UIApplication.shared.open(url, options: [:], completionHandler: nil)
	}
	
	
	// MARK: Cells
	func loadingCell() -> [UITableViewSectionModel] {
		self.tableViewData = [UITableViewSectionModel(withHeader: nil, footer: nil, andCellModels: [LoadingCellModel(estimatedRowHeight: LoadingCellModel.defaultHeight(), loadingMessage: "")] )]
		
		return self.tableViewData
	}
	
	func errorCell() -> [UITableViewSectionModel] {
		self.tableViewData = [UITableViewSectionModel(withHeader: nil, footer: nil, andCellModels: [ErrorCellModel(estimatedRowHeight: ErrorCellModel.defaultHeight(), errorMessage: "error_wallet_balance".localized)] )]
		
		return self.tableViewData
	}
	
	func createTransactionHistoryCells(filterByToken: Token?, filterByAddress: String?) -> [UITableViewSectionModel] {
		var sections: [UITableViewSectionModel] = []
		var tokenCells: [UITableViewCellModel] = []
		
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "MMM dd yyyy"
		
		let transactionHistory = DependencyManager.shared.tzktClient.currentTransactionHistory(filterByToken: filterByToken, orFilterByAddress: filterByAddress)
		let sortedKeys = transactionHistory.keys.sorted { $0 > $1 }
		
		if transactionHistory.count == 0 {
			return [UITableViewSectionModel(withHeader: nil, footer: nil, andCellModels: [EmptyActivityCellModel()])]
		}
		
		for key in sortedKeys {
			let dateString = dateFormatter.string(from: Date(timeIntervalSince1970: key))
			
			tokenCells = []
			for transaction in transactionHistory[key] ?? [] {
				
				switch transaction.subType {
					case .send, .nativeTokenSend:
						tokenCells.append(createSendTransactionModel(transaction: transaction))
					
					case .receive, .nativeTokenReceive:
						tokenCells.append(createReceiveTransactionModel(transaction: transaction))
					
					case .exchangeXTZToToken, .exchangeTokenToXTZ:
						tokenCells.append(createExchangeTransactionModel(transaction: transaction))
					
					case .delegation:
						tokenCells.append(createDelegationTransactionModel(transaction: transaction))
					
					case .reveal:
						tokenCells.append(createRevealTransactionModel(transaction: transaction))
					
					case .approve:
						tokenCells.append(createApproveTransactionModel(transaction: transaction))
					
					default:
						tokenCells.append(createUnknownTransactionModel(transaction: transaction))
				}
			}
			
			sections.append(UITableViewSectionModel(withHeader: dateString, footer: nil, andCellModels: tokenCells))
		}
		
		return sections
	}
	
	func createSendTransactionModel(transaction: TzKTTransaction) -> TransactionHistoryCellModelProtocol {
		var type = ""
		var amount = ""
		var fee = ""
		let destinationAddress = transaction.nativeTokenSendDestination ?? transaction.target?.address
		let addressString = contactService.displayStringForAddress(destinationAddress)
		let addressDisplay = "act_destination_to".localized(addressString)
		
		if transaction.status == .applied {
			let symbol = transaction.token?.symbol ?? "?"
			
			type = "act_type_send".localized.uppercased()
			amount = "-" + transaction.amount.normalisedRepresentation + " \(symbol)"
			fee = "act_network_fee".localized(transaction.networkFee.normalisedRepresentation) + " XTZ"
			
			return TransactionHistoryCellModel(operationHash: transaction.hash, type: type, amount: amount, address: addressDisplay, fee: fee)
			
		} else {
			type = "act_type_failed".localized("act_type_send".localized).uppercased()
			amount = "act_empty".localized
			fee = "act_network_fee".localized("act_empty".localized)
			
			return TransactionHistoryFailedCellModel(operationHash: transaction.hash, type: type, amount: amount, address: addressDisplay, fee: fee)
		}
	}
	
	func createReceiveTransactionModel(transaction: TzKTTransaction) -> TransactionHistoryCellModelProtocol {
		var type = ""
		var amount = ""
		let fee = "act_network_fee".localized("act_empty".localized)
		var addressString = ""
		
		// Check if what we received was a reward from a baker, display slightly differently if so
		if let bakerWhoSentReward = DependencyManager.shared.tezosBakerService.checkIfBakerSentTransaction(bakerAddress: transaction.sender.address) {
			type = "act_type_reward".localized.uppercased()
			addressString = bakerWhoSentReward
			
		} else {
			type = "act_type_receive".localized.uppercased()
			addressString = contactService.displayStringForAddress(transaction.sender.address)
		}
		
		let addressDisplay = "act_destination_from".localized(addressString)
		
		if transaction.status == .applied {
			let symbol = transaction.token?.symbol ?? "?"
			amount = "+" + tokenPriceService.format(decimal: transaction.amount.toNormalisedDecimal() ?? 0, numberStyle: .decimal, maximumFractionDigits: transaction.token?.decimalPlaces ?? 0) + " \(symbol)"
			
			return TransactionHistoryCellModel(operationHash: transaction.hash, type: type, amount: amount, address: addressDisplay, fee: fee)
			
		} else {
			type = "act_type_failed".localized("act_type_receive".localized).uppercased()
			amount = "act_empty".localized
			
			return TransactionHistoryFailedCellModel(operationHash: transaction.hash, type: type, amount: amount, address: addressDisplay, fee: fee)
		}
	}
	
	func createExchangeTransactionModel(transaction: TzKTTransaction) -> TransactionHistoryCellModelProtocol {
		var type = ""
		var amountSent = ""
		var amountReceived = ""
		var exchangeRate = ""

		var fee = ""
		
		let fromSymbol = transaction.token?.symbol ?? "?"
		let toSymbol = transaction.secondaryToken?.symbol ?? "?"
		let secondaryAmount = transaction.secondaryAmount ?? TokenAmount.zeroBalance(decimalPlaces: 0)
		
		if transaction.status == .applied {
			
			amountSent = "-" + tokenPriceService.format(decimal: transaction.amount.toNormalisedDecimal() ?? 0, numberStyle: .decimal, maximumFractionDigits: transaction.token?.decimalPlaces ?? 0) + " \(fromSymbol)"
			amountReceived = "+" + tokenPriceService.format(decimal: secondaryAmount.toNormalisedDecimal() ?? 0, numberStyle: .decimal, maximumFractionDigits: transaction.secondaryToken?.decimalPlaces ?? 0) + " \(toSymbol)"
			fee = "act_network_fee".localized(transaction.networkFee.normalisedRepresentation) + " XTZ"
			
			let secondaryDecimal = secondaryAmount.toNormalisedDecimal() ?? 1
			let primaryDecimal = transaction.amount.toNormalisedDecimal() ?? 1
			
			let rate = (secondaryDecimal / primaryDecimal).rounded(scale: 6, roundingMode: .down)
			exchangeRate = "1 \(fromSymbol) → \(rate) \(toSymbol)"
			
			return TransactionHistoryExchangeCellModel(operationHash: transaction.hash, amountSent: amountSent, amountReceived: amountReceived, exchangeRate: exchangeRate, networkFee: fee)
			
		} else {
			type = "act_type_failed".localized("act_type_exchange".localized).uppercased()
			amountSent = "act_empty".localized
			exchangeRate = "\(transaction.amount) \(fromSymbol) → \(secondaryAmount) \(toSymbol)"
			fee = "act_network_fee".localized("act_empty".localized)
			
			return TransactionHistoryFailedCellModel(operationHash: transaction.hash, type: type, amount: amountSent, address: exchangeRate, fee: fee)
		}
	}
	
	func createDelegationTransactionModel(transaction: TzKTTransaction) -> TransactionHistoryCellModelProtocol {
		var type = ""
		var amount = ""
		var fee = ""
		var addressString = ""
		
		let didRemove = (transaction.newDelegate == nil)
		let addressToUse = (didRemove ? transaction.prevDelegate?.address : transaction.newDelegate?.address) ?? ""
		
		if let bakerName = DependencyManager.shared.tezosBakerService.checkIfBakerSentTransaction(bakerAddress: addressToUse) {
			addressString = bakerName
		} else {
			addressString = (addressToUse).tezosAddressFormat()
		}
		
		let addressDisplay = didRemove ? addressString : "act_destination_to".localized(addressString)
		
		if transaction.status == .applied {
			type = didRemove ? "act_type_delegation_remove".localized.uppercased() : "act_type_delegation".localized.uppercased()
			amount = "act_empty".localized
			fee = "act_network_fee".localized(transaction.networkFee.normalisedRepresentation) + " XTZ"
			
			return TransactionHistoryCellModel(operationHash: transaction.hash, type: type, amount: amount, address: addressDisplay, fee: fee)
			
		} else {
			type = "act_type_failed".localized("act_type_delegation".localized).uppercased()
			amount = "act_empty".localized
			fee = "act_network_fee".localized("act_empty".localized)
			
			return TransactionHistoryFailedCellModel(operationHash: transaction.hash, type: type, amount: amount, address: addressDisplay, fee: fee)
		}
	}
	
	func createRevealTransactionModel(transaction: TzKTTransaction) -> TransactionHistoryCellModelProtocol {
		var type = ""
		var amount = ""
		var fee = ""
		let address = "act_from_my_wallet".localized
		
		if transaction.status == .applied {
			type = "act_type_reveal".localized.uppercased()
			amount = "act_empty".localized
			fee = "act_network_fee".localized(transaction.networkFee.normalisedRepresentation) + " XTZ"
			
			return TransactionHistoryCellModel(operationHash: transaction.hash, type: type, amount: amount, address: address, fee: fee)
			
		} else {
			type = "act_type_failed".localized("act_type_reveal".localized).uppercased()
			amount = "act_empty".localized
			fee = "act_network_fee".localized("act_empty".localized)
			
			return TransactionHistoryFailedCellModel(operationHash: transaction.hash, type: type, amount: amount, address: address, fee: fee)
		}
	}
	
	func createApproveTransactionModel(transaction: TzKTTransaction) -> TransactionHistoryCellModelProtocol {
		var type = ""
		var amount = ""
		var fee = ""
		var address = ""
		
		if transaction.sender.address == DependencyManager.shared.accountService.currentAddress() {
			address = "act_destination_from".localized("act_from_my_wallet".localized)
			
		} else {
			let addressString = contactService.displayStringForAddress(transaction.sender.address)
			address = "act_destination_from".localized(addressString)
		}
		
		if transaction.status == .applied {
			let symbol = transaction.token?.symbol ?? "?"
			
			type = "act_type_approve".localized.uppercased()
			amount = tokenPriceService.format(decimal: transaction.amount.toNormalisedDecimal() ?? 0, numberStyle: .decimal, maximumFractionDigits: transaction.token?.decimalPlaces ?? 0) + " \(symbol)"
			fee = "act_network_fee".localized(transaction.networkFee.normalisedRepresentation) + " XTZ"
			
			return TransactionHistoryCellModel(operationHash: transaction.hash, type: type, amount: amount, address: address, fee: fee)
			
		} else {
			type = "act_type_failed".localized("act_type_approve".localized).uppercased()
			amount = "act_empty".localized
			fee = "act_network_fee".localized("act_empty".localized)
			
			return TransactionHistoryFailedCellModel(operationHash: transaction.hash, type: type, amount: amount, address: address, fee: fee)
		}
	}
	
	func createUnknownTransactionModel(transaction: TzKTTransaction) -> TransactionHistoryCellModelProtocol {
		var type = ""
		var amount = ""
		var fee = ""
		let addressString = contactService.displayStringForAddress(transaction.target?.address)
		let addressDisplay = "act_destination_to".localized(addressString)
		
		if transaction.status == .applied {
			type = "act_type_unknown".localized.uppercased()
			amount = "act_empty".localized
			fee = "act_network_fee".localized(transaction.networkFee.normalisedRepresentation) + " XTZ"
			
			return TransactionHistoryCellModel(operationHash: transaction.hash, type: type, amount: amount, address: addressDisplay, fee: fee)
			
		} else {
			type = "act_type_failed".localized("act_type_unknown".localized).uppercased()
			amount = "act_empty".localized
			fee = "act_network_fee".localized("act_empty".localized)
			
			return TransactionHistoryFailedCellModel(operationHash: transaction.hash, type: type, amount: amount, address: addressDisplay, fee: fee)
		}
	}
}
