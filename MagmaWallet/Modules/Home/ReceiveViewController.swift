//
//  ReceiveViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 27/01/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import camlKit
import MagmaWalletKit
import Toast_Swift
import MobileCoreServices

class ReceiveViewController: UIViewController {
	
	@IBOutlet weak var containerView: UIView!
	@IBOutlet weak var titleLabel: ThemeLabel!
	@IBOutlet weak var qrCodeImageView: UIImageView!
	@IBOutlet weak var addressLabel: UILabel!
	@IBOutlet weak var copyToClipboardButton: UIButton!
	
	var token: Token?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		titleLabel.text = "rec_title_token".localized(token?.symbol ?? "XTZ")
		
		let address = DependencyManager.shared.accountService.currentAddress()
		addressLabel.text = address
		qrCodeImageView.image = address?.generateQRCode()
		
		let attributes: [NSAttributedString.Key: Any] = [
			NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue,
			NSAttributedString.Key.foregroundColor: UIColor.black
		]
		let underlineAttributedString = NSAttributedString(string: "rec_action_copy".localized, attributes: attributes)
		copyToClipboardButton.setAttributedTitle(underlineAttributedString, for: .normal)
	}
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		containerView.roundCorners(corners: [.topLeft, .topRight], radius: 12)
	}
	
	@IBAction func copyButtonTapped(_ sender: UIButton) {
		UIPasteboard.general.setObjects([self], localOnly: true, expirationDate: Date(timeIntervalSinceNow: 60))
		self.view.makeToast("action_copy_success".localized)
	}
	
	@IBAction func closeTapped(_ sender: UIButton) {
		self.dismiss(animated: true, completion: nil)
	}
	
	@IBAction func shareButtonTapped(_ sender: Any) {
		guard let address = DependencyManager.shared.accountService.currentAddress() else {
			return
		}
		
		let addressAndSubject = ActivityAddressAndSubect(subject: "rec_action_share_subject".localized, address: address)
        let activityViewController = UIActivityViewController(activityItems: [addressAndSubject], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.containerView
		activityViewController.completionWithItemsHandler = { (activityType, completed: Bool, returnedItems: [Any]?, error: Error?) in
			(UIApplication.shared.delegate as? AppDelegate)?.setGlobalStyles()
		}
		
		// Share sheet copies naviagtion bar appearence settings, need to temporarily remove them so cancel buttons are visbile
		(UIApplication.shared.delegate as? AppDelegate)?.removeNavigationStyles()
		
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
	}
}

extension ReceiveViewController: NSItemProviderWriting {
	static var writableTypeIdentifiersForItemProvider: [String] {
		return [ kUTTypeUTF8PlainText as String ]
	}
	
	func loadData(withTypeIdentifier typeIdentifier: String, forItemProviderCompletionHandler completionHandler: @escaping (Data?, Error?) -> Void) -> Progress? {
		guard let address = DependencyManager.shared.accountService.currentAddress() else {
			completionHandler(nil, NSError(domain: "No wallet address found", code: -1, userInfo: nil))
			return nil
		}
		
		completionHandler(address.data(using: .utf8), nil)
		return nil
	}
}

class ActivityAddressAndSubect: NSObject, UIActivityItemSource {
	
	let subject: String
    let address: String

    init(subject: String, address: String) {
        self.subject = subject
        self.address = address
		
		super.init()
    }

    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return address
    }

	func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        return address
    }

	func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivity.ActivityType?) -> String {
        return subject
    }
}
