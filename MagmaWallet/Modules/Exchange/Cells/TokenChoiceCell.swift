//
//  TokenChoiceCell.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 07/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import os.log

struct TokenChoiceCellModel: UITableViewCellModel {
	
	static let staticIdentifier = "TokenChoiceCell"
	let cellId: String = TokenChoiceCellModel.staticIdentifier
	let estimatedRowHeight: CGFloat = 60
	
	let icon: UIImage
	let symbol: String
	let name: String
	let balance: String
	let currency: String
}

class TokenChoiceCell: UITableViewCell, UITableViewCellMVVM {
	
	@IBOutlet weak var iconView: UIImageView!
	@IBOutlet weak var symbolLabel: UILabel!
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var balanceLabel: UILabel!
	@IBOutlet weak var currencyLabel: UILabel!
	
	func setup(withModel model: UITableViewCellModel) {
		guard let cellModel = model as? TokenChoiceCellModel else {
			os_log(.error, log: .mvvm, "Cell model passed in to 'TokenChoiceCell' did not match required type 'TokenChoiceCellModel', instead found '%@'", String(describing: model.self))
			return
		}
		
		self.iconView.image = cellModel.icon
		self.symbolLabel.text = cellModel.symbol
		self.nameLabel.text = cellModel.name
		self.balanceLabel.text = cellModel.balance
		self.currencyLabel.text = cellModel.currency
	}
}
