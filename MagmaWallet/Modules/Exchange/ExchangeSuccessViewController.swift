//
//  ExchangeSuccessViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 08/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import Sentry
import Lottie

class ExchangeSuccessViewController: UIViewController {
	
	@IBOutlet weak var animationView: AnimationView?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		if !Thread.current.isRunningXCTest {
			SentrySDK.capture(message: "Exchange Succeeded")
		}
		
		self.navigationController?.setNavigationBarHidden(true, animated: false)
		DependencyManager.shared.transactionService.resetStoredData()
		
		if let animation = Animation.named("lottie-success") {
			animationView?.animation = animation
		}
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		animationView?.play()
	}
	
	@IBAction func returnToExchangetapped(_ sender: Any) {
		self.navigationController?.popToExchange()
	}
	
	@IBAction func goToWalletTapped(_ sender: Any) {
		self.navigationController?.popToWallet()
	}
}
