//
//  ExchangeViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 06/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import camlKit
import MagmaWalletKit

class ExchangeViewController: UIViewController {
	
	@IBOutlet weak var tokenBalanceLabel: ThemeLabel?
	@IBOutlet weak var tokenFromButton: ThemeButton?
	@IBOutlet weak var tokenToButton: ThemeButton?
	@IBOutlet weak var maxButton: ThemeButton?
	@IBOutlet weak var textField: ThemeTextfield?
	@IBOutlet weak var textFieldCurrencyLabel: ThemeLabel?
	@IBOutlet weak var conversionRateButton: ThemeButton?
	@IBOutlet weak var toTokenAmountLabel: ThemeLabel?
	@IBOutlet weak var toCurrencyLabel: ThemeLabel?
	@IBOutlet weak var poweredByDexterButton: ThemeButton?
	@IBOutlet weak var advancedOptionsButton: ThemeButton?
	@IBOutlet weak var reviewDetailsButton: ThemeButton?
	@IBOutlet weak var reviewDetailsBottomConstraint: NSLayoutConstraint?
	
	private var isUIDisabled = false
	private var viewAlphaValueColltion: [UIView: CGFloat] = [:]
	private let viewModel = ExchangeViewModel()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		textField?.themeTextfieldValidationDelegate = self
		textField?.addDoneToolbar()
	}
	
	func disableUI() {
		if isUIDisabled {
			return
		}
		
		isUIDisabled = true
		
		for view in view.subviews where view != poweredByDexterButton {
			viewAlphaValueColltion[view] = view.alpha
			view.alpha = 0.5
			
			if view is ThemeButton, let button = view as? ThemeButton {
				button.isEnabled = false
			}
		}
		
		tokenFromButton?.isEnabled = false
		tokenToButton?.isEnabled = false
	}
	
	func enableUI() {
		if !isUIDisabled {
			return
		}
		
		isUIDisabled = false
		
		for view in view.subviews where view != poweredByDexterButton {
			view.alpha = viewAlphaValueColltion[view] ?? 1
			
			if view is ThemeButton, let button = view as? ThemeButton {
				button.isEnabled = true
			}
		}
		
		tokenFromButton?.isEnabled = true
		tokenToButton?.isEnabled = true
	}
	
	func refreshUI() {
		viewModel.update { [weak self] (response) in
			
			if let shouldDisable = self?.viewModel.isUIDisabled, shouldDisable {
				self?.disableUI()
				
			} else if let isDisabled = self?.isUIDisabled, isDisabled {
				self?.enableUI()
			}
			
			self?.tokenFromButton?.setTitle(self?.viewModel.fromButtonString, for: .normal)
			self?.tokenToButton?.setTitle(self?.viewModel.toButtonString, for: .normal)
			self?.tokenBalanceLabel?.text = "exc_balance_from".localized(self?.viewModel.fromTokenBalance ?? "")
			self?.textFieldCurrencyLabel?.text = self?.viewModel.fromCurrencyString
			self?.conversionRateButton?.setTitle(self?.viewModel.conversionRateString, for: .normal)
			self?.textField?.validator = self?.viewModel.textFieldValidator
			self?.toTokenAmountLabel?.text = self?.viewModel.toAmountString
			self?.toCurrencyLabel?.text = self?.viewModel.toCurrencyString
			self?.maxButton?.isEnabled = self?.viewModel.isMaxEnabled ?? true
			
			self?.showAllSubviews()
			self?.hideActivity()
		}
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		DependencyManager.shared.homeService.addDelegate(self)
		
		self.tabBarController?.navigationItem.title = "wlt_navigation_exchange".localized.uppercased()
		self.navigationController?.setNavigationBarHidden(false, animated: false)
		
		if DependencyManager.shared.homeService.isRefreshingAll() {
			self.hideAllSubviews()
			self.showActivity()
		} else {
			let storedValue = DependencyManager.shared.transactionService.exchangeData.amountToExchange
			self.textField?.text = (storedValue == TokenAmount.zeroBalance(decimalPlaces: 0)) ? "" : storedValue.normalisedRepresentation
			self.textField?.revalidateTextfield()
			refreshUI()
		}
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		DependencyManager.shared.homeService.removeDelegate(self)
	}
	
	@IBAction func toTokenButtonTapped(_ sender: Any) {
		textField?.resignFirstResponder()
		
		DependencyManager.shared.transactionService.exchangeData.tokenSelectionInProgress = .primary
		self.performSegue(withIdentifier: "tokenChoiceSegue", sender: self)
	}
	
	@IBAction func fromTokenButtonTapped(_ sender: Any) {
		textField?.resignFirstResponder()
		
		DependencyManager.shared.transactionService.exchangeData.tokenSelectionInProgress = .secondary
		self.performSegue(withIdentifier: "tokenChoiceSegue", sender: self)
	}
	
	@IBAction func maxButtonTapped(_ sender: Any) {
		self.textField?.text = self.viewModel.maxValue()
		self.textField?.revalidateTextfield()
	}
	
	@IBAction func switchTokensButtonTapped(_ sender: Any) {
		textField?.resignFirstResponder()
		
		let transactionService = DependencyManager.shared.transactionService
		let tempPrimary = transactionService.exchangeData.selectedPrimaryTokenIndex
		
		transactionService.exchangeData.selectedPrimaryTokenIndex = transactionService.exchangeData.selectedSecondaryTokenIndex
		transactionService.exchangeData.selectedSecondaryTokenIndex = tempPrimary
		
		tokenChanged()
	}
	
	@IBAction func advancedOptionsButtonTapped(_ sender: ThemeButton) {
		textField?.resignFirstResponder()
	}
	
	@IBAction func reviewDetailsButtonTapped(_ sender: ThemeButton) {
		textField?.resignFirstResponder()
		
		DependencyManager.shared.transactionService.transactionType = .exchange
		let transactionPriceImpact = DependencyManager.shared.transactionService.exchangeData.conversion.displayPriceImpact
		
		if transactionPriceImpact >= 50 {
			self.performSegue(withIdentifier: "priceImapctErrorSegue", sender: true)
			
		} else if transactionPriceImpact >= 5 && transactionPriceImpact < 50 {
			self.performSegue(withIdentifier: "priceImpactWarningSegue", sender: true)
			
		}  else if transactionPriceImpact >= 1 && transactionPriceImpact < 5 {
			self.performSegue(withIdentifier: "priceImpactNoticeSegue", sender: true)
			
		} else {
			self.performSegue(withIdentifier: "reviewDetailsSegue", sender: true)
		}
	}
	
	func tokenChanged() {
		textField?.text = ""
		textField?.revalidateTextfield()
	}
}

extension ExchangeViewController: HomeServiceDelegate {
	
	func didBeginRefreshingData(balancesAndPricesOnly: Bool) {
		
	}
	
	func didEndRefreshingData(balancesAndPricesOnly: Bool, error: ErrorResponse?) {
		if let err = error {
			let errorDetails = ErrorMessageService.errorTitleAndMessage(for: err)
			self.view.makeToast(errorDetails.message)
		}
		
		refreshUI()
	}
}

extension ExchangeViewController: ThemeTextfieldValidationProtocol {
	
	func validated(_ validated: Bool, textfield: ThemeTextfield, forText text: String) {
		let selectedToken = DependencyManager.shared.dexterTokens[DependencyManager.shared.transactionService.exchangeData.selectedPrimaryTokenIndex]
		
		if text == "" {
			DependencyManager.shared.transactionService.updateAmountToExchange(amount: TokenAmount.zeroBalance(decimalPlaces: 0))
			
		} else if validated, let amount = TokenAmount(fromNormalisedAmount: text, decimalPlaces: selectedToken.decimalPlaces) {
			DependencyManager.shared.transactionService.updateAmountToExchange(amount: amount)
		}
		
		reviewDetailsButton?.isEnabled = (
			validated &&
			DependencyManager.shared.transactionService.exchangeData.amountToExchange > TokenAmount.zeroBalance(decimalPlaces: 0) &&
			DependencyManager.shared.transactionService.exchangeData.conversion.minimum > TokenAmount.zeroBalance(decimalPlaces: 0)
		)
		refreshUI()
	}
}
