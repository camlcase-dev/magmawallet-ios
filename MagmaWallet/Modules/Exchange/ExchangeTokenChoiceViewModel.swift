//
//  ExchangeTokenChoiceViewModel.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 07/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import camlKit
import os.log

class ExchangeTokenChoiceViewModel: ViewModel {
	
	var tableViewData: [UITableViewSectionModel] = []
	
	func update(withInput input: [String : Any]? = [:], completion: @escaping (ViewModelResponse) -> Void) {
		let transactionService = DependencyManager.shared.transactionService
		
		let isSelectingPrimary = transactionService.exchangeData.tokenSelectionInProgress == .primary
		let currentPrimaryToken = DependencyManager.shared.dexterTokens[transactionService.exchangeData.selectedPrimaryTokenIndex]
		let currentSecondaryToken = DependencyManager.shared.dexterTokens[transactionService.exchangeData.selectedSecondaryTokenIndex]
		let tokenPriceService = DependencyManager.shared.tokenPriceService
		
		var cellModels: [UITableViewCellModel] = []
		for token in DependencyManager.shared.dexterTokens {
			
			if !ExchangeTokenChoiceViewModel.doesTokenHaveLiquidity(token) {
				continue
			}
			
			// Work out balances and prices
			let currencyAmount = token.balance * token.localCurrencyRate
			let currencyAsString = tokenPriceService.format(decimal: currencyAmount, numberStyle: .currency)
			
			// Decide if the token should be included
			if isSelectingPrimary && token.balance > TokenAmount.zeroBalance(decimalPlaces: 0) {
				
				// Only show tokens for which we have a balance
				let balanceDispaly = tokenPriceService.format(decimal: token.balance.toNormalisedDecimal() ?? 0, numberStyle: .decimal, maximumFractionDigits: token.decimalPlaces)
				cellModels.append(TokenChoiceCellModel(icon: token.icon ?? UIImage(), symbol: token.symbol, name: token.name, balance: balanceDispaly, currency: currencyAsString))
				
			} else if !isSelectingPrimary && token.symbol != currentPrimaryToken.symbol && token.symbol != currentSecondaryToken.symbol {
				
				// Show every available token, except the one currently selected
				cellModels.append(TokenChoiceCellModel(icon: token.icon ?? UIImage(), symbol: token.symbol, name: token.name, balance: "", currency: ""))
			}
		}
		
		tableViewData = [UITableViewSectionModel(withHeader: nil, footer: nil, andCellModels: cellModels)]
		
		completion(ViewModelResponse(success: true, responseCode: nil, userDisplayString: nil, errorResponse: nil))
	}
	
	static func doesTokenHaveLiquidity(_ token: Token) -> Bool {
		if token.tokenType == .xtz {
			// XTZ doesn't have liquidity, ignoring this case with a successful return, so XTZ will always show up on token lists for trading
			return true
		}
		
		return token.dexterXTZPool > XTZAmount.zero() && token.dexterTokenPool > TokenAmount.zero()
	}
	
	static func firstTokenWithLiquidity() -> (index: Int, token: Token)? {
		for (index, token) in DependencyManager.shared.dexterTokens.enumerated() {
			if token.tokenType != .xtz && doesTokenHaveLiquidity(token) {
				return (index: index, token: token)
			}
		}
		
		return nil
	}
}
