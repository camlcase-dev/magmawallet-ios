//
//  ExchangeTokenChoiceViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 07/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit

class ExchangeTokenChoiceViewController: UIViewController {
	
	@IBOutlet weak var containerView: UIView!
	@IBOutlet weak var tableView: UITableView!
	
	private let viewModel = ExchangeTokenChoiceViewModel()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		tableView.delegate = self
		
		viewModel.update { [weak self] (response) in
			self?.tableView.tableViewData = self?.viewModel.tableViewData
			self?.tableView.reloadData()
		}
	}
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		containerView.roundCorners(corners: [.topLeft, .topRight], radius: 12)
	}
	
	@IBAction func closeButtonTapped(_ sender: Any) {
		self.dismiss(animated: true, completion: nil)
	}
}

extension ExchangeTokenChoiceViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		
		let transactionService = DependencyManager.shared.transactionService
		let cellModel = self.viewModel.tableViewData[indexPath.section].cellModels[indexPath.row] as? TokenChoiceCellModel
		let selectedTokenIndex = DependencyManager.shared.dexterTokens.firstIndex(where: { $0.symbol == cellModel?.symbol }) ?? 0
		
		if transactionService.exchangeData.tokenSelectionInProgress == .primary {
			transactionService.exchangeData.selectedPrimaryTokenIndex = selectedTokenIndex
			
		} else {
			transactionService.exchangeData.selectedSecondaryTokenIndex = selectedTokenIndex
		}
		
		// Can't swap the same tokens
		if transactionService.exchangeData.selectedPrimaryTokenIndex == transactionService.exchangeData.selectedSecondaryTokenIndex
			&& transactionService.exchangeData.tokenSelectionInProgress == .primary {
			
			transactionService.exchangeData.selectedSecondaryTokenIndex = (transactionService.exchangeData.selectedPrimaryTokenIndex == 0) ? 1 : 0
		}
		
		// Currently don't support token to token swaps
		if transactionService.exchangeData.selectedPrimaryTokenIndex != 0 && transactionService.exchangeData.selectedPrimaryTokenIndex != 0 {
			
			if transactionService.exchangeData.tokenSelectionInProgress == .primary {
				transactionService.exchangeData.selectedSecondaryTokenIndex = 0
			} else {
				transactionService.exchangeData.selectedPrimaryTokenIndex = 0
			}
		}
		
		
		guard let navController = self.presentingViewController as? UINavigationController,
			let tabBarController = navController.viewControllers.last as? UITabBarController,
			let parentVc = tabBarController.viewControllers?[1] as? ExchangeViewController else {
				return
		}
		
		parentVc.tokenChanged()
		self.dismiss(animated: true, completion: nil)
	}
}
