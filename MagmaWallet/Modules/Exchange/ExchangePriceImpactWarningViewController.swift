//
//  ExchangePriceImpactWarningViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 14/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import MagmaWalletKit

class ExchangePriceImpactWarningViewController: UIViewController {
	
	@IBOutlet weak var titleLabel: ThemeLabel!
	@IBOutlet weak var priceImapctLabel: ThemeLabel!
	@IBOutlet weak var infoLabel: ThemeLabel!
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		let priceImpact = DependencyManager.shared.transactionService.exchangeData.conversion.displayPriceImpact
		
		if priceImpact < 3 {
			titleLabel.text = "exc_price_impact_notice_title".localized
			infoLabel.text = "exc_price_impact_notice_info".localized
			
		} else if priceImpact < 5 {
			titleLabel.text = "exc_price_impact_warning_title".localized
			infoLabel.text = "exc_price_impact_warning_info".localized
			
		} else if priceImpact < 50 {
			titleLabel.text = "exc_price_impact_warning_title".localized
			infoLabel.text = "exc_price_impact_warning_big_info".localized
		}
		
		priceImapctLabel.text = "\(priceImpact)%"
	}
	
	@IBAction func backToExchangeTapped(_ sender: Any) {
		self.navigationController?.popToExchange()
	}
}
