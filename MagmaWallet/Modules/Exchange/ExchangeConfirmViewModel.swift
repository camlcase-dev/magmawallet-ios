//
//  ExchangeConfirmViewModel.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 08/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import camlKit
import os.log
import Sentry

class ExchangeConfirmViewModel: ViewModel {
	
	var tokenToSendString = ""
	var tokenToSendCurrencyString = ""
	var tokenToReceiveString = ""
	var tokenToReceiveCurrencyString = ""
	var liquidityFeeString = ""
	var liquidityFeeCurrencyString = ""
	var networkFeeString = ""
	var networkFeeCurrencyString = ""
	var priceImpactString = ""
	var conversionRateString = ""
	
	func update(withInput input: [String : Any]? = [:], completion: @escaping (ViewModelResponse) -> Void) {
		guard let currentWallet = DependencyManager.shared.accountService.currentWallet() else {
			completion(ViewModelResponse(success: false, responseCode: nil, userDisplayString: nil, errorResponse: ErrorResponse.error(string: "", errorType: .unknownWallet)))
			return
		}
		
		let transactionService = DependencyManager.shared.transactionService
		let fromToken = DependencyManager.shared.dexterTokens[transactionService.exchangeData.selectedPrimaryTokenIndex]
		let toToken = DependencyManager.shared.dexterTokens[transactionService.exchangeData.selectedSecondaryTokenIndex]
		
		let amountToSell = transactionService.exchangeData.amountToExchange
		let amountToBuyMinimum = transactionService.exchangeData.conversion.minimum
		let amountToBuyExpected = transactionService.exchangeData.conversion.expected
		let timeout = transactionService.exchangeData.timeoutLimit
		
		// Check if Dexter has sufficient volume
		if let error = validateLimits(tokenToBuy: toToken, tokenToSell: fromToken, amountToBuy: amountToBuyExpected) {
			completion(ViewModelResponse(success: false, responseCode: nil, userDisplayString: nil, errorResponse: error))
			return
		}
		
		// Create operations and estimate
		self.createOperations(fromToken: fromToken, toToken: toToken, amountToSell: amountToSell, minAmountToBuy: amountToBuyMinimum, wallet: currentWallet, timeout: timeout) { (operations, error) in
			guard let ops = operations else {
				completion(ViewModelResponse(success: false, responseCode: nil, userDisplayString: nil, errorResponse: error ?? ErrorResponse.unknownError()))
				return
			}
			
			DependencyManager.shared.tezosNodeClient.estimate(operations: ops, withWallet: currentWallet) { (result) in
				switch result {
					case .success(let ops):
						self.processOps(fromToken: fromToken, toToken: toToken, amountToSell: amountToSell, expectedAmountToBuy: amountToBuyExpected, operations: ops)
						completion(ViewModelResponse(success: true, responseCode: nil, userDisplayString: nil, errorResponse: nil))
						
					case .failure(let error):
						completion(ViewModelResponse(success: false, responseCode: nil, userDisplayString: nil, errorResponse: error))
				}
			}
		}
	}
	
	func validateLimits(tokenToBuy: Token, tokenToSell: Token, amountToBuy: TokenAmount) -> ErrorResponse? {
		if tokenToBuy.tokenType == .xtz {
			if amountToBuy > tokenToSell.dexterXTZPool {
				return ErrorResponse.error(string: "", errorType: .exchangeNotEnoughTez)
			}
			
		} else {
			if amountToBuy > tokenToBuy.dexterTokenPool {
				return ErrorResponse.error(string: "", errorType: .exchangeNotEnoughFa)
			}
		}
		
		return nil
	}
	
	/// Selling FA tokens requires checking what the current allowance set by the user is. This function will generate the operation objects and query the allowance if necessary
	func createOperations(fromToken: Token, toToken: Token, amountToSell: TokenAmount, minAmountToBuy: TokenAmount, wallet: Wallet, timeout: Int, completion: @escaping (([camlKit.Operation]?, ErrorResponse?) -> Void)) {
		
		if fromToken.tokenType == .xtz {
			let operations = OperationFactory.dexterXtzToToken(xtzAmount: amountToSell.toXTZAmount(), minTokenAmount: minAmountToBuy, token: toToken, wallet: wallet, timeout: TimeInterval(60 * timeout))
			completion(operations, nil)
			
		} else {
			
			// Commenting out support for inDEXter as the service is being deprecated.
			// For now, we will always assuming the current allowance is non-zero
			/*
			guard let dexterAddress = fromToken.dexterExchangeAddress else {
				completion(nil, ErrorResponse.error(string: "", errorType: .unknownError))
				return
			}
			
			
			DependencyManager.shared.tezosNodeClient.inDEXTerService.fa12Allowance(forToken: fromToken, owner: wallet.address, spender: dexterAddress) { (tokenAmount, error) in
				guard let amount = tokenAmount else {
					completion(nil, error ?? ErrorResponse.unknownError())
					return
				}
				
				let operations = OperationFactory.dexterTokenToXTZ(tokenAmount: amountToSell, minXTZAmount: minAmountToBuy.toXTZAmount(), token: fromToken, currentAllowance: amount, wallet: wallet, timeout: TimeInterval(60 * timeout))
				completion(operations, nil)
			}
			*/
			
			let nonZeroAmount = TokenAmount(fromNormalisedAmount: 1, decimalPlaces: 0)
			let operations = OperationFactory.dexterTokenToXTZ(tokenAmount: amountToSell, minXTZAmount: minAmountToBuy.toXTZAmount(), token: fromToken, currentAllowance: nonZeroAmount, wallet: wallet, timeout: TimeInterval(60 * timeout))
			completion(operations, nil)
		}
	}
	
	func processOps(fromToken: Token, toToken: Token, amountToSell: TokenAmount, expectedAmountToBuy: TokenAmount, operations: [camlKit.Operation]) {
		let transactionService = DependencyManager.shared.transactionService
		let tokenPriceService = DependencyManager.shared.tokenPriceService
		var updatedAmountToSell = amountToSell
		
		let operationFee = operations.map({ $0.operationFees?.allFees() ?? XTZAmount.zero() }).reduce(XTZAmount.zero(), +)
		let xtzBalance = (DependencyManager.shared.dexterTokens[0].balance - XTZAmount(fromNormalisedAmount: 0.000001)) // User must leave 1 mutez in their wallet or end up paying extra fees which cost more
		let xtzLocalPrice = DependencyManager.shared.dexterTokens[0].localCurrencyRate
		let priceImpact = DependencyManager.shared.transactionService.exchangeData.conversion.displayPriceImpact
		
		// If the user is trying to send the maximum amount of XTZ, we need to deduct the fee from the sending amount first
		if fromToken.tokenType == .xtz && (amountToSell + operationFee) > xtzBalance {
			updatedAmountToSell += (xtzBalance - (amountToSell + operationFee))
			
			if let smartContractOperationOperation = operations.last as? OperationSmartContractInvocation {
				smartContractOperationOperation.amount = updatedAmountToSell.rpcRepresentation
			}
		}
		
		// Store updated transactions
		transactionService.transactionType = .exchange
		transactionService.exchangeData.operations = operations
		transactionService.updateAmountToExchange(amount: updatedAmountToSell)
		
		// Format strings
		self.tokenToSendString = tokenPriceService.format(decimal: updatedAmountToSell.toNormalisedDecimal() ?? 0, numberStyle: .decimal, maximumFractionDigits: updatedAmountToSell.decimalPlaces) + " \(fromToken.symbol)"
		if fromToken.tokenType == .xtz {
			self.tokenToSendCurrencyString = tokenPriceService.format(decimal: updatedAmountToSell * fromToken.localCurrencyRate, numberStyle: .currency)
		} else {
			self.tokenToSendCurrencyString = " "
		}
		
		self.tokenToReceiveString = tokenPriceService.format(decimal: expectedAmountToBuy.toNormalisedDecimal() ?? 0, numberStyle: .decimal, maximumFractionDigits: expectedAmountToBuy.decimalPlaces) + " \(toToken.symbol)"
		if toToken.tokenType == .xtz {
			self.tokenToReceiveCurrencyString = tokenPriceService.format(decimal: expectedAmountToBuy * toToken.localCurrencyRate, numberStyle: .currency)
		} else {
			self.tokenToReceiveCurrencyString = " "
		}
		
		self.liquidityFeeString = "\(transactionService.exchangeData.conversion.liquidityFee.normalisedRepresentation) \(fromToken.symbol)"
		self.liquidityFeeCurrencyString = tokenPriceService.format(decimal: (transactionService.exchangeData.conversion.liquidityFee * fromToken.localCurrencyRate), numberStyle: .currency)
		self.networkFeeString = operationFee.normalisedRepresentation + " XTZ"
		
		self.networkFeeCurrencyString = tokenPriceService.format(decimal: operationFee * xtzLocalPrice, numberStyle: .currency)
		self.priceImpactString = "exc_price_impact_notice_section_title".localized() + " \(priceImpact)%"
		self.conversionRateString = "1 \(fromToken.symbol) = \(transactionService.exchangeData.conversion.displayExchangeRate) \(toToken.symbol)"
	}
}
