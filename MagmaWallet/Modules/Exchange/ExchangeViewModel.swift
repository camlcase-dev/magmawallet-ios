//
//  ExchangeViewModel.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 06/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import camlKit
import MagmaWalletKit
import os.log

class ExchangeViewModel: ViewModel {
	
	var textFieldValidator = TokenAmountValidator(isXTZ: true, balanceLimit: TokenAmount.zero())
	
	var fromButtonString: String = ""
	var toButtonString: String = ""
	
	var fromTokenBalance: String = ""
	var conversionRateString: String = ""
	
	var fromCurrencyString: String = ""
	var toAmountString: String = ""
	var toCurrencyString: String = ""
	
	var isMaxEnabled = true
	var isUIDisabled = false
	
	func update(withInput input: [String : Any]? = [:], completion: @escaping (ViewModelResponse) -> Void) {
		
		let transactionService = DependencyManager.shared.transactionService
		let accountService = DependencyManager.shared.accountService
		let tokenPriceService = DependencyManager.shared.tokenPriceService
		let fromToken = DependencyManager.shared.dexterTokens[transactionService.exchangeData.selectedPrimaryTokenIndex]
		let toToken = DependencyManager.shared.dexterTokens[transactionService.exchangeData.selectedSecondaryTokenIndex]
		
		// Can't use Exchange if you have no balance(s)
		isUIDisabled = accountService.isWalletEmpty()
		
		// Code to run on the first time viewModel gets called
		if fromButtonString == "" {
			transactionService.updateAmountToExchange(amount: TokenAmount.zeroBalance(decimalPlaces: 0)) // Setup default currency exchanges
		}
		
		fromButtonString = fromToken.symbol
		toButtonString = toToken.symbol
		
		fromTokenBalance = tokenPriceService.format(decimal: fromToken.balance.toNormalisedDecimal() ?? 0, numberStyle: .decimal, maximumFractionDigits: fromToken.decimalPlaces)
		textFieldValidator = TokenAmountValidator(isXTZ: fromToken.tokenType == .xtz, balanceLimit: fromToken.balance, decimalPlaces: fromToken.decimalPlaces)
		conversionRateString = "1 \(fromToken.symbol) = \(transactionService.exchangeData.conversion.displayExchangeRate) \(toToken.symbol)"
		
		isMaxEnabled = fromToken.balance > transactionService.exchangeData.amountToExchange
		
		fromCurrencyString = tokenPriceService.format(decimal: transactionService.exchangeData.amountToExchange * fromToken.localCurrencyRate, numberStyle: .currency)
		toAmountString = tokenPriceService.format(decimal: transactionService.exchangeData.conversion.expected.toNormalisedDecimal() ?? 0, numberStyle: .decimal, maximumFractionDigits: toToken.decimalPlaces)
		toCurrencyString = tokenPriceService.format(decimal: transactionService.exchangeData.conversion.expected * toToken.localCurrencyRate, numberStyle: .currency)
		
		completion(ViewModelResponse(success: true, responseCode: nil, userDisplayString: nil, errorResponse: nil))
	}
	
	func maxValue() -> String {
		let transactionService = DependencyManager.shared.transactionService
		let fromToken = DependencyManager.shared.dexterTokens[transactionService.exchangeData.selectedPrimaryTokenIndex]
		
		return fromToken.balance.normalisedRepresentation
	}
}
