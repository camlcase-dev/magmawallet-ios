//
//  ExchangeConfirmViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 08/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import camlKit
import MagmaWalletKit

class ExchangeConfirmViewController: UIViewController {
	
	@IBOutlet weak var tokenToSendLabel: ThemeLabel!
	@IBOutlet weak var tokenToSendCurrencyLabel: ThemeLabel!
	@IBOutlet weak var tokenToReceiveLabel: ThemeLabel!
	@IBOutlet weak var tokenToReceiveCurrencyLabel: ThemeLabel!
	@IBOutlet weak var liqudiityFeeLabel: ThemeLabel!
	@IBOutlet weak var liquidityFeeCurrencyLabel: ThemeLabel!
	@IBOutlet weak var netwrokFeeLabel: ThemeLabel!
	@IBOutlet weak var networkFeeCurrencyLabel: ThemeLabel!
	@IBOutlet weak var priceImpactLabel: ThemeLabel!
	@IBOutlet weak var conversionRateLabel: ThemeLabel!
	
	private let viewModel = ExchangeConfirmViewModel()
	private var errorResponse: ErrorResponse = ErrorResponse.unknownError()
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.hideAllSubviews()
		self.showActivity()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		viewModel.update(withInput: nil) { [weak self] (response) in
			self?.tokenToSendLabel.text = self?.viewModel.tokenToSendString
			self?.tokenToSendCurrencyLabel.text = self?.viewModel.tokenToSendCurrencyString
			self?.tokenToReceiveLabel.text = self?.viewModel.tokenToReceiveString
			self?.tokenToReceiveCurrencyLabel.text = self?.viewModel.tokenToReceiveCurrencyString
			self?.liqudiityFeeLabel.text = self?.viewModel.liquidityFeeString
			self?.liquidityFeeCurrencyLabel.text = self?.viewModel.liquidityFeeCurrencyString
			self?.netwrokFeeLabel.text = self?.viewModel.networkFeeString
			self?.networkFeeCurrencyLabel.text = self?.viewModel.networkFeeCurrencyString
			self?.priceImpactLabel.text = self?.viewModel.priceImpactString
			self?.conversionRateLabel.text = self?.viewModel.conversionRateString
			
			// Adding delay as when its quick its visually jarring
			DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
				if !response.success {
					self?.hideActivity()
					self?.errorResponse = response.errorResponse ?? ErrorResponse.unknownError()
					self?.performSegue(withIdentifier: "failSegue", sender: self)
					
				} else {
					self?.showAllSubviews()
					self?.hideActivity()
				}
			}
		}
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "failSegue", let vc = segue.destination as? ExchangeFailureViewController {
			vc.errorResponse = self.errorResponse
		}
	}
	
	@IBAction func infoButotnTapped(_ sender: Any) {
		self.performSegue(withIdentifier: "burnFeeInfoSegue", sender: self)
	}
}
