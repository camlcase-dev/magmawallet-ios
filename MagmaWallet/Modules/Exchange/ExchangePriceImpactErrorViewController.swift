//
//  ExchangePriceImpactErrorViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 14/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit

class ExchangePriceImpactErrorViewController: UIViewController {
	
	@IBAction func backToExchangeTapped(_ sender: Any) {
		self.navigationController?.popToExchange()
	}
}
