//
//  ExchangeFailureViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 08/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import camlKit
import MagmaWalletKit
import Sentry

class ExchangeFailureViewController: UIViewController {
	
	@IBOutlet weak var titleLabel: ThemeLabel!
	@IBOutlet weak var subTitleLabel: ThemeLabel!
	
	var errorResponse: ErrorResponse = ErrorResponse.unknownError()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		if !Thread.current.isRunningXCTest {
			SentrySDK.capture(message: "Exchange Failed")
		}
		
		let errorDetails = ErrorMessageService.errorTitleAndMessage(for: errorResponse)
		titleLabel.text = errorDetails.title
		subTitleLabel.text = errorDetails.message
		
		self.navigationController?.setNavigationBarHidden(true, animated: false)
	}
	
	@IBAction func reviewAndTryAgainTapped(_ sender: Any) {
		self.navigationController?.setNavigationBarHidden(false, animated: false)
		self.navigationController?.popToExchange()
	}
}
