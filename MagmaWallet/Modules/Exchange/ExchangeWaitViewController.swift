//
//  ExchangeWaitViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 08/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import camlKit
import MagmaWalletKit
import Lottie
import os.log

// TODO: make UIViewController re-usbale

class ExchangeWaitViewController: UIViewController {
	
	@IBOutlet weak var titleLabel: ThemeLabel!
	@IBOutlet weak var animationView: AnimationView!
	
	private let viewModel = ExchangeWaitViewModel()
	private var errorResponse: ErrorResponse = ErrorResponse.unknownError()
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.navigationController?.setNavigationBarHidden(true, animated: false)
		
		// Disable 60 second background refresh refresh
		// DependencyManager.shared.homeService.stopPolling()
		
		let animation = Animation.named("lottie-waiting")
		animationView.animation = animation
		animationView.loopMode = .loop
		
		let transactionService = DependencyManager.shared.transactionService
		let fromToken = DependencyManager.shared.dexterTokens[transactionService.exchangeData.selectedPrimaryTokenIndex]
		let toToken = DependencyManager.shared.dexterTokens[transactionService.exchangeData.selectedSecondaryTokenIndex]
		
		titleLabel.text = "exc_progress_title".localized(fromToken.symbol, toToken.symbol)
		
		// Stop / resume searching if user goes in / out of background
		NotificationCenter.default.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(appMovedToForeground), name: UIApplication.didBecomeActiveNotification, object: nil)
		
		
		// Kick off transaction here
		self.hideAllSubviews()
		self.showActivity()
		
		os_log(.debug, log: .network, "Exchanging tokens")
		viewModel.update() { [weak self] (response) in
			// Adding delay as when its quick its visually jarring
			DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
				if !response.success {
					os_log(.error, log: .network, "Exchanging tokens failed")
					self?.hideActivity()
					self?.errorResponse = response.errorResponse ?? ErrorResponse.unknownError()
					self?.performSegue(withIdentifier: "failSegue", sender: self)
					
				} else {
					os_log(.debug, log: .network, "Exchanging tokens succeeded")
					self?.showAllSubviews()
					self?.hideActivity()
					self?.appMovedToForeground()
				}
			}
		}
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		NotificationCenter.default.removeObserver(self, name: UIApplication.didEnterBackgroundNotification, object: nil)
		NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
		
		// Disable 60 second background refresh refresh
		// DependencyManager.shared.homeService.startPolling()
	}
	
	@objc func appMovedToBackground() {
		self.animationView.stop()
		DependencyManager.shared.tzktClient.cancelWait()
	}
	
	@objc func appMovedToForeground() {
		guard let opHash = DependencyManager.shared.transactionService.exchangeData.inProgressOpHash else {
			return
		}
		
		// Lottie seems to need a brief delay between background and foreground, otherwise it doesn't unpause
		DispatchQueue.main.asyncAfter(deadline: .now()+1) { [weak self] in
			self?.animationView.play()
		}
		
		// Search for Block with TZKT
		DependencyManager.shared.tzktClient.waitForInjection(ofHash: opHash) { [weak self] (success, serviceError, operationError) in
			if success {
				os_log(.debug, log: .tzkt, "Found block, refreshing network data")
				DependencyManager.shared.homeService.addDelegate(self)
				DependencyManager.shared.homeService.refreshAllNetworkData(forceRefresh: true)
				
			} else if let err = serviceError {
				os_log(.error, log: .tzkt, "TZKT returned a server error: %@", "\(err)")
				self?.performSegue(withIdentifier: "serviceFailSegue", sender: self)
				
			} else {
				let err = operationError ?? ErrorResponse.unknownError()
				
				os_log(.debug, log: .tzkt, "Found block, for a failed operation: %@", "\(err)")
				self?.errorResponse = err
				self?.performSegue(withIdentifier: "failSegue", sender: self)
			}
		}
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		self.animationView.stop()
		
		if segue.identifier == "failSegue", let vc = segue.destination as? ExchangeFailureViewController {
			vc.errorResponse = self.errorResponse
		}
	}
	
	@IBAction func returnToExchangeTapped(_ sender: Any) {
		DependencyManager.shared.homeService.removeDelegate(self)
		self.animationView.stop()
		
		DependencyManager.shared.tzktClient.cancelWait()
		DependencyManager.shared.transactionService.resetStoredData()
		self.navigationController?.popToExchange()
	}
	
	@IBAction func goToWalletTapped(_ sender: Any) {
		DependencyManager.shared.homeService.removeDelegate(self)
		self.animationView.stop()
		
		DependencyManager.shared.tzktClient.cancelWait()
		DependencyManager.shared.transactionService.resetStoredData()
		self.navigationController?.popToWallet()
	}
}

extension ExchangeWaitViewController: HomeServiceDelegate {
	func didBeginRefreshingData(balancesAndPricesOnly: Bool) {
		
	}
	
	func didEndRefreshingData(balancesAndPricesOnly: Bool, error: ErrorResponse?) {
		DependencyManager.shared.homeService.removeDelegate(self)
		self.performSegue(withIdentifier: "successSegue", sender: self)
	}
}
