//
//  ExchangeAdvancedOptionsViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 14/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import MagmaWalletKit

class ExchangeAdvancedOptionsViewController: UIViewController {
	
	static let defaultSlippageOptions: [Double] = [0.1, 0.5, 1.0]
	static let defaultTimeout: Int = 20
	
	@IBOutlet weak var containerView: UIView!
	@IBOutlet weak var containerViewBottomContstraint: NSLayoutConstraint!
	@IBOutlet weak var slippageTextfield: ThemeTextfield!
	@IBOutlet weak var slippageErrorStackView: UIStackView!
	@IBOutlet weak var slippageErrorLabel: ThemeLabel!
	@IBOutlet weak var slippageOption1Button: ThemeButton!
	@IBOutlet weak var slippageOption2Button: ThemeButton!
	@IBOutlet weak var slippageOption3Button: ThemeButton!
	@IBOutlet weak var timeoutTextfield: ThemeTextfield!
	@IBOutlet weak var timeoutErrorStackView: UIStackView!
	@IBOutlet weak var timeoutErrorLabel: ThemeLabel!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		slippageTextfield.validator = NumberValidator(min: 0.01, max: 49.99, decimalPlaces: 2)
		slippageTextfield.themeTextfieldDelegate = self
		slippageTextfield.themeTextfieldValidationDelegate = self
		slippageTextfield.keyboardType = .decimalPad
		slippageTextfield.rightModeTextFont = UIFont.barlowFont(.regular, withSize: 14)
		slippageTextfield.rightMode = .text("%")
		slippageTextfield.darkMode = true
		slippageTextfield.borderWidth = 0
		slippageTextfield.addDoneToolbar()
		
		timeoutTextfield.validator = NumberValidator(min: 1, max: 1440, decimalPlaces: 0)
		timeoutTextfield.themeTextfieldDelegate = self
		timeoutTextfield.themeTextfieldValidationDelegate = self
		timeoutTextfield.keyboardType = .decimalPad
		timeoutTextfield.rightModeTextFont = UIFont.barlowFont(.regular, withSize: 14)
		timeoutTextfield.rightMode = .text("minutes")
		timeoutTextfield.darkMode = true
		slippageTextfield.borderWidth = 0
		timeoutTextfield.addDoneToolbar()
		
		if let index = ExchangeAdvancedOptionsViewController.defaultSlippageOptions.firstIndex(of: DependencyManager.shared.transactionService.exchangeData.slippageLimit) {
			setButtonToSelected(index: index)
			
		} else {
			slippageTextfield.text = DependencyManager.shared.transactionService.exchangeData.slippageLimit.description
		}
		
		if DependencyManager.shared.transactionService.exchangeData.timeoutLimit != ExchangeAdvancedOptionsViewController.defaultTimeout {
			timeoutTextfield.text = DependencyManager.shared.transactionService.exchangeData.timeoutLimit.description
		}
		
		displayError(error: false, slippage: true)
		displayError(error: false, slippage: false)
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		NotificationCenter.default.addObserver(self, selector: #selector(customKeyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(customKeyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
		NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
	}
	
	@objc func customKeyboardWillShow(notification: NSNotification) {
		if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
			containerViewBottomContstraint.constant = keyboardSize.height
		}
	}
	
	@objc func customKeyboardWillHide(notification: NSNotification) {
		containerViewBottomContstraint.constant = 0
	}
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		containerView.roundCorners(corners: [.topLeft, .topRight], radius: 12)
	}
	
	func setButtonToSelected(index: Int) {
		if index != -1 {
			slippageTextfield.text = nil
		}
		
		let buttons = [slippageOption1Button, slippageOption2Button, slippageOption3Button]
		
		for (i, button) in buttons.enumerated() {
			button?.titleLabel?.textColor = UIColor.black
			
			if index == i {
				button?.isSelected = true
				DependencyManager.shared.transactionService.exchangeData.slippageLimit = ExchangeAdvancedOptionsViewController.defaultSlippageOptions[index]
				
			} else {
				button?.isSelected = false
			}
		}
	}
	
	func selectedButtonIndex() -> Int {
		if slippageOption1Button.isSelected { return 0 }
		else if slippageOption2Button.isSelected { return 1 }
		else if slippageOption3Button.isSelected { return 2 }
		
		return -1
	}
	
	func unselectAllButtons() {
		slippageOption1Button.isSelected = false
		slippageOption2Button.isSelected = false
		slippageOption3Button.isSelected = false
	}
	
	@IBAction func choice1Tapped(_ sender: Any) {
		setButtonToSelected(index: 0)
	}
	
	@IBAction func choice2Tapped(_ sender: Any) {
		setButtonToSelected(index: 1)
	}
	
	@IBAction func choice3Tapped(_ sender: Any) {
		setButtonToSelected(index: 2)
	}
	
	
	func displayError(error: Bool, slippage: Bool) {
		let stackview = slippage ? slippageErrorStackView : timeoutErrorStackView
		
		for view in stackview?.arrangedSubviews ?? [] {
			view.isHidden = !error
		}
	}
	
	@IBAction func closeButtonTapped(_ sender: Any) {
		if slippageErrorLabel.isHidden, let limit = Double(slippageTextfield.text ?? "") {
			DependencyManager.shared.transactionService.exchangeData.slippageLimit = limit
			setButtonToSelected(index: -1)
			
		} else if (slippageTextfield.text == nil || slippageTextfield.text == "") && selectedButtonIndex() == -1 {
			setButtonToSelected(index: 1)
		}
		
		if timeoutErrorLabel.isHidden, let limitInt = Int(timeoutTextfield.text ?? "") {
			DependencyManager.shared.transactionService.exchangeData.timeoutLimit = limitInt
		}
		
		slippageTextfield.resignFirstResponder()
		timeoutTextfield.resignFirstResponder()
		self.dismiss(animated: true, completion: nil)
	}
}

extension ExchangeAdvancedOptionsViewController: ThemeTextfieldProtocol, ThemeTextfieldValidationProtocol {
	
	func rightButtonTapped(_ textfield: ThemeTextfield) {
		
	}
	
	func didClear() {
		
	}
	
	func didTapDone() {
		
	}
	
	func didBeingEditing(_ textField: UITextField) {
	}
	
	func didEndEditing(_ textField: UITextField) {
	}
	
	func validated(_ validated: Bool, textfield: ThemeTextfield, forText text: String) {
		let isSlippage = textfield == slippageTextfield
		
		if isSlippage {
			unselectAllButtons()
		}
		
		if isSlippage, !validated, text == "" {
			// If user removes the text from the slippage field, auto-select the default option
			displayError(error: false, slippage: isSlippage)
			
		} else if validated {
			displayError(error: false, slippage: isSlippage)
			
		} else if !validated && text != "" {
			if isSlippage, let limit = Decimal(string: text), limit >= 50 {
				slippageErrorLabel.text = "error_slippage_exceed".localized
				
			} else if isSlippage {
				slippageErrorLabel.text = "error_invalid_amount".localized
			}
			
			displayError(error: true, slippage: isSlippage)
			
		} else {
			displayError(error: false, slippage: isSlippage)
		}
	}
}
