//
//  ExchangeWaitViewModel.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 08/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import camlKit
import os.log

class ExchangeWaitViewModel: ViewModel {
	
	func update(withInput input: [String : Any]? = [:], completion: @escaping (ViewModelResponse) -> Void) {
		let transactionService = DependencyManager.shared.transactionService
		let ops = transactionService.exchangeData.operations
		
		guard ops.count > 0, let currentWallet = DependencyManager.shared.accountService.currentWallet()else {
			completion(ViewModelResponse(success: false, responseCode: nil, userDisplayString: nil, errorResponse: ErrorResponse.unknownError()))
			return
		}
		
		DependencyManager.shared.tezosNodeClient.send(operations: ops, withWallet: currentWallet) { (result) in
			switch result {
				case .success(let opHash):
					os_log(.debug, log: .tezos, "Exchnaged: %@ for: %@", transactionService.exchangeData.amountToExchange.normalisedRepresentation, transactionService.exchangeData.conversion.expected.normalisedRepresentation)
					transactionService.exchangeData.inProgressOpHash = opHash
					completion(ViewModelResponse(success: true, responseCode: nil, userDisplayString: nil, errorResponse: nil))
					
				case .failure(let error):
					os_log(.error, log: .tezos, "Error Exchanging: %@", "\(error)")
					completion(ViewModelResponse(success: false, responseCode: nil, userDisplayString: nil, errorResponse: error))
			}
		}
	}
}
