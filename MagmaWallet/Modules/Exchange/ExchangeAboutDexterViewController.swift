//
//  ExchangeAboutDexterViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 22/09/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit

class ExchangeAboutDexterViewController: UIViewController {
	
	@IBOutlet weak var containerView: UIView!
	@IBOutlet weak var readMoreButton: UIButton!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		
		let attributes: [NSAttributedString.Key: Any] = [
			NSAttributedString.Key.font: UIFont.barlowFont(.bold, withSize: 16),
			NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue,
			NSAttributedString.Key.foregroundColor: UIColor.black
		]
		let underlineAttributedString = NSAttributedString(string: "exc_dexter_info_link".localized, attributes: attributes)
		
		readMoreButton.setAttributedTitle(underlineAttributedString, for: .normal)
		readMoreButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
		readMoreButton.semanticContentAttribute = UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
	}
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		containerView.roundCorners(corners: [.topLeft, .topRight], radius: 12)
	}
	
	@IBAction func closeButtonTapped(_ sender: Any) {
		self.dismiss(animated: true, completion: nil)
	}
	
	@IBAction func readMoreButtonTapped(_ sender: Any) {
		if let url = URL(string: "https://dexter.exchange/") {
			UIApplication.shared.open(url, options: [:], completionHandler: nil)
		}
	}
}
