//
//  ContactTransactionHistoryViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 07/09/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import camlKit
import MagmaWalletKit

class ContactTransactionHistoryViewController: UIViewController {
	
	@IBOutlet weak var headingLabel: ThemeLabel!
	@IBOutlet weak var tableView: UITableView!
	
	var contact: TezosContact? = nil
	
	private let viewModel = ActivityViewModel()
	private let refreshControl = UIRefreshControl()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		headingLabel.text = "con_details_history".localized.uppercased()
		
		self.refreshControl.tintColor = UIColor.white
		self.refreshControl.addTarget(self, action: #selector(pullToRefresh), for: UIControl.Event.valueChanged)
		
		self.tableView.rowHeight = UITableView.automaticDimension
		self.tableView.addSubview(refreshControl)
		self.tableView.delegate = self
		
		// Add shared cells
		self.tableView.register(UINib(nibName: TransactionHistoryCellModel.staticIdentifier, bundle: nil), forCellReuseIdentifier: TransactionHistoryCellModel.staticIdentifier)
		self.tableView.register(UINib(nibName: TransactionHistoryExchangeCellModel.staticIdentifier, bundle: nil), forCellReuseIdentifier: TransactionHistoryExchangeCellModel.staticIdentifier)
		self.tableView.register(UINib(nibName: TransactionHistoryFailedCellModel.staticIdentifier, bundle: nil), forCellReuseIdentifier: TransactionHistoryFailedCellModel.staticIdentifier)
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		DependencyManager.shared.homeService.addDelegate(self)
		
		if DependencyManager.shared.homeService.isRefreshingAll() {
			showRefreshControl()
			
		} else {
			refreshUI()
		}
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		DependencyManager.shared.homeService.removeDelegate(self)
		
		hideRefreshControl()
	}
	
	@objc func pullToRefresh() {
		DependencyManager.shared.homeService.addDelegate(self)
		DependencyManager.shared.homeService.refreshAllNetworkData()
	}
	
	func refreshUI() {
		guard let con = contact else {
			return
		}
		
		viewModel.update(withInput: [ActivityViewModel.ActivityViewModelInputKeys.address.rawValue: con.tezosAddress], completion: { [weak self] (response) in
			self?.tableView.tableViewData = self?.viewModel.tableViewData
			self?.tableView.reloadData()
			self?.hideRefreshControl()
		})
	}
	
	func showRefreshControl() {
		if refreshControl.isRefreshing {
			return
		}
		
		// Offset tableview to show refresh control
		let contentOffset = CGPoint(x: 0, y: -refreshControl.frame.height)
		tableView?.setContentOffset(contentOffset, animated: true)
		
		refreshControl.beginRefreshing()
	}
	
	func hideRefreshControl() {
		refreshControl.endRefreshing()
	}
}

extension ContactTransactionHistoryViewController: HomeServiceDelegate {
	
	func didBeginRefreshingData(balancesAndPricesOnly: Bool) {
		
	}
	
	func didEndRefreshingData(balancesAndPricesOnly: Bool, error: ErrorResponse?) {
		if balancesAndPricesOnly {
			return
		}
		
		if let err = error {
			self.tableView.tableViewData = self.viewModel.errorCell()
			self.tableView.reloadData()
			hideRefreshControl()
			
			let errorDetails = ErrorMessageService.errorTitleAndMessage(for: err)
			self.view.makeToast(errorDetails.message)
		} else {
			refreshUI()
		}
	}
}

extension ContactTransactionHistoryViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 20
	}
	
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		
		if section <= self.viewModel.tableViewData.count-1 {
			let containerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 20))
			let sectionHeaderLabel = UILabel(frame: CGRect(x: 24, y: 0, width: tableView.frame.width, height: 20))
			sectionHeaderLabel.text = self.viewModel.tableViewData[section].header
			sectionHeaderLabel.textColor = UIColor(named: "SemiTransparentWhiteText")
			sectionHeaderLabel.font = UIFont.barlowFont(.semiBold, withSize: 13)
			
			containerView.addSubview(sectionHeaderLabel)
			
			return containerView
		}
		
		return nil
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		self.viewModel.openBlockExplorer(forIndexPath: indexPath)
	}
}
