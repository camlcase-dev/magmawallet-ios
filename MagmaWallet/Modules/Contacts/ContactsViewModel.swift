//
//  ContactsViewModel.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 02/09/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import os.log
import Contacts

class ContactsViewModel: ViewModel {
	
	var tableViewData: [UITableViewSectionModel] = []
	
	func update(withInput input: [String : Any]? = [:], completion: @escaping (ViewModelResponse) -> Void) {
		
		var cellModels: [UITableViewCellModel] = []
		
		// If contacts is still processing, or we are still downloading the initial balance, show a loading
		if DependencyManager.shared.contactService.isFetching() {
			cellModels = [ LoadingCellModel(estimatedRowHeight: LoadingCellModel.defaultHeight(), loadingMessage: "") ]
			
		} else {
			let contacts = DependencyManager.shared.contactService.getContacts()
			
			if contacts.count == 0 {
				cellModels = [ EmptyTableViewMessageCellModel(message: "snd_no_contacts".localized) ]
				
			} else {
				for contact in contacts {
					cellModels.append( ContactCellModel(name: contact.fullName, address: contact.tezosAddress) )
				}
			}
		}
		
		tableViewData = [ UITableViewSectionModel(withHeader: nil, footer: nil, andCellModels: cellModels) ]
		
		completion(ViewModelResponse(success: true, responseCode: nil, userDisplayString: nil, errorResponse: nil))
	}
}
