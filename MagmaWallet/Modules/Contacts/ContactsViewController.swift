//
//  ContactsViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 03/09/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit

class ContactsViewController: UIViewController, ContactServiceDelegate {
	
	@IBOutlet weak var tableView: UITableView!
	
	private let viewModel = ContactsViewModel()
	private var selectedIndex = 0
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.tableView.delegate = self
		
		// Add shared cells
		self.tableView.register(UINib(nibName: EmptyTableViewMessageCellModel.staticIdentifier, bundle: nil), forCellReuseIdentifier: EmptyTableViewMessageCellModel.staticIdentifier)
		self.tableView.register(UINib(nibName: ContactCellModel.staticIdentifier, bundle: nil), forCellReuseIdentifier: ContactCellModel.staticIdentifier)
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		DependencyManager.shared.contactService.delegate = self
		
		// Make sure we clear any sotred data when this screen opens
		DependencyManager.shared.transactionService.transactionType = .send
		DependencyManager.shared.transactionService.sendData.recipientName = nil
		DependencyManager.shared.transactionService.sendData.recipientAddress = nil
		
		self.tabBarController?.navigationItem.title = "wlt_navigation_contacts".localized.uppercased()
		self.navigationController?.setNavigationBarHidden(false, animated: false)
		
		let button = UIButton(type: .custom)
		button.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
		button.setBackgroundImage(UIImage(named: "Add Contact"), for: .normal)
		button.addTarget(self, action: #selector(goToAddContact), for: .touchUpInside)
		
		let barButton = UIBarButtonItem(customView: button)
		barButton.accessibilityIdentifier = "add-contact-button"
		
		self.tabBarController?.navigationItem.rightBarButtonItem = barButton
		
		reloadContactsDisplay()
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		self.tabBarController?.navigationItem.rightBarButtonItem = nil
	}
	
	func finishedFetchingContacts() {
		reloadContactsDisplay()
	}
	
	func reloadContactsDisplay() {
		viewModel.update { [weak self] (response) in
			self?.tableView.tableViewData = self?.viewModel.tableViewData
			self?.tableView.reloadData()
		}
	}
	
	@objc func goToAddContact() {
		if DependencyManager.shared.contactService.currentAuthorizationStatus() != .authorized {
			self.performSegue(withIdentifier: "permissionsSegue", sender: self)
			
		} else {
			self.performSegue(withIdentifier: "addContactSegue", sender: self)
		}
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let detailsVC = segue.destination as? ContactDetailsViewController {
			detailsVC.contact = DependencyManager.shared.contactService.getContact(atIndex: selectedIndex)
		}
	}
}

extension ContactsViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let cellModels = self.tableView.tableViewData?[indexPath.section].cellModels
		if cellModels?.count == 1 && cellModels?.first is EmptyTableViewMessageCellModel {
			return
		}
		
		self.selectedIndex = indexPath.row
		self.performSegue(withIdentifier: "contactDetailsSegue", sender: self)
	}
}
