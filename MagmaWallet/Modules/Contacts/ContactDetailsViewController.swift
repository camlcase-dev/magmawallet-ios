//
//  ContactDetailsViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 04/09/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import Toast_Swift
import MobileCoreServices

class ContactDetailsViewController: UIViewController {
	
	var contact: TezosContact? = nil
	
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var addressLabel: UILabel!
	@IBOutlet weak var tableView: UITableView!
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		guard let con = contact else {
			nameLabel.text = ""
			addressLabel.text = ""
			return
		}
		
		nameLabel.text = con.fullName
		addressLabel.text = con.tezosAddress
		tableView.delegate = self
		
		addressLabel.isUserInteractionEnabled = true
		addressLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(copyButtonTapped(_:))))
		
		let shouldDisableSend = (DependencyManager.shared.accountService.isWalletEmpty() || DependencyManager.shared.homeService.isRefreshingAll())
		self.tableView.tableViewData = [UITableViewSectionModel(withHeader: nil, footer: nil, andCellModels: [
			ContactDetailsCellModel(icon: UIImage(named: "Contact-Send"), title: "con_details_send".localized, disabled: shouldDisableSend),
			ContactDetailsCellModel(icon: UIImage(named: "Contact-History"), title: "con_details_history".localized, disabled: false),
			ContactDetailsCellModel(icon: UIImage(named: "Contact-Edit"), title: "con_details_edit".localized, disabled: false),
			ContactDetailsCellModel(icon: UIImage(named: "Contact-Delete"), title: "con_details_delete".localized, disabled: false)
		])]
		self.tableView.reloadData()
	}
	
	@IBAction func copyButtonTapped(_ sender: Any) {
		UIPasteboard.general.setObjects([self], localOnly: true, expirationDate: Date(timeIntervalSinceNow: 60))
		self.view.makeToast("action_copy_success".localized)
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let vc = segue.destination as? ContactTransactionHistoryViewController {
			vc.contact = contact
			
		} else if let vc = segue.destination as? AddContactViewController {
			vc.contactToEdit = contact
		}
	}
}

extension ContactDetailsViewController: NSItemProviderWriting {
	static var writableTypeIdentifiersForItemProvider: [String] {
		return [ kUTTypeUTF8PlainText as String ]
	}
	
	func loadData(withTypeIdentifier typeIdentifier: String, forItemProviderCompletionHandler completionHandler: @escaping (Data?, Error?) -> Void) -> Progress? {
		guard let address = contact?.tezosAddress else {
			completionHandler(nil, NSError(domain: "No wallet address found", code: -1, userInfo: nil))
			return nil
		}
		
		completionHandler(address.data(using: .utf8), nil)
		return nil
	}
}

extension ContactDetailsViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		
		guard let con = contact else {
			return
		}
		
		
		if indexPath.row == 0 {
			if let cellModel = self.tableView.tableViewData?[0].cellModels[indexPath.row] as? ContactDetailsCellModel, cellModel.disabled {
				self.alert(errorWithMessage: "wlt_empty_status".localized)
				return
			}
			
			DependencyManager.shared.transactionService.transactionType = .send
			DependencyManager.shared.transactionService.sendData.recipientName = con.fullName
			DependencyManager.shared.transactionService.sendData.recipientAddress = con.tezosAddress
			DependencyManager.shared.transactionService.needsToChooseToken { (chooseToken) in
				let vc = chooseToken ? UIStoryboard.sendChooseTokenViewController() : UIStoryboard.sendChooseAmountViewController()
				self.navigationController?.pushViewController(vc, animated: true)
			}
			
		} else if indexPath.row == 1 {
			self.performSegue(withIdentifier: "transactionHistorySegue", sender: self)
			
		} else if indexPath.row == 2 {
			self.performSegue(withIdentifier: "editContactSegue", sender: self)
			
		} else if indexPath.row == 3 {
			self.alert(withTitle: "con_delete_title".localized, andMessage: "con_delete_device".localized, okText: "action_confirm".localized, okAction: { [weak self] (okAction) in
				self?.deleteContact()
			}, cancelText: "cancel".localized, cancelAction: nil)
		}
	}
	
	func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
		return UIView()
	}
	
	func deleteContact() {
		guard let con = contact else {
			self.alert(errorWithMessage: "con_error_delete".localized)
			return
		}
		
		self.showActivity(clearBackground: false)
		
		var result = false
		if con.containsOnlyMagmaData {
			result = DependencyManager.shared.contactService.deleteContact(withContactId: con.contactIdentifier)
			
		} else {
			result = DependencyManager.shared.contactService.removeCurrentAddressFromContact(withContactId: con.contactIdentifier)
		}
		
		if !result {
			self.alert(errorWithMessage: "con_error_delete".localized)
		}
		
		DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
			self.hideActivity()
			self.navigationController?.popViewController(animated: true)
		}
	}
}
