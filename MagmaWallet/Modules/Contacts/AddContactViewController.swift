//
//  AddContactViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 04/09/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import MagmaWalletKit
import ContactsUI
import Sentry

class AddContactViewController: UIViewController {
	
	@IBOutlet weak var nameTextfield: ThemeTextfield!
	@IBOutlet weak var nameErrorView: UIView!
	@IBOutlet weak var addressTextfield: ThemeTextfield!
	@IBOutlet weak var addressErrorView: UIView!
	@IBOutlet weak var addressErrorLabel: ThemeLabel!
	@IBOutlet weak var saveContactButton: ThemeButton!
	
	private var nameValidated = false
	private var addressValidated = false
	private var selectedId: String? = nil
	private var textfieldValidator = TezosAddressValidator(ownAddress: DependencyManager.shared.accountService.currentAddress() ?? "")
	
	var contactToEdit: TezosContact? = nil
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.nameTextfield.validator = EmptyStringValidator()
		self.nameTextfield.themeTextfieldValidationDelegate = self
		self.nameTextfield.themeTextfieldDelegate = self
		self.nameTextfield.rightMode = .contactSearch
		self.nameTextfield.bottomBorder = false
		self.nameTextfield.borderStyle = UITextField.BorderStyle.none
		self.nameTextfield.cornerRadius = 4
		self.nameTextfield.maskToBounds = true
		self.nameTextfield.font = UIFont.barlowFont(.medium, withSize: 18)
		self.nameTextfield.backgroundColor = UIColor(named: "TextViewBackground")
		self.nameTextfield.attributedPlaceholder = NSAttributedString(string: "con_add_name_placeholder".localized, attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "TextFieldBorder") ?? .white])
		self.nameErrorView.isHidden = true
		
		self.addressTextfield.validator = textfieldValidator
		self.addressTextfield.themeTextfieldValidationDelegate = self
		self.addressTextfield.themeTextfieldDelegate = self
		self.addressTextfield.rightMode = .scanAndClear
		self.addressTextfield.bottomBorder = false
		self.addressTextfield.borderStyle = UITextField.BorderStyle.none
		self.addressTextfield.cornerRadius = 4
		self.addressTextfield.maskToBounds = true
		self.addressTextfield.font = UIFont.barlowFont(.medium, withSize: 18)
		self.addressTextfield.backgroundColor = UIColor(named: "TextViewBackground")
		self.addressTextfield.attributedPlaceholder = NSAttributedString(string: "snd_recipient_hint".localized, attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "TextFieldBorder") ?? .white])
		self.addressErrorView.isHidden = true
		
		self.saveContactButton.isEnabled = false
		
		// remove permissions screen from stack, user will never be allowed to return to it
		if let vcCount = self.navigationController?.viewControllers.count, vcCount > 2,
			self.navigationController?.viewControllers[vcCount-2] is ContactsPermissionViewController {
				self.navigationController?.viewControllers.remove(at: vcCount-2)
		}
		
		// Check if we are in edit mode, or tapped a "Add to contacts button"
		if let editContact = contactToEdit {
			selectedId = editContact.contactIdentifier
			nameTextfield.text = editContact.fullName
			nameTextfield.revalidateTextfield()
			addressTextfield.text = editContact.tezosAddress
			addressTextfield.revalidateTextfield()
			
			// No matter if system contact, or not we can't access contact book during edit mode
			nameTextfield.rightMode = .none
			
			if !editContact.containsOnlyMagmaData {
				nameTextfield.isEnabled = false
				nameTextfield.backgroundColor = UIColor(named: "TextViewBackground")?.withAlphaComponent(0.1)
				nameTextfield.textColor = UIColor.white.withAlphaComponent(0.25)
			}
			
		} else if let alreadyEnteredRecipient = DependencyManager.shared.transactionService.sendData.recipientAddress {
			addressTextfield.text = alreadyEnteredRecipient
			addressTextfield.revalidateTextfield()
		}
	}
	
	@IBAction func saveButtonTapped(_ sender: Any) {
		self.showActivity(clearBackground: false)
		
		if let existingId = selectedId {
			
			var name: String? = nil
			if let contact = contactToEdit, contact.containsOnlyMagmaData {
				name = nameTextfield.text
			}
			
			if !DependencyManager.shared.contactService.editContact(withContactId: existingId, address: addressTextfield.text ?? "", andName: name) {
				self.hideActivity()
				self.alert(errorWithMessage: "con_error".localized)
				return
			}
			
		} else {
			if !DependencyManager.shared.contactService.addNewContact(name: nameTextfield.text ?? "", address: addressTextfield.text ?? "") {
				SentrySDK.capture(message: "Add Contact Failed")
				self.hideActivity()
				self.alert(errorWithMessage: "con_error".localized)
				return
				
			} else {
				SentrySDK.capture(message: "Add Contact Succeeded")
			}
		}
		
		DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
			self.hideActivity()
			self.navigateToNextScreen()
		}
	}
	
	func navigateToNextScreen() {
		let inSendFlow = self.navigationController?.containsViewController(controller: ChooseAddressContainerViewController.self) ?? false
		
		if inSendFlow {
			DependencyManager.shared.transactionService.transactionType = .send
			DependencyManager.shared.transactionService.sendData.recipientName = nameTextfield.text ?? ""
			DependencyManager.shared.transactionService.sendData.recipientAddress = addressTextfield.text ?? ""
			DependencyManager.shared.transactionService.needsToChooseToken { (chooseToken) in
				let vc = chooseToken ? UIStoryboard.sendChooseTokenViewController() : UIStoryboard.sendChooseAmountViewController()
				self.navigationController?.pushViewController(vc, animated: true)
			}
			
		} else {
			self.navigationController?.popToContacts()
		}
	}
}

// MARK: Validation
extension AddContactViewController: ThemeTextfieldValidationProtocol, ThemeTextfieldProtocol {
	
	func validated(_ validated: Bool, textfield: ThemeTextfield, forText text: String) {
		
		if textfield.rightMode == .scanAndClear {
			addressValidated = validated
			addressErrorView.isHidden = validated
			
			if !validated {
				addressErrorLabel.text = textfieldValidator.isNotOwnAddress(text: text) ? "error_invalid_address".localized : "error_own_address".localized
			}
			
		} else {
			nameValidated = validated
			nameErrorView.isHidden = validated
		}
		
		saveContactButton.isEnabled = (addressValidated && nameValidated)
	}
	
	func rightButtonTapped(_ textfield: ThemeTextfield) {
		if textfield.rightMode == .scanAndClear {
			let vc = ScanViewController()
			vc.delegate = self
			
			self.navigationController?.pushViewController(vc, animated: true)
			
		} else {
			let vc = CNContactPickerViewController()
			vc.delegate = self
			
			// System popups copy naviagtion bar appearence settings, need to temporarily remove them so cancel buttons are visbile
			(UIApplication.shared.delegate as? AppDelegate)?.removeNavigationStyles()
			self.present(vc, animated: true, completion: nil)
		}
	}
	
	func didClear() {
		
	}
	
	func didTapDone() {
		
	}
	
	func didBeingEditing(_ textField: UITextField) {
	}
	
	func didEndEditing(_ textField: UITextField) {
	}
}

// MARK: QR code scanning
extension AddContactViewController: ScanViewControllerDelegate {
	
	func scannedQRCode(code: String) {
		self.addressTextfield.text = code
		self.addressTextfield.revalidateTextfield()
	}
}

// MARK: Contacts
extension AddContactViewController: CNContactPickerDelegate {
	
	func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
		(UIApplication.shared.delegate as? AppDelegate)?.setGlobalStyles()
		
		picker.dismiss(animated: true, completion: nil)
	}
	
	func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
		if let _ = TezosContact.existingTezosAddress(contact) {
			self.alert(errorWithMessage: "con_error_already_exists".localized)
			return
		}
		
		(UIApplication.shared.delegate as? AppDelegate)?.setGlobalStyles()
		nameTextfield.text = TezosContact.fullName(fromContact: contact)
		nameTextfield.revalidateTextfield()
		
		selectedId = contact.identifier
		
		picker.dismiss(animated: true, completion: nil)
	}
}
