//
//  ContactDetailsCell.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 07/09/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import MagmaWalletKit
import os.log

struct ContactDetailsCellModel: UITableViewCellModel {
	
	static let staticIdentifier = "ContactDetailsCell"
	let cellId: String = ContactDetailsCellModel.staticIdentifier
	let estimatedRowHeight: CGFloat = 64
	
	let icon: UIImage?
	let title: String
	let disabled: Bool
}

class ContactDetailsCell: UITableViewCell, UITableViewCellMVVM {
	
	@IBOutlet weak var iconView: UIImageView!
	@IBOutlet weak var titleLabel: ThemeLabel!
	
	func setup(withModel model: UITableViewCellModel) {
		guard let cellModel = model as? ContactDetailsCellModel else {
			os_log(.error, log: .mvvm, "Cell model passed in to 'ContactDetailsCell' did not match required type 'ContactDetailsCellModel', instead found '%@'", String(describing: model.self))
			return
		}
		
		iconView.image = cellModel.icon
		titleLabel.text = cellModel.title
		
		if cellModel.disabled {
			titleLabel.textColor = titleLabel.textColor.withAlphaComponent(0.5)
			iconView.alpha = 0.5
		}
	}
}
