//
//  ContactsPermissionViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 04/09/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import os.log

class ContactsPermissionViewController: UIViewController {
	
	@IBAction func approveButtonTapped(_ sender: Any) {
		
		let authStatus = DependencyManager.shared.contactService.currentAuthorizationStatus()
		if authStatus != .denied && authStatus != .restricted {
			requestAccess()
			
		} else {
			linkToSettings()
		}
	}
	
	func requestAccess() {
		DependencyManager.shared.contactService.requestAccessIfNeeded { (granted, error) in
			if let err = error {
				os_log(.error, log: .contacts, "Error while granting permission: %@", "\(err)")
				self.alert(errorWithMessage: "con_permission_error".localized)
				return
			}
			
			if granted {
				self.performSegue(withIdentifier: "addContactSegue", sender: self)
				
			} else {
				self.navigationController?.popViewController(animated: true)
			}
		}
	}
	
	func linkToSettings() {
		guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
			return
		}
		
		if UIApplication.shared.canOpenURL(settingsUrl) {
			UIApplication.shared.open(settingsUrl, completionHandler: nil)
		}
	}
}
