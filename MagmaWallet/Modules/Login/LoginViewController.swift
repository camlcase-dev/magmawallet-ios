//
//  LoginViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 26/03/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import MagmaWalletKit
import LocalAuthentication

class LoginViewController: UIViewController {
	
	
	@IBOutlet weak var backButton: UIButton!
	@IBOutlet weak var textField: ThemeTextfield!
	@IBOutlet weak var errorMessageStackView: UIStackView!
	@IBOutlet weak var useBiometricButton: UIButton!
	@IBOutlet weak var continueButton: UIButton!
	@IBOutlet weak var continueButtonBottomConstraint: NSLayoutConstraint!
	
	private let biometricChoice = DependencyManager.shared.accountService.getBiometricChoice()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		errorMessageStackView.isHidden = true
		
		if biometricChoice == .none {
			useBiometricButton.isHidden = true
			
		} else if biometricChoice == .touchID {
			useBiometricButton.setTitle("action_use_biometric".localized("onb_biometric_touchid".localized), for: .normal)
			
		} else {
			useBiometricButton.setTitle("action_use_biometric".localized("onb_biometric_faceid".localized), for: .normal)
		}
		
		textField.validator = LengthValidator(maxLength: 6, minLength: 4)
		textField.themeTextfieldValidationDelegate = self
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		// Can't use the current UIViewController extension keyboard code.
		// Button moves up initially, but snaps back to bottom on viewDidAppear, needed to tweak it for this screen
		NotificationCenter.default.addObserver(self, selector: #selector(customKeyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
		
		self.navigationController?.setNavigationBarHidden(true, animated: false)
		self.navigationItem.hidesBackButton = true
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		if biometricChoice != .none {
			biometricLogin()
		} else {
			textField.becomeFirstResponder()
		}
	}
	
	@IBAction func backButtonTapped(_ sender: UIButton) {
		if !DependencyManager.shared.accountService.setOnboardingtatus(status: .none) {
			self.alert(errorWithMessage: "error_cant_save_wallet".localized)
		}
		
		if self.isModal {
			if let nav = self.presentingViewController as? UINavigationController {
				nav.popToRootViewController(animated: false)
				self.dismiss(animated: true, completion: nil)
			}
		} else {
			self.navigationController?.popToRootViewController(animated: false)
		}
	}
	
	@IBAction func useBiometricTapped(_ sender: UIButton) {
		biometricLogin()
	}
	
	@IBAction func continueButtonTapped(_ sender: Any) {
		if !validateAndLogin(text: textField.text ?? "") {
			errorMessageStackView.isHidden = false
		}
	}
	
	func biometricLogin() {
		let myContext = LAContext()
		var authError: NSError?
		
		if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
			appDelegate.launchingSystemPopup = true
		}
		
		if myContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
			myContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: "lgn_touchid_description".localized) { [weak self] success, evaluateError in
				DispatchQueue.main.async {
					if success {
						self?.successfulLogin()
					} else {
						self?.textField.becomeFirstResponder()
					}
				}
			}
		}
	}
	
	func validateAndLogin(text: String) -> Bool {
		if DependencyManager.shared.accountService.validatePinCode(code: text, temp: false) {
			self.successfulLogin()
			return true
			
		} else {
			return false
		}
	}
	
	func successfulLogin() {
		if self.isModal {
			self.dismiss(animated: true, completion: nil)
		} else {
			// If not modal we are coming from the welcome screen after logging in again. Reset the onboarding status
			if !DependencyManager.shared.accountService.setOnboardingtatus(status: .onboardingComplete) {
				self.alert(errorWithMessage: "error_cant_save_wallet".localized)
			}
			self.performSegue(withIdentifier: "homeSegue", sender: self)
		}
	}
	
	@objc func customKeyboardWillShow(notification: NSNotification) {
		if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
			continueButtonBottomConstraint.constant = keyboardSize.height
		}
	}
}

extension LoginViewController: ThemeTextfieldValidationProtocol {
	
	func validated(_ validated: Bool, textfield: ThemeTextfield, forText text: String) {
		continueButton.isEnabled = validated
		
		if errorMessageStackView.isHidden == false {
			errorMessageStackView.isHidden = true
		}
		
		if validated {
			let _ = validateAndLogin(text: text)
		}
	}
}
