//
//  ChooseAddressContainerViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 03/02/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import MagmaWalletKit

class ChooseAddressContainerViewController: UIViewController {
	
	public let chooseTokenViewModel = ChooseTokenViewModel()
	
	@IBOutlet weak var segmentedControl: ThemeSegmentedControl!
	@IBOutlet weak var addContactButton: UIButton!
	@IBOutlet weak var enterAddressContainerView: UIView!
	@IBOutlet weak var contactsContainerView: UIView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Make sure we clear any sotred data when this screen opens
		DependencyManager.shared.transactionService.transactionType = .send
		DependencyManager.shared.transactionService.sendData.recipientName = nil
		DependencyManager.shared.transactionService.sendData.recipientAddress = nil
		
		self.navigationItem.hidesBackButton = false
		segmentedControlValueChanged(segmentedControl)
		
		// Decision on which VC to go to next will depend on how many options available on next screen
		// `.update` doesn't trigger any network code and will be ready in time. Simply re-using the logic
		chooseTokenViewModel.update { (response) in }
		
		self.segmentedControl.setTitle("snd_enter_address".localized, forSegmentAt: 0)
		self.segmentedControl.setTitle("snd_select_address".localized, forSegmentAt: 1)
	}
	
	@IBAction func segmentedControlValueChanged(_ sender: ThemeSegmentedControl) {
		if segmentedControl.selectedSegmentIndex == 0 {
			self.addContactButton.isHidden = true
			self.enterAddressContainerView.isHidden = false
			self.contactsContainerView.isHidden = true
			
		} else {
			for child in self.children {
				if let enterViewController = child as? ChooseAddressEnterViewController {
					enterViewController.textField.resignFirstResponder()
				}
			}
			
			self.addContactButton.isHidden = false
			self.enterAddressContainerView.isHidden = true
			self.contactsContainerView.isHidden = false
		}
	}
	
	@IBAction func addContactTapped(_ sender: UIButton) {
		if DependencyManager.shared.contactService.currentAuthorizationStatus() != .authorized {
			let vc = UIStoryboard.contactPermissionsViewController()
			self.navigationController?.pushViewController(vc, animated: true)
			
		} else {
			let vc = UIStoryboard.addContactViewController()
			self.navigationController?.pushViewController(vc, animated: true)
		}
	}
}
