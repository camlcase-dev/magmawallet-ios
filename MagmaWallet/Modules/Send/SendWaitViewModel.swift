//
//  SendWaitViewModel.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 01/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import camlKit
import os.log

class SendWaitViewModel: ViewModel {
	
	func update(withInput input: [String : Any]? = [:], completion: @escaping (ViewModelResponse) -> Void) {
		let transactionService = DependencyManager.shared.transactionService
		let ops = transactionService.sendData.operations
		
		guard ops.count > 0, let currentWallet = DependencyManager.shared.accountService.currentWallet() else {
			completion(ViewModelResponse(success: false, responseCode: nil, userDisplayString: nil, errorResponse: ErrorResponse.unknownError()))
			return
		}
		
		DependencyManager.shared.tezosNodeClient.send(operations: ops, withWallet: currentWallet) { (result) in
			switch result {
				case .success(let opHash):
					os_log(.debug, log: .tezos, "Sent %@ to %@: hash - %@", transactionService.sendData.amountToSend.description, transactionService.sendData.recipientDisplay(), opHash)
					transactionService.sendData.inProgressOpHash = opHash
					completion(ViewModelResponse(success: true, responseCode: nil, userDisplayString: nil, errorResponse: nil))
					
				case .failure(let error):
					os_log(.error, log: .tezos, "Error sending: %@", "\(error)")
					completion(ViewModelResponse(success: false, responseCode: nil, userDisplayString: nil, errorResponse: error))
			}
		}
	}
}
