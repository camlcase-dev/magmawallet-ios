//
//  ChooseTokenViewModel.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 30/04/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import camlKit

class ChooseTokenViewModel: ViewModel {
	
	var tableViewData: [UITableViewSectionModel] = []
	
	func update(withInput input: [String : Any]? = [:], completion: @escaping (ViewModelResponse) -> Void) {
		
		var cellModels: [UITableViewCellModel] = []
		let tokenPriceService = DependencyManager.shared.tokenPriceService
		
		for token in DependencyManager.shared.balances {
			if token.balance > TokenAmount.zeroBalance(decimalPlaces: 0) {
				let tokenString = tokenPriceService.format(decimal: token.balance.toNormalisedDecimal() ?? 0, numberStyle: .decimal, maximumFractionDigits: token.decimalPlaces)
				let currencyString = tokenPriceService.format(decimal: (token.balance * token.localCurrencyRate), numberStyle: .currency)
				cellModels.append(ChooseTokenCellModel(amount: tokenString, symbol: token.symbol, currencyString: currencyString, token: token))
			}
		}
		
		tableViewData = [UITableViewSectionModel(withHeader: nil, footer: nil, andCellModels: cellModels)]
		completion(ViewModelResponse(success: true, responseCode: nil, userDisplayString: nil, errorResponse: nil))
	}
}
