//
//  ConfirmSendViewModel.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 06/02/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import camlKit
import os.log

class ConfirmSendViewModel: ViewModel {
	
	var sendTokenString = ""
	var sendCurrencyString = ""
	var networkFeeTokenString = ""
	var networkFeeCurrencyString = ""
	var totalTokenString = ""
	var totalCurrencyString = ""
	var addressHeadingString = ""
	var addressString = ""
	
	func update(withInput input: [String : Any]? = [:], completion: @escaping (ViewModelResponse) -> Void) {
		let transactionService = DependencyManager.shared.transactionService
		let accountService = DependencyManager.shared.accountService
		let tokenType = DependencyManager.shared.balances[transactionService.sendData.selectedTokenIndex]
		
		guard let currentWallet = accountService.currentWallet() else {
			completion(ViewModelResponse(success: false, responseCode: nil, userDisplayString: nil, errorResponse: ErrorResponse.error(string: "", errorType: .unknownWallet)))
			return
		}
		
		// User must leave 1 mutez in their wallet or end up paying extra fees which cost more
		// Users also can't send all of their XTZ if they are delegated, they must leave 1 mutez in place (forbidden by blockchain)
		let xtzMaxPossibleBalance = (DependencyManager.shared.balances[0].balance - XTZAmount(fromNormalisedAmount: 0.000001))
		var updatedSendingAmount = transactionService.sendData.amountToSend
		if tokenType.tokenType == .xtz && updatedSendingAmount >= xtzMaxPossibleBalance {
			updatedSendingAmount = xtzMaxPossibleBalance
		}
		
		// Estimate and process operations
		let operations = OperationFactory.sendOperation(updatedSendingAmount, of: tokenType, from: currentWallet.address, to: transactionService.sendData.recipientAddress ?? "")
		DependencyManager.shared.tezosNodeClient.estimate(operations: operations, withWallet: currentWallet) { (result) in
			switch result {
				case .success(let ops):
					self.processOps(sendingAmount: updatedSendingAmount, token: tokenType, operations: ops)
					completion(ViewModelResponse(success: true, responseCode: nil, userDisplayString: nil, errorResponse: nil))
					
				case .failure(let error):
					os_log("Error estimating: %@", log: .tezos, type: .error, "\(error)")
					completion(ViewModelResponse(success: false, responseCode: nil, userDisplayString: nil, errorResponse: error))
			}
		}
	}
	
	func processOps(sendingAmount: TokenAmount, token: Token, operations: [camlKit.Operation]) {
		let transactionService = DependencyManager.shared.transactionService
		let tokenPriceService = DependencyManager.shared.tokenPriceService
		var updatedSendingAmount = sendingAmount
		
		let operationFee = operations.map({ $0.operationFees?.allFees() ?? XTZAmount.zero() }).reduce(XTZAmount.zero(), +)
		let xtzBalance = (DependencyManager.shared.balances[0].balance - XTZAmount(fromNormalisedAmount: 0.000001)) // User must leave 1 mutez in their wallet or end up paying extra fees which cost more
		let xtzLocalPrice = DependencyManager.shared.balances[0].localCurrencyRate
		
		
		// If the user is trying to send the maximum amount of XTZ, we need to deduct the fee from the sending amount first
		if token.tokenType == .xtz && (sendingAmount + operationFee) > xtzBalance {
			updatedSendingAmount += (xtzBalance - (sendingAmount + operationFee))
			
			if let transactionOperation = operations.last as? OperationTransaction {
				transactionOperation.amount = updatedSendingAmount.rpcRepresentation
			}
		}
		
		// Store updated transactions
		transactionService.sendData.operations = operations
		transactionService.sendData.amountToSend = updatedSendingAmount
		
		// Display fee strings
		self.networkFeeTokenString = operationFee.normalisedRepresentation + " XTZ"
		self.networkFeeCurrencyString = tokenPriceService.format(decimal: (operationFee * xtzLocalPrice), numberStyle: .currency)
		self.sendTokenString =  updatedSendingAmount.normalisedRepresentation + " " + token.symbol
		
		if token.tokenType == .xtz {
			self.sendCurrencyString = tokenPriceService.format(decimal: (updatedSendingAmount * token.localCurrencyRate), numberStyle: .currency)
		} else {
			self.sendCurrencyString = " "
		}
		
		if token.tokenType == .xtz {
			let totalAmount = (updatedSendingAmount + operationFee)
			self.totalTokenString = totalAmount.normalisedRepresentation + " XTZ"
			self.totalCurrencyString = tokenPriceService.format(decimal: (totalAmount * token.localCurrencyRate), numberStyle: .currency)
			
		} else {
			let totalCurrency = (updatedSendingAmount * token.localCurrencyRate) + (operationFee * xtzLocalPrice)
			self.totalTokenString = "\(updatedSendingAmount.normalisedRepresentation) \(token.symbol)\n + \(operationFee.normalisedRepresentation) XTZ"
			self.totalCurrencyString = tokenPriceService.format(decimal: totalCurrency, numberStyle: .currency)
		}
		
		if let name = transactionService.sendData.recipientName {
			self.addressHeadingString = "snd_address_contact_section".localized(name)
			
		} else {
			self.addressHeadingString = "snd_address_section".localized
		}
		
		self.addressString = transactionService.sendData.recipientAddress ?? ""
	}
}
