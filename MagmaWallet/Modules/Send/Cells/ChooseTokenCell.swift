//
//  ChooseTokenCell.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 30/04/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import camlKit
import MagmaWalletKit
import os.log

struct ChooseTokenCellModel: UITableViewCellModel {
	
	static let staticIdentifier = "ChooseTokenCell"
	let cellId: String = ChooseTokenCellModel.staticIdentifier
	let estimatedRowHeight: CGFloat = 110
	
	let amount: String
	let symbol: String
	let currencyString: String
	let token: Token
}

class ChooseTokenCell: UITableViewCell, UITableViewCellMVVM {
	
	@IBOutlet weak var tokenAmountLabel: ThemeLabel!
	@IBOutlet weak var tokenSymbolLabel: ThemeLabel!
	@IBOutlet weak var currencyLabel: ThemeLabel!
	
	func setup(withModel model: UITableViewCellModel) {
		guard let cellModel = model as? ChooseTokenCellModel else {
			os_log(.error, log: .mvvm, "Cell model passed in to 'ChooseTokenCell' did not match required type 'ChooseTokenCellModel', instead found '%@'", String(describing: model.self))
			return
		}
		
		tokenAmountLabel.text = cellModel.amount
		tokenSymbolLabel.text = cellModel.symbol
		currencyLabel.text = cellModel.currencyString
	}
}
