//
//  ChooseAddressEnterViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 02/09/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import MagmaWalletKit
import os.log

class ChooseAddressEnterViewController: UIViewController {
	
	@IBOutlet weak var textField: ThemeTextfield!
	@IBOutlet weak var errorView: UIView!
	@IBOutlet weak var errorLabel: ThemeLabel!
	@IBOutlet weak var addToContactsButton: ThemeButton!
	@IBOutlet weak var continueButton: ThemeButton!
	
	private var textfieldValidator = TezosAddressValidator(ownAddress: DependencyManager.shared.accountService.currentAddress() ?? "")
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.textField.validator = textfieldValidator
		self.textField.themeTextfieldValidationDelegate = self
		self.textField.themeTextfieldDelegate = self
		self.textField.rightMode = .scanAndClear
		self.textField.bottomBorder = false
		self.textField.borderStyle = UITextField.BorderStyle.none
		self.textField.cornerRadius = 4
		self.textField.maskToBounds = true
		self.textField.font = UIFont.barlowFont(.medium, withSize: 18)
		self.textField.backgroundColor = UIColor(named: "TextViewBackground")
		self.textField.attributedPlaceholder = NSAttributedString(string: "snd_recipient_hint".localized, attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "TextFieldBorder") ?? .white])
		self.addToContactsButton.isHidden = true
		self.errorView.isHidden = true
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.navigationController?.setNavigationBarHidden(false, animated: false)
		self.navigationItem.title = ""
	}
	
	@IBAction func continueTapped(_ sender: ThemeButton) {
		let transactionService = DependencyManager.shared.transactionService
		transactionService.transactionType = .send
		transactionService.sendData.recipientAddress = textField.text
		
		guard let parent = self.parent as? ChooseAddressContainerViewController else {
			os_log(.error, log: .viewLifeCycle, "Can't find parent for ChooseAddressEnterViewController, defaulting to chooseTokenSegue")
			self.parent?.performSegue(withIdentifier: "chooseTokenSegue", sender: self)
			return
		}
		
		// If coming from the home screen, and there are more than 1 token option to display, show user all tokens
		if transactionService.sendData.selectedTokenIndex == -1 && parent.chooseTokenViewModel.tableViewData[0].cellModels.count > 1 {
			parent.performSegue(withIdentifier: "chooseTokenSegue", sender: self)
		} else {
			parent.performSegue(withIdentifier: "chooseAmountSegue", sender: self)
		}
	}
	
	@IBAction func addToContactsTapped(_ sender: Any) {
		if DependencyManager.shared.contactService.currentAuthorizationStatus() != .authorized {
			let vc = UIStoryboard.contactPermissionsViewController()
			self.navigationController?.pushViewController(vc, animated: true)
			
		} else {
			let vc = UIStoryboard.addContactViewController()
			self.navigationController?.pushViewController(vc, animated: true)
		}
	}
}

// MARK: Validation
extension ChooseAddressEnterViewController: ThemeTextfieldValidationProtocol, ThemeTextfieldProtocol {
	
	func validated(_ validated: Bool, textfield: ThemeTextfield, forText text: String) {
		self.continueButton.isEnabled = validated
		self.errorView.isHidden = validated
		
		if validated {
			DependencyManager.shared.transactionService.sendData.recipientAddress = text
			
			// Check to see if the entered or scanned address matches an existing contact
			let nameOrAddress = DependencyManager.shared.contactService.displayStringForAddress(text)
			if nameOrAddress != text {
				DependencyManager.shared.transactionService.sendData.recipientName = nameOrAddress
			} else {
				DependencyManager.shared.transactionService.sendData.recipientName = nil
			}
			
			self.addToContactsButton.isHidden = false
			
		} else {
			errorLabel.text = textfieldValidator.isNotOwnAddress(text: text) ? "error_invalid_address".localized : "error_own_address".localized
		}
	}
	
	func rightButtonTapped(_ textfield: ThemeTextfield) {
		let vc = ScanViewController()
		vc.delegate = self
		
		self.parent?.navigationController?.pushViewController(vc, animated: true)
	}
	
	func didClear() {
		
	}
	
	func didTapDone() {
		
	}
	
	func didBeingEditing(_ textField: UITextField) {
	}
	
	func didEndEditing(_ textField: UITextField) {
	}
}

// MARK: QR code scanning
extension ChooseAddressEnterViewController: ScanViewControllerDelegate {
	
	func scannedQRCode(code: String) {
		self.textField.text = code
		self.textField.revalidateTextfield()
	}
}
