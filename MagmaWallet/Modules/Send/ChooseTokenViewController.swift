//
//  ChooseTokenViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 30/04/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit

class ChooseTokenViewController: UIViewController {
	
	@IBOutlet weak var tableView: UITableView!
	
	private let viewModel = ChooseTokenViewModel()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.navigationController?.setNavigationBarHidden(false, animated: false)
		
		tableView.delegate = self
		
		self.viewModel.update { [weak self] (response) in
			self?.tableView.tableViewData = self?.viewModel.tableViewData
		}
	}
}

extension ChooseTokenViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let cellModel = self.viewModel.tableViewData[indexPath.section].cellModels[indexPath.row] as? ChooseTokenCellModel
		DependencyManager.shared.transactionService.sendData.selectedTokenIndex = DependencyManager.shared.balances.firstIndex(where: { $0.symbol == cellModel?.token.symbol }) ?? 0
		
		self.performSegue(withIdentifier: "chooseAmountSegue", sender: self)
	}
}
