//
//  ChooseAmountViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 03/02/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import camlKit
import MagmaWalletKit

class ChooseAmountViewController: UIViewController {
	
	@IBOutlet weak var titleLabel: ThemeLabel?
	@IBOutlet weak var subtitleLabel: ThemeLabel?
	@IBOutlet weak var textField: ThemeTextfield?
	@IBOutlet weak var textFieldCurrencyLabel: ThemeLabel?
	@IBOutlet weak var errorView: UIView?
	@IBOutlet weak var errorLabel: ThemeLabel?
	@IBOutlet weak var sendAllButton: ThemeButton?
	@IBOutlet weak var continueButton: ThemeButton?
	@IBOutlet weak var continueButtonBottomConstraint: NSLayoutConstraint?
	
	private var selectedToken: Token = DependencyManager.shared.balances[0]
	private var selectedTokenBalance: TokenAmount = TokenAmount.zero()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.navigationController?.setNavigationBarHidden(false, animated: false)
		
		errorView?.isHidden = true
		
		// Setup labels
		let tokenPriceService = DependencyManager.shared.tokenPriceService
		let selectedTokenIndex = DependencyManager.shared.transactionService.sendData.selectedTokenIndex
		
		if selectedTokenIndex != -1 {
			selectedToken = DependencyManager.shared.balances[selectedTokenIndex]
		} else {
			DependencyManager.shared.transactionService.sendData.selectedTokenIndex = 0
			selectedToken = DependencyManager.shared.balances[0]
		}
		
		selectedTokenBalance = selectedToken.balance
		
		self.titleLabel?.text = "snd_amount_title".localized(selectedToken.symbol)
		self.subtitleLabel?.text = "snd_amount_balance".localized(selectedToken.balance.normalisedRepresentation) + " \(selectedToken.symbol)"
		self.textFieldCurrencyLabel?.text = tokenPriceService.placeholderCurrencyString()
		
		
		// SetupTextField
		var validator = TokenAmountValidator()
		validator.isXTZ = selectedToken.tokenType == .xtz
		validator.balanceLimit = selectedTokenBalance
		validator.decimalPlaces = selectedToken.decimalPlaces
		
		textField?.validator = validator
		textField?.themeTextfieldValidationDelegate = self
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		NotificationCenter.default.addObserver(self, selector: #selector(customKeyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		textField?.becomeFirstResponder()
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
	}
	
	@IBAction func sendAllTapped(_ sender: ThemeButton) {
		textField?.text = selectedTokenBalance.normalisedRepresentation
		textField?.revalidateTextfield()
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.destination is ConfirmSendViewController, let amount = TokenAmount(fromNormalisedAmount: textField?.text ?? "", decimalPlaces: selectedToken.decimalPlaces) {
			DependencyManager.shared.transactionService.sendData.amountToSend = amount
		}
	}
	
	@objc func customKeyboardWillShow(notification: NSNotification) {
		if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
			continueButtonBottomConstraint?.constant = keyboardSize.height
		}
	}
}

extension ChooseAmountViewController: ThemeTextfieldValidationProtocol {
	
	func validated(_ validated: Bool, textfield: ThemeTextfield, forText text: String) {
		if let tokenAmount = TokenAmount(fromNormalisedAmount: text, decimalPlaces: selectedToken.decimalPlaces) {
			textFieldCurrencyLabel?.text = DependencyManager.shared.tokenPriceService.format(decimal: (tokenAmount * selectedToken.localCurrencyRate), numberStyle: .currency)
			continueButton?.isEnabled = (validated && tokenAmount > TokenAmount.zeroBalance(decimalPlaces: 0))
		}
		
		if text != "" {
			errorView?.isHidden = validated
		}
	}
}
