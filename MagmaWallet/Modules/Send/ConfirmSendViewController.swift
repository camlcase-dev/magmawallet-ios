//
//  ConfirmSendViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 06/02/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import camlKit
import MagmaWalletKit

class ConfirmSendViewController: UIViewController {
	
	@IBOutlet weak var sendTokenLabel: ThemeLabel!
	@IBOutlet weak var sendCurrencyLabel: ThemeLabel!
	@IBOutlet weak var networkFeeDetailsButton: ThemeButton!
	@IBOutlet weak var networkFeeTokenLabel: ThemeLabel!
	@IBOutlet weak var networkFeeCurrencyLabel: ThemeLabel!
	@IBOutlet weak var totalTokenLabel: ThemeLabel!
	@IBOutlet weak var totalCurrencyLabel: ThemeLabel!
	@IBOutlet weak var addressHeadingLabel: ThemeLabel!
	@IBOutlet weak var addressLabel: ThemeLabel!
	
	private let viewModel = ConfirmSendViewModel()
	private var errorResponse: ErrorResponse = ErrorResponse.unknownError()
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.hideAllSubviews()
		self.showActivity()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		viewModel.update(withInput: nil) { [weak self] (response) in
			self?.sendTokenLabel.text = self?.viewModel.sendTokenString
			self?.sendCurrencyLabel.text = self?.viewModel.sendCurrencyString
			self?.networkFeeTokenLabel.text = self?.viewModel.networkFeeTokenString
			self?.networkFeeCurrencyLabel.text = self?.viewModel.networkFeeCurrencyString
			self?.totalTokenLabel.text = self?.viewModel.totalTokenString
			self?.totalCurrencyLabel.text = self?.viewModel.totalCurrencyString
			self?.addressHeadingLabel.text = self?.viewModel.addressHeadingString
			self?.addressLabel.text = self?.viewModel.addressString
			
			// Adding delay as when its quick its visually jarring
			DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
				if !response.success {
					self?.hideActivity()
					self?.errorResponse = response.errorResponse ?? ErrorResponse.unknownError()
					self?.performSegue(withIdentifier: "failSegue", sender: self)
					
				} else {
					self?.showAllSubviews()
					self?.hideActivity()
				}
			}
		}
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "failSegue", let vc = segue.destination as? SendFailureViewController {
			vc.errorResponse = self.errorResponse
		}
	}
}
