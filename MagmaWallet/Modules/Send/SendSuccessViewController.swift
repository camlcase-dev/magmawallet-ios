//
//  SendSuccessViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 01/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import MagmaWalletKit
import Sentry
import Lottie

class SendSuccessViewController: UIViewController {
	
	@IBOutlet weak var animationView: AnimationView?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		if !Thread.current.isRunningXCTest {
			SentrySDK.capture(message: "Send Succeeded")
		}
		
		self.navigationController?.setNavigationBarHidden(true, animated: false)
		DependencyManager.shared.transactionService.resetStoredData()
		
		if let animation = Animation.named("lottie-success") {
			animationView?.animation = animation
		}
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		animationView?.play()
	}
	
	@IBAction func returnToWalletTapped(_ sender: ThemeButton) {
		DependencyManager.shared.transactionService.resetStoredData()
		self.navigationController?.popToWallet()
	}
}
