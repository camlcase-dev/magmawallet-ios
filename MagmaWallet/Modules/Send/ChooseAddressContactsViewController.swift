//
//  ChooseAddressContactsViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 02/09/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import os.log

class ChooseAddressContactsViewController: UIViewController, ContactServiceDelegate {
	
	@IBOutlet weak var tableView: UITableView!
	
	private let viewModel = ContactsViewModel()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Add shared cells
		self.tableView.register(UINib(nibName: EmptyTableViewMessageCellModel.staticIdentifier, bundle: nil), forCellReuseIdentifier: EmptyTableViewMessageCellModel.staticIdentifier)
		self.tableView.register(UINib(nibName: ContactCellModel.staticIdentifier, bundle: nil), forCellReuseIdentifier: ContactCellModel.staticIdentifier)
		self.tableView.delegate = self
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		DependencyManager.shared.contactService.delegate = self
		reloadContactsDisplay()
	}
	
	func finishedFetchingContacts() {
		reloadContactsDisplay()
	}
	
	func reloadContactsDisplay() {
		viewModel.update { [weak self] (response) in
			self?.tableView.tableViewData = self?.viewModel.tableViewData
			self?.tableView.reloadData()
		}
	}
}

extension ChooseAddressContactsViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let cellModels = self.tableView.tableViewData?[indexPath.section].cellModels
		if cellModels?.count == 1 && cellModels?.first is EmptyTableViewMessageCellModel {
			return
		}
		
		let transactionService = DependencyManager.shared.transactionService
		let contact = DependencyManager.shared.contactService.getContact(atIndex: indexPath.row)
		
		transactionService.transactionType = .send
		transactionService.sendData.recipientName = contact?.fullName
		transactionService.sendData.recipientAddress = contact?.tezosAddress
		
		guard let parent = self.parent as? ChooseAddressContainerViewController else {
			os_log(.error, log: .viewLifeCycle, "Can't find parent for ChooseAddressContactsViewController, defaulting to chooseTokenSegue")
			self.parent?.performSegue(withIdentifier: "chooseTokenSegue", sender: self)
			return
		}
		
		// If coming from the home screen, and there are more than 1 token option to display, show user all tokens
		if transactionService.sendData.selectedTokenIndex == -1 && parent.chooseTokenViewModel.tableViewData[0].cellModels.count > 1 {
			parent.performSegue(withIdentifier: "chooseTokenSegue", sender: self)
		} else {
			parent.performSegue(withIdentifier: "chooseAmountSegue", sender: self)
		}
	}
}
