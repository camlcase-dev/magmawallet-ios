//
//  CreateAddressViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 14/01/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import Sentry

class CreateAddressViewController: UIViewController {
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		/*
		if let recoveryPhrase = DependencyManager.shared.transactionService.walletRecoveryData.recoveryPhrase {
			
			// Import a wallet from a user supplied recovery phrase
			if DependencyManager.shared.accountService.importWallet(mnemonic: recoveryPhrase), DependencyManager.shared.accountService.setOnboardingtatus(status: .recoveryPhraseComplete) {
				if !Thread.current.isRunningXCTest {
					SentrySDK.capture(message: "Wallet Recovered")
				}
				
				self.performSegue(withIdentifier: "successSegue", sender: self)
				
			} else {
				self.alert(errorWithMessage: "error_cant_save_wallet".localized)
			}
			
		} else {
			
			// Create a new wallet with a new recovery phrase and cache it on disk
			if DependencyManager.shared.accountService.createWallet(), DependencyManager.shared.accountService.setOnboardingtatus(status: .walletCreationComplete) {
				if !Thread.current.isRunningXCTest {
					SentrySDK.capture(message: "Wallet Created")
				}
				
				self.performSegue(withIdentifier: "successSegue", sender: self)
				
			} else {
				self.alert(errorWithMessage: "error_cant_save_wallet".localized)
			}
		}
		*/
	}
}
