//
//  RecoveryPhraseDisplayViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 16/04/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import camlKit
import MagmaWalletKit
import Toast_Swift

class RecoveryPhraseDisplayViewController: UIViewController {
	
	@IBOutlet weak var headingTopConstraint: NSLayoutConstraint!
	@IBOutlet weak var containerStackView: UIStackView!
	@IBOutlet weak var leftColumnStackView: UIStackView!
	@IBOutlet weak var rightColumnStackView: UIStackView!
	@IBOutlet weak var derivationPathStackview: UIStackView!
	@IBOutlet weak var derivationPathLabel: ThemeLabel!
	@IBOutlet weak var messageLabel: ThemeLabel!
	@IBOutlet weak var continueButton: UIButton!
	@IBOutlet weak var timeoutWarningView: UIView!
	@IBOutlet weak var timeoutWarningLabel: ThemeLabel!
	
	private let screenTimeout = 30 // seconds
	private var timeoutTask: DispatchWorkItem? = nil
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.navigationController?.navigationItem.largeTitleDisplayMode = .never
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		if let wallet = WalletCacheService().fetchPrimaryWallet() {
			self.clearStackViews()
			self.setupStackViews(withMnemonic: wallet.mnemonic)
			
			if let hdwallet = wallet as? HDWallet {
				derivationPathLabel.text = hdwallet.derivationPath
			} else {
				derivationPathStackview.isHidden = true
			}
		}
		
		
		// If there is a SettingsViewController in the nav stack, then we are re-showing this screen from settings
		// in this case we hide the continue button and the checkbox
		if let containsSettings = self.navigationController?.containsSettingViewController(), containsSettings {
			headingTopConstraint.constant = 16
			messageLabel.text = "set_recovery_message".localized
			timeoutWarningLabel.text = "set_recovery_timeout".localized("\(screenTimeout)")
			continueButton.isHidden = true
			timeoutTask = DispatchWorkItem(block: {
				self.navigationController?.popToSettings()
			})
			
			if let task = timeoutTask {
				DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(screenTimeout), execute: task)
			}
			
		} else {
			timeoutWarningView.isHidden = true
			derivationPathStackview.isHidden = true
		}
	}
	
	override func viewDidDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		sanitiseLabels()
		
		timeoutTask?.cancel()
	}
	
	func clearStackViews() {
		if self.leftColumnStackView.arrangedSubviews.count == 1 {
			let leftPlaceHolderView = self.leftColumnStackView.arrangedSubviews[0]
			let rightPlaceHolderView = self.rightColumnStackView.arrangedSubviews[0]
			
			self.leftColumnStackView.removeArrangedSubview(leftPlaceHolderView)
			leftPlaceHolderView.removeConstraints(leftPlaceHolderView.constraints)
			leftPlaceHolderView.removeFromSuperview()
			
			self.rightColumnStackView.removeArrangedSubview(rightPlaceHolderView)
			rightPlaceHolderView.removeConstraints(rightPlaceHolderView.constraints)
			rightPlaceHolderView.removeFromSuperview()
		}
	}
	
	func setupStackViews(withMnemonic mnemonic: String) {
		let words = mnemonic.components(separatedBy: " ")
		
		if words.count <= 2 {
			return
		}
		
		let leftCount = Int(ceil(Double(words.count)/2))
		let leftWords = words[0..<leftCount]
		let rightwords = words[leftCount..<words.count]
		
		for (index, word) in leftWords.enumerated() {
			let label = UILabel()
			label.attributedText = customAttributedString(forIndex: index+1, andWord: word)
			label.textColor = UIColor.white
			label.accessibilityIdentifier = "mnemonic-\(index+1)"
			
			leftColumnStackView.addArrangedSubview(label)
		}
		
		for (index, word) in rightwords.enumerated() {
			let label = UILabel()
			label.attributedText = customAttributedString(forIndex: index+1+leftCount, andWord: word)
			label.textColor = UIColor.white
			label.accessibilityIdentifier = "mnemonic-\(index+1+leftCount)"
			
			rightColumnStackView.addArrangedSubview(label)
		}
	}
	
	func sanitiseLabels() {
		for leftLabel in leftColumnStackView.arrangedSubviews {
			if let lbl = leftLabel as? UILabel {
				lbl.text = ""
			}
			
			leftColumnStackView.removeArrangedSubview(leftLabel)
		}
		
		for rightLabel in rightColumnStackView.arrangedSubviews {
			if let lbl = rightLabel as? UILabel {
				lbl.text = ""
			}
			
			rightColumnStackView.removeArrangedSubview(rightLabel)
		}
	}
	
	func customAttributedString(forIndex index: Int, andWord word: String) -> NSAttributedString {
		let numberFontAttribute = [NSAttributedString.Key.font: ThemeLabel.seedPhraseNumberFont]
		let wordFontAttribute = [NSAttributedString.Key.font: ThemeLabel.seedPhraseWordFont]
		
		let numberAttributedString = NSMutableAttributedString(string: "\(index)", attributes: numberFontAttribute)
		let wordAttributedString = NSMutableAttributedString(string: "  \(word)", attributes: wordFontAttribute)
		numberAttributedString.append(wordAttributedString)
		
		return numberAttributedString
	}
}
