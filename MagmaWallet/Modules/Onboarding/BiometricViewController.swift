//
//  BiometricViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 27/02/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import Lottie
import LocalAuthentication

class BiometricViewController: UIViewController {
	
	@IBOutlet weak var animationView: AnimationView!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var useBiometricButton: UIButton!
	
	private var isTouchID: Bool = false
	private var biometricString: String = ""
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		let animation = Animation.named("lottie-authid")
		animationView.animation = animation
		
		self.isTouchID = (DependencyManager.shared.accountService.availableBiometricType() == .touchID)
		self.biometricString = (self.isTouchID ? "onb_biometric_touchid".localized : "onb_biometric_faceid".localized)
		self.titleLabel.text = "onb_biometric_prompt".localized(self.biometricString)
		self.useBiometricButton.setTitle("action_yes_use_biometric".localized(self.biometricString), for: .normal)
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		animationView.play()
	}
	
	@IBAction func useBiometricTapped(_ sender: UIButton) {
		
		// TouchID doesn't need prior permission as the user has to put their finger on the button to scan it.
		// FaceID needs to request access to use, as the system scans their face automatically
		if DependencyManager.shared.accountService.availableBiometricType() == .touchID {
			DependencyManager.shared.accountService.setBiometricChoice(type: .touchID)
			navigate()
		} else {
			triggerFaceId()
		}
	}
	
	@IBAction func maybeLaterTapped(_ sender: UIButton) {
		DependencyManager.shared.accountService.setBiometricChoice(type: .none)
		navigate()
	}
	
	func navigate() {
		if DependencyManager.shared.accountService.setOnboardingtatus(status: .biometricChoiceComplete) {
			self.performSegue(withIdentifier: "confirmationSegue", sender: self)
		} else {
			self.alert(errorWithMessage: "error_cant_save_wallet".localized)
		}
	}
	
	func triggerFaceId() {
		let myContext = LAContext()
		var authError: NSError?
		
		if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
			appDelegate.launchingSystemPopup = true
		}
		
		if myContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
			myContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: "lgn_touchid_description".localized) { [weak self] success, evaluateError in
				DispatchQueue.main.async {
					if success {
						DependencyManager.shared.accountService.setBiometricChoice(type: .faceID)
						self?.navigate()
					}
				}
			}
		} else {
			alert(errorWithMessage: "onb_faceid_disabled".localized)
		}
	}
}

