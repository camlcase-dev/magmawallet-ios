//
//  AlreadyPairedViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 11/06/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit

class AlreadyPairedViewController: UIViewController {
	
	var tappedCreateWallet = true
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.navigationController?.setNavigationBarHidden(true, animated: false)
		self.navigationItem.hidesBackButton = true
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		self.navigationController?.setNavigationBarHidden(false, animated: false)
		self.navigationItem.hidesBackButton = false
		self.navigationItem.largeTitleDisplayMode = .never
	}
	
	@IBAction func continueButtonTapped(_ sender: Any) {
		
		self.alert(
			withTitle: "onb_already_paired_popup_title".localized,
			andMessage: "onb_already_paired_popup_detail".localized,
			okText: "onb_already_paired_continue".localized,
			okAction: { [weak self] (action) in
				
				let segueIdentifier = (self?.tappedCreateWallet ?? true) ? "continueCreate" : "continueImport"
				self?.performSegue(withIdentifier: segueIdentifier, sender: self)
				
			}, cancelAction: { [weak self] (action) in
				self?.navigationController?.popToWelcome()
			}
		)
	}
	
	@IBAction func returnButtonTapped(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
}
