//
//  WalletTypeChoiceViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 19/03/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import UIKit

class WalletTypeChoiceViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
	@IBAction func walletWithOutPassphrasedTapped(_ sender: Any) {
		
		if !DependencyManager.shared.accountService.createWallet(withPassphrase: "") {
			self.alert(errorWithMessage: "error_generic".localized)
			
		} else {
			self.performSegue(withIdentifier: "continueSegue", sender: self)
		}
	}
}
