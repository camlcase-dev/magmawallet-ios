//
//  ImportBalanceCell.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 12/03/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import UIKit
import MagmaWalletKit
import os.log

struct ImportBalanceCellModel: UITableViewCellModel {
	
	static let staticIdentifier = "importBalanceCell"
	let cellId: String = ImportBalanceCellModel.staticIdentifier
	let estimatedRowHeight: CGFloat = 95
	
	let address: String
	let balance: String
	let otherTokens: Bool
}

class ImportBalanceCell: UITableViewCell, UITableViewCellMVVM {
	
	@IBOutlet weak var addressLabel: ThemeLabel!
	@IBOutlet weak var balanceTitleLabel: ThemeLabel!
	@IBOutlet weak var balanceValueLabel: ThemeLabel!
	@IBOutlet weak var otherTokensTitleLabel: ThemeLabel!
	@IBOutlet weak var otherTokenValueLabel: ThemeLabel!
	
	func setup(withModel model: UITableViewCellModel) {
		guard let cellModel = model as? ImportBalanceCellModel else {
			os_log(.error, log: .mvvm, "Cell model passed in to 'ImportBalanceCell' did not match required type 'ImportBalanceCellModel', instead found '%@'", String(describing: model.self))
			return
		}
		
		if cellModel.balance != "" {
			addressLabel.text = cellModel.address
			balanceTitleLabel.text = "onb_import_balance_subtitle".localized("")
			balanceValueLabel.text = cellModel.balance
			otherTokensTitleLabel.text = "onb_import_balance_other_tokens".localized("")
			otherTokenValueLabel.text = cellModel.otherTokens ? "onb_import_balance_other_tokens_yes".localized : "onb_import_balance_other_tokens_no".localized
			
		} else {
			addressLabel.text = cellModel.address
			balanceTitleLabel.text = ""
			balanceValueLabel.text = ""
			otherTokensTitleLabel.text = ""
			otherTokenValueLabel.text = ""
		}
	}
}
