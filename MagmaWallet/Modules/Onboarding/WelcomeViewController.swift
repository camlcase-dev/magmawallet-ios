//
//  WelcomeViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 20/04/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import MagmaWalletKit

class WelcomeViewController: UIViewController {
	
	@IBOutlet weak var loginButton: UIButton?
	@IBOutlet weak var createButton: ThemeButton!
	@IBOutlet weak var importButton: ThemeButton!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Users are only allowed to Login if there are account details on the device.
		// Login button is essentially a fallback, for allowing users to cancel a login and get back to it, incase of a forgotten PIN etc.
		if DependencyManager.shared.accountService.currentAddress() != nil && DependencyManager.shared.accountService.hasPinCodeSet() && DependencyManager.shared.accountService.getOnboardingStatus() == .none {
			loginButton?.isHidden = false
		}
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.navigationController?.setNavigationBarHidden(true, animated: false)
		self.navigationItem.hidesBackButton = true
		
		DependencyManager.shared.transactionService.resetStoredData()
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		self.navigationController?.setNavigationBarHidden(false, animated: false)
		self.navigationItem.hidesBackButton = false
		self.navigationItem.largeTitleDisplayMode = .never
	}
	
	@IBAction func createWalletButtonTapped(_ sender: Any) {
		DependencyManager.shared.transactionService.walletCreationData.type = .new
		
		if loginButton?.isHidden == false {
			performSegue(withIdentifier: "alreadyPaired", sender: sender)
			
		} else {
			performSegue(withIdentifier: "createWallet", sender: sender)
		}
	}
	
	@IBAction func importWalletButtonTapped(_ sender: Any) {
		DependencyManager.shared.transactionService.walletCreationData.type = .import
		
		if loginButton?.isHidden == false {
			performSegue(withIdentifier: "alreadyPaired", sender: sender)
			
		} else {
			performSegue(withIdentifier: "importWallet", sender: sender)
		}
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "alreadyPaired", let destination = segue.destination as? AlreadyPairedViewController {
			guard let buttonTapped = sender as? ThemeButton else {
				return
			}
			
			destination.tappedCreateWallet = (buttonTapped == createButton)
		}
	}
}
