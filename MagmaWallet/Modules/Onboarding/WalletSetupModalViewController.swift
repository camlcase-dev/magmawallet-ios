//
//  WalletSetupModalViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 10/03/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import UIKit

class WalletSetupModalViewController: UIViewController {

	@IBOutlet weak var containerView: UIView!
	
	override func viewDidLoad() {
        super.viewDidLoad()
    }
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		
		containerView.roundCorners(corners: [.topLeft, .topRight], radius: 4)
	}
	
	@IBAction func articleButtonTapped(_ sender: Any) {
		if let url = URL(string: "https://blog.trezor.io/5-reasons-why-you-should-use-a-passphrase-and-3-reasons-why-you-maybe-shouldnt-411c3935ac81") {
			UIApplication.shared.open(url, options: [:], completionHandler: nil)
		}
	}
}
