//
//  File.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 16/01/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import Lottie

class OnboardingSuccessViewController: UIViewController {
	
	@IBOutlet weak var animationView: AnimationView?
	
	@IBInspectable var isFinal: Bool = false
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		if let aView = animationView {
			let animation = Animation.named("lottie-success")
			aView.animation = animation
		}
		
		
		// This screen is displayed in 3 places,
		// 1. At the very end of the onboarding flow, where we record the onboarding as finished
		// 2. While creating a new wallet, it will be display before the pincode, and after the recovery phrase
		// 3. After an import, before the pin. Here we also record `.recoveryPhraseComplete` because the launchViewController already uses that to direct to the pin code controller
		
		if isFinal {
			self.navigationController?.setNavigationBarHidden(true, animated: false)
			self.navigationItem.hidesBackButton = true
			
			if !DependencyManager.shared.accountService.setOnboardingtatus(status: .onboardingComplete) {
				self.alert(errorWithMessage: "error_cant_save_wallet".localized)
			}
		} else {
			if !DependencyManager.shared.accountService.setOnboardingtatus(status: .recoveryPhraseComplete) {
				self.alert(errorWithMessage: "error_cant_save_wallet".localized)
			}
		}
		
		DependencyManager.shared.transactionService.resetStoredData()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		if let aView = animationView {
			aView.play()
		}
	}
}
