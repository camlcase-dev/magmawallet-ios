//
//  SetPassphraseViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 10/03/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import UIKit
import MagmaWalletKit

class SetPassphraseViewController: UIViewController {
	
	@IBOutlet weak var textfield: ThemeTextfield!
	@IBOutlet weak var continueButton: ThemeButton!
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		textfield.validator = LengthValidator(maxLength: 50, minLength: 0)
    }
	
	@IBAction func continueButtonTapped(_ sender: Any) {
		
		if !DependencyManager.shared.accountService.createWallet(withPassphrase: textfield.text ?? "") {
			self.alert(errorWithMessage: "error_generic".localized)
			
		} else {
			self.performSegue(withIdentifier: "continueSegue", sender: self)
		}
	}
}
