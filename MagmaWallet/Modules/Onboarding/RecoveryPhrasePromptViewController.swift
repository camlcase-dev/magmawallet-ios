//
//  RecoveryPhrasePromptViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 16/04/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit

class RecoveryPhrasePromptViewController: UIViewController {
	
	var result = false
	
	override func viewDidLoad() {
		super.viewDidLoad()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		let _ = DependencyManager.shared.accountService.setOnboardingtatus(status: .walletCreationComplete)
	}
}
