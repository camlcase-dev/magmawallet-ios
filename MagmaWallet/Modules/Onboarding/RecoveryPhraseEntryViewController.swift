//
//  RecoveryPhraseEntryViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 16/04/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import camlKit
import WalletCore
import MagmaWalletKit

class RecoveryPhraseEntryViewController: UIViewController {
	
	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var textview: UITextView!
	@IBOutlet weak var textviewErrorView: UIView!
	@IBOutlet weak var passphraseTextfield: ThemeTextfield!
	@IBOutlet weak var advancedButton: UIButton!
	@IBOutlet weak var advancedOptionsStackview: UIStackView!
	@IBOutlet weak var derivationPathTextfield: ThemeTextfield!
	@IBOutlet weak var derivationErrorStackView: UIStackView!
	@IBOutlet weak var confirmButton: ThemeButton!
	
	private var textviewValid = false
	private var derivationPathValid = true
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		textviewErrorView.isHidden = true
		derivationErrorStackView.isHidden = true
		advancedOptionsStackview.isHidden = true
		
		textview.delegate = self
		
		passphraseTextfield.returnKeyType = .done
		derivationPathTextfield.returnKeyType = .done
		derivationPathTextfield.validator = DerivationPathValidator()
		derivationPathTextfield.themeTextfieldValidationDelegate = self
		derivationPathTextfield.themeTextfieldDelegate = self
		
		let uneditableAttributes = [
			NSAttributedString.Key.font: UIFont.barlowFont(.regular, withSize: 18),
			NSAttributedString.Key.foregroundColor: UIColor(red: 255.0 / 255.0, green: 255.0 / 255.0, blue: 255.0 / 255.0, alpha: 0.5)
		]
		let uneditableString = NSMutableAttributedString(string: "m/44'/1729'", attributes: uneditableAttributes)
		
		let editableAttributes = [
			NSAttributedString.Key.font: UIFont.barlowFont(.regular, withSize: 18),
			NSAttributedString.Key.foregroundColor: UIColor(red: 255.0 / 255.0, green: 255.0 / 255.0, blue: 255.0 / 255.0, alpha: 1)
		]
		let editableString = NSMutableAttributedString(string: "/0'/0'", attributes: editableAttributes)
		uneditableString.append(editableString)
		
		derivationPathTextfield.attributedText = uneditableString
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		NotificationCenter.default.addObserver(self, selector: #selector(customKeyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(customKeyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
		NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
	}
	
	@IBAction func advancedOptionsTapped(_ sender: Any) {
		let imageName = advancedOptionsStackview.isHidden ? "caret-down" : "caret-right"
		advancedButton.setImage(UIImage(named: imageName), for: .normal)
		advancedOptionsStackview.isHidden = !advancedOptionsStackview.isHidden
		
		// Make sure the derivation path field doesn't scroll off the end when displayed
		let derivationPathFieldRelative = derivationPathTextfield.convert(derivationPathTextfield.frame, to: UIApplication.shared.keyWindow)
		let derivationPathBottom = derivationPathFieldRelative.origin.y + derivationPathFieldRelative.size.height + 24
		let scrollViewBottom = scrollView.frame.origin.y + scrollView.frame.height
		
		
		if !advancedOptionsStackview.isHidden {
			if scrollViewBottom < derivationPathBottom {
				scrollView.setContentOffset(CGPoint(x: 0, y: (derivationPathBottom - scrollViewBottom)), animated: true)
			}
			
		} else {
			scrollView.contentOffset = CGPoint(x: 0, y: 0)
		}
	}
	
	@objc func customKeyboardWillShow(notification: NSNotification) {
		if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
			let scrollViewDistancefromBottom = UIScreen.main.bounds.height - (scrollView.frame.origin.y + scrollView.frame.height)
			var contentInset: UIEdgeInsets = self.scrollView.contentInset
			contentInset.bottom = (keyboardSize.height - scrollViewDistancefromBottom) + 8
			
			scrollView.contentInset = contentInset
		}
	}
	
	@objc func customKeyboardWillHide(notification: NSNotification) {
		var contentInset:UIEdgeInsets = self.scrollView.contentInset
		contentInset.bottom = 0
		
		scrollView.contentInset = contentInset
	}
	
	func updateValidationUI(valid: Bool) {
		if valid {
			textview.borderWidth = 0
			textview.borderColor = UIColor.clear
			textviewErrorView.isHidden = true
		} else {
			textview.borderWidth = 2
			textview.borderColor = UIColor.white
			textviewErrorView.isHidden = false
		}
	}
	
	func updateConfirmButton() {
		if textviewValid && derivationPathValid {
			confirmButton.isEnabled = true
		} else {
			confirmButton.isEnabled = false
		}
	}
	
	@IBAction func confirmButtonTapped(_ sender: Any) {
		
		// Confirm the words entered match the standard
		if !WalletUtils.isMnemonicValid(mnemonic: textview.text) {
			updateValidationUI(valid: false)
			return
		}
		
		DependencyManager.shared.transactionService.walletCreationData.mnemonic = stripSymbols(text: textview.text)
		DependencyManager.shared.transactionService.walletCreationData.passphrase = passphraseTextfield.text ?? ""
		DependencyManager.shared.transactionService.walletCreationData.derivationPath = derivationPathTextfield.text ?? HDWallet.defaultDerivationPath
		
		let walletData = DependencyManager.shared.transactionService.walletCreationData
		
		if derivationPathTextfield.text != HDWallet.defaultDerivationPath,
		   DependencyManager.shared.accountService.importWallet(mnemonic: walletData.mnemonic, passpharse: walletData.passphrase, type: .hd, derivationPath: walletData.derivationPath) {
			self.performSegue(withIdentifier: "successSegue", sender: self)
			
		} else if createBothWalletDetails() {
			self.performSegue(withIdentifier: "checkBalanceSegue", sender: self)
			
		} else {
			self.alert(errorWithMessage: "error_cant_save_wallet".localized)
		}
	}
	
	func createBothWalletDetails() -> Bool {
		let mnemonic = DependencyManager.shared.transactionService.walletCreationData.mnemonic
		let passphrase = DependencyManager.shared.transactionService.walletCreationData.passphrase
		
		let linearWallet = LinearWallet.create(withMnemonic: mnemonic, passphrase: passphrase)
		let hdWallet = HDWallet.create(withMnemonic: mnemonic, passphrase: passphrase)
		
		if let lWallet = linearWallet, let hWallet = hdWallet {
			
			DependencyManager.shared.transactionService.walletCreationData.importlinearWalletAddress = lWallet.address
			DependencyManager.shared.transactionService.walletCreationData.importHdWalletAddress = hWallet.address
			
			return true
			
		} else {
			return false
		}
	}
}

extension RecoveryPhraseEntryViewController: UITextViewDelegate {
	
	func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
		if (text as NSString).rangeOfCharacter(from: CharacterSet.newlines).location == NSNotFound {
			if let textViewString = textView.text, let swtRange = Range(range, in: textViewString) {
				var fullString = textViewString.replacingCharacters(in: swtRange, with: text)
				fullString = stripSymbols(text: fullString)
				
				if validateRecoveryPhrase(text: fullString) {
					updateValidationUI(valid: true)
					textviewValid = true
				} else {
					updateValidationUI(valid: false)
					textviewValid = false
				}
				
				updateConfirmButton()
			}
			
			return true
		}
		
		textView.resignFirstResponder()
		return false
	}
	
	func stripSymbols(text: String) -> String {
		var newString = text.replacingOccurrences(of: ",", with: "")
		newString = newString.replacingOccurrences(of: ".", with: "")
		newString = newString.replacingOccurrences(of: "\n", with: "")
		
		return newString
	}
	
	func validateRecoveryPhrase(text: String) -> Bool {
		let set = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ ")
		
		// Empty input
		if text.trimmingCharacters(in: .whitespaces).isEmpty {
			return false
		}
		
		// Check for only words and sapces
		if text.rangeOfCharacter(from: set.inverted) != nil {
			return false
		}
		
		// If only words and spaces, and at least 12 words pass
		if text.components(separatedBy: " ").count >= 12 {
			return true
		}
		
		return false
	}
}

extension RecoveryPhraseEntryViewController: ThemeTextfieldValidationProtocol, ThemeTextfieldProtocol {
	
	func rightButtonTapped(_ textfield: ThemeTextfield) {
		
	}
	
	func didClear() {
		
	}
	
	func didTapDone() {
		
	}
	
	func didBeingEditing(_ textField: UITextField) {
		
	}
	
	func didEndEditing(_ textField: UITextField) {
		
	}
	
	
	func validated(_ validated: Bool, textfield: ThemeTextfield, forText text: String) {
		derivationPathValid = validated
		
		if !derivationPathValid {
			derivationErrorStackView.isHidden = false
		} else {
			derivationErrorStackView.isHidden = true
		}
		
		updateConfirmButton()
	}
}
