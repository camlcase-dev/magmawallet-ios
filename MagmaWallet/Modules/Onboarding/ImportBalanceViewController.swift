//
//  ImportBalanceViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 12/03/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import UIKit

class ImportBalanceViewController: UIViewController {
	
	@IBOutlet weak var tableView: UITableView!
	
	private let viewModel = ImportBalanceViewModel()
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		self.tableView.delegate = self
		self.tableView.register(UINib(nibName: LoadingCellModel.staticIdentifier, bundle: nil), forCellReuseIdentifier: LoadingCellModel.staticIdentifier)
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		tableView.tableViewData = viewModel.loadingCell()
		tableView.reloadData()
		
		viewModel.update { [weak self] (response) in
			self?.tableView.tableViewData = self?.viewModel.tableViewData
			self?.tableView.reloadData()
		}
	}
}

extension ImportBalanceViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 30
	}
	
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		let containerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 30))
		let sectionHeaderLabel = UILabel(frame: CGRect(x: 24, y: 0, width: tableView.frame.width, height: 20))
		sectionHeaderLabel.text = self.viewModel.tableViewData[section].header
		sectionHeaderLabel.textColor = UIColor(named: "SemiTransparentWhiteText")
		sectionHeaderLabel.font = UIFont.barlowFont(.semiBold, withSize: 13)
		
		containerView.addSubview(sectionHeaderLabel)
		
		return containerView
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		
		let walletData = DependencyManager.shared.transactionService.walletCreationData
		var result = false
		
		if indexPath.section == 0 {
			// Tapped the HD cell
			result = DependencyManager.shared.accountService.importWallet(mnemonic: walletData.mnemonic, passpharse: walletData.passphrase, type: .hd, derivationPath: walletData.derivationPath)
			
		} else {
			// Tapped the Linear cell
			result = DependencyManager.shared.accountService.importWallet(mnemonic: walletData.mnemonic, passpharse: walletData.passphrase, type: .linear, derivationPath: nil)
		}
		
		if result {
			self.performSegue(withIdentifier: "successSegue", sender: self)
		} else {
			self.alert(errorWithMessage: "error_cant_save_wallet".localized)
		}
	}
}
