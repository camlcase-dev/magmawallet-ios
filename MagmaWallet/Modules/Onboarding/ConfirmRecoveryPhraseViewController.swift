//
//  ConfirmRecoveryPhraseViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 10/03/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import UIKit
import MagmaWalletKit
import camlKit

class ConfirmRecoveryPhraseViewController: UIViewController {
	
	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var textview: UITextView!
	@IBOutlet weak var textviewErrorView: UIView!
	@IBOutlet weak var textfieldLabel: ThemeLabel!
	@IBOutlet weak var textfield: ThemeTextfield!
	@IBOutlet weak var continueButton: ThemeButton!
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		textview.delegate = self
		textviewErrorView.isHidden = true
		
		textfield.validator = LengthValidator(maxLength: 50, minLength: 0)
		
		let storedWallets = WalletCacheService().readFromDiskAndDecrypt()
		if let firstRecord = storedWallets?.first, (firstRecord.passphrase == nil || firstRecord.passphrase == "") {
			textfieldLabel.isHidden = true
			textfield.isHidden = true
		}
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		NotificationCenter.default.addObserver(self, selector: #selector(customKeyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(customKeyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
		NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
	}
	
	@objc func customKeyboardWillShow(notification: NSNotification) {
		if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
			let scrollViewDistancefromBottom = UIScreen.main.bounds.height - (scrollView.frame.origin.y + scrollView.frame.height)
			var contentInset: UIEdgeInsets = self.scrollView.contentInset
			contentInset.bottom = (keyboardSize.height - scrollViewDistancefromBottom) + 8
			
			scrollView.contentInset = contentInset
		}
	}
	
	@objc func customKeyboardWillHide(notification: NSNotification) {
		var contentInset:UIEdgeInsets = self.scrollView.contentInset
		contentInset.bottom = 0
		
		scrollView.contentInset = contentInset
	}
	
	@IBAction func continueTapped(_ sender: Any) {
		
		let storedWallets = WalletCacheService().readFromDiskAndDecrypt()
		guard let firstRecord = storedWallets?.first else {
			self.alert(errorWithMessage: "onb_recovery_phrase_backup_error".localized)
			return
		}
		
		if textview.text == firstRecord.mnemonic, (textfield.text ?? "") == firstRecord.passphrase {
			self.performSegue(withIdentifier: "toPinSegue", sender: self)
			
		} else {
			self.alert(errorWithMessage: "onb_recovery_phrase_backup_error".localized)
		}
	}
	
	@IBAction func skipTapped(_ sender: Any) {
		self.performSegue(withIdentifier: "toPinSegue", sender: self)
	}
	
	func updateValidationUI(valid: Bool) {
		if valid {
			textview.borderWidth = 0
			textview.borderColor = UIColor.clear
			textviewErrorView.isHidden = true
		} else {
			textview.borderWidth = 2
			textview.borderColor = UIColor.white
			textviewErrorView.isHidden = false
		}
	}
}

extension ConfirmRecoveryPhraseViewController: UITextViewDelegate {
	
	func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
		if (text as NSString).rangeOfCharacter(from: CharacterSet.newlines).location == NSNotFound {
			if let textViewString = textView.text, let swtRange = Range(range, in: textViewString) {
				var fullString = textViewString.replacingCharacters(in: swtRange, with: text)
				fullString = stripSymbols(text: fullString)
				
				if validateRecoveryPhrase(text: fullString) {
					updateValidationUI(valid: true)
					continueButton.isEnabled = true
				} else {
					updateValidationUI(valid: false)
					continueButton.isEnabled = false
				}
			}
			
			return true
		}
		
		textView.resignFirstResponder()
		return false
	}
	
	func stripSymbols(text: String) -> String {
		var newString = text.replacingOccurrences(of: ",", with: "")
		newString = newString.replacingOccurrences(of: ".", with: "")
		newString = newString.replacingOccurrences(of: "\n", with: "")
		
		return newString
	}
	
	func validateRecoveryPhrase(text: String) -> Bool {
		let set = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ ")
		
		// Empty input
		if text.trimmingCharacters(in: .whitespaces).isEmpty {
			return false
		}
		
		// Check for only words and sapces
		if text.rangeOfCharacter(from: set.inverted) != nil {
			return false
		}
		
		// If only words and spaces, and at least 12 words pass
		if text.components(separatedBy: " ").count >= 12 {
			return true
		}
		
		return false
	}
}
