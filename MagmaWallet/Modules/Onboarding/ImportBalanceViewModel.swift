//
//  ImportBalanceViewModel.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 12/03/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import UIKit
import os.log

class ImportBalanceViewModel: ViewModel {
	
	var tableViewData: [UITableViewSectionModel] = []
	
	func update(withInput input: [String : Any]? = [:], completion: @escaping (ViewModelResponse) -> Void) {
		
		let linearAddress = DependencyManager.shared.transactionService.walletCreationData.importlinearWalletAddress
		DependencyManager.shared.betterCallDevClient.accountAndTokenBalances(forAddress: linearAddress) { (linearResult) in
			
			if case .failure(let error) = linearResult {
				os_log("Error fetching balances for import account: %@", log: .tezos, type: .error, "\(error)")
				completion(ViewModelResponse(success: false, responseCode: nil, userDisplayString: nil, errorResponse: error))
				return
			}
			
			let hdAddress = DependencyManager.shared.transactionService.walletCreationData.importHdWalletAddress
			DependencyManager.shared.betterCallDevClient.accountAndTokenBalances(forAddress: hdAddress) { [weak self] (hdResult) in
				
				if case .success(let linearDetails) = linearResult, case .success(let hdDetails) = hdResult {
					
					let hdBalance = DependencyManager.shared.tokenPriceService.format(
						decimal: hdDetails.account.balance.toNormalisedDecimal() ?? 0,
						numberStyle: .decimal,
						maximumFractionDigits: hdDetails.account.balance.decimalPlaces
					)
					let linearBalance = DependencyManager.shared.tokenPriceService.format(
						decimal: linearDetails.account.balance.toNormalisedDecimal() ?? 0,
						numberStyle: .decimal,
						maximumFractionDigits: linearDetails.account.balance.decimalPlaces
					)
					
					self?.tableViewData = [
						UITableViewSectionModel(withHeader: "onb_import_balance_hdwallet".localized, footer: nil, andCellModels: [
							ImportBalanceCellModel(address: hdDetails.account.address, balance: hdBalance + " XTZ", otherTokens: hdDetails.tokens.balances.count > 0)
						]),
						UITableViewSectionModel(withHeader: "onb_import_balance_non_hdwallet".localized, footer: nil, andCellModels: [
							ImportBalanceCellModel(address: linearDetails.account.address, balance: linearBalance + " XTZ", otherTokens: linearDetails.tokens.balances.count > 0)
						])
					]
					
					completion(ViewModelResponse(success: true, responseCode: nil, userDisplayString: nil, errorResponse: nil))
					
				} else {
					os_log("Error fetching balances for import accounts", log: .tezos, type: .error)
					
					// If we can get balances and tokens, just display the addresses
					self?.tableViewData = [
						UITableViewSectionModel(withHeader: "onb_import_balance_hdwallet".localized, footer: nil, andCellModels: [
							ImportBalanceCellModel(address: hdAddress, balance: "", otherTokens: false)
						]),
						UITableViewSectionModel(withHeader: "onb_import_balance_non_hdwallet".localized, footer: nil, andCellModels: [
							ImportBalanceCellModel(address: linearAddress, balance: "", otherTokens: false)
						])
					]
					
					completion(ViewModelResponse(success: true, responseCode: nil, userDisplayString: nil, errorResponse: nil))
				}
				
			}
		}
	}
	
	func loadingCell() -> [UITableViewSectionModel] {
		self.tableViewData = [UITableViewSectionModel(withHeader: nil, footer: nil, andCellModels: [LoadingCellModel(estimatedRowHeight: (UIScreen.main.bounds.height - 300), loadingMessage: "")] )]
		
		return self.tableViewData
	}
	
	func errorCell() -> [UITableViewSectionModel] {
		self.tableViewData = [UITableViewSectionModel(withHeader: nil, footer: nil, andCellModels: [ErrorCellModel(estimatedRowHeight: (UIScreen.main.bounds.height - 300), errorMessage: "error_wallet_balance".localized)] )]
		
		return self.tableViewData
	}
}
