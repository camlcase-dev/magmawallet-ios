//
//  PinCodeViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 25/03/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import MagmaWalletKit

class PinCodeViewController: UIViewController {
	
	@IBInspectable var isConfirmPin: Bool = false
	
	@IBOutlet weak var backButton: UIButton?
	@IBOutlet weak var textField: ThemeTextfield?
	@IBOutlet weak var errorMessageStackView: UIStackView?
	@IBOutlet weak var continueButton: UIButton?
	@IBOutlet weak var continueButtonBottomConstraint: NSLayoutConstraint!
	
	private var cameFromSettings: Bool = false
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		if let containsSettings = self.navigationController?.containsSettingViewController(), containsSettings {
			cameFromSettings = true
			backButton?.isHidden = true
		}
		
		errorMessageStackView?.isHidden = true
		
		textField?.validator = LengthValidator(maxLength: 6, minLength: 4)
		textField?.themeTextfieldValidationDelegate = self
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		// Can't use the current UIViewController extension keyboard code.
		// Button moves up initially, but snaps back to bottom on viewDidAppear, needed to tweak it for this screen
		NotificationCenter.default.addObserver(self, selector: #selector(customKeyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		textField?.becomeFirstResponder()
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
	}
	
	@IBAction func continueButtonTapped(_ sender: UIButton) {
		if !isConfirmPin {
			DependencyManager.shared.accountService.setPinCode(code: textField?.text ?? "", temp: true)
			self.performSegue(withIdentifier: "confirmSegue", sender: self)
			
		} else {
			if DependencyManager.shared.accountService.validatePinCode(code: textField?.text ?? "", temp: true) {
				DependencyManager.shared.accountService.setPinCode(code: textField?.text ?? "", temp: false)
				navigateToNextScreen()
				
			} else {
				errorMessageStackView?.isHidden = false
			}
		}
	}
	
	func navigateToNextScreen() {
		
		// If we ultaimtely come from the settings screen, we are editing pin. Need to return instead of going to biometric or home
		if cameFromSettings {
			self.navigationController?.popToSettings()
		} else {
			if DependencyManager.shared.accountService.setOnboardingtatus(status: .pinCodeCreationComplete) {
				if DependencyManager.shared.accountService.isBiometricCapable() {
					self.performSegue(withIdentifier: "biometricSegue", sender: self)
					
				} else {
					DependencyManager.shared.accountService.setBiometricChoice(type: .none)
					self.performSegue(withIdentifier: "confirmationSegue", sender: self)
				}
			} else {
				self.alert(errorWithMessage: "error_cant_save_wallet".localized)
			}
		}
	}
	
	@objc func customKeyboardWillShow(notification: NSNotification) {
		if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
			continueButtonBottomConstraint.constant = keyboardSize.height
		}
	}
}

extension PinCodeViewController: ThemeTextfieldValidationProtocol {
	
	func validated(_ validated: Bool, textfield: ThemeTextfield, forText text: String) {
		
		if isConfirmPin {
			errorMessageStackView?.isHidden = DependencyManager.shared.accountService.validateInProgressPin(code: text)
			continueButton?.isEnabled = validated && errorMessageStackView?.isHidden == true
			
		} else {
			continueButton?.isEnabled = validated
		}
	}
}
