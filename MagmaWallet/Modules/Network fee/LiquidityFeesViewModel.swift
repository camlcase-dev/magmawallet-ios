//
//  LiquidityFeesViewModel.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 24/02/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import Foundation
import camlKit

class LiquidityFeeViewModel: ViewModel {
	
	var liquidityFeeAmount = ""
	var liquidityFeeCurrency = ""
	
	func update(withInput input: [String : Any]? = [:], completion: @escaping (ViewModelResponse) -> Void) {
		
		let liquidityFee = DependencyManager.shared.transactionService.exchangeData.conversion.liquidityFee
		let selectedTokenIndex = DependencyManager.shared.transactionService.exchangeData.selectedPrimaryTokenIndex
		let selectedToken = DependencyManager.shared.dexterTokens[selectedTokenIndex]
		let liquidityCurrencyPrice = DependencyManager.shared.dexterTokens[selectedTokenIndex].localCurrencyRate
		let tokenPriceService = DependencyManager.shared.tokenPriceService
		
		liquidityFeeAmount = liquidityFee.normalisedRepresentation + " \(selectedToken.symbol)"
		liquidityFeeCurrency = tokenPriceService.format(decimal: liquidityFee * liquidityCurrencyPrice, numberStyle: .currency)
		
		completion(ViewModelResponse(success: true, responseCode: nil, userDisplayString: nil, errorResponse: nil))
	}
}
