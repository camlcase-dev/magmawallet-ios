//
//  NetworkFeeModalViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 24/02/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import UIKit

class NetworkFeeModalViewController: UIViewController {
	
	@IBOutlet weak var containerView: UIView!
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		containerView.roundCorners(corners: [.topLeft, .topRight], radius: 12)
	}
}
