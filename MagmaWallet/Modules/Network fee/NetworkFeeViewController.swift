//
//  NetworkFeeViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 23/02/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import UIKit

class NetworkFeeViewController: UIViewController {
	
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var aboutFeesView: UIView!
	
	private let viewModel = NetworkFeeViewModel()
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		aboutFeesView.isUserInteractionEnabled = true
		aboutFeesView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openInfoModal)))
		
		viewModel.update { (response) in
			self.tableView.tableViewData = self.viewModel.tableViewData
			self.tableView.reloadData()
		}
	}
	
	@objc func openInfoModal() {
		self.performSegue(withIdentifier: "infoModalSegue", sender: self)
	}
}
