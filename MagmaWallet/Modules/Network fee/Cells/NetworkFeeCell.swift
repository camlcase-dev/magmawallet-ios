//
//  NetworkFeeCell.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 23/02/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import UIKit
import os.log

struct NetworkFeeCellModel: UITableViewCellModel {
	
	static let staticIdentifier = "NetworkFeeCell"
	let cellId: String = NetworkFeeCellModel.staticIdentifier
	let estimatedRowHeight: CGFloat = 83
	
	let title: String
	let amount: String
	let currency: String
}

class NetworkFeeCell: UITableViewCell, UITableViewCellMVVM {
	
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var amountLabel: UILabel!
	@IBOutlet weak var currencyLabel: UILabel!
	
	func setup(withModel model: UITableViewCellModel) {
		guard let cellModel = model as? NetworkFeeCellModel else {
			os_log(.error, log: .mvvm, "Cell model passed in to 'NetworkFeeCell' did not match required type 'NetworkFeeCellModel', instead found '%@'", String(describing: model.self))
			return
		}
		
		self.titleLabel.text = cellModel.title
		self.amountLabel.text = cellModel.amount
		self.currencyLabel.text = cellModel.currency
	}
}

