//
//  LiquidityFeesViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 24/02/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import UIKit
import MagmaWalletKit

class LiquidityFeesViewController: UIViewController {
	
	@IBOutlet weak var liquidityFeeAmountLabel: ThemeLabel!
	@IBOutlet weak var liquidityFeeCurrencyLabel: ThemeLabel!
	@IBOutlet weak var aboutFeesView: UIView!
	
	private let viewModel = LiquidityFeeViewModel()
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		aboutFeesView.isUserInteractionEnabled = true
		aboutFeesView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openInfoModal)))
		
		viewModel.update { [weak self] (response) in
			self?.liquidityFeeAmountLabel.text = self?.viewModel.liquidityFeeAmount
			self?.liquidityFeeCurrencyLabel.text = self?.viewModel.liquidityFeeCurrency
		}
	}
	
	@objc func openInfoModal() {
		self.performSegue(withIdentifier: "infoModalSegue", sender: self)
	}
}
