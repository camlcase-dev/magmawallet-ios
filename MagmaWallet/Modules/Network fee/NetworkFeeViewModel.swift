//
//  NetworkFeeViewModel.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 23/02/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import Foundation
import camlKit

class NetworkFeeViewModel: ViewModel {
	
	var tableViewData: [UITableViewSectionModel] = []
	
	func update(withInput input: [String : Any]? = [:], completion: @escaping (ViewModelResponse) -> Void) {
		
		let transactionService = DependencyManager.shared.transactionService
		var operations = transactionService.transactionType == .send ? transactionService.sendData.operations : transactionService.exchangeData.operations
		
		var bakerFee = XTZAmount.zero()
		var revealFee = XTZAmount.zero()
		var allocationFee = XTZAmount.zero()
		var burnFee = XTZAmount.zero()
		var total = XTZAmount.zero()
		
		
		// Special case for reveal fee
		if let firstOp = operations.first, firstOp.operationKind == .reveal {
			revealFee += firstOp.operationFees?.allNetworkFees() ?? XTZAmount.zero()
			bakerFee += firstOp.operationFees?.transactionFee ?? XTZAmount.zero()
			operations.remove(at: 0)
		}
		
		// Handle every other case
		operations.forEach { (op) in
			bakerFee += op.operationFees?.transactionFee ?? XTZAmount.zero()
			
			op.operationFees?.networkFees.forEach { (networkFee) in
				networkFee.keys.forEach { (key) in
					switch key {
						case .allocationFee:
							allocationFee += networkFee[key] ?? XTZAmount.zero()
						
						case .burnFee:
							burnFee += networkFee[key] ?? XTZAmount.zero()
					}
				}
			}
		}
		
		total = (bakerFee + revealFee + allocationFee + burnFee)
		
		
		// Take values and create dynamic cells
		let xtzPrice = DependencyManager.shared.balances[0].localCurrencyRate
		let tokenPriceService = DependencyManager.shared.tokenPriceService
		let bakerFeeCurrency = tokenPriceService.format(decimal: bakerFee * xtzPrice, numberStyle: .currency)
		
		var cellModels: [UITableViewCellModel] = [NetworkFeeCellModel(title: "fees_detail_baker".localized, amount: bakerFee.normalisedRepresentation + " XTZ", currency: bakerFeeCurrency)]
		
		if revealFee > XTZAmount.zero() {
			let revealCurrency = tokenPriceService.format(decimal: revealFee * xtzPrice, numberStyle: .currency)
			cellModels.append(NetworkFeeCellModel(title: "fees_detail_reveal".localized, amount: revealFee.normalisedRepresentation + " XTZ", currency: revealCurrency))
		}
		
		if allocationFee > XTZAmount.zero() {
			let allocationCurrency = tokenPriceService.format(decimal: allocationFee * xtzPrice, numberStyle: .currency)
			cellModels.append(NetworkFeeCellModel(title: "fees_detail_allocation".localized, amount: allocationFee.normalisedRepresentation + " XTZ", currency: allocationCurrency))
		}
		
		if burnFee > XTZAmount.zero() {
			let burnCurrency = tokenPriceService.format(decimal: burnFee * xtzPrice, numberStyle: .currency)
			cellModels.append(NetworkFeeCellModel(title: "fees_detail_burn".localized, amount: burnFee.normalisedRepresentation + " XTZ", currency: burnCurrency))
		}
		
		let totalCurrency = tokenPriceService.format(decimal: total * xtzPrice, numberStyle: .currency)
		cellModels.append(NetworkFeeCellModel(title: "fees_detail_total".localized, amount: total.normalisedRepresentation + " XTZ", currency: totalCurrency))
		tableViewData = [UITableViewSectionModel(withHeader: nil, footer: nil, andCellModels: cellModels)]
		
		completion(ViewModelResponse(success: true, responseCode: nil, userDisplayString: nil, errorResponse: nil))
	}
}
