//
//  LaunchViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 13/01/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import KeychainSwift
import camlKit
import MagmaWalletKit
import LocalAuthentication

class LaunchViewController: UIViewController {
	
	@IBOutlet weak var titleTextImageView: UIImageView!
	@IBOutlet weak var appIcon: UIImageView!
	@IBOutlet weak var appIconTopConstraint: NSLayoutConstraint!
	@IBOutlet weak var footerText: ThemeLabel!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Migrating from TezosKit to camlKit
		// Due to differences in how libraries store and fetch wallets, we need to check to see is there any wallet details stored in the keychain
		// If so, use it to create a camlKit wallet, store it on disk and then remove the keychain references
		if let tezosKitWalletMnemonic = DependencyManager.shared.keychain.get("io.camlcase.tezos.wallet.mnemonic"),
		   DependencyManager.shared.accountService.importWallet(mnemonic: tezosKitWalletMnemonic, passpharse: "", type: .linear, derivationPath: nil) {
			DependencyManager.shared.keychain.delete("io.camlcase.tezos.wallet.mnemonic")
		}
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.navigationController?.setNavigationBarHidden(true, animated: false)
		self.navigationItem.hidesBackButton = true
		
		if appIconTopConstraint.constant == 0 {
			appIcon.alpha = 0
			footerText.alpha = 0
		}
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		if appIconTopConstraint.constant == 0 {
			let titleTextImageBottom = titleTextImageView.frame.origin.y + titleTextImageView.frame.height
			let appIconDestinationTop = titleTextImageBottom + 8
			let appIconSourceTop = appIcon.frame.origin.y
			appIconTopConstraint.constant = (appIconSourceTop - appIconDestinationTop)
			
			UIView.animateKeyframes(withDuration: 0.75, delay: 0.0, options: UIView.KeyframeAnimationOptions(rawValue: (2 << 16)), animations: { [weak self] in
				self?.appIcon.alpha = 1
				self?.footerText.alpha = 1
				self?.view.layoutIfNeeded()
				
			}) { [weak self] (success) in
				self?.navigate()
			}
		} else {
			DispatchQueue.main.asyncAfter(deadline: .now() + 0.75) { [weak self] in
				self?.navigate()
			}
		}
	}
	
	func navigate() {
		// This does not control the entire onboarding flow. This is merely used to recover the last step should the app be killed in the background before everything is done.
		// The flow is controlled via segues in the Onboarding.storyboard ... as it should be
		var onboardingStatus = DependencyManager.shared.accountService.getOnboardingStatus()
		let biometricCapable = DependencyManager.shared.accountService.isBiometricCapable()
		let currentAddress = DependencyManager.shared.accountService.currentAddress()
		
		// Now that users deleting the app clears the wallet, check to see if a user is returning from deleting the app, and reset status
		if currentAddress == nil && onboardingStatus != .none {
			let _ = DependencyManager.shared.accountService.setOnboardingtatus(status: .none)
			onboardingStatus = .none
		}
		
		switch onboardingStatus {
			case .none:
				let transition = CATransition()
				transition.duration = 0.5
				transition.type = CATransitionType.fade
				transition.isRemovedOnCompletion = true
				self.navigationController?.view.layer.add(transition, forKey: nil)
				
				self.performSegue(withIdentifier: "onboarding", sender: self)
			
			case .walletCreationComplete:
				showNavigationBar()
				self.performSegue(withIdentifier: "recoveryPhrasePrompt", sender: self)
				injectWelcomeToNavStack()
			
			case .recoveryPhraseComplete:
				showNavigationBar()
				self.performSegue(withIdentifier: "pincode", sender: self)
				injectWelcomeToNavStack()
			
			case .pinCodeCreationComplete:
				if biometricCapable {
					showNavigationBar()
					self.performSegue(withIdentifier: "biometric", sender: self)
					injectWelcomeToNavStack()
				} else {
					self.performSegue(withIdentifier: "home", sender: self)
				}
			
			case .biometricChoiceComplete:
				self.performSegue(withIdentifier: "home", sender: self)
			
			case .onboardingComplete:
				self.performSegue(withIdentifier: "login", sender: self)
		}
	}
	
	func showNavigationBar() {
		self.navigationController?.setNavigationBarHidden(false, animated: false)
		self.navigationItem.hidesBackButton = false
		self.navigationItem.largeTitleDisplayMode = .never
	}
	
	func injectWelcomeToNavStack() {
		if let vc = UIStoryboard(name: "Onboarding", bundle: nil).instantiateInitialViewController() {
			self.navigationController?.viewControllers.insert(vc, at: (self.navigationController?.viewControllers.count ?? 2) - 1)
		}
	}
}
