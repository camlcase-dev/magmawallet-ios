//
//  DelegateSuccessViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 05/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import MagmaWalletKit
import Sentry
import Lottie

class DelegateSuccessViewController: UIViewController {
	
	@IBOutlet weak var titleLabel: ThemeLabel!
	@IBOutlet weak var subtitleLabel: ThemeLabel!
	@IBOutlet weak var animationView: AnimationView?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		
		// Removed baker
		if DependencyManager.shared.transactionService.delegateData.selectedBakerAddress == nil {
			titleLabel.text = "del_undelegate_success_title".localized
			subtitleLabel.text = "del_undelegate_success_subtitle".localized
			DependencyManager.shared.accountService.clearDelegate()
			
			if !Thread.current.isRunningXCTest {
				SentrySDK.capture(message: "Baker Remove Succeeded")
			}
			
		} else {
			titleLabel.text = "del_success_title".localized
			subtitleLabel.text = "del_success_subtitle".localized
			
			if !Thread.current.isRunningXCTest {
				SentrySDK.capture(message: "Baker Selection Succeeded")
			}
		}
		
		if let animation = Animation.named("lottie-success") {
			animationView?.animation = animation
		}
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		animationView?.play()
	}
	
	@IBAction func finishButtonTapped(_ sender: ThemeButton) {
		self.navigationController?.popToSettings()
	}
}
