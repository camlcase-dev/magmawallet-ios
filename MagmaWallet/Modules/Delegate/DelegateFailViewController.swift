//
//  DelegateFailViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 05/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import camlKit
import MagmaWalletKit
import Sentry

class DelegateFailViewController: UIViewController {
	
	@IBOutlet weak var titleLabel: ThemeLabel!
	@IBOutlet weak var subTitleLabel: ThemeLabel!
	
	var errorResponse: ErrorResponse = ErrorResponse.unknownError()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		if !Thread.current.isRunningXCTest {
			SentrySDK.capture(message: "Baker Selection Failed")
		}
		
		// Switch to a customised Baker error message
		if errorResponse.errorType == .invalidAddress {
			errorResponse.errorType = .invalidBaker
		}
		
		let errorDetails = ErrorMessageService.errorTitleAndMessage(for: errorResponse)
		titleLabel.text = errorDetails.title
		subTitleLabel.text = errorDetails.message
	}
	
	@IBAction func tryAgainButtonTapped(_ sender: ThemeButton) {
		self.navigationController?.popToDelegate()
	}
	
	@IBAction func backToSettingsButtonTapped(_ sender: ThemeButton) {
		self.navigationController?.popToSettings()
	}
}
