//
//  DelegateViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 21/01/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import camlKit
import MagmaWalletKit
import os.log

class DelegateViewController: UIViewController {
	
	@IBOutlet weak var dropdownTextField: ThemeButton?
	@IBOutlet weak var infoLabel: ThemeLabel?
	@IBOutlet weak var confirmButton: ThemeButton?
	
	let viewModel = DelegateViewModel()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.navigationController?.setNavigationBarHidden(true, animated: false)
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		viewModel.update { [weak self] (response) in
			
			if let name = self?.viewModel.selectedBakerName {
				self?.dropdownTextField?.setTitle(name, for: .normal)
			}
			
			self?.infoLabel?.text = self?.viewModel.infoString
		}
	}
	
	func bakerChosen() {
		dropdownTextField?.setTitle(viewModel.selectedBakerName, for: .normal)
		confirmButton?.isEnabled = (self.viewModel.enoughBalanceToCoverFee && !self.viewModel.bakerSelectionUnchanged)
		
		let titleString = viewModel.selectedBakerAddress == nil ? "del_undelegate_action".localized : "del_confirm".localized
		confirmButton?.setTitle(titleString, for: .normal)
	}
	
	@IBAction func poweredByButtonTapped(_ sender: ThemeButton) {
		if let url = URL(string: "https://baking-bad.org/") {
			UIApplication.shared.open(url, options: [:], completionHandler: nil)
		}
	}
	
	@IBAction func confirmButtonTapped(_ sender: ThemeButton) {
		DependencyManager.shared.transactionService.delegateData.selectedBakerAddress = viewModel.selectedBakerAddress
		
		let currentBalance = DependencyManager.shared.balances[0].balance
		if let selectedMinimum = viewModel.selectedBakerMinimum, selectedMinimum > currentBalance {
			self.performSegue(withIdentifier: "minimumDelegateWarning", sender: self)
			
		} else {
			self.performSegue(withIdentifier: "waitSegue", sender: self)
		}
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "minimumDelegateWarning", let destination = segue.destination as? DelegateMinimumNoticeViewController {
			destination.bakerMinimum = viewModel.selectedBakerMinimum ?? XTZAmount.zero()
		}
	}
	
	@IBAction func cancelButtonTapped(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
}
