//
//  DelegateViewModel.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 22/01/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import camlKit
import os.log

class DelegateViewModel: ViewModel {
	
	var selectedBakerName: String? = nil
	var selectedBakerAddress: String? = nil
	var selectedBakerMinimum: XTZAmount? = nil
	var bakerSelectionUnchanged = false
	var infoString: String = ""
	var enoughBalanceToCoverFee = false
	
	func update(withInput input: [String: Any]? = nil, completion: @escaping (ViewModelResponse) -> Void) {
		
		let accountService = DependencyManager.shared.accountService
		let currentBakerAddress = accountService.currentDelegate()
		
		if selectedBakerName == nil && currentBakerAddress != nil {
			selectedBakerName = DependencyManager.shared.tezosBakerService.bakerNameForAddress(address: accountService.currentDelegate())
		}
		
		let delegationFee = OperationFees.defaultFees(operationKind: .delegation).allFees()
		infoString = "del_info".localized(delegationFee.normalisedRepresentation)
		
		if DependencyManager.shared.balances[0].balance >= delegationFee {
			enoughBalanceToCoverFee = true
		}
		
		completion(ViewModelResponse(success: true, responseCode: nil, userDisplayString: nil, errorResponse: nil))
	}
}
