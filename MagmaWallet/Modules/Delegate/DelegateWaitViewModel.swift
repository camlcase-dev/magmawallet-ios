//
//  DelegateWaitViewModel.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 05/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import camlKit
import os.log

class DelegateWaitViewModel: ViewModel {
	
	func update(withInput input: [String: Any]? = nil, completion: @escaping (ViewModelResponse) -> Void) {
		
		guard let currentWallet = DependencyManager.shared.accountService.currentWallet() else {
			completion(ViewModelResponse(success: false, responseCode: nil, userDisplayString: nil, errorResponse: ErrorResponse.error(string: "", errorType: .unknownWallet)))
			return
		}
		
		let transactionService = DependencyManager.shared.transactionService
		
		
		// Create delegate or undelegate operations
		var operations: [camlKit.Operation] = []
		if let destination = transactionService.delegateData.selectedBakerAddress {
			operations = OperationFactory.delegateOperation(to: destination, from: currentWallet.address)
			
		} else {
			operations = OperationFactory.undelegateOperation(address: currentWallet.address)
		}
		
		
		// Estimate and send
		DependencyManager.shared.tezosNodeClient.estimate(operations: operations, withWallet: currentWallet) { [weak self] (result) in
			switch result {
				case .success(let ops):
					
					DependencyManager.shared.tezosNodeClient.send(operations: ops, withWallet: currentWallet) { (result) in
						switch result {
							case .success(let string):
								os_log(.debug, log: .tezos, "Delegate successful, operation hash: %@", string)
								DependencyManager.shared.transactionService.delegateData.inProgressOpHash = string
								completion(ViewModelResponse(success: true, responseCode: nil, userDisplayString: nil, errorResponse: nil))
								
							case .failure(let error):
								self?.handleError(error: error, completion: completion)
						}
					}
					
				case .failure(let error):
					self?.handleError(error: error, completion: completion)
			}
		}
	}
	
	func handleError(error: ErrorResponse, completion: @escaping (ViewModelResponse) -> Void) {
		var errorToReturn = error
		
		if errorToReturn.errorType == .insufficientFunds {
			errorToReturn.errorType = .insufficientFundsDelegation
		}
		
		completion(ViewModelResponse(success: false, responseCode: nil, userDisplayString: nil, errorResponse: errorToReturn))
	}
}
