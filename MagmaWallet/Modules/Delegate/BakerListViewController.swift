//
//  BakerListViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 05/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import MagmaWalletKit

class BakerListViewContorller: UIViewController {
	
	@IBOutlet weak var containerView: UIView!
	@IBOutlet weak var textField: ThemeTextfield!
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var topConstraint: NSLayoutConstraint!
	@IBOutlet weak var errorMessageStackView: UIStackView!
	
	private let viewModel = BakerListViewModel()
	private var isValidAddress = false
	private var startingTopConstraintConstant: CGFloat = 0
	private var navController: UINavigationController?
	private var parentVc: DelegateViewController?
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		tableView.delegate = self
		
		self.textField.validator = TezosAddressValidator(ownAddress: DependencyManager.shared.accountService.currentAddress() ?? "")
		self.textField.themeTextfieldDelegate = self
		self.textField.themeTextfieldValidationDelegate = self
		self.textField.rightMode = .scanAndClear
		self.textField.addDoneToolbar()
		self.errorMessageStackView.isHidden = true
	}
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		containerView.roundCorners(corners: [.topLeft, .topRight], radius: 12)
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		startingTopConstraintConstant = topConstraint.constant
		NotificationCenter.default.addObserver(self, selector: #selector(customKeyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(customKeyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
		
		navController = self.presentingViewController as? UINavigationController
		parentVc = navController?.viewControllers.last as? DelegateViewController
		
		viewModel.bakerSelectionRemovedDelegate = self
		viewModel.removeTapped = parentVc?.viewModel.selectedBakerName == "del_undelegate_label".localized
		viewModel.update { [weak self] (response) in
			self?.tableView.tableViewData = self?.viewModel.tableViewData
			self?.tableView.reloadData()
		}
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
		NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
	}
	
	@objc func customKeyboardWillShow(notification: NSNotification) {
		// in iOS 14, this event fires once per key press. Filed a bug
		if startingTopConstraintConstant == topConstraint.constant {
			topConstraint.constant -= 100
		}
	}
	
	@objc func customKeyboardWillHide(notification: NSNotification) {
		if startingTopConstraintConstant == topConstraint.constant {
			topConstraint.constant += 100
		}
	}
	
	@IBAction func closeTapped(_ sender: UIButton? = nil) {
		textField.revalidateTextfield()
		textField.resignFirstResponder()
		
		if isValidAddress, let parentVc = parentVc {
			let addressText = textField.text ?? ""
			let bakerNameOrAddress = DependencyManager.shared.tezosBakerService.bakerNameForAddress(address: addressText)
			
			parentVc.viewModel.selectedBakerName = bakerNameOrAddress
			parentVc.viewModel.selectedBakerAddress = addressText
			parentVc.viewModel.selectedBakerMinimum = nil
			parentVc.bakerChosen()
		}
		
		self.dismiss(animated: true, completion: nil)
	}
}

extension BakerListViewContorller: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		let cellModel = viewModel.tableViewData[indexPath.section].cellModels[indexPath.row]
		
		guard let parentVc = parentVc, let parsedModel = cellModel as? BakerCellModelProtocol else {
			return
		}
		
		let bakerNameOrAddress = DependencyManager.shared.tezosBakerService.bakerNameForAddress(address: parsedModel.bakerAddress)
		
		parentVc.viewModel.selectedBakerName = bakerNameOrAddress
		parentVc.viewModel.selectedBakerAddress = parsedModel.bakerAddress
		parentVc.viewModel.selectedBakerMinimum = parsedModel.bakerMinimum
		parentVc.viewModel.bakerSelectionUnchanged = cellModel is BakerCellSelectedModel
		parentVc.bakerChosen()
		
		self.dismiss(animated: true, completion: nil)
	}
}

extension BakerListViewContorller: BakerSelectionRemovedDelegate {
	
	func bakerRemoveTapped() {
		guard let parentVc = parentVc else {
			return
		}
		
		tableView.tableViewData = viewModel.tableViewData
		tableView.reloadData()
		
		parentVc.viewModel.selectedBakerName = "del_undelegate_label".localized
		parentVc.viewModel.selectedBakerAddress = nil
		parentVc.viewModel.selectedBakerMinimum = nil
		parentVc.viewModel.bakerSelectionUnchanged = false
		parentVc.bakerChosen()
	}
}

extension BakerListViewContorller: ThemeTextfieldValidationProtocol, ThemeTextfieldProtocol {
	
	func validated(_ validated: Bool, textfield: ThemeTextfield, forText text: String) {
		isValidAddress = validated
		
		if text != "" {
			self.errorMessageStackView.isHidden = validated
		}
	}
	
	func rightButtonTapped(_ textfield: ThemeTextfield) {
		let vc = ScanViewController()
		vc.delegate = self
		
		self.present(vc, animated: true, completion: nil)
	}
	
	func didClear() {
		self.errorMessageStackView.isHidden = true
	}
	
	func didTapDone() {
		closeTapped(UIButton())
	}
	
	func didBeingEditing(_ textField: UITextField) {
	}
	
	func didEndEditing(_ textField: UITextField) {
	}
}

extension BakerListViewContorller: ScanViewControllerDelegate {
	
	func scannedQRCode(code: String) {
		self.textField.text = code
		self.closeTapped()
	}
}
