//
//  DelegateMinimumNoticeViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 08/01/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import UIKit
import camlKit
import MagmaWalletKit

class DelegateMinimumNoticeViewController: UIViewController {
	
	var bakerMinimum: XTZAmount = XTZAmount.zero()
	
	@IBOutlet weak var bakerAmount: ThemeLabel!
	@IBOutlet weak var balanceAmount: ThemeLabel!
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.navigationController?.setNavigationBarHidden(false, animated: false)
		
		let accountBalance = DependencyManager.shared.balances[0].balance
		
		self.bakerAmount.text = "\(bakerMinimum.normalisedRepresentation) XTZ"
		self.balanceAmount.text = "\(accountBalance.normalisedRepresentation) XTZ"
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		self.navigationController?.setNavigationBarHidden(true, animated: false)
	}
	
	@IBAction func back(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
}
