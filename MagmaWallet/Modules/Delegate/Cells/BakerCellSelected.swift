//
//  BakerCell.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 15/04/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import camlKit
import Kingfisher
import os.log

protocol BakerCellModelProtocol {
	var bakerAddress: String { get }
	var bakerMinimum: XTZAmount? { get }
}

protocol BakerSelectionRemovedDelegate: class {
	func bakerRemoveTapped()
}

struct BakerCellSelectedModel: UITableViewCellModel, BakerCellModelProtocol {
	
	static let staticIdentifier = "BakerCellSelected"
	let cellId: String = BakerCellSelectedModel.staticIdentifier
	let estimatedRowHeight: CGFloat = 71
	
	let imageCacheKey: String?
	let bakerName: String
	let bakerAddress: String
	let bakerMinimum: XTZAmount?
	let removedTapped: Bool
	
	weak var delegate: BakerSelectionRemovedDelegate?
}

class BakerCellSelected: UITableViewCell, UITableViewCellMVVM {
	
	@IBOutlet weak var bakerIconView: UIImageView!
	@IBOutlet weak var bakerNameLabel: UILabel!
	@IBOutlet weak var bakerAddressLabel: UILabel!
	@IBOutlet weak var removeButton: UIButton!
	
	private var cellModel: BakerCellSelectedModel?
	
	func setup(withModel model: UITableViewCellModel) {
		guard let cellModel = model as? BakerCellSelectedModel else {
			os_log(.error, log: .mvvm, "Cell model passed in to 'BakerCellSelected' did not match required type 'BakerCellSelectedModel', instead found '%@'", String(describing: model.self))
			return
		}
		
		self.cellModel = cellModel
		self.bakerNameLabel.text = cellModel.bakerName
		self.bakerAddressLabel.text = cellModel.bakerAddress
		
		
		// Check for remove button state
		if cellModel.removedTapped {
			removeButton.isHidden = true
			bakerIconView.alpha = 0.5
			bakerNameLabel.alpha = 0.5
			bakerAddressLabel.alpha = 0.5
			
		} else {
			let attributes: [NSAttributedString.Key: Any] = [
				NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue,
				NSAttributedString.Key.foregroundColor: UIColor(named: "SplashBackground") ?? UIColor.black
			]
			let underlineAttributedString = NSAttributedString(string: "del_baker_remove".localized, attributes: attributes)
			removeButton.setAttributedTitle(underlineAttributedString, for: .normal)
		}
		
		
		// Process image
		if let imageKey = cellModel.imageCacheKey {
			ImageCache.default.retrieveImage(forKey: imageKey) { [weak self] (result) in
				
				switch result {
					case .success(let imageResult):
						if imageResult.cacheType.cached {
							self?.bakerIconView.image = imageResult.image
							
						} else {
							self?.retryImageDownload()
						}
					
					case .failure(let error):
						os_log(.error, log: .bakers, "Error fetching image cache: %@", "\(error)")
						self?.retryImageDownload()
				}
			}
		}
	}
	
	func retryImageDownload() {
		os_log(.debug, log: .bakers, "Attempting to re-try image download")
		
		// Fallback incase there was an error fetching an image
		if let url = URL(string: cellModel?.imageCacheKey ?? "") {
			DependencyManager.shared.tezosBakerService.downloadImage(forBakerURL: url, completion: { (image) in
				self.bakerIconView.image = image
			})
		}
	}
	
	@IBAction func removeButtonTapped(_ sender: Any) {
		cellModel?.delegate?.bakerRemoveTapped()
	}
}
