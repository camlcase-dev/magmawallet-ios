//
//  BakerCell.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 06/01/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import UIKit
import camlKit
import Kingfisher
import os.log

struct BakerCellModel: UITableViewCellModel, BakerCellModelProtocol {
	
	static let staticIdentifier = "BakerCell"
	let cellId: String = BakerCellModel.staticIdentifier
	let estimatedRowHeight: CGFloat = 71
	
	let imageCacheKey: String?
	let bakerName: String
	let bakerAddress: String
	let bakerMinimum: XTZAmount?
}

class BakerCell: UITableViewCell, UITableViewCellMVVM {
	
	@IBOutlet weak var bakerIconView: UIImageView!
	@IBOutlet weak var bakerNameLabel: UILabel!
	@IBOutlet weak var bakerAddressLabel: UILabel!
	
	private var cellModel: BakerCellModel?
	
	func setup(withModel model: UITableViewCellModel) {
		guard let cellModel = model as? BakerCellModel else {
			os_log(.error, log: .mvvm, "Cell model passed in to 'BakerCell' did not match required type 'BakerCellModel', instead found '%@'", String(describing: model.self))
			return
		}
		
		self.cellModel = cellModel
		self.bakerNameLabel.text = cellModel.bakerName
		self.bakerAddressLabel.text = cellModel.bakerAddress
		
		if let imageKey = cellModel.imageCacheKey {
			ImageCache.default.retrieveImage(forKey: imageKey) { [weak self] (result) in
				
				switch result {
					case .success(let imageResult):
						if imageResult.cacheType.cached {
							self?.bakerIconView.image = imageResult.image
							
						} else {
							self?.retryImageDownload()
						}
						
					case .failure(let error):
						os_log(.error, log: .bakers, "Error fetching image cache: %@", "\(error)")
						self?.retryImageDownload()
				}
			}
		}
	}
	
	func retryImageDownload() {
		os_log(.debug, log: .bakers, "Attempting to re-try image download")
		
		// Fallback incase there was an error fetching an image
		if let url = URL(string: cellModel?.imageCacheKey ?? "") {
			DependencyManager.shared.tezosBakerService.downloadImage(forBakerURL: url, completion: { (image) in
				self.bakerIconView.image = image
			})
		}
	}
}

