//
//  BakerListViewModel.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 05/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import os.log

class BakerListViewModel: ViewModel {
	
	var tableViewData: [UITableViewSectionModel] = []
	var removeTapped = false
	
	weak var bakerSelectionRemovedDelegate: BakerSelectionRemovedDelegate?
	
	
	func update(withInput input: [String: Any]? = nil, completion: @escaping (ViewModelResponse) -> Void) {
		
		let accountService = DependencyManager.shared.accountService
		let currentBaker = accountService.currentDelegate()
		
		var cellModels: [UITableViewCellModel] = []
		for baker in DependencyManager.shared.tezosBakerService.allBakers() {
			
			if baker.address == currentBaker {
				cellModels.insert(BakerCellSelectedModel(imageCacheKey: baker.logo, bakerName: baker.name, bakerAddress: baker.address, bakerMinimum: baker.minDelegationAsXTZ(), removedTapped: removeTapped, delegate: self), at: 0)
			} else {
				cellModels.append(BakerCellModel(imageCacheKey: baker.logo, bakerName: baker.name, bakerAddress: baker.address, bakerMinimum: baker.minDelegationAsXTZ()))
			}
		}
		
		tableViewData = [UITableViewSectionModel(withHeader: nil, footer: nil, andCellModels: cellModels)]
		
		completion(ViewModelResponse(success: true, responseCode: nil, userDisplayString: nil, errorResponse: nil))
	}
}

extension BakerListViewModel: BakerSelectionRemovedDelegate {
	
	func bakerRemoveTapped() {
		removeTapped = true
		update { [weak self] (response) in
			self?.bakerSelectionRemovedDelegate?.bakerRemoveTapped()
		}
	}
}
