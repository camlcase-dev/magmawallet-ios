//
//  SettingsCurrentPinViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 15/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import MagmaWalletKit

class SettingsCurrentPinViewController: UIViewController {
	
	@IBOutlet weak var textField: ThemeTextfield!
	@IBOutlet weak var errorStackView: UIStackView!
	@IBOutlet weak var continueButton: ThemeButton!
	@IBOutlet weak var continueButtonBottomConstraint: NSLayoutConstraint!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		errorStackView?.isHidden = true
		
		textField.validator = LengthValidator(maxLength: 6, minLength: 4)
		textField.themeTextfieldValidationDelegate = self
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		// Can't use the current UIViewController extension keyboard code.
		// Button moves up initially, but snaps back to bottom on viewDidAppear, needed to tweak it for this screen
		NotificationCenter.default.addObserver(self, selector: #selector(customKeyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		textField.becomeFirstResponder()
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
	}
	
	@objc func customKeyboardWillShow(notification: NSNotification) {
		if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
			continueButtonBottomConstraint.constant = keyboardSize.height
		}
	}
	
	@IBAction func continueButtonTapped(_ sender: Any) {
		if DependencyManager.shared.accountService.validatePinCode(code: textField.text ?? "", temp: false) {
			let vc = UIStoryboard.pincodeViewController()
			self.navigationController?.pushViewController(vc, animated: true)
			
		} else {
			errorStackView?.isHidden = false
		}
	}
}

extension SettingsCurrentPinViewController: ThemeTextfieldValidationProtocol {
	
	func validated(_ validated: Bool, textfield: ThemeTextfield, forText text: String) {
		continueButton.isEnabled = validated
		
		if errorStackView.isHidden == false {
			errorStackView.isHidden = true
		}
	}
}
