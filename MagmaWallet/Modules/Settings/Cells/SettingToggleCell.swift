//
//  SettingsCell.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 04/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import MagmaWalletKit
import os.log

protocol SettingToggleCellProtocol: class {
	func toggleChanged(cellTitle: String, selected: Bool)
}

struct SettingToggleCellModel: UITableViewCellModel {
	
	static let staticIdentifier = "SettingToggleCell"
	let cellId: String = SettingToggleCellModel.staticIdentifier
	let estimatedRowHeight: CGFloat = 68
	
	let title: String
	let toggleSelected: Bool
	let delegate: SettingToggleCellProtocol?
}

struct SettingToggleDescriptionCellModel: UITableViewCellModel {
	
	static let staticIdentifier = "SettingToggleDescriptionCell"
	let cellId: String = SettingToggleDescriptionCellModel.staticIdentifier
	let estimatedRowHeight: CGFloat = 89
	
	let title: String
	let subTitle: String
	let toggleSelected: Bool
	let delegate: SettingToggleCellProtocol?
}

class SettingToggleCell: UITableViewCell, UITableViewCellMVVM {
	
	@IBOutlet weak var titleLabel: ThemeLabel!
	@IBOutlet weak var subtitleLabel: ThemeLabel?
	@IBOutlet weak var toggleSwitch: UISwitch!
	
	weak var delegate: SettingToggleCellProtocol?
	
	func setup(withModel model: UITableViewCellModel) {
		selectionStyle = .none
		
		if let cellModel = model as? SettingToggleDescriptionCellModel {
			titleLabel.text = cellModel.title
			subtitleLabel?.text = cellModel.subTitle
			toggleSwitch.isOn = cellModel.toggleSelected
			delegate = cellModel.delegate
			
		} else if let cellModel = model as? SettingToggleCellModel {
			titleLabel.text = cellModel.title
			toggleSwitch.isOn = cellModel.toggleSelected
			delegate = cellModel.delegate
		}
	}
	
	@IBAction func toggleChanged(_ sender: UISwitch) {
		guard let txt = titleLabel.text else {
			return
		}
		
		delegate?.toggleChanged(cellTitle: txt, selected: sender.isOn)
	}
}
