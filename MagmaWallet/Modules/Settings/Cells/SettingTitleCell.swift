//
//  SettingTitleCell.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 04/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import MagmaWalletKit
import os.log

struct SettingTitleCellModel: UITableViewCellModel {
	
	static let staticIdentifier = "SettingTitleCell"
	let cellId: String = SettingTitleCellModel.staticIdentifier
	let estimatedRowHeight: CGFloat = 68
	
	let title: String
}

class SettingTitleCell: UITableViewCell, UITableViewCellMVVM {
	
	@IBOutlet weak var titleLabel: ThemeLabel!
	
	func setup(withModel model: UITableViewCellModel) {
		guard let cellModel = model as? SettingTitleCellModel else {
			os_log(.error, log: .mvvm, "Cell model passed in to 'SettingTitleCell' did not match required type 'SettingTitleCellModel', instead found '%@'", String(describing: model.self))
			return
		}
		
		titleLabel.text = cellModel.title
	}
}
