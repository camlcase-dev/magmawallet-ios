//
//  AttributionCell.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 11/01/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import UIKit
import MagmaWalletKit
import os.log

struct AttributionCellModel: UITableViewCellModel {
	
	static let staticIdentifier = "attributionCell"
	let cellId: String = AttributionCellModel.staticIdentifier
	let estimatedRowHeight: CGFloat = 84.5
	
	let title: String
	let subtitle: String
	let urlSnippet: String
	let url: URL
}

class AttributionCell: UITableViewCell, UITableViewCellMVVM {
	
	@IBOutlet weak var titleLabel: ThemeLabel!
	@IBOutlet weak var subtitleLabel: ThemeLabel!
	@IBOutlet weak var urlSnippetLabel: ThemeLabel!
	
	func setup(withModel model: UITableViewCellModel) {
		guard let cellModel = model as? AttributionCellModel else {
			os_log(.error, log: .mvvm, "Cell model passed in to 'AttributionCell' did not match required type 'AttributionCellModel', instead found '%@'", String(describing: model.self))
			return
		}
		
		titleLabel.text = cellModel.title
		subtitleLabel.text = cellModel.subtitle
		urlSnippetLabel.text = cellModel.urlSnippet
	}
}
