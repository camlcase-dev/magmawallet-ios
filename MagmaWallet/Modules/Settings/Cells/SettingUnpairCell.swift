//
//  SettingUnpairCell.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 04/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import os.log

struct SettingUnpairCellModel: UITableViewCellModel {
	
	static let staticIdentifier = "SettingUnpairCell"
	let cellId: String = SettingUnpairCellModel.staticIdentifier
	let estimatedRowHeight: CGFloat = 91
}

class SettingUnpairCell: UITableViewCell, UITableViewCellMVVM {
	
	func setup(withModel model: UITableViewCellModel) {
	}
}
