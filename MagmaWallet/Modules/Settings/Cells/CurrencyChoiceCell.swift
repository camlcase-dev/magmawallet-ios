//
//  CurrencyChoiceCell.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 09/12/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import MagmaWalletKit
import os.log

struct CurrencyChoiceCellModel: UITableViewCellModel {
	
	static let staticIdentifier = "CurrencyChoiceCell"
	let cellId: String = CurrencyChoiceCellModel.staticIdentifier
	let estimatedRowHeight: CGFloat = 52
	
	let currencyCode: String
	let longName: String
}

class CurrencyChoiceCell: UITableViewCell, UITableViewCellMVVM {
	
	@IBOutlet weak var codeLabel: ThemeLabel!
	@IBOutlet weak var nameLabel: ThemeLabel!
	
	func setup(withModel model: UITableViewCellModel) {
		guard let cellModel = model as? CurrencyChoiceCellModel else {
			os_log(.error, log: .mvvm, "Cell model passed in to 'CurrencyChoiceCell' did not match required type 'CurrencyChoiceCellModel', instead found '%@'", String(describing: model.self))
			return
		}
		
		codeLabel.text = cellModel.currencyCode
		nameLabel.text = cellModel.longName
	}
}
