//
//  SettingTitleSubTitleCell.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 05/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import MagmaWalletKit
import os.log

struct SettingTitleSubTitleCellModel: UITableViewCellModel {
	
	static let staticIdentifier = "SettingTitleSubTitleCell"
	let cellId: String = SettingTitleSubTitleCellModel.staticIdentifier
	let estimatedRowHeight: CGFloat = 79
	
	let title: String
	let subTitle: String
	let tappable: Bool
}

class SettingTitleSubTitleCell: UITableViewCell, UITableViewCellMVVM {
	
	@IBOutlet weak var titleLabel: ThemeLabel!
	@IBOutlet weak var subTitleLabel: ThemeLabel!
	
	func setup(withModel model: UITableViewCellModel) {
		guard let cellModel = model as? SettingTitleSubTitleCellModel else {
			os_log(.error, log: .mvvm, "Cell model passed in to 'SettingTitleSubTitleCell' did not match required type 'SettingTitleSubTitleCellModel', instead found '%@'", String(describing: model.self))
			return
		}
		
		titleLabel.text = cellModel.title
		subTitleLabel.text = cellModel.subTitle
		
		selectionStyle = cellModel.tappable ? .default : .none
	}
}
