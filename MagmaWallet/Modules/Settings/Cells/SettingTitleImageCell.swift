//
//  SettingTitleImageCell.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 06/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import MagmaWalletKit
import os.log

struct SettingTitleImageCellModel: UITableViewCellModel {
	
	static let staticIdentifier = "SettingTitleImageCell"
	let cellId: String = SettingTitleImageCellModel.staticIdentifier
	let estimatedRowHeight: CGFloat = 68
	
	let title: String
}

class SettingTitleImageCell: UITableViewCell, UITableViewCellMVVM {
	
	@IBOutlet weak var titleLabel: ThemeLabel!
	
	func setup(withModel model: UITableViewCellModel) {
		guard let cellModel = model as? SettingTitleImageCellModel else {
			os_log(.error, log: .mvvm, "Cell model passed in to 'SettingTitleImageCell' did not match required type 'SettingTitleImageCellModel', instead found '%@'", String(describing: model.self))
			return
		}
		
		titleLabel.text = cellModel.title
	}
}
