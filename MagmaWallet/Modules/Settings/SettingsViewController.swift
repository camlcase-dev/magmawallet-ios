//
//  SettingsViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 20/01/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import Sentry
import os.log

class SettingsViewController: UIViewController {
	
	@IBOutlet weak var tableView: UITableView?
	
	private let viewModel = SettingsViewModel()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		tableView?.delegate = self
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.tabBarController?.navigationItem.title = "wlt_navigation_settings".localized.uppercased()
		self.navigationController?.setNavigationBarHidden(false, animated: false)
		
		viewModel.update { [weak self] (response) in
			if response.success {
				self?.tableView?.tableViewData = self?.viewModel.tableViewData
				self?.tableView?.reloadData()
			}
		}
	}
	
	func deleteAndNavigate() {
		self.hideAllSubviews()
		self.showActivity()
		
		DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
			let _ = DependencyManager.shared.accountService.deleteWallet()
			
			// Disable 60 second background refresh refresh
			// DependencyManager.shared.homeService.stopPolling()
		}
		
		DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
			self.hideActivity()
			self.navigationController?.popToRootViewController(animated: false)
		}
	}
}

extension SettingsViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		
		// Check for other cells
		let cellModel = viewModel.tableViewData[indexPath.section].cellModels[indexPath.row]
		
		if let model = cellModel as? SettingTitleSubTitleCellModel, model.tappable == false {
			return
		}
		
		
		// Check cell model type for unique types.
		// Unpair cell = delete wallet
		// title + subtitle cell = delegation
		// title + image cell = share feedback
		// title cell = switch statement for multiple usecases
		if cellModel is SettingUnpairCellModel {
			alert(withTitle: "set_unpair_popup_title".localized, andMessage: "set_unpair_popup_body".localized, okAction: { [weak self] (action) in
				if !Thread.current.isRunningXCTest {
					SentrySDK.capture(message: "Wallet Unpaired")
				}
				
				self?.deleteAndNavigate()
			})
			
		} else if cellModel is SettingTitleSubTitleCellModel {
			guard let currentModel = cellModel as? SettingTitleSubTitleCellModel else {
				return
			}
			
			switch currentModel.title {
				case "set_baker".localized:
					self.performSegue(withIdentifier: "delegateSegue", sender: self)
				
				case "set_currency".localized:
					self.performSegue(withIdentifier: "localCurrencySegue", sender: self)
				
				default:
					os_log(.error, log: .default, "Unknwon settings option tapped: %@", currentModel.title)
					return
			}
			
		} else if cellModel is SettingTitleImageCellModel {
			guard let currentModel = cellModel as? SettingTitleImageCellModel else {
				return
			}
			
			switch currentModel.title {
				case "set_share_feedback".localized:
					if let url = URL(string: "https://docs.google.com/forms/d/e/1FAIpQLSdynaHhnvnJ_lu_00O8bUYBjpvgNMMc3-AarfG9uBKULOCPrQ/viewform?usp=sf_link") {
						UIApplication.shared.open(url, options: [:], completionHandler: nil)
					}
				
				case "set_about".localized:
					if let url = URL(string: "https://magmawallet.io/about/") {
						UIApplication.shared.open(url, options: [:], completionHandler: nil)
					}
				
				case "set_privacy".localized:
					if let url = URL(string: "https://magmawallet.io/ios/privacy-policy/") {
						UIApplication.shared.open(url, options: [:], completionHandler: nil)
					}
				
				case "set_terms".localized:
					if let url = URL(string: "https://magmawallet.io/ios/terms/") {
						UIApplication.shared.open(url, options: [:], completionHandler: nil)
					}
				
				case "set_newsletter".localized:
					if let url = URL(string: "https://camlcase.io/newsletter/") {
						UIApplication.shared.open(url, options: [:], completionHandler: nil)
					}
				
				default:
					os_log(.error, log: .default, "Unknwon settings option tapped: %@", currentModel.title)
					return
			}
				   
		} else if cellModel is SettingTitleCellModel {
			guard let currentModel = cellModel as? SettingTitleCellModel else {
				return
			}
			
			switch currentModel.title {
				case "set_edit_pin".localized:
					self.performSegue(withIdentifier: "editPinSegue", sender: self)
				
				case "set_recovery".localized:
					self.performSegue(withIdentifier: "recoveryPhraseSegue", sender: self)
				
				case "set_attributions_title".localized:
					self.performSegue(withIdentifier: "attributionsSegue", sender: self)
				
				case "set_dynamic_urls_title".localized:
					self.performSegue(withIdentifier: "networkSettings", sender: self)
				
				default:
					os_log(.error, log: .default, "Unknwon settings option tapped: %@", currentModel.title)
					return
			}
		}
	}
		
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 30
	}
	
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		
		if section <= self.viewModel.tableViewData.count-1 {
			let containerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 30))
			let sectionHeaderLabel = UILabel(frame: CGRect(x: 24, y: 0, width: tableView.frame.width, height: 20))
			sectionHeaderLabel.text = self.viewModel.tableViewData[section].header
			sectionHeaderLabel.textColor = UIColor(named: "SemiTransparentWhiteText")
			sectionHeaderLabel.font = UIFont.barlowFont(.regular, withSize: 12)
			
			containerView.addSubview(sectionHeaderLabel)
			
			return containerView
		}
		
		return nil
	}
}
