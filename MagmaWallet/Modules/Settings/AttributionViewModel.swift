//
//  AttributionViewModel.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 11/01/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import Foundation
import os.log

class AttributionViewModel: ViewModel {
	
	var tableViewData: [UITableViewSectionModel] = []
	
	func update(withInput input: [String : Any]? = [:], completion: @escaping (ViewModelResponse) -> Void) {
		
		tableViewData = [
			UITableViewSectionModel(withHeader: nil, footer: nil, andCellModels: [
				AttributionCellModel(title: "Baking Bad", subtitle: "set_attributions_baker_data".localized, urlSnippet: "baking-bad.org", url: URL(string: "https://baking-bad.org/")!),
				AttributionCellModel(title: "Better Call Dev", subtitle: "set_attributions_activity_data".localized, urlSnippet: "better-call.dev", url: URL(string: "https://better-call.dev/")!),
				AttributionCellModel(title: "CoinGecko", subtitle: "set_attributions_pricing_data".localized, urlSnippet: "coingecko.com", url: URL(string: "https://coingecko.com/")!),
				AttributionCellModel(title: "Lokalise", subtitle: "set_attributions_strings".localized, urlSnippet: "lokalise.com", url: URL(string: "https://lokalise.com/")!),
				AttributionCellModel(title: "TzKT", subtitle: "set_attributions_activity_data".localized, urlSnippet: "tzkt.io", url: URL(string: "https://tzkt.io/")!)
			])
		]
		
		completion(ViewModelResponse(success: true, responseCode: nil, userDisplayString: nil, errorResponse: nil))
	}
}
