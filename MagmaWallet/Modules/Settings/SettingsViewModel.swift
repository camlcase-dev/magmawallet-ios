//
//  SettingsViewModel.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 18/02/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import os.log

class SettingsViewModel: ViewModel {
	
	var tableViewData: [UITableViewSectionModel] = []
	
	func update(withInput input: [String : Any]? = [:], completion: @escaping (ViewModelResponse) -> Void) {
		let accountService = DependencyManager.shared.accountService
		let tokenPriceService = DependencyManager.shared.tokenPriceService
		let biometricToggleSelected = (accountService.getBiometricChoice() != .none)
		let biometricType = accountService.availableBiometricType() == .touchID ? "onb_biometric_touchid".localized : "onb_biometric_faceid".localized
		let bakerName = DependencyManager.shared.tezosBakerService.bakerNameForAddress(address: accountService.currentDelegate())
		let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
		let buildNumber = Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? ""
		let versionDisplayString = "set_version".localized("\(appVersion).\(buildNumber)")
		
		#if TESTNET
			let network = "Testnet"
		#else
			let network = "Mainnet"
		#endif
		
		tableViewData = [
			UITableViewSectionModel(withHeader: "set_group_staking".localized, footer: nil, andCellModels: [
				SettingTitleSubTitleCellModel(title: "set_baker".localized, subTitle: bakerName, tappable: true)
			]),
			UITableViewSectionModel(withHeader: "set_group_security".localized, footer: nil, andCellModels: [
				SettingToggleCellModel(title: "set_biometric".localized(biometricType), toggleSelected: biometricToggleSelected, delegate: self),
				SettingTitleCellModel(title: "set_edit_pin".localized),
				SettingTitleCellModel(title: "set_recovery".localized)
			]),
			UITableViewSectionModel(withHeader: "set_group_about".localized, footer: nil, andCellModels: [
				SettingTitleImageCellModel(title: "set_share_feedback".localized),
				SettingTitleImageCellModel(title: "set_about".localized),
				SettingTitleImageCellModel(title: "set_privacy".localized),
				SettingTitleImageCellModel(title: "set_terms".localized),
				SettingTitleImageCellModel(title: "set_newsletter".localized),
				SettingTitleCellModel(title: "set_attributions_title".localized)
			]),
			UITableViewSectionModel(withHeader: "set_group_other".localized, footer: nil, andCellModels: [
				SettingTitleCellModel(title: "set_dynamic_urls_title".localized),
				SettingTitleSubTitleCellModel(title: "set_currency".localized, subTitle: tokenPriceService.selectedCurrency?.uppercased() ?? "", tappable: true),
				SettingTitleSubTitleCellModel(title: versionDisplayString, subTitle: network, tappable: false),
				SettingUnpairCellModel()
			])
		]
		
		completion(ViewModelResponse(success: true, responseCode: nil, userDisplayString: nil, errorResponse: nil))
	}
}

extension SettingsViewModel: SettingToggleCellProtocol {
	
	func toggleChanged(cellTitle: String, selected: Bool) {
		
		switch cellTitle {
				
			// Biometric
			default:
				if selected {
					let availableOption = DependencyManager.shared.accountService.availableBiometricType()
					DependencyManager.shared.accountService.setBiometricChoice(type: availableOption)
					
				} else {
					DependencyManager.shared.accountService.setBiometricChoice(type: .none)
				}
		}
	}
}
