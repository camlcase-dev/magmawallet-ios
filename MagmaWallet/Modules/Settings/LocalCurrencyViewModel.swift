//
//  LocalCurrencyViewModel.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 09/12/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import camlKit
import os.log

class LocalCurrencyViewModel: ViewModel {
	
	var tableViewData: [UITableViewSectionModel] = []
	
	private let tokenPriceService = DependencyManager.shared.tokenPriceService
	private let popularKeys = ["usd", "eur", "gbp", "jpy", "rub", "inr", "btc", "eth"]
	
	
	func update(withInput input: [String : Any]? = [:], completion: @escaping (ViewModelResponse) -> Void) {
		guard let rates = tokenPriceService.coinGeckoRates?.rates else {
			completion(ViewModelResponse(success: false, responseCode: nil, userDisplayString: nil, errorResponse: nil))
			return
		}
		
		var popularCells: [CurrencyChoiceCellModel] = []
		for key in popularKeys {
			if let obj = rates[key] {
				popularCells.append(CurrencyChoiceCellModel(currencyCode: key.uppercased(), longName: obj.name))
			}
		}
		
		var allCells: [CurrencyChoiceCellModel] = []
		for key in rates.keys.sorted(by: <) {
			allCells.append(CurrencyChoiceCellModel(currencyCode: key.uppercased(), longName: rates[key]?.name ?? ""))
		}
		
		tableViewData = [
			UITableViewSectionModel(withHeader: "set_currency_popular".localized, footer: nil, andCellModels: popularCells),
			UITableViewSectionModel(withHeader: "set_currency_all".localized, footer: nil, andCellModels: allCells)
		]
		
		completion(ViewModelResponse(success: true, responseCode: nil, userDisplayString: nil, errorResponse: nil))
	}
	
	func changeCurrency(indexPath: IndexPath, completion: @escaping ((ErrorResponse?) -> Void)) {
		guard tableViewData.count > 0, let model = tableViewData[indexPath.section].cellModels[indexPath.row] as? CurrencyChoiceCellModel else {
			completion(ErrorResponse.unknownError())
			return
		}
		
		tokenPriceService.updateSelectedCurrency(code: model.currencyCode, completion: completion)
	}
}
