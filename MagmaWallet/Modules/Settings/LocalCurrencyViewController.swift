//
//  LocalCurrencyViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 09/12/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit

class LocalCurrencyViewController: UIViewController {
	
	@IBOutlet weak var tableView: UITableView!
	
	private let viewModel = LocalCurrencyViewModel()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		tableView?.delegate = self
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.tabBarController?.navigationItem.title = "wlt_navigation_settings".localized.uppercased()
		self.navigationController?.setNavigationBarHidden(false, animated: false)
		
		viewModel.update { [weak self] (response) in
			if response.success {
				self?.tableView?.tableViewData = self?.viewModel.tableViewData
				self?.tableView?.reloadData()
			}
		}
	}
}

extension LocalCurrencyViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 30
	}
	
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		
		if section <= self.viewModel.tableViewData.count-1 {
			let containerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 30))
			let sectionHeaderLabel = UILabel(frame: CGRect(x: 24, y: 0, width: tableView.frame.width, height: 20))
			sectionHeaderLabel.text = self.viewModel.tableViewData[section].header
			sectionHeaderLabel.textColor = UIColor(named: "SemiTransparentWhiteText")
			sectionHeaderLabel.font = UIFont.barlowFont(.regular, withSize: 12)
			
			containerView.addSubview(sectionHeaderLabel)
			
			return containerView
		}
		
		return nil
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		
		self.showActivity()
		viewModel.changeCurrency(indexPath: indexPath) { [weak self] (error) in
			self?.hideActivity()
			
			if let err = error {
				let errorDetails = ErrorMessageService.errorTitleAndMessage(for: err)
				self?.view.makeToast(errorDetails.message)
				
			} else {
				self?.navigationController?.popViewController(animated: true)
			}
		}
	}
}
