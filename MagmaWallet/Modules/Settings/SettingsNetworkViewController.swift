//
//  SettingsNetworkViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 20/04/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import UIKit
import MagmaWalletKit
import camlKit

class SettingsNetworkViewController: UIViewController {
	
	@IBOutlet weak var mainTextfield: ThemeTextfield!
	@IBOutlet weak var parseTextfield: ThemeTextfield!
	@IBOutlet weak var tzktTextfield: ThemeTextfield!
	@IBOutlet weak var betterCallDevTextfield: ThemeTextfield!
	@IBOutlet weak var confirmButton: ThemeButton!
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		mainTextfield.text = DependencyManager.shared.currentMainNodeURL
		mainTextfield.validator = URLValidator()
		mainTextfield.themeTextfieldValidationDelegate = self
		
		parseTextfield.text = DependencyManager.shared.currentParseNodeURL
		parseTextfield.validator = URLValidator()
		parseTextfield.themeTextfieldValidationDelegate = self
		
		tzktTextfield.text = DependencyManager.shared.currentTzKTURL
		tzktTextfield.validator = URLValidator()
		tzktTextfield.themeTextfieldValidationDelegate = self
		
		betterCallDevTextfield.text = DependencyManager.shared.currentBetterCallDevURL
		betterCallDevTextfield.validator = URLValidator()
		betterCallDevTextfield.themeTextfieldValidationDelegate = self
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		DependencyManager.shared.homeService.addDelegate(self)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		
		DependencyManager.shared.homeService.removeDelegate(self)
	}
	
	@IBAction func confirmTapped(_ sender: Any) {
		DependencyManager.shared.currentMainNodeURL = mainTextfield.text ?? ""
		DependencyManager.shared.currentParseNodeURL = parseTextfield.text ?? ""
		DependencyManager.shared.currentTzKTURL = tzktTextfield.text ?? ""
		DependencyManager.shared.currentBetterCallDevURL = betterCallDevTextfield.text ?? ""
		DependencyManager.shared.createClientsWithCurrentURLs()
		
		DependencyManager.shared.homeService.refreshAllNetworkData()
	}
	
	@IBAction func resetTapped(_ sender: Any) {
		mainTextfield.text = DependencyManager.shared.defaultMainNodeURL
		parseTextfield.text = DependencyManager.shared.defaultParseNodeURL
		tzktTextfield.text = DependencyManager.shared.defaultTzKTURL
		betterCallDevTextfield.text = DependencyManager.shared.defaultBetterCallDevURL
		
		mainTextfield.revalidateTextfield()
		parseTextfield.revalidateTextfield()
		tzktTextfield.revalidateTextfield()
		betterCallDevTextfield.revalidateTextfield()
	}
}

extension SettingsNetworkViewController: HomeServiceDelegate {
	
	func didBeginRefreshingData(balancesAndPricesOnly: Bool) {
		self.showActivity(clearBackground: false)
	}
	
	func didEndRefreshingData(balancesAndPricesOnly: Bool, error: ErrorResponse?) {
		self.hideActivity()
		
		if let err = error {
			let errorDetails = ErrorMessageService.errorTitleAndMessage(for: err)
			self.view.makeToast(errorDetails.message)
			
		} else {
			self.navigationController?.popViewController(animated: true)
		}
	}
}

extension SettingsNetworkViewController: ThemeTextfieldValidationProtocol {
	
	func validated(_ validated: Bool, textfield: ThemeTextfield, forText text: String) {
		
		textfield.isValidated = validated
		
		if textfield.isValidated {
			textfield.borderWidth = 0
			textfield.borderColor = UIColor.clear
		} else {
			textfield.borderWidth = 2
			textfield.borderColor = UIColor.white
		}
		
		if mainTextfield.isValidated && parseTextfield.isValidated && tzktTextfield.isValidated && betterCallDevTextfield.isValidated {
			confirmButton.isEnabled = true
		} else {
			confirmButton.isEnabled = false
		}
	}
}
