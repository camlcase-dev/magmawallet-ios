//
//  AttributionsViewController.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 11/01/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import UIKit

class AttributionsViewController: UIViewController {
	
	@IBOutlet weak var tableView: UITableView!
	
	private let viewModel = AttributionViewModel()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		tableView?.delegate = self
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		//self.tabBarController?.navigationItem.title = "wlt_navigation_settings".localized.uppercased()
		//self.navigationController?.setNavigationBarHidden(false, animated: false)
		
		viewModel.update { [weak self] (response) in
			if response.success {
				self?.tableView?.tableViewData = self?.viewModel.tableViewData
				self?.tableView?.reloadData()
			}
		}
	}
}

extension AttributionsViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		
		let cellModel = viewModel.tableViewData[indexPath.section].cellModels[indexPath.row]
		
		if let model = cellModel as? AttributionCellModel {
			UIApplication.shared.open(model.url, options: [:], completionHandler: nil)
		}
	}
	
	func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
		return UIView()
	}
}
