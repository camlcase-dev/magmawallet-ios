//
//  TokenPriceService.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 24/01/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import camlKit
import os.log
import Keys

class TokenPriceService {
	
	private var fetchingPrices = false
	private var fetchingRates = false
	private let session: URLSession
	private var dispatchGroupPrices = DispatchGroup()
	private var dispatchGroupDexterRates = DispatchGroup()
	private var dispatchGroupAll = DispatchGroup()
	
	private static let currencyFileName = "currency.json"
	
	private let coinGeckoPriceURL = "https://api.coingecko.com/api/v3/simple/price?ids=tezos&vs_currencies="
	private let coinGeckoExchangeRatesURL = "https://api.coingecko.com/api/v3/exchange_rates"
	
	static let currencyRateLastDateKey = "com.currency.last.update"
	static let selectedCurrencyKey = "com.currency.selected"
	
	public var coinGeckoRates: CoinGeckoExchangeRateResponse? = nil
	public var selectedCurrency: String? = nil
	public var selectedCurrencyMultiplier: Decimal = 0
	
	
	
	init(session: URLSession = .shared) {
		self.session = session
	}
	
	func updateAllPrices(completion: @escaping ((ErrorResponse?) -> Void)) {
		
		updatePrices { [weak self] in
			
			// Update dexter Exchange rates after we have all the prices
			// FA1.2 tokens don't exist on public exchanges. We get our prices as a bi-product of tezos local currency price, compared to dexter exchange rate
			self?.updateDexterExchangeRates { (error) in
				DispatchQueue.main.async { completion(error) }
			}
		}
	}
	
	func updateSelectedCurrency(code: String, completion: @escaping ((ErrorResponse?) -> Void)) {
		selectedCurrency = code.lowercased()
		UserDefaults.standard.setValue(selectedCurrency, forKey: TokenPriceService.selectedCurrencyKey)
		updateAllPrices(completion: completion)
	}
	
	private func updatePrices(completion: @escaping (() -> Void)) {
		if !fetchingPrices {
			fetchingPrices = true
			
			fetchExchangeRatesIfNeeded { [weak self] (response) in
				
				self?.fetchTezosPrice { (decimal) in
					self?.fetchingPrices = false
					self?.selectedCurrencyMultiplier = decimal
					DependencyManager.shared.balances[0].localCurrencyRate = decimal // XTZ price gets set here, token prices are set later, using data from Dexter
					DependencyManager.shared.dexterTokens[0].localCurrencyRate = decimal
					
					DispatchQueue.main.async { completion() }
				}
			}
		}
	}
	
	// TODO: try to migrate to camlKit.NetworkService
	private func fetchTezosPrice(completion: @escaping ((Decimal) -> Void)) {
		guard let selectedCur = selectedCurrency else {
			completion(0)
			return
		}
		
		let urlString = coinGeckoPriceURL + selectedCur
		
		guard let fullEndpointURL = URL(string: urlString) else {
			os_log(.error, log: .network, "Can't create URL for token pricing service: %@", urlString)
			completion(0)
			return
		}
		
		var request = URLRequest(url: fullEndpointURL)
		request.addValue("application/json", forHTTPHeaderField: "accept")
		
		session.dataTask(with: request) { data, response, serverError in
			if let d = data {
				do {
					let json = try JSONDecoder().decode(CoinGeckoCurrentPrice.self, from: d)
					os_log(.debug, log: .network, "Found price for XTZ, %@ value of %@", selectedCur, "\(json.price())")
					
					completion(json.price())
					
				} catch {
					os_log(.error, log: .network, "parsing response from CoinGecko price API")
					completion(0.0)
				}
			} else {
				completion(0.0)
			}
		}.resume()
	}
	
	private func fetchExchangeRatesIfNeeded(completion: @escaping ((CoinGeckoExchangeRateResponse?) -> Void)) {
		os_log(.debug, log: .network, "Checking if currency rates need to be updated")
		
		let tempDateFormatter = DateFormatter()
		tempDateFormatter.dateFormat = "yyyy-MM-dd"
		
		let lastDate = UserDefaults.standard.string(forKey: TokenPriceService.currencyRateLastDateKey)
		let currentDate = tempDateFormatter.string(from: Date())
		
		if lastDate == currentDate, let storedResponse = DiskService.read(type: CoinGeckoExchangeRateResponse.self, fromFileName: TokenPriceService.currencyFileName) {
			updateStoredCurrencyRateValues(rates: storedResponse)
			completion(coinGeckoRates)
			
		} else {
			fetchExhcangeRates(completion: completion)
		}
	}
	
	// TODO: try to migrate to camlKit.NetworkService
	private func fetchExhcangeRates(completion: @escaping ((CoinGeckoExchangeRateResponse?) -> Void)) {
		let urlString = coinGeckoExchangeRatesURL
		
		guard let fullEndpointURL = URL(string: urlString) else {
			os_log(.error, log: .network, "Can't create URL for token pricing service: %@", urlString)
			completion(nil)
			return
		}
		
		var request = URLRequest(url: fullEndpointURL)
		request.addValue("application/json", forHTTPHeaderField: "accept")
		
		session.dataTask(with: request) { [weak self] data, response, serverError in
			if let d = data {
				do {
					let json = try JSONDecoder().decode(CoinGeckoExchangeRateResponse.self, from: d)
					os_log(.debug, log: .network, "Fetched CoinGecko exchange rates")
					
					self?.updateStoredCurrencyRateValues(rates: json)
					if !DiskService.write(encodable: json, toFileName: TokenPriceService.currencyFileName) {
						os_log("Failed to write currencies to disk", log: .currencyRates, type: .error)
					}
					
					completion(json)
					
				} catch {
					os_log(.error, log: .network, "parsing response from CoinGecko exchange rate API")
					completion(nil)
				}
			} else {
				completion(nil)
			}
		}.resume()
	}
	
	func deleteRatesFromDisk() {
		let _ = DiskService.delete(fileName: TokenPriceService.currencyFileName)
	}
	
	private func updateStoredCurrencyRateValues(rates: CoinGeckoExchangeRateResponse) {
		
		// If selected currency empty, check for stored value
		if selectedCurrency == nil {
			selectedCurrency = UserDefaults.standard.string(forKey: TokenPriceService.selectedCurrencyKey)
		}
		
		// If no stored value, check for system value
		if selectedCurrency == nil {
			let systemCurrency = Locale.current.currencyCode?.lowercased() ?? "usd"
			let currencyObject = rates.rates[systemCurrency]
			
			if currencyObject != nil {
				let lowercaseCurrency = systemCurrency.lowercased()
				UserDefaults.standard.setValue(lowercaseCurrency, forKey: TokenPriceService.selectedCurrencyKey)
				selectedCurrency = lowercaseCurrency
				
			} else {
				UserDefaults.standard.setValue("usd", forKey: TokenPriceService.selectedCurrencyKey)
				selectedCurrency = "usd"
			}
		}
		
		
		// Store in object to make accessible
		coinGeckoRates = rates
		
		
		// Record date and time of request to avoid unnecessary polling
		let tempDateFormatter = DateFormatter()
		tempDateFormatter.dateFormat = "yyyy-MM-dd"
		UserDefaults.standard.set(tempDateFormatter.string(from: Date()), forKey: TokenPriceService.currencyRateLastDateKey)
	}
	
	private func updateDexterExchangeRates(completion: @escaping ((ErrorResponse?) -> Void)) {
		if !fetchingRates {
			fetchingRates = true
			
			DependencyManager.shared.tezosNodeClient.getDexterPoolData(forTokens: DependencyManager.shared.dexterTokens) { [weak self] (errors) in
				guard errors?.count != DependencyManager.shared.dexterTokens.count-1 else {
					os_log("All dexter pool data fetches failed", log: .inDEXTer, type: .error)
					self?.fetchingRates = false
					completion(errors?.first)
					return
				}
				
				for (index, token) in DependencyManager.shared.dexterTokens.enumerated() where token.tokenType != .xtz {
					os_log("%@ - PoolData: xtz: %@, token: %@", log: .tezos, type: .debug, token.symbol, token.dexterXTZPool.normalisedRepresentation, token.dexterTokenPool.normalisedRepresentation)
					
					// If we don't have a market rate, set dollarPrice to 0
					if token.dexterXTZPool > XTZAmount.zero() && token.dexterTokenPool > TokenAmount.zeroBalance(decimalPlaces: 0) {
						let marketRate = DexterCalculationService.shared.tokenToXtzMarketRate(dexterXtzPool: token.dexterXTZPool, dexterTokenPool: token.dexterTokenPool)
						DependencyManager.shared.dexterTokens[index].localCurrencyRate = ((self?.selectedCurrencyMultiplier ?? 0) * (marketRate ?? 0))
						
					} else {
						DependencyManager.shared.dexterTokens[index].localCurrencyRate = 0
					}
				}
				
				self?.fetchingRates = false
				completion(nil)
			}
		}
	}
	
	
	
	// MARK: - Shared instance helpers
	public func sharedNumberFormatter() -> NumberFormatter {
		let numberFormatter = NumberFormatter()
		
		// If user has selected a currency from our list, change the symbol
		if let selectedCur = selectedCurrency, let obj = coinGeckoRates?.rates[selectedCur] {
			
			if obj.type == "crypto" {
				// When displaying crypto prices, symbol should always be on the right hand side. But this is not a configurable option
				// Create a locale with a known right hand symbol setting, and change the rest of the settings to the current locale
				let currentLocale = Locale.current
				let cryptoLocale = Locale(identifier: "es_ES")
				
				numberFormatter.locale = cryptoLocale
				numberFormatter.currencyDecimalSeparator = currentLocale.decimalSeparator
				numberFormatter.currencyGroupingSeparator = currentLocale.groupingSeparator
				numberFormatter.decimalSeparator = currentLocale.decimalSeparator
				numberFormatter.groupingSeparator = currentLocale.groupingSeparator
				numberFormatter.currencySymbol = " "+obj.unit
				
				
			} else {
				numberFormatter.currencySymbol = obj.unit
			}
		}
		
		return numberFormatter
	}
	
	public func placeholderCurrencyString() -> String {
		let numberFormatter = sharedNumberFormatter()
		numberFormatter.numberStyle = .currency
		
		return numberFormatter.string(from: 0.00) ?? "0"
	}
	
	public func format(decimal: Decimal, numberStyle: NumberFormatter.Style, maximumFractionDigits: Int? = nil) -> String {
		let numberFormatter = sharedNumberFormatter()
		numberFormatter.numberStyle = numberStyle
		
		if let maxDigits = maximumFractionDigits {
			numberFormatter.maximumFractionDigits = maxDigits
		} else {
			numberFormatter.maximumFractionDigits = (numberStyle == .currency ? 2 : 6)
		}
		
		let decimalRounded = decimal.rounded(scale: numberFormatter.maximumFractionDigits, roundingMode: .down)
		let decimalAsNumber = decimalRounded as NSNumber
		let outputString = numberFormatter.string(from: decimalAsNumber) ?? "0"
		
		return outputString
	}
}
