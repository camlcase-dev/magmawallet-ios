//
//  MoonPayService.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 10/12/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import Keys
import os.log


class MoonPayService {
	
	private let session: URLSession
	private let countriesFileName = "moonpay-countries.json"
	private let baseURL = "https://api.moonpay.com/v3/"
	private let dateFormatter = DateFormatter()
	
	private static let countryLastQueryDateKey = "com.moonpay.country.last.update"
	private static let usaSupportLastQueryDateKey = "com.moonpay.usasupport.last.update"
	private static let usaSupportKey = "com.moonpay.usasupport"
	
	private var countries: [MoonPayCountry] = []
	private var supportInUSA: Bool = false
	
	
	init(session: URLSession) {
		self.session = session
		self.dateFormatter.dateFormat = "yyyy-MM-dd"
	}
	
	func fetchSupportedCountriesIfNeeded(completion: @escaping (() -> Void)) {
		let lastDate = UserDefaults.standard.string(forKey: MoonPayService.countryLastQueryDateKey)
		let currentDate = self.dateFormatter.string(from: Date())
		let readFromDisk = self.readCountriesFromDisk()
		
		guard lastDate != currentDate && !readFromDisk else {
			os_log(.debug, log: .moonpay, "Fetch of countries not needed")
			completion()
			return
		}
		
		// If not updated today, fetch API key and query
		let keys = MagmaWalletKeys()
		#if TESTNET
			let key = keys.moonPayAPIKey_testnet
		#else
			let key = keys.moonPayAPIKey
		#endif
		
		let urlString = baseURL + "countries?apiKey=\(key)"
		guard let fullEndpointURL = URL(string: urlString) else {
			os_log(.error, log: .moonpay, "Can't create URL for moonpay service: %@", urlString)
			completion()
			return
		}
		
		var request = URLRequest(url: fullEndpointURL)
		request.addValue("application/json", forHTTPHeaderField: "accept")
		
		session.dataTask(with: request) { [weak self] data, response, serverError in
			if let d = data {
				do {
					let json = try JSONDecoder().decode([MoonPayCountry].self, from: d)
					os_log(.debug, log: .moonpay, "Fetched MoonPay countries")
					
					UserDefaults.standard.set(self?.dateFormatter.string(from: Date()), forKey: MoonPayService.countryLastQueryDateKey)
					self?.countries = json
					self?.writeCountriesToDisk(countires: json)
					completion()
					
				} catch (let error) {
					os_log(.error, log: .moonpay, "Error: parsing response from moonpay API: %@", "\(error)")
					completion()
				}
			} else {
				completion()
			}
		}.resume()
	}
	
	func fetchSupportedInUSAIfNeeded(completion: @escaping (() -> Void)) {
		let lastDate = UserDefaults.standard.string(forKey: MoonPayService.usaSupportLastQueryDateKey)
		let currentDate = self.dateFormatter.string(from: Date())
		
		guard lastDate != currentDate else {
			self.supportInUSA = UserDefaults.standard.bool(forKey: MoonPayService.usaSupportKey)
			os_log(.debug, log: .moonpay, "Fetch of currencies not needed")
			completion()
			return
		}
		
		// If not updated today, fetch API key and query
		let keys = MagmaWalletKeys()
		#if TESTNET
			let key = keys.moonPayAPIKey_testnet
		#else
			let key = keys.moonPayAPIKey
		#endif
		
		let urlString = baseURL + "currencies?apiKey=\(key)"
		guard let fullEndpointURL = URL(string: urlString) else {
			os_log(.error, log: .moonpay, "Can't create URL for moonpay service: %@", urlString)
			completion()
			return
		}
		
		var request = URLRequest(url: fullEndpointURL)
		request.addValue("application/json", forHTTPHeaderField: "accept")
		
		session.dataTask(with: request) { [weak self] data, response, serverError in
			if let d = data {
				do {
					let json = try JSONDecoder().decode([MoonPayCurrency].self, from: d)
					os_log(.debug, log: .moonpay, "Fetched MoonPay currency")
					
					let filtered = json.filter({ $0.code == "xtz" })
					if let first = filtered.first, let supported = first.isSupportedInUS {
						os_log(.debug, log: .moonpay, "Found XTZ, setting USA support to: %@", "\(supported)")
						self?.supportInUSA = supported
						
					} else {
						os_log(.debug, log: .moonpay, "Counld not find XTZ, setting USA support to false")
						self?.supportInUSA = false
					}
					
					
					UserDefaults.standard.set(self?.dateFormatter.string(from: Date()), forKey: MoonPayService.usaSupportLastQueryDateKey)
					UserDefaults.standard.set(self?.supportInUSA, forKey: MoonPayService.usaSupportKey)
					completion()
					
				} catch (let error) {
					os_log(.error, log: .moonpay, "Error: parsing response from moonpay API: %@", "\(error)")
					completion()
				}
			} else {
				completion()
			}
		}.resume()
	}
	
	func isMoonPayAvailable(forLocale locale: Locale = Locale.current) -> Bool {
		let currentCountry = locale.regionCode
		os_log(.debug, log: .moonpay, "Current country code: %@", currentCountry ?? "-")
		
		if currentCountry == "US" {
			return supportInUSA
			
		} else {
			let filtered = countries.filter({ $0.alpha2 == currentCountry })
			if let first = filtered.first {
				return first.isBuyAllowed
				
			} else {
				return false
			}
		}
	}
	
	func clearMoonPayData() {
		UserDefaults.standard.removeObject(forKey: MoonPayService.countryLastQueryDateKey)
		UserDefaults.standard.removeObject(forKey: MoonPayService.usaSupportLastQueryDateKey)
	}
	
	
	
	// MARK: - Private functions
	
	private func writeCountriesToDisk(countires: [MoonPayCountry]) {
		if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
			let fileURL = dir.appendingPathComponent(countriesFileName)
			
			// Check if there was previously a file, if so remove it
			if FileManager.default.fileExists(atPath: fileURL.absoluteString) {
				do {
					try FileManager.default.removeItem(at: fileURL)
					
				} catch (let error) {
					os_log(.error, log: .moonpay, "Failed to delete previous json file: %@", error.localizedDescription)
				}
			}
			
			// Write the current contents to disk
			do {
				let encodedData = try? JSONEncoder().encode(countires)
				try encodedData?.write(to: fileURL)
				os_log(.debug, log: .moonpay, "Serialised countries to Disk")
				
				return
				
			} catch (let error) {
				os_log(.error, log: .moonpay, "Failed to write to disk: %@", error.localizedDescription)
			}
		}
		
		os_log(.error, log: .moonpay, "Failed to find documents directory")
	}
	
	private func readCountriesFromDisk() -> Bool {
		if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
			let fileURL = dir.appendingPathComponent(countriesFileName)
			
			do {
				let data = try Data(contentsOf: fileURL)
				let countries = try JSONDecoder().decode([MoonPayCountry].self, from: data)
				self.countries = countries
				
				return true
				
			} catch (let error) {
				os_log(.error, log: .moonpay, "Failed to parse list from disk: %@", error.localizedDescription)
			}
		}
		
		os_log(.error, log: .moonpay, "Unable to locate file")
		return false
	}
}
