//
//  HomeService.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 28/04/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import camlKit
import os.log

protocol HomeServiceDelegate: class {
	func didBeginRefreshingData(balancesAndPricesOnly: Bool)
	func didEndRefreshingData(balancesAndPricesOnly: Bool, error: ErrorResponse?)
}

class HomeService {
	
	private let dispatchGroupPrices = DispatchGroup()
	private let dispatchGroupAll = DispatchGroup()
	private var refreshingPrices = false
	private var refreshingAll = false
	private let delegates = NSHashTable<AnyObject>.weakObjects()
	private var autoRefreshTimer: Timer?
	private var compeletedFirstFullQuery = false
	
	func addDelegate(_ obj: HomeServiceDelegate?) {
		if let o = obj {
			delegates.add(o)
		}
	}
	
	func removeDelegate(_ obj: HomeServiceDelegate?) {
		if let o = obj {
			delegates.remove(o)
		}
	}
	
	private func allDelegates() -> [HomeServiceDelegate] {
		return delegates.allObjects as! [HomeServiceDelegate]
	}
	
	func isRefreshingPricesOnly() -> Bool {
		return refreshingPrices
	}
	
	func isRefreshingAll() -> Bool {
		return refreshingAll
	}
	
	func isRefreshingAnything() -> Bool {
		return refreshingPrices || refreshingAll
	}
	
	func refreshPricesAndBalancesOnly() {
		if refreshingPrices || refreshingAll { return }
		
		// Set internal state and notify listeners
		self.refreshingPrices = true
		self.allDelegates().forEach { (delegate) in
			DispatchQueue.main.async {
				delegate.didBeginRefreshingData(balancesAndPricesOnly: true)
			}
		}
		
		// Execute queries and notify listeners when done
		self.executeBalanceAndPriceQueries { [weak self] (error) in
			self?.refreshingPrices = false
			self?.allDelegates().forEach { (delegate) in
				delegate.didEndRefreshingData(balancesAndPricesOnly: true, error: error)
			}
		}
	}
	
	func refreshAllNetworkData(forceRefresh: Bool = false) {
		if refreshingPrices || refreshingAll { return }
		
		guard let walletAddress = DependencyManager.shared.accountService.currentAddress() else {
			self.allDelegates().forEach { (delegate) in
				delegate.didEndRefreshingData(balancesAndPricesOnly: false, error: ErrorResponse.error(string: "", errorType: .unknownWallet))
			}
			return
		}
		
		DispatchQueue.global(qos: .background).async { [weak self] in
			
			// Set internal state and notify listeners
			self?.refreshingAll = true
			self?.allDelegates().forEach { (delegate) in
				DispatchQueue.main.async {
					delegate.didBeginRefreshingData(balancesAndPricesOnly: false)
				}
			}
			
			var tezosError: ErrorResponse? = nil
			let accountService = DependencyManager.shared.accountService
			let tzktClient = DependencyManager.shared.tzktClient
			let contactService = DependencyManager.shared.contactService
			let moonpayService = DependencyManager.shared.moonPayService
			
			
			// Query all balances and prices
			self?.dispatchGroupAll.enter()
			self?.executeBalanceAndPriceQueries(forceRefresh: forceRefresh, completion: { [weak self] (error) in
				tezosError = error
				self?.dispatchGroupAll.leave()
			})
			
			
			// Query Transaction history from TzKT
			self?.dispatchGroupAll.enter()
			os_log(.debug, log: .network, "Fetching Transaction history")
			
			tzktClient.refreshTransactionHistory(forAddress: walletAddress, andSupportedTokens: DependencyManager.shared.balances) {
				os_log(.debug, log: .network, "Finished fetching Transaction history")
				self?.dispatchGroupAll.leave()
			}
			
			
			// Query delegate from network
			self?.dispatchGroupAll.enter()
			os_log(.debug, log: .network, "Fetching delegate for wallet: %@", walletAddress)
			
			accountService.refreshDelegate { [weak self] in
				os_log(.debug, log: .network, "Finished fetching delegate")
				self?.dispatchGroupAll.leave()
			}
			
			
			// Make sure ContactService is finished refreshing
			if contactService.isFetching() {
				self?.dispatchGroupAll.enter()
				os_log(.debug, log: .network, "Checking contact processing status")
				
				contactService.finishedRefreshingCallback = { [weak self] in
					os_log(.debug, log: .network, "Finished processing contacts")
					self?.dispatchGroupAll.leave()
				}
			}
			
			
			// Query moonpay
			self?.dispatchGroupAll.enter()
			self?.dispatchGroupAll.enter()
			os_log(.debug, log: .network, "Fetching moonpay data")
			
			moonpayService.fetchSupportedCountriesIfNeeded {
				os_log(.debug, log: .moonpay, "Finished fetching moonpay countries")
				self?.dispatchGroupAll.leave()
			}
			
			moonpayService.fetchSupportedInUSAIfNeeded {
				os_log(.debug, log: .moonpay, "Finished fetching moonpay usa support")
				self?.dispatchGroupAll.leave()
			}
			
			
			// When all done, call completion
			self?.dispatchGroupAll.notify(queue: .main) { [weak self] in
				os_log(.debug, log: .network, "Finished refreshing network data")
				
				// update state and notify listeners
				self?.refreshingAll = false
				self?.compeletedFirstFullQuery = true
				
				// Disable 60 second background refresh refresh
				//self?.startPolling()
				
				self?.allDelegates().forEach { (delegate) in
					delegate.didEndRefreshingData(balancesAndPricesOnly: false, error: tezosError)
				}
			}
		}
	}
	
	func executeBalanceAndPriceQueries(forceRefresh: Bool = false, completion: @escaping ((ErrorResponse?) -> Void)) {
		DispatchQueue.global(qos: .background).async { [weak self] in
			var error: ErrorResponse? = nil
			let accountService = DependencyManager.shared.accountService
			
			guard let walletAddress = DependencyManager.shared.accountService.currentAddress() else {
				self?.dispatchGroupPrices.notify(queue: .main) {
					completion(ErrorResponse.error(string: "", errorType: .unknownWallet))
				}
				return
			}
			
			// Query wallet balance from tezos RPC
			self?.dispatchGroupPrices.enter()
			os_log(.debug, log: .network, "Fetching Balance for wallet: %@", walletAddress)
			
			accountService.refreshBalances(forceRefresh: forceRefresh) { (result) in
				switch result {
					case .success(_):
						os_log(.debug, log: .network, "Successfully got all balances, Fetching token price list")
						
						// Query token prices from external API and Exchange rates from Dexter
						DependencyManager.shared.tokenPriceService.updateAllPrices { [weak self] (err) in
							os_log(.debug, log: .network, "Finished updating all the prices")
							error = err
							self?.dispatchGroupPrices.leave()
						}
						
					case .failure(let err):
						os_log(.error, log: .network, "Error fetching balances: %@", "\(err)")
						error = err
						self?.dispatchGroupPrices.leave()
				}
			}
			
			// When all done, call completion
			self?.dispatchGroupPrices.notify(queue: .main) {
				completion(error)
			}
		}
	}
	
	
	
	// MARK: - Auto refresh
	func startPolling() {
		// To make things easier and avoid many screens needing to handle this logic,
		// we will trigger `startPolling` in appDelegate `didBecomeActive`, and `stopPolling` in `applicationWillResignActive`
		// but we want to wait until AFTER the first call to `refreshAllNetworkData` before starting this
		// so we set two triggers to make sure a) multiple calls are ignored, b) nothing will be trigger until the first pass has gone through
		if autoRefreshTimer != nil || !compeletedFirstFullQuery {
			return
		}
		
		autoRefreshTimer = Timer.scheduledTimer(withTimeInterval: 60, repeats: true, block: { [weak self] (timer) in
			self?.refreshPricesAndBalancesOnly()
		})
	}
	
	func stopPolling() {
		autoRefreshTimer?.invalidate()
		autoRefreshTimer = nil
	}
}
