//
//  TezosBakersService.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 15/04/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import Kingfisher
import os.log

class TezosBakerService {
	
	private let session: URLSession
	private var dataTask: URLSessionDataTask?
	private var bakers: [TezosBaker] = []
	
	private static let fileName = "bakers.json"
	
	init(session: URLSession = .shared) {
		self.session = session
	}
	
	func allBakers() -> [TezosBaker] {
		if bakers.count == 0 {
			bakers = readBakersFromDisk()
		}
		
		return bakers
	}
	
	func bakerNameForAddress(address: String?) -> String {
		if address == nil {
			return ""
		}
		
		for baker in bakers {
			if baker.address == address {
				return baker.name
			}
		}
		
		return address ?? ""
	}
	
	func checkIfBakerSentTransaction(bakerAddress: String) -> String? {
		for baker in bakers {
			if baker.address == bakerAddress {
				return baker.name
			}
		}
		
		return nil
	}
	
	func fetchAndStoreTezosBakers(completion: @escaping ((Bool) -> Void)) {
		#if targetEnvironment(simulator)
		// TODO: change to 24 caching
		// If running on simulator, no need to flood the API with hundreds of requests per day, if its cached locally
		if allBakers().count > 0 {
			completion(true)
			return
		}
		#endif
		
		guard let bakerSourceURL = URL(string: "https://api.baking-bad.org/v2/bakers?accuracy=precise&timing=stable&health=active") else {
			completion(false)
			return
		}
		
		var request = URLRequest(url: bakerSourceURL)
		request.addValue("application/json", forHTTPHeaderField: "accept")
		
		dataTask = session.dataTask(with: request) { [weak self] data, response, serverError in
			if let d = data {
				self?.processTezosBakers(data: d)
				
				if let bakers = self?.bakers {
					self?.downloadImages(forBakers: bakers)
				}
				
				completion(self?.bakers.count != 0)
				
			} else {
				os_log(.error, log: .bakers, "Failed to fetch data: %@", "\(String(describing: serverError))")
				completion(false)
			}
		}
		
		dataTask?.resume()
	}
}



// MARK: - Helper functions

extension TezosBakerService {
	
	/**
	Take data, decode into Tezos baker objects, filter and cache
	*/
	private func processTezosBakers(data: Data) {
		do {
			let tezosBakers = try JSONDecoder().decode([TezosBaker].self, from: data)
			self.bakers = filterBakers(tezosBakers: tezosBakers)
			writeBakersToDisk(tezosBakers: bakers)
			os_log(.debug, log: .bakers, "fetching bakers complete")
			
		} catch (let error) {
			os_log(.error, log: .bakers, "Failed to parse data: %@", "\(error)")
		}
	}
	
	/**
	Filter bakers so that we don't display any that are full, have a fee that is too high, has a low efficeny etc.
	*/
	private func filterBakers(tezosBakers: [TezosBaker]) -> [TezosBaker] {
		var filteredBakers: [TezosBaker] = []
		for baker in tezosBakers {
			if baker.passesFilter() {
				filteredBakers.append(baker)
			}
		}
		
		filteredBakers = filteredBakers.sorted(by: { $0.estimatedRoi > $1.estimatedRoi })
		
		#if TESTNET
			// While on testnet, sub the array to the length of the testnet list of bakers, then swap addresses
			filteredBakers = Array(filteredBakers[0..<DependencyManager.shared.testnetBakers.count])
			for index in 0..<DependencyManager.shared.testnetBakers.count {
				filteredBakers[index].address = DependencyManager.shared.testnetBakers[index]
			}
		#endif
		
		return filteredBakers
	}
	
	/**
	Cache the baker list to disk to avoid overloading the API
	*/
	private func writeBakersToDisk(tezosBakers: [TezosBaker]) {
		if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
			let fileURL = dir.appendingPathComponent(TezosBakerService.fileName)
			
			// Check if there was previously a file, if so remove it
			deleteBakersFromDisk()
			
			// Write the current contents to disk
			do {
				let encodedData = try? JSONEncoder().encode(tezosBakers)
				try encodedData?.write(to: fileURL)
				
				os_log(.debug, log: .bakers, "Serialised bakers to Disk")
				return
				
			} catch (let error) {
				os_log(.error, log: .bakers, "Failed to write to disk: %@", error.localizedDescription)
			}
		}
		
		os_log(.error, log: .bakers, "Failed to find documents directory")
	}
	
	/**
	Read the cahced bakers from disk
	*/
	func readBakersFromDisk() -> [TezosBaker] {
		if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
			let fileURL = dir.appendingPathComponent(TezosBakerService.fileName)
			
			do {
				let data = try Data(contentsOf: fileURL)
				let tezosbakers = try JSONDecoder().decode([TezosBaker].self, from: data)
				
				return tezosbakers
				
			} catch (let error) {
				os_log(.error, log: .bakers, "Failed to parse list from disk: %@", error.localizedDescription)
				return []
			}
		}
		
		os_log(.error, log: .bakers, "Unable to locate file")
		return []
	}
	
	func deleteBakersFromDisk() {
		if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
			let fileURL = dir.appendingPathComponent(TezosBakerService.fileName)
			
			// Check if there was previously a file, if so remove it
			if FileManager.default.fileExists(atPath: fileURL.path) {
				do {
					try FileManager.default.removeItem(at: fileURL)
					return
					
				} catch (let error) {
					os_log(.error, log: .bakers, "Failed to delete previous json file: %@", error.localizedDescription)
				}
			}
		}
	}
	
	func clearInMemoryBakers() {
		self.bakers = []
	}
	
	/**
	Download baker icons in the background while user is onboarding
	*/
	private func downloadImages(forBakers bakers: [TezosBaker]) {
		ImageCache.default.clearMemoryCache()
		ImageCache.default.clearDiskCache()
		ImageCache.default.diskStorage.config.expiration = .never
		
		ImagePrefetcher(urls: bakers.compactMap({ $0.logoURL }), options: nil, progressBlock: nil) { (skipped, failed, completed) in
			os_log(.debug, log: .bakers, "Baker images downloaded")
			
			if !skipped.isEmpty {
				os_log(.error, log: .bakers, "Some images skipped")
			}
			
			if !failed.isEmpty {
				os_log(.error, log: .bakers, "Some images failed")
			}
			
		}.start()
	}
	
	/**
	Download a single baker image
	*/
	func downloadImage(forBakerURL bakerURL: URL, completion: @escaping ((UIImage?) -> Void)) {
		KingfisherManager.shared.retrieveImage(with: bakerURL) { (result) in
			switch result {
				case .success(let imageResult):
					os_log(.debug, log: .bakers, "Single Baker image downloaded")
					ImageCache.default.store(imageResult.image, forKey: bakerURL.absoluteString)
					completion(imageResult.image)
				
				case .failure(let error):
					os_log(.error, log: .bakers, "Single image failed to download: %@", error.localizedDescription)
					completion(nil)
			}
		}
	}
}
