//
//  SpendingLimitContract.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 28/02/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import os.log

struct SpendingLimitContract {
	
	/*
	func createStorage(withLimit limit: Tez?, andAddress address: String) -> MichelsonParameter? {
		var limitString = ""
		if let lim = limit, lim > Tez.zeroBalance {
			limitString = "{\"prim\":\"Some\",\"args\":[{\"int\":\"\(lim.rpcRepresentation)\"}]}]}"
		} else {
			limitString = "{\"prim\":\"None\"}]}"
		}
		
		let michelineStorage = "{\"prim\":\"Pair\",\"args\":[{\"prim\":\"Pair\",\"args\":[{\"prim\":\"None\"},\(limitString),{\"string\":\"\(address)\"}]}"
		
		return MichelsonService.serializeMichelineString(michelineStorage)
	}
	
	func fetchSpendingLimitContract() -> MichelsonParameter? {
		if let path = Bundle.main.path(forResource: "spending_limit", ofType: "json") {
			do {
				let michelineContract = try String(contentsOf: URL(fileURLWithPath: path), encoding: .utf8)
				
				return MichelsonService.serializeMichelineString(michelineContract)
				
			} catch let error {
				os_log(.error, log: .tezos, "Failed to parse spending_limit json file: %@", error.localizedDescription)
			}
		}
		
		return nil
	}
	*/
}
