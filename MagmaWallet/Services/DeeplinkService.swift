//
//  DeeplinkService.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 08/12/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import SafariServices
import os.log


class DeeplinkService {
	
	func handle(application: UIApplication, url: URL, andOptions: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
		guard let components = NSURLComponents(url: url, resolvingAgainstBaseURL: true),
			  let urlHost = url.host,
			  let root = (application.delegate as? AppDelegate)?.window?.rootViewController as? UINavigationController,
			  let onscreenController = root.viewControllers.last else {
			os_log("Invalid URL: %@", log: .default, type: .error, "\(url.absoluteString)")
			return false
		}
		
		switch urlHost {
			case "wallet":
				return handleWallet(root: root, onscreenController: onscreenController, params: components.queryItems)
				
			default:
				root.popToWallet()
				return true
		}
	}
	
	private func handleWallet(root: UINavigationController, onscreenController: UIViewController, params: [URLQueryItem]?) -> Bool {
		guard let params = params else {
			root.popToWallet()
			return true
		}
		
		// If there is a presented controller, that is an instance of `SFSafariViewController`, just dismiss it and check for MoonPay result query parmas
		if let presented = onscreenController.presentedViewController, presented is SFSafariViewController {
			onscreenController.dismiss(animated: true) {
				for queryItem in params {
					if queryItem.name == "transactionStatus" && queryItem.value == "completed" {
						DependencyManager.shared.homeService.refreshAllNetworkData()
						
						if let tabbar = onscreenController as? TabBarController, let home = tabbar.viewControllers?.first {
							home.view.makeToast("buy_success_toast".localized)
							
						} else if onscreenController is TokenDetailsViewController {
							onscreenController.view.makeToast("buy_success_toast".localized)
							
						} else {
							root.popToWallet()
						}
						
						break
						
					} else if queryItem.name == "transactionStatus" && queryItem.value == "pending" {
						onscreenController.view.makeToast("buy_pending_toast".localized)
						break
						
					} else if queryItem.name == "transactionStatus" && queryItem.value == "error" {
						onscreenController.view.makeToast("buy_error_toast".localized)
						break
					}
				}
			}
		} else {
			// Else, just return to wallet
			root.popToWallet()
		}
		
		return true
	}
}
