//
//  AccountService.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 13/01/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import KeychainSwift
import camlKit
import os.log
import Sentry
import LocalAuthentication

enum OnboardingStatus: String {
	case none
	case walletCreationComplete
	case recoveryPhraseComplete
	case pinCodeCreationComplete
	case biometricChoiceComplete
	case onboardingComplete
}

class AccountService {
	
	private var currentWalletAddress: String?
	private let keychain: KeychainSwift
	
	// Keys
	private static let enhancedErrorLoggingKey = "io.camlcase.tezos.error.logging"
	private static let currentDelegateKey = "io.camlcase.tezos.current.delegate"
	
	private static let onboaringStatusKey = "io.camlcase.tezos.onboarding.status"
	private static let biometricTypeKey = "io.camlcase.tezos.biometrics.type"
	private static let pinCodeKey = "io.camlcase.tezos.pincode"
	private static let pinCodeTempKey = "io.camlcase.tezos.pincode.temp"
	
	required init(keychain: KeychainSwift) {
		self.keychain = keychain
	}
	
	
	
	// MARK: Wallet / state
	func setOnboardingtatus(status: OnboardingStatus) -> Bool {
		return keychain.set(status.rawValue, forKey: AccountService.onboaringStatusKey, withAccess: .accessibleWhenUnlockedThisDeviceOnly)
	}
	
	func getOnboardingStatus() -> OnboardingStatus {
		guard let keychainString = keychain.get(AccountService.onboaringStatusKey), let status = OnboardingStatus(rawValue: keychainString) else {
			return .none
		}
		
		return status
	}
	
	func setPinCode(code: String, temp: Bool) {
		keychain.set(code, forKey: temp ? AccountService.pinCodeTempKey : AccountService.pinCodeKey, withAccess: .accessibleWhenUnlockedThisDeviceOnly)
		
		if temp == false {
			keychain.delete(AccountService.pinCodeTempKey)
		}
	}
	
	func hasPinCodeSet() -> Bool {
		let storedCode = keychain.get(AccountService.pinCodeKey)
		
		return storedCode != nil
	}
	
	func validatePinCode(code: String, temp: Bool) -> Bool {
		let storedCode = keychain.get(temp ? AccountService.pinCodeTempKey : AccountService.pinCodeKey)
		
		return storedCode == code
	}
	
	/**
	As the user is typing, we only want to display an error, if what they have typed so far doesn't match.
	
	e.g. having entered on the first screen "012345" and on confirm typed (so far) "01", function will return true.
	Same example, if on confirm user typed (so far) "10", function will return false
	*/
	func validateInProgressPin(code: String) -> Bool {
		let storedCode = keychain.get(AccountService.pinCodeTempKey) ?? ""
		
		if storedCode.count < 1 || code.count > storedCode.count {
			return false
		}
		
		let storedSubstring = storedCode[storedCode.startIndex..<code.endIndex]
		
		return storedSubstring == code
	}
	
	func isBiometricCapable() -> Bool {
		switch(availableBiometricType()) {
			case .none:
				return false
			case .touchID, .faceID:
				return true
			@unknown default:
				return true
		}
	}
	
	func availableBiometricType() -> LABiometryType {
		let authContext = LAContext()
		let _ = authContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
		
		return authContext.biometryType
	}
	
	func setBiometricChoice(type: LABiometryType) {
		keychain.set("\(type.rawValue)", forKey: AccountService.biometricTypeKey, withAccess: .accessibleWhenUnlockedThisDeviceOnly)
	}
	
	func getBiometricChoice() -> LABiometryType {
		guard let rawValueString = keychain.get(AccountService.biometricTypeKey), let rawValueInt = Int(rawValueString), let type = LABiometryType(rawValue: rawValueInt) else {
			return .none
		}
		
		return type
	}
	
	func deleteAllOnboardinStatus() {
		let result1 = keychain.delete(AccountService.onboaringStatusKey)
		let result2 = keychain.delete(AccountService.biometricTypeKey)
		let result3 = keychain.delete(AccountService.pinCodeKey)
		let result4 = keychain.delete(AccountService.pinCodeTempKey)
		
		// Record failure to remove
		if !result1 || !result2 || !result3 || !result4 {
			os_log(.debug, log: .default, "Unable to delete all keychain: %i, %i, %i, %i", result1, result2, result3, result4)
			
			let event = Sentry.Event(level: .warning)
			event.exceptions = [Exception(value: "Keychain Failure", type: "Deleting onboarding status")]
			event.extra = ["result1": result1, "result2": result2, "result3": result3]
			SentrySDK.capture(event: event)
			
			// As a backup, try again
			keychain.delete(AccountService.onboaringStatusKey)
			keychain.delete(AccountService.biometricTypeKey)
			keychain.delete(AccountService.pinCodeKey)
			keychain.delete(AccountService.pinCodeTempKey)
		}
	}
	
	
	
	// MARK: Wallet
	func createWallet(withPassphrase passphrase: String) -> Bool {
		let clearResult = WalletCacheService().deleteCacheAndKeys()
		let wallet = HDWallet.create(withMnemonicLength: .twelve, passphrase: passphrase)
		let result = WalletCacheService().cache(wallet: wallet!, andPassphrase: passphrase)
		
		if clearResult && result {
			currentWalletAddress = wallet?.address
		} else {
			os_log(.error, log: .tezos, "Failed to create and store wallet")
		}
		
		return result
	}
	
	func importWallet(mnemonic: String, passpharse: String, type: WalletType, derivationPath: String?) -> Bool {
		let clearResult = WalletCacheService().deleteCacheAndKeys()
		var wallet: Wallet? = nil
		
		switch type {
			case .linear:
				wallet = LinearWallet.create(withMnemonic: mnemonic, passphrase: passpharse)
				
			case .hd:
				wallet = HDWallet.create(withMnemonic: mnemonic, passphrase: passpharse, derivationPath: derivationPath ?? HDWallet.defaultDerivationPath)
		}
		
		
		// Check that wallet is created and stored
		guard let w = wallet, clearResult, WalletCacheService().cache(wallet: w, andPassphrase: passpharse) else {
			os_log("Unable to import wallet", log: .tezos, type: .error)
			return false
		}
		
		currentWalletAddress = wallet?.address
		return true
	}
	
	func currentWallet() -> Wallet? {
		return WalletCacheService().fetchPrimaryWallet()
	}
	
	func currentAddress() -> String? {
		// Its safe to store the Tz1 address in memory. check if its in memory and return it.
		// if not fetch from disk, store and return
		if let address = currentWalletAddress {
			return address
			
		} else {
			currentWalletAddress = WalletCacheService().fetchPrimaryWallet()?.address
			return currentWalletAddress
		}
	}
	
	func walletMnemonic() -> String? {
		return WalletCacheService().fetchPrimaryWallet()?.mnemonic
	}
	
	func deleteWallet() -> Bool {
		let keychainResult = keychain.clear()
		let walletCahceResult = WalletCacheService().deleteCacheAndKeys()
		currentWalletAddress = nil
		clearDelegate()
		
		for token in DependencyManager.shared.balances {
			if token.tokenType == .xtz {
				token.balance = XTZAmount.zero()
				
			} else {
				token.balance = TokenAmount.zeroBalance(decimalPlaces: token.decimalPlaces)
			}
		}
		
		// Keychain may be empty as its being used a lot less now. Keychain.clear() will return an error if keychain is empty. This is expected and can be ignored
		return (keychainResult || keychain.lastResultCode == OSStatus(-25300)) && walletCahceResult
	}
	
	
	
	// MARK: Delegate
	func currentDelegate() -> String? {
		return UserDefaults.standard.string(forKey: AccountService.currentDelegateKey)
	}
	
	func refreshDelegate(completion: @escaping () -> Void) {
		guard let currentWallet = self.currentWalletAddress else {
			completion()
			return
		}
		
		DependencyManager.shared.tezosNodeClient.getDelegate(forAddress: currentWallet) { (result) in
			switch result {
				case .success(let delegate):
					UserDefaults.standard.setValue(delegate, forKey: AccountService.currentDelegateKey)
					
				case .failure(let error):
					// Tezos node returns a 404 if the account has no delegate instead of a 200 with an empty response
					if error.httpStatusCode == 404 {
						UserDefaults.standard.removeObject(forKey: AccountService.currentDelegateKey)
						
					} else {
						os_log(.error, log: .tezos, "Error fetching delegate: %@", "\(error)")
					}
			}
			
			completion()
		}
	}
	
	func clearDelegate() {
		UserDefaults.standard.removeObject(forKey: AccountService.currentDelegateKey)
	}
	
	
	
	// MARK: Balances
	func refreshBalances(forceRefresh: Bool, completion: @escaping (Result<Bool, ErrorResponse>) -> Void) {
		guard let currentWallet = self.currentWalletAddress else {
			completion(Result.failure(ErrorResponse.unknownError()))
			return
		}
		
		DependencyManager.shared.betterCallDevClient.getAllBalances(forAddress: currentWallet) { [weak self] (result) in
			switch result {
				case .success(let tokens):
					os_log(.debug, log: .tezos, "All balances updated successfully")
					DependencyManager.shared.balances = tokens
					self?.copyBalancesToDexterTokens()
					
					completion(Result.success(true))
					
				case .failure(let errorResponse):
					os_log(.error, log: .tezos, "Error fetching balances: %@", "\(errorResponse)")
					completion(Result.failure(errorResponse))
			}
		}
	}
	
	// temporary function to copy data across as a workaround. Need to migrate `Token` and `DependencyManager.shared.balances / ...dexterTokens` to come from the same sources and share common logic
	private func copyBalancesToDexterTokens() {
		for balance in DependencyManager.shared.balances {
			for dexterToken in DependencyManager.shared.dexterTokens {
				
				if balance.tokenType == .xtz && dexterToken.tokenType == .xtz {
					dexterToken.balance = balance.balance
					break
					
				} else if balance.tokenContractAddress == dexterToken.tokenContractAddress {
					dexterToken.balance = TokenAmount(fromRpcAmount: balance.balance.rpcRepresentation, decimalPlaces: dexterToken.decimalPlaces) ?? TokenAmount.zero()
					break
				}
			}
		}
	}
	
	func isWalletEmpty() -> Bool {
		for token in DependencyManager.shared.balances {
			if token.balance > TokenAmount.zeroBalance(decimalPlaces: token.decimalPlaces) {
				return false
			}
		}
		
		return true
	}
}
