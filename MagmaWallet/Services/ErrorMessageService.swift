//
//  ErrorMessageService.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 18/03/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import camlKit
import Sentry
import os.log

struct ErrorMessageService {
	
	static func messageDetails(for error: ErrorResponse) -> (key: String, rawMessage: String?) {
		var key: String = "error_generic"
		var rawMessage: String? = nil
		
		switch error.errorType {
			case .unknownError:
				key = "error_transaction_generic"
				rawMessage = error.errorString
				
			case .unknownWallet:
				key = "error_wallet"
			case .unknownBalance:
				key = "error_wallet_balance"
			case .unknownParseError:
				key = "error_transaction_generic"
				
			case .internalApplicationError:
				key = "error_transaction_generic"
				rawMessage = error.errorString
				
			case .noInternetConnection:
				key = "error_transaction_no_internet"
			case .requestTimeOut:
				key = "error_transaction_time_out"
			case .tooManyRedirects:
				key = "error_transaction_generic"
			case .atsUnsecureConnection:
				key = "error_transaction_generic"
			case .exchangeDataOutOfSync:
				key = "error_transaction_exchange_out_of_sync"
			case .exchangeHigherThanZero:
				key = "error_transaction_exchange_higher_than_zero"
			case .insufficientFunds:
				key = "error_transaction_insufficient_funds"
			case .insufficientFundsDelegation:
				key = "error_transaction_insufficient_funds_delegation"
			case .delegationUnchanged:
				key = "error_transaction_delegation_unchanged"
			case .emptyTransaction:
				key = "error_transaction_empty_transaction"
			case .invalidAddress:
				key = "error_transaction_invalid_address"
			case .bakerCantDelegate:
				key = "error_transaction_baker_cant_delegate"
			case .invalidBaker:
				key = "error_transaction_invalid_baker"
			case .exchangeTimeout:
				key = "error_transaction_timeout"
			case .exchangeNotEnoughFa:
				key = "error_not_enough_fa"
			case .exchangeNotEnoughTez:
				key = "error_transaction_not_enough_tez"
				
			case .tokenToTokenUnavailable:
				key = "error_token_to_token"
			case .counterError:
				key = "error_transaction_counter"
			case .dexterNotEnoughFA:
				key = "error_not_enough_fa"
			case .dexterNotEnoughTez:
				key = "error_not_enough_tez"
		}
		
		return (key: key, rawMessage: rawMessage)
	}
	
	static func errorTitleAndMessage(for error: ErrorResponse) -> (title: String, message: String) {
		let details = messageDetails(for: error)
		
		if let rawMessage = details.rawMessage {
			return (title: details.key.localized, message: rawMessage)
			
		} else {
			return (title: "\(details.key)_title".localized, message: details.key.localized)
		}
	}
	
	static func logSentryEvent(for error: ErrorResponse) {
		if Thread.current.isRunningXCTest || (error.httpStatusCode == 404 && error.requestURL?.lastPathComponent == "delegate") {
			return
		}
		
		let event = Sentry.Event(level: .warning)
		if let errorRequest = error.requestURL {
			event.exceptions = [Exception(value: "Network Error", type: error.errorType.rawValue)]
			event.extra = [
				"requestURL": errorRequest.absoluteString,
				"requestJSON": error.requestJSON ?? "-",
				"responseJSON": error.responseJSON ?? "-",
				"httpStatusCode": error.httpStatusCode ?? "-",
				"errorObject": "\(String(describing: error.errorObject))",
				"errorString": error.errorString ?? "-"
			]
			
		} else {
			event.exceptions = [Exception(value: "Application Error", type: error.errorType.rawValue)]
			event.extra = [
				"errorObject": "\(String(describing: error.errorObject))",
				"errorString": error.errorString ?? "-"
			]
		}
		
		SentrySDK.capture(event: event)
	}
}
