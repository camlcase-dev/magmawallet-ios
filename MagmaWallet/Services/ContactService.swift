//
//  ContactService.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 03/09/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import Contacts
import os.log

protocol ContactServiceDelegate: class {
	func finishedFetchingContacts()
}

class ContactService {
	
	enum SaveRequestType {
		case add
		case edit
		case delete
	}
	
	let contactStore: CNContactStore
	let saveRequestType: CNSaveRequest.Type
	
	var finishedRefreshingCallback: (() -> Void)? = nil
	weak var delegate: ContactServiceDelegate? = nil
	
	private let contactFetchQueue: DispatchQueue
	private var isRefreshing: Bool = false {
		didSet {
			if isRefreshing == false, let callback = finishedRefreshingCallback {
				callback()
				finishedRefreshingCallback = nil
			}
		}
	}
	private var contacts: [TezosContact] = []
	private static let fileName = "contacts.json"
	private let contactKeys: [CNKeyDescriptor] = [
		// We are only interested in Name and Instant Messages (where we store the Tezos Address)
		CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
		CNContactInstantMessageAddressesKey as CNKeyDescriptor,
		
		// Edit functionality depends on whether the contact was only created to hold a Tezos Address or has other data
		// This data will be ignored, we are only interested in if it is present or not, to determine if its a pre-existing contact
		CNContactPhoneNumbersKey as CNKeyDescriptor,
		CNContactEmailAddressesKey as CNKeyDescriptor,
		CNContactUrlAddressesKey as CNKeyDescriptor,
		CNContactPostalAddressesKey as CNKeyDescriptor,
		CNContactSocialProfilesKey as CNKeyDescriptor
	]
	
	
	init(contactStore: CNContactStore, saveRequestType: CNSaveRequest.Type) {
		self.contactStore = contactStore
		self.saveRequestType = saveRequestType
		self.contactFetchQueue = DispatchQueue(label: "contactsservice.contacts", qos: .background, attributes: [], autoreleaseFrequency: .inherit, target: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(fetchContacts), name: .CNContactStoreDidChange, object: nil)
	}
	
	deinit {
		NotificationCenter.default.removeObserver(self, name: .CNContactStoreDidChange, object: nil)
	}
	
	func currentAuthorizationStatus() -> CNAuthorizationStatus {
		return type(of: contactStore).authorizationStatus(for: CNEntityType.contacts)
	}
	
	func requestAccessIfNeeded(completion: @escaping ((Bool, Error?) -> Void)) {
		let authStatus = currentAuthorizationStatus()
		if authStatus == .notDetermined {
			(UIApplication.shared.delegate as? AppDelegate)?.launchingSystemPopup = true
			contactStore.requestAccess(for: CNEntityType.contacts) { (granted, error) in
				if let err = error {
					os_log("Error requesting contacts permissions: %@", log: .contacts, type: .error, "\(err)")
				}
				DispatchQueue.main.async { completion(granted, error) }
			}
			
		} else if authStatus == .authorized {
			DispatchQueue.main.async { completion(true, nil) }
			
		} else {
			DispatchQueue.main.async { completion(false, nil) }
		}
	}
	
	func isFetching() -> Bool {
		return isRefreshing
	}
	
	func getContacts() -> [TezosContact] {
		if contacts.count == 0 {
			contacts = readContactsFromDisk()
		}
		
		return contacts
	}
	
	/**
	Used for getting the contact when tapping on a UITableViewCell
	*/
	func getContact(atIndex index: Int) -> TezosContact? {
		if contacts.count > 0 && index >= 0 && index < contacts.count {
			return contacts[index]
		}
		
		return nil
	}
	
	/**
	Return the contacts name or the address passed in if no contact found
	*/
	func displayStringForAddress(_ address: String?) -> String {
		guard let address = address else {
			return ""
		}
		
		for contact in contacts {
			if contact.tezosAddress == address {
				return contact.fullName
			}
		}
		
		return address.tezosAddressFormat()
	}
	
	@objc func fetchContacts() {
		if isRefreshing || currentAuthorizationStatus() != .authorized {
			return
		}
		
		isRefreshing = true
		let fetchRequest = CNContactFetchRequest(keysToFetch: contactKeys)
		fetchRequest.sortOrder = CNContactSortOrder.userDefault
		
		contactFetchQueue.async { [weak self] in
			os_log(.debug, log: .contacts, "Fetching contacts")
			
			do {
				var tempContacts: [TezosContact] = []
				try self?.contactStore.enumerateContacts(with: fetchRequest) { (contact, bool) in
					if let contact = TezosContact(withContact: contact) {
						tempContacts.append(contact)
					}
				}
				
				#if TESTNET
					self?.contacts = tempContacts.filter { $0.networkProtocol != .mainnet }
				#else
					self?.contacts = tempContacts.filter { $0.networkProtocol == .mainnet }
				#endif
				
				self?.writeContactsToDisk()
				
				// Add a brief delay to counter timing issues and multiple calls to update contacts from the OS contact change event
				DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
					self?.isRefreshing = false
					self?.delegate?.finishedFetchingContacts()
				}
				
			} catch (let error) {
				os_log(.error, log: .contacts, "Error fetching contacts: %@", "\(error)")
				
				DispatchQueue.main.async {
					self?.isRefreshing = false
					self?.delegate?.finishedFetchingContacts()
				}
			}
		}
	}
	
	func writeContactsToDisk() {
		if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
			let fileURL = dir.appendingPathComponent(ContactService.fileName)
			
			// Check if there was previously a file, if so remove it
			deleteContactsFromDisk()
			
			// Write the current contents to disk
			do {
				let encodedData = try? JSONEncoder().encode(contacts)
				try encodedData?.write(to: fileURL)
				
				os_log(.debug, log: .contacts, "Serialised contacts to Disk")
				return
				
			} catch (let error) {
				os_log(.error, log: .contacts, "Failed to write to disk: %@", error.localizedDescription)
			}
		}
		
		os_log(.error, log: .contacts, "Failed to find documents directory")
	}
	
	func readContactsFromDisk() -> [TezosContact] {
		if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
			let fileURL = dir.appendingPathComponent(ContactService.fileName)
			
			do {
				let data = try Data(contentsOf: fileURL)
				let dataArray = try JSONDecoder().decode([TezosContact].self, from: data)
				
				return dataArray
				
			} catch (let error) {
				os_log(.error, log: .contacts, "Failed to parse list from disk: %@", error.localizedDescription)
				return []
			}
		}
		
		os_log(.error, log: .contacts, "Unable to locate file")
		return []
	}
	
	func deleteContactsFromDisk() {
		if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
			let fileURL = dir.appendingPathComponent(ContactService.fileName)
			
			// Check if there was previously a file, if so remove it
			if FileManager.default.fileExists(atPath: fileURL.path) {
				do {
					try FileManager.default.removeItem(at: fileURL)
					return
					
				} catch (let error) {
					os_log(.error, log: .contacts, "Failed to delete previous json file: %@", "\(error)")
				}
			}
		}
	}
	
	func clearInMemoryContacts() {
		self.contacts = []
	}
	
	func editContact(withContactId contactId: String, address: String, andName name: String?) -> Bool {
		guard let contact = try? contactStore.unifiedContact(withIdentifier: contactId, keysToFetch: contactKeys),
			let mutableContact = contact.mutableCopy() as? CNMutableContact else {
				return false
		}
		
		if let n = name, let tempTezosContact = TezosContact(withContact: mutableContact), tempTezosContact.containsOnlyMagmaData {
			mutableContact.givenName = n
		}
		
		let addressString = TezosContact.createSystemContactAddress(address)
		let instantMessageAddress = CNInstantMessageAddress(username: addressString, service: TezosContact.systemContactLabel)
		let instantMessage = CNLabeledValue<CNInstantMessageAddress>(label: TezosContact.systemContactLabel, value: instantMessageAddress)
		
		if let existing = TezosContact.existingTezosAddress(contact) {
			mutableContact.instantMessageAddresses[existing.instantMessageIndex] = instantMessage
			
		} else {
			mutableContact.instantMessageAddresses.append(instantMessage)
		}
		
		return createAndRunSaveRequest(forContact: mutableContact, ofType: .edit)
	}
	
	func addNewContact(name: String, address: String) -> Bool {
		let contact = CNMutableContact()
		contact.givenName = name
		
		let addressString = TezosContact.createSystemContactAddress(address)
		let instantMessageAddress = CNInstantMessageAddress(username: addressString, service: TezosContact.systemContactLabel)
		let intantMessage = CNLabeledValue<CNInstantMessageAddress>(label: TezosContact.systemContactLabel, value: instantMessageAddress)
		contact.instantMessageAddresses.append(intantMessage)
		
		return createAndRunSaveRequest(forContact: contact, ofType: .add)
	}
	
	func deleteContact(withContactId contactId: String) -> Bool {
		guard let contact = try? contactStore.unifiedContact(withIdentifier: contactId, keysToFetch: contactKeys),
			let mutableContact = contact.mutableCopy() as? CNMutableContact,
			let tempTezosContact = TezosContact(withContact: mutableContact),
			tempTezosContact.containsOnlyMagmaData else {
				return false
		}
		
		return createAndRunSaveRequest(forContact: mutableContact, ofType: .delete)
	}
	
	func removeCurrentAddressFromContact(withContactId contactId: String) -> Bool {
		guard let contact = try? contactStore.unifiedContact(withIdentifier: contactId, keysToFetch: contactKeys),
			let mutableContact = contact.mutableCopy() as? CNMutableContact else {
				return false
		}
		
		if let existing = TezosContact.existingTezosAddress(contact) {
			mutableContact.instantMessageAddresses.remove(at: existing.instantMessageIndex)
		}
		
		return createAndRunSaveRequest(forContact: mutableContact, ofType: .edit)
	}
	
	func createAndRunSaveRequest(forContact contact: CNMutableContact?, ofType type: SaveRequestType) -> Bool {
		guard let contact = contact else {
			return false
		}
		
		let saveRequest = saveRequestType.self.init()
		
		switch type {
			case .add:
				saveRequest.add(contact, toContainerWithIdentifier: contactStore.defaultContainerIdentifier())
			
			case .edit:
				saveRequest.update(contact)
			
			case .delete:
				saveRequest.delete(contact)
		}
		
		do {
			try contactStore.execute(saveRequest)
			return true
			
		} catch (let error) {
			os_log(.error, log: .contacts, "Unable to update contact: %@", "\(error)")
			return false
		}
	}
}
