//
//  TransactionService.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 04/02/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import camlKit
import os.log

enum TransactionType {
	case send
	case exchange
	case delegate
}

enum WalletCreationType {
	case new
	case `import`
}

enum ExchangeSide {
	case primary
	case secondary
}

struct TransactionServiceSendData {
	var recipientAddress: String?
	var recipientName: String?
	var selectedTokenIndex = -1
	var amountToSend = TokenAmount.zeroBalance(decimalPlaces: 0)
	var operations: [camlKit.Operation] = []
	var inProgressOpHash: String? = nil
	
	func recipientDisplay() -> String {
		if let name = recipientName {
			return name
		}
		
		return recipientAddress ?? ""
	}
}

struct TransactionServiceExchangeData {
	static let slippageLimitDefaultsKey = "com.magma.exhcnage.slippage.limit"
	static let timeoutLimitDefaultsKey = "com.magma.exhcnage.timeout.limit"
	
	var tokenSelectionInProgress: ExchangeSide = .primary
	var selectedPrimaryTokenIndex = 0
	var selectedSecondaryTokenIndex = 1
	var amountToExchange = TokenAmount.zeroBalance(decimalPlaces: 0)
	var conversion = DexterCalculationResult(expected: TokenAmount.zero(), minimum: TokenAmount.zero(), liquidityFee: TokenAmount.zero(), displayExchangeRate: 0, displayPriceImpact: 0)
	var operations: [camlKit.Operation] = []
	var inProgressOpHash: String? = nil
	
	var slippageLimit: Double {
		didSet {
			UserDefaults.standard.set(slippageLimit, forKey: TransactionServiceExchangeData.slippageLimitDefaultsKey)
		}
	}
	
	var timeoutLimit: Int {
		didSet {
			UserDefaults.standard.set(timeoutLimit, forKey: TransactionServiceExchangeData.timeoutLimitDefaultsKey)
		}
	}
	
	init() {
		if UserDefaults.standard.object(forKey: TransactionServiceExchangeData.slippageLimitDefaultsKey) != nil {
			slippageLimit = UserDefaults.standard.double(forKey: TransactionServiceExchangeData.slippageLimitDefaultsKey)
		} else {
			slippageLimit = ExchangeAdvancedOptionsViewController.defaultSlippageOptions[1]
		}
		
		if UserDefaults.standard.object(forKey: TransactionServiceExchangeData.timeoutLimitDefaultsKey) != nil {
			timeoutLimit = UserDefaults.standard.integer(forKey: TransactionServiceExchangeData.timeoutLimitDefaultsKey)
		} else {
			timeoutLimit = ExchangeAdvancedOptionsViewController.defaultTimeout
		}
	}
}

struct TransactionServiceDelegateData {
	var selectedBakerAddress: String? = nil
	var inProgressOpHash: String? = nil
}

struct TransactionServiceWalletCreationData {
	var type: WalletCreationType = .new
	var passphrase: String = ""
	var mnemonic: String = ""
	var derivationPath: String = ""
	
	var importlinearWalletAddress = ""
	var importHdWalletAddress = ""
}

class TransactionService {
	
	// MARK: Constants
	public static let oneMutez = XTZAmount(fromNormalisedAmount: 0.000001)
	
	
	// MARK: State
	var transactionType: TransactionType = .send
	var sendData = TransactionServiceSendData()
	var exchangeData = TransactionServiceExchangeData()
	var delegateData = TransactionServiceDelegateData()
	var walletCreationData = TransactionServiceWalletCreationData()
	
	
	func updateAmountToExchange(amount: TokenAmount) {
		exchangeData.amountToExchange = amount
		
		let tokenFrom = DependencyManager.shared.dexterTokens[exchangeData.selectedPrimaryTokenIndex]
		let tokenTo = DependencyManager.shared.dexterTokens[exchangeData.selectedSecondaryTokenIndex]
		
		if amount > TokenAmount.zeroBalance(decimalPlaces: 0) {
			
			// If an amount has been entered, figure out if its XtzToToken or tokenToXtz and then call the appropriate camlKit calcualtion function
			let service = DexterCalculationService.shared
			
			if tokenFrom.tokenType == .xtz {
				guard let result = service.calculateXtzToToken(xtzToSell: amount.toXTZAmount(), dexterXtzPool: tokenTo.dexterXTZPool, dexterTokenPool: tokenTo.dexterTokenPool, maxSlippage: exchangeData.slippageLimit / 100) else {
					os_log("Unable to calculate XtzToToken", log: .tezos, type: .error)
					return
				}
				exchangeData.conversion = result
				
			} else {
				guard let result = service.calcualteTokenToXTZ(tokenToSell: exchangeData.amountToExchange, dexterXtzPool: tokenFrom.dexterXTZPool, dexterTokenPool: tokenFrom.dexterTokenPool, maxSlippage: exchangeData.slippageLimit / 100) else {
					os_log("Unable to calculate tokenToXTZ", log: .tezos, type: .error)
					return
				}
				
				exchangeData.conversion = result
			}
			
		} else {
			// If no amount has been entered, just display the market rate for the given swap
			var marketRate = Decimal(0)
			if tokenFrom.tokenType == .xtz {
				marketRate = DexterCalculationService.shared.xtzToTokenMarketRate(dexterXtzPool: tokenTo.dexterXTZPool, dexterTokenPool: tokenTo.dexterTokenPool) ?? 0
			} else {
				marketRate = DexterCalculationService.shared.tokenToXtzMarketRate(dexterXtzPool: tokenFrom.dexterXTZPool, dexterTokenPool: tokenFrom.dexterTokenPool) ?? 0
			}
			
			exchangeData.conversion = DexterCalculationResult(expected: TokenAmount.zero(), minimum: TokenAmount.zero(), liquidityFee: TokenAmount.zero(), displayExchangeRate: marketRate, displayPriceImpact: 0)
		}
	}
	
	func resetStoredData() {
		transactionType = .send
		sendData = TransactionServiceSendData()
		exchangeData = TransactionServiceExchangeData()
		delegateData = TransactionServiceDelegateData()
		
		// Scrub senstivie data, before creating new object
		walletCreationData.mnemonic = String(repeating: "0", count: walletCreationData.mnemonic.count)
		walletCreationData.passphrase = String(repeating: "0", count: walletCreationData.passphrase.count)
		walletCreationData.derivationPath = String(repeating: "0", count: walletCreationData.derivationPath.count)
		walletCreationData = TransactionServiceWalletCreationData()
		
		updateAmountToExchange(amount: TokenAmount.zeroBalance(decimalPlaces: 0))
	}
	
	func needsToChooseToken(completion: @escaping ((Bool) -> Void)) {
		let viewModel = ChooseTokenViewModel()
		viewModel.update { [weak self] (response) in
			completion((self?.sendData.selectedTokenIndex == -1 && viewModel.tableViewData[0].cellModels.count > 1))
		}
	}
}
