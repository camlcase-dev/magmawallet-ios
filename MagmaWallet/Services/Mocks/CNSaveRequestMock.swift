//
//  CNSaveRequestMock.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 14/09/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Contacts

class CNSaveRequestMock: CNSaveRequest {
	
	var transactionType: ContactService.SaveRequestType = .add
	var contact: CNMutableContact = CNMutableContact()
	
	override func add(_ contact: CNMutableContact, toContainerWithIdentifier identifier: String?) {
		self.contact = contact
		self.transactionType = .add
		
		super.add(contact, toContainerWithIdentifier: identifier)
	}
	
	override func update(_ contact: CNMutableContact) {
		self.contact = contact
		self.transactionType = .edit
		
		super.update(contact)
	}
	
	override func delete(_ contact: CNMutableContact) {
		self.contact = contact
		self.transactionType = .delete
		
		super.delete(contact)
	}
}
