//
//  URLSessionMock.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 12/06/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import os.log

class URLSessionMock: URLSession {
	
	override class var shared: URLSessionMock {
		get {
			return sharedMock
		}
	}
	
	public var triggerLowBalance = false
	
	private static let sharedMock = URLSessionMock()
	private var tzktOperationSearchCount = 0
	private var lastForgeRequestData = Data()
	
	typealias CompletionHandler = (Data?, URLResponse?, Error?) -> Void
	
	override func dataTask(with url: URL, completionHandler: @escaping CompletionHandler) -> URLSessionDataTask {
		return dataTask(with: URLRequest(url: url), completionHandler: completionHandler)
	}
	
	override func dataTask(with request: URLRequest, completionHandler: @escaping CompletionHandler) -> URLSessionDataTask {
		let mockResponse = responseFor(request: request)
		
		return URLSessionDataTaskMock {
			let mockResponseObject = HTTPURLResponse(url: request.url!, statusCode: mockResponse.statusCode, httpVersion: "mock", headerFields: nil)
			completionHandler(mockResponse.data, mockResponseObject, mockResponse.error)
		}
	}
	
	
	
	// MARK: - Helpers
	
	private func responseFor(request: URLRequest?) -> (data: Data?, statusCode: Int, error: Error?) {
		guard let request = request else {
			return (data: nil, statusCode: 500, error: nil)
		}
		
		// Check for error objects, or error JSON Data to return
		if let url = request.url {
			
			let errResponse = URLSessionMockErrorGenerator.shared.error(forURL: url)
			let errDataResponse = URLSessionMockErrorGenerator.shared.errorData(forURL: url)
			
			if let err = errResponse.error {
				let statusCode = errResponse.statusCode ?? 500
				return (data: nil, statusCode: statusCode, error: err)
				
			} else if let errData = errDataResponse.data {
				let statusCode = errDataResponse.statusCode ?? 500
				return (data: errData, statusCode: statusCode, error: nil)
			}
			
		}
		
		return (data: dataFor(request: request), statusCode: 200, error: nil)
	}
	
	private func dataFor(request: URLRequest) -> Data {
		guard let url = request.url else {
			fatalError("Request has no URL: \(request)")
		}
		
		os_log(.debug, log: .mock, "Attempting to mock: %@", url.absoluteString)
		
		if isCamlKitURL(url: url) {
			os_log(.debug, log: .mock, "Mocking URL: TezosKit")
			return dataForCamlKitRequest(request)
			
		} else if isTzktURL(url: url) {
			os_log(.debug, log: .mock, "Mocking URL: TZKT or Better Call Dev")
			return dataForTzktRequest(request)
			
		} else if url.absoluteString == "https://api.baking-bad.org/v2/bakers?accuracy=precise&timing=stable&health=active" {
			os_log(.debug, log: .mock, "Mocking URL: bakers")
			return URLSessionMock.generateDataFromStubFile(filename: "bakers-stub")
			
		} else if url.absoluteString.contains("https://dev.indexter.dexter.exchange/fa12/") && url.absoluteString.contains("/allowance/") {
			os_log(.debug, log: .mock, "Mocking URL: inDEXTer")
			return URLSessionMock.generateDataFromStubFile(filename: "InDEXTerAllowance", fileExtension: "txt")
			
		} else if url.absoluteString.contains("https://dev.indexter.dexter.exchange/fa12/") && url.absoluteString.contains("/balance/") {
			os_log(.debug, log: .mock, "Mocking URL: inDEXTer")
			return URLSessionMock.generateDataFromStubFile(filename: "InDEXTerBalance", fileExtension: "txt")
			
		} else if url.absoluteString == "https://api.coingecko.com/api/v3/exchange_rates" {
			os_log(.debug, log: .mock, "Mocking URL: coingecko exchange rates")
			return URLSessionMock.generateDataFromStubFile(filename: "coingecko-exchangerates")
			
		} else if url.absoluteString == "https://api.coingecko.com/api/v3/simple/price?ids=tezos&vs_currencies=usd" {
			os_log(.debug, log: .mock, "Mocking URL: coingecko tezos usd rate")
			return URLSessionMock.generateDataFromStubFile(filename: "coingecko-tezos-usd-rate")
			
		} else if url.absoluteString == "https://api.coingecko.com/api/v3/simple/price?ids=tezos&vs_currencies=eur" {
			os_log(.debug, log: .mock, "Mocking URL: coingecko tezos usd rate")
			return URLSessionMock.generateDataFromStubFile(filename: "coingecko-tezos-eur-rate")
			
		} else if url.absoluteString.contains("https://api.moonpay.com/v3/countries") {
			os_log(.debug, log: .mock, "Mocking URL: MoonPay countries")
			return URLSessionMock.generateDataFromStubFile(filename: "moonpay-countries")
			
		}  else if url.absoluteString.contains("https://api.moonpay.com/v3/currencies") {
			os_log(.debug, log: .mock, "Mocking URL: MoonPay currencies")
			return URLSessionMock.generateDataFromStubFile(filename: "moonpay-currencies")
		}
		
		fatalError("Mock data requested for unhandled URL: \(url.absoluteString)")
	}
	
	private func dataForCamlKitRequest(_ request: URLRequest) -> Data {
		guard let url = request.url else {
			fatalError("Request has no URL: \(request)")
		}
		
		// Dexter contract XTZ and Token Balances
		if url.lastPathComponent == "balance" && url.path.contains("KT1") {
			return URLSessionMock.generateDataFromStubFile(filename: "dexterContractXTZBalance-stub", fileExtension: "txt")
			
		} else if url.lastPathComponent == "storage" && url.path.contains("KT1"){
			return URLSessionMock.generateDataFromStubFile(filename: "dexterContractTokenBalance-stub")
		}
		
		// Fetch delegate
		else if url.lastPathComponent == "delegate" {
			return "\"\(DependencyManager.shared.testnetBakers[0])\"".data(using: .utf8) ?? Data()
		}
		
		
		// Fetch wallet balance
		else if url.lastPathComponent == "balance" && url.path.contains("tz1") {
			if URLSessionMock.shared.triggerLowBalance {
				return URLSessionMock.generateDataFromStubFile(filename: "fetchBalanceLow-stub", fileExtension: "txt")
			} else {
				return URLSessionMock.generateDataFromStubFile(filename: "fetchBalance-stub", fileExtension: "txt")
			}
		}
		
		
		// Metadata for requests
		else if url.lastPathComponent == "manager_key" {
			return URLSessionMock.generateDataFromStubFile(filename: "fetchManagerKey-stub", fileExtension: "txt")
			
		} else if url.lastPathComponent == "counter" {
			return URLSessionMock.generateDataFromStubFile(filename: "fetchCounter-stub", fileExtension: "txt")
			
		} else if url.lastPathComponent == "head" {
			return URLSessionMock.generateDataFromStubFile(filename: "fetchHead-stub", fileExtension: "json")
			
		} else if url.lastPathComponent == "version" {
			return URLSessionMock.generateDataFromStubFile(filename: "version-stub", fileExtension: "json")
			
		} else if url.lastPathComponent == "constants" {
			return URLSessionMock.generateDataFromStubFile(filename: "constants-stub", fileExtension: "json")
			
		} else if url.lastPathComponent == "run_operation" {
			if let json = try? JSONSerialization.jsonObject(with: request.httpBody ?? Data(), options: .allowFragments) as? [String: Any] {
				let contentsArray = (json["operation"] as? [String: Any])?["contents"] as? [[String: Any]]
				if contentsArray?.count == 2 {
					return URLSessionMock.generateDataFromStubFile(filename: "runOperation2-stub", fileExtension: "json")
				} else {
					return URLSessionMock.generateDataFromStubFile(filename: "runOperation-stub", fileExtension: "json")
				}
			}
			
			return URLSessionMock.generateDataFromStubFile(filename: "runOperation-stub", fileExtension: "json")
			
		} else if url.absoluteString.contains("/helpers/forge/operations") {
			
			// Remove the "counter" as it doesn't get returned from the network, and wrap it in a JSON array
			let requestBodyAsString = String(data: request.httpBody!, encoding: .utf8)!
			let updatedString = "[\(requestBodyAsString)]"
			
			URLSessionMock.shared.lastForgeRequestData = updatedString.data(using: .utf8)!
			
			return URLSessionMock.generateDataFromStubFile(filename: "forgeOperation-stub", fileExtension: "txt")
		}
		else if url.absoluteString.contains("/helpers/parse/operations") {
			return URLSessionMock.shared.lastForgeRequestData
		}
		else if url.absoluteString.contains("/helpers/preapply/operations") {
			return URLSessionMock.generateDataFromStubFile(filename: "preapplyOperation-stub", fileExtension: "json")
		}
		else if url.absoluteString.contains("/injection/operation") {
			return URLSessionMock.generateDataFromStubFile(filename: "injection-stub", fileExtension: "txt")
		}
		
		
		// Unhandled Error
		fatalError("Mock data requested for unhandled TezosKit URL: \(url.absoluteString)")
	}
	
	private func dataForTzktRequest(_ request: URLRequest) -> Data {
		guard let url = request.url else {
			fatalError("Request has no URL: \(request)")
		}
		
		// Transaction History
		let components = url.pathComponents
		
		if components.count == 4 && components[2] == "operations" && components[3] != "transactions" {
			// Block search, simulate waiting for a second request / response
			if tzktOperationSearchCount == 0 {
				tzktOperationSearchCount += 1
				return "[]".data(using: .utf8) ?? Data()
			} else {
				tzktOperationSearchCount = 0
				return URLSessionMock.generateDataFromStubFile(filename: "tzkt-operation-search")
			}
			
		} else if components[2] == "accounts" {
			let tzbtcContract = DependencyManager.shared.dexterTokens[1].tokenContractAddress ?? ""
			let tzbtcDexter = DependencyManager.shared.dexterTokens[1].dexterExchangeAddress ?? ""
			
			// Transaction history main
			return URLSessionMock.generateDataFromStubFile(filename: "transaction-history-main", replacing: ["<TZBTC-CONTRACT-ADDRESS>": tzbtcContract, "<TZBTC-DEXTER-ADDRESS>": tzbtcDexter])
			
		} else if components.count == 4 && components[2] == "operations" && components[3] == "transactions" {
			let tzbtcContract = DependencyManager.shared.dexterTokens[1].tokenContractAddress ?? ""
			
			// Transaction history native receive
			return URLSessionMock.generateDataFromStubFile(filename: "transaction-history-native-receive", replacing: ["<TZBTC-CONTRACT-ADDRESS>": tzbtcContract])
			
		} else if url.host == "api.better-call.dev" && components[2] != "account" {
			
			// Better call dev more detailed error
			return URLSessionMock.generateDataFromStubFile(filename: "better-call-dev-error")
			
		} else if url.host == "api.better-call.dev" && components[2] == "account" && components.last != "token_balances" {
			return URLSessionMock.generateDataFromStubFile(filename: "better-call-dev-account")
			
		} else if url.host == "api.better-call.dev" && components.last == "token_balances" {
			return URLSessionMock.generateDataFromStubFile(filename: "better-call-dev-token-balances")
		}
		
		// Unhandled Error
		fatalError("Mock data requested for unhandled Tzkt or Better call Dev URL: \(url.absoluteString)")
	}
	
	private func isCamlKitURL(url: URL) -> Bool {
		let scheme = url.scheme ?? ""
		let host = url.host ?? ""
		let baseURL = scheme+"://"+host
		
		if baseURL == DependencyManager.shared.tezosClientConfig.primaryNodeURL.absoluteString.prefix(baseURL.count) || baseURL == DependencyManager.shared.tezosClientConfig.parseNodeURL.absoluteString.prefix(baseURL.count) {
			return true
		}
		
		return false
	}
	
	private func isTzktURL(url: URL) -> Bool {
		let scheme = url.scheme ?? ""
		let host = url.host ?? ""
		let baseURL = scheme+"://"+host
		
		if baseURL == DependencyManager.shared.tezosClientConfig.tzktURL.absoluteString.prefix(baseURL.count) || baseURL == DependencyManager.shared.tezosClientConfig.betterCallDevURL.absoluteString.prefix(baseURL.count) {
			return true
		}
		
		return false
	}
	
	private func doesPredicatesContainDestination(predicates: [[String: Any]]) -> Bool {
		for predicate in predicates {
			if let field = predicate["field"] as? String, field == "destination" {
				return true
			}
		}
		
		return false
	}
	
	class func generateDataFromStubFile(filename: String, fileExtension: String = "json") -> Data {
		guard let path = Bundle.init(for: URLSessionMock.self).path(forResource: filename, ofType: fileExtension),
			let stubData = try? Data(contentsOf: URL(fileURLWithPath: path)) else {
				fatalError("Can't find or read `\(filename).\(fileExtension)`")
		}
		
		return stubData
	}
	
	class func generateDataFromStubFile(filename: String, fileExtension: String = "json", replacing: [String: String]) -> Data {
		let data = generateDataFromStubFile(filename: filename, fileExtension: fileExtension)
		
		guard let dataString = String(data: data, encoding: .utf8) else {
			fatalError("Can't find or read `\(filename).\(fileExtension)`")
		}
		
		var updatedString = dataString
		replacing.keys.forEach { (key) in
			updatedString = updatedString.replacingOccurrences(of: key, with: replacing[key] ?? "")
		}
		
		return updatedString.data(using: .utf8) ?? Data()
	}
}
