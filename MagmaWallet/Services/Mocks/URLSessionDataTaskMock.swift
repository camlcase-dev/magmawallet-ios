//
//  URLSessionDataTaskMock.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 16/06/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation

class URLSessionDataTaskMock: URLSessionDataTask {
	private let closure: () -> Void
	
	init(closure: @escaping () -> Void) {
		self.closure = closure
	}
	
	// We override the 'resume' method and simply call our closure
	// instead of actually resuming any task.
	override func resume() {
		closure()
	}
}
