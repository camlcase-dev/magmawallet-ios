//
//  CNContactStoreMock.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 14/09/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import Contacts

enum CNContactStoreMockError: Error {
	case IdentifierNeedsToBeInt
	case NeedCNSaveRequestMock
	case NicknameAsInt
}

public class CNContactStoreMock: CNContactStore {
	
	private var contacts: [CNContact] = []
	
	public static let mockAddress1 = "tz1abc123def456"
	public static let mockAddress2 = "tz1def456abc123"
	
	override init() {
		super.init()
		contacts = createMockContacts()
	}
	
	
	// MARK: - ContactServiceContactStore
	
	public override class func authorizationStatus(for entityType: CNEntityType) -> CNAuthorizationStatus {
		return .authorized
	}
	
	public override func requestAccess(for entityType: CNEntityType, completionHandler: @escaping (Bool, Error?) -> Void) {
		completionHandler(true, nil)
	}
	
	public override func enumerateContacts(with fetchRequest: CNContactFetchRequest, usingBlock block: @escaping (CNContact, UnsafeMutablePointer<ObjCBool>) -> Void) throws {
		let ok = UnsafeMutablePointer<ObjCBool>.allocate(capacity: 1)
		ok[0] = false
		
		for contact in contacts {
			block(contact, ok)
		}
	}
	
	public override func unifiedContact(withIdentifier identifier: String, keysToFetch keys: [CNKeyDescriptor]) throws -> CNContact {
		guard let idAsInt = Int(identifier) else {
			throw CNContactStoreMockError.IdentifierNeedsToBeInt
		}
		
		return contacts[idAsInt]
	}
	
	public override func execute(_ saveRequest: CNSaveRequest) throws {
		guard let saveRequestMock = saveRequest as? CNSaveRequestMock else {
			throw CNContactStoreMockError.NeedCNSaveRequestMock
		}
		
		switch saveRequestMock.transactionType {
			case .add:
				contacts.append(saveRequestMock.contact)
			
			case .edit:
				guard let nicknameAsInt = Int(saveRequestMock.contact.nickname) else {
					throw CNContactStoreMockError.NicknameAsInt
				}
				
				contacts[nicknameAsInt] = saveRequestMock.contact
			
			case .delete:
				contacts.removeAll { (contact) -> Bool in
					return contact.givenName == saveRequestMock.contact.givenName && contact.familyName == saveRequestMock.contact.familyName
				}
		}
	}
	
	public override func defaultContainerIdentifier() -> String {
		return "CNContactStoreMock.mock"
	}
	
	
	
	// MARK: - Helper functions
	
	public func resetMockContacts() {
		contacts = createMockContacts()
	}
	
	/*
	public func createNewMagmaContact(withAddress address: String) -> CNMutableContact {
		let contact = createMutableContactWithTezos(withAddress: address)
		contact.givenName = "Testy McTestface"
		contact.nickname = "\(contacts.count)"
		
		return contact
	}
	*/
	
	public func createMockContacts() -> [CNContact] {
		let contact1 = createMutableContactWithSystemData()
		contact1.givenName = "John"
		contact1.familyName = "Doe"
		contact1.nickname = "0"
		
		let contact2 = createMutableContactWithSystemDataAndTezos()
		contact2.givenName = "Jane"
		contact2.familyName = "Doe"
		contact2.nickname = "1"
		
		let contact3 = createMutableContactWithTezos()
		contact3.givenName = "Boaty McBoatface"
		contact3.nickname = "2"
		
		return [contact1, contact2, contact3]
	}
	
	private func createMutableContactWithSystemData() -> CNMutableContact {
		let contact = CNMutableContact()
		
		let postalAddress = CNMutablePostalAddress()
		postalAddress.city = "CityName"
		postalAddress.country = "CountryName"
		let labeledPostalAddress = CNLabeledValue<CNPostalAddress>(label: CNLabelHome, value: postalAddress)
		contact.postalAddresses = [labeledPostalAddress]
		
		return contact
	}
	
	private func createMutableContactWithTezos(withAddress address: String = CNContactStoreMock.mockAddress1) -> CNMutableContact {
		let contact = CNMutableContact()
		
		let addressString = TezosContact.createSystemContactAddress(address)
		let instantMessageAddress = CNInstantMessageAddress(username: addressString, service: TezosContact.systemContactLabel)
		let intantMessage = CNLabeledValue<CNInstantMessageAddress>(label: TezosContact.systemContactLabel, value: instantMessageAddress)
		contact.instantMessageAddresses.append(intantMessage)
		
		return contact
	}
	
	private func createMutableContactWithSystemDataAndTezos() -> CNMutableContact {
		let contact = createMutableContactWithSystemData()
		let tempContact = createMutableContactWithTezos()
		
		contact.instantMessageAddresses = tempContact.instantMessageAddresses
		
		return contact
	}
}
