//
//  URLSessionMockErrorGenerator.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 16/06/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation

class URLSessionMockErrorGenerator {
	
	static let shared = URLSessionMockErrorGenerator()
	
	private var errorToReturn: Error? = nil
	private var errorStatusCode: Int? = nil
	private var urlToReturnErrorFor: URL? = nil
	private var errorData: Data? = nil
	private var errorDataStatusCode: Int? = nil
	
	private init() {}
	
	func trigger(error: Error, statusCode: Int? = nil, forURL url: URL) {
		errorToReturn = error
		errorStatusCode = statusCode
		urlToReturnErrorFor = url
	}
	
	func error(forURL url: URL) -> (error: Error?, statusCode: Int?) {
		if urlToReturnErrorFor?.path == url.path {
			return (error: errorToReturn, statusCode: errorStatusCode)
		}
		
		return (error: nil, statusCode: nil)
	}
	
	func trigger(errorData: Data, statusCode: Int? = nil, forURL url: URL) {
		self.errorData = errorData
		self.errorDataStatusCode = statusCode
		urlToReturnErrorFor = url
	}
	
	func errorData(forURL url: URL) -> (data: Data?, statusCode: Int?) {
		if urlToReturnErrorFor?.path == url.path {
			return (data: errorData, statusCode: errorDataStatusCode)
		}
		
		return (data: nil, statusCode: nil)
	}
	
	func clear() {
		errorToReturn = nil
		errorStatusCode = nil
		errorData = nil
		errorDataStatusCode = nil
		urlToReturnErrorFor = nil
	}
	
	
	
	// MARK: - Helpers
	func triggerGasExhaustedError() {
		let url = DependencyManager.shared.tezosClientConfig.primaryNodeURL.appendingPathComponent("/chains/main/blocks/BMLJNz9wE7r7uYCm53RkxJMDvhxgpJ6ugQ9yb1fNHqN4ReeiVC3/helpers/preapply/operations")
		let data = URLSessionMock.generateDataFromStubFile(filename: "gasExhausted")
		
		self.trigger(errorData: data, forURL: url)
	}
	
	func triggerBalanceTooLowError() {
		let url = DependencyManager.shared.tezosClientConfig.primaryNodeURL.appendingPathComponent("/chains/main/blocks/head/helpers/scripts/run_operation")
		let data = URLSessionMock.generateDataFromStubFile(filename: "balanceTooLow")
		
		self.trigger(errorData: data, forURL: url)
	}
	
	func triggerInvalidDexterRequestError() {
		let url = DependencyManager.shared.tezosClientConfig.primaryNodeURL.appendingPathComponent("/chains/main/blocks/head/helpers/scripts/run_operation")
		let data = URLSessionMock.generateDataFromStubFile(filename: "invalidDexterRequest")
		
		self.trigger(errorData: data, forURL: url)
	}
	
	func triggerInvalidAddressError() {
		let url = DependencyManager.shared.tezosClientConfig.primaryNodeURL.appendingPathComponent("/chains/main/blocks/head/helpers/scripts/run_operation")
		let data = URLSessionMock.generateDataFromStubFile(filename: "invalidAddress", fileExtension: "txt")
		
		self.trigger(errorData: data, statusCode: 400, forURL: url)
	}
	
	func triggerTzktBlockSearchServerError() {
		let url = DependencyManager.shared.tezosClientConfig.primaryNodeURL.appendingPathComponent("/v1/operations/onkGZ6eFRrQ8Zv5PXfKmxVMfkHXYWc8kMhPXc5J4cXa8yp2p4nT")
		self.trigger(error: NSError(domain: "mock", code: 404, userInfo: nil), statusCode: 404, forURL: url)
	}
	
	func triggerTzktBlockSearchDeadlineError() {
		let url = DependencyManager.shared.tezosClientConfig.primaryNodeURL.appendingPathComponent("/v1/operations/onkGZ6eFRrQ8Zv5PXfKmxVMfkHXYWc8kMhPXc5J4cXa8yp2p4nT")
		let data = URLSessionMock.generateDataFromStubFile(filename: "tzkt-operation-search-error")
		
		self.trigger(errorData: data, statusCode: 200, forURL: url)
	}
	
	func triggerNoInternetErrorOnWallet() {
		let url = DependencyManager.shared.tezosClientConfig.primaryNodeURL.appendingPathComponent("/chains/main/blocks/head/context/contracts/tz1e4hAp7xpjekmXnYe4677ELGA3UxR79EFb/balance")
		let error = URLSessionMockErrorGenerator.noInternetConnectionError()
		
		self.trigger(error: error, statusCode: 400, forURL: url)
	}
	
	
	
	// MARK: - Static helpers
	static func noInternetConnectionError() -> Error {
		return NSError(domain: "NSURLErrorDomain", code: -1009, userInfo: ["NSUnderlyingError": "0x2805a0090", "Code":-1009, "NSLocalizedDescription":"The Internet connection appears to be offline."])
	}
}
