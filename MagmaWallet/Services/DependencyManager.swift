//
//  DependencyManager.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 27/01/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import camlKit
import KeychainSwift
import Contacts
import Keys

class DependencyManager {
	
	static let shared = DependencyManager()
	
	var keychain: KeychainSwift
	
	// camlKit clients
	var tezosClientConfig: TezosNodeClientConfig
	var tezosNodeClient: TezosNodeClient
	var tzktClient: TzKTClient
	var betterCallDevClient: BetterCallDevClient
	
	// magma services
	var homeService: HomeService
	var accountService: AccountService
	var tokenPriceService: TokenPriceService
	var transactionService: TransactionService
	var tezosBakerService: TezosBakerService
	var contactService: ContactService
	var deeplinkService: DeeplinkService
	var moonPayService: MoonPayService
	
	// Properties and helpers
	let sharedSession: URLSession
	
	var balances: [Token] = [Token(icon: nil, name: "token_tezos".localized, symbol: "XTZ", tokenType: .xtz, balance: XTZAmount.zero(), tokenContractAddress: nil, dexterExchangeAddress: nil)]
	var dexterTokens: [Token] = []
	var testnetBakers: [String] = []
	let blockExplorerBaseURL: String
	
	// URL's
	var currentMainNodeURL: String {
		set {
			UserDefaults.standard.setValue(newValue, forKey: "io.camlcase.tezos.current.main.url")
		}
		get {
			return UserDefaults.standard.string(forKey: "io.camlcase.tezos.current.main.url") ?? defaultMainNodeURL
		}
	}
	
	var currentParseNodeURL: String {
		set {
			UserDefaults.standard.setValue(newValue, forKey: "io.camlcase.tezos.current.parse.url")
		}
		get {
			return UserDefaults.standard.string(forKey: "io.camlcase.tezos.current.parse.url") ?? defaultParseNodeURL
		}
	}
	
	var currentTzKTURL: String {
		set {
			UserDefaults.standard.setValue(newValue, forKey: "io.camlcase.tezos.current.tzkt.url")
		}
		get {
			return UserDefaults.standard.string(forKey: "io.camlcase.tezos.current.tzkt.url") ?? defaultTzKTURL
		}
	}
	
	var currentBetterCallDevURL: String {
		set {
			UserDefaults.standard.setValue(newValue, forKey: "io.camlcase.tezos.current.bcd.url")
		}
		get {
			return UserDefaults.standard.string(forKey: "io.camlcase.tezos.current.bcd.url") ?? defaultBetterCallDevURL
		}
	}
	
	let defaultMainNodeURL: String
	let defaultParseNodeURL: String
	let defaultTzKTURL: String
	let defaultBetterCallDevURL: String
	let tezosChainName: TezosChainName
	let networkType: TezosNodeClientConfig.NetworkType
	
	
	
	// MARK: - Init
	
	private init() {
		
		let keys = MagmaWalletKeys()
		
		#if TESTNET
		let urlCredentials = keys.tezosNodeURL_testnet_credentials
		#else
		let urlCredentials = keys.tezosNodeURL_credentials
		#endif
		
		
		// Tweak some settings depending on whether we are running in an XCTTest case or not
		if Thread.current.isRunningXCTest {
			keychain = KeychainSwift(keyPrefix: "mock-")
			contactService = ContactService(contactStore: CNContactStoreMock(), saveRequestType: CNSaveRequestMock.self)
			
			sharedSession = URLSessionMock.shared
			
		} else {
			keychain = KeychainSwift()
			contactService = ContactService(contactStore: CNContactStore(), saveRequestType: CNSaveRequest.self)
			
			let config = URLSessionConfiguration.default
			config.httpAdditionalHeaders = ["Authorization": "Basic \(urlCredentials)"]
			config.timeoutIntervalForRequest = 30
			config.timeoutIntervalForResource = 60
			config.urlCache = nil
			config.requestCachePolicy = .reloadIgnoringLocalCacheData
			sharedSession = URLSession(configuration: config)
			
			// To prevent stale cache issues, everytime DepenedencyManager is created, clear the entire cache
			URLCache.shared.removeAllCachedResponses()
		}
		
		
		// Take the session and command line arguments, then setup all the clients and configs to determine the blockchain network to use
		#if TESTNET
			defaultMainNodeURL = "https://api.tez.ie/rpc/florencenet"
			defaultParseNodeURL = "https://florencenet.smartpy.io"
			defaultTzKTURL = "https://api.florencenet.tzkt.io"
			defaultBetterCallDevURL = "https://api.better-call.dev/"
			tezosChainName = .florencenet
			networkType = .testnet
			blockExplorerBaseURL = "https://florencenet.tzkt.io/"
		
		#else
			defaultMainNodeURL = "https://api.tez.ie/rpc/mainnet"
			defaultParseNodeURL = "https://mainnet.smartpy.io"
			defaultTzKTURL = "https://api.tzkt.io"
			defaultBetterCallDevURL = "https://api.better-call.dev/"
			tezosChainName = .mainnet
			networkType = .mainnet
			blockExplorerBaseURL = "https://tzkt.io/"
		
		#endif
		
		// Setup the rest of the Magma services
		homeService = HomeService()
		accountService = AccountService(keychain: keychain)
		tokenPriceService = TokenPriceService(session: sharedSession)
		transactionService = TransactionService()
		tezosBakerService = TezosBakerService(session: sharedSession)
		deeplinkService = DeeplinkService()
		moonPayService = MoonPayService(session: sharedSession)
		
		
		// Temporary init of camlKit dependecnies so that we can call self to make URL's changable without code duplication. Needs to be refactored when more time available
		tezosClientConfig = TezosNodeClientConfig(withDefaultsForNetworkType: .mainnet)
		tezosNodeClient = TezosNodeClient(config: tezosClientConfig)
		betterCallDevClient = BetterCallDevClient(networkService: tezosNodeClient.networkService, config: tezosClientConfig)
		tzktClient = TzKTClient(networkService: tezosNodeClient.networkService, config: tezosClientConfig, betterCallDevClient: betterCallDevClient)
		
		
		// Call functions after all store properties init
		setupTokens()
		setupTestnetBakers()
		setupErrorCallback()
		createClientsWithCurrentURLs()
	}
	
	func setupTokens() {
		#if TESTNET
		
		dexterTokens = [
			Token(icon: #imageLiteral(resourceName: "tezos-logo"), name: "token_tezos".localized, symbol: "XTZ", tokenType: .xtz, balance: XTZAmount.zero(), tokenContractAddress: nil, dexterExchangeAddress: nil),
			Token(icon: #imageLiteral(resourceName: "tzBTC"), name: "token_tzbtc".localized, symbol: "tzBTC",
				  tokenType: .fa1_2, balance: TokenAmount.zeroBalance(decimalPlaces: 8), tokenContractAddress: "KT1CUg39jQF8mV6nTMqZxjUUZFuz1KXowv3K", dexterExchangeAddress: "KT1BYYLfMjufYwqFtTSYJND7bzKNyK7mjrjM"),
			Token(icon: #imageLiteral(resourceName: "USDtz"), name: "token_usdtz".localized, symbol: "USDtz",
				  tokenType: .fa1_2, balance: TokenAmount.zeroBalance(decimalPlaces: 6), tokenContractAddress: "KT1FCMQk44tEP9fm9n5JJEhkSk1TW3XQdaWH", dexterExchangeAddress: "KT1RfTPvrfAQDGAJ7wB71EtwxLQgjmfz59kE")
		]
		
		#else
		
		dexterTokens = [
			Token(icon: #imageLiteral(resourceName: "tezos-logo"), name: "token_tezos".localized, symbol: "XTZ", tokenType: .xtz, balance: XTZAmount.zero(), tokenContractAddress: nil, dexterExchangeAddress: nil),
			Token(icon: #imageLiteral(resourceName: "ethtz"), name: "token_ethtz".localized, symbol: "ETHtz",
				  tokenType: .fa1_2, balance: TokenAmount.zeroBalance(decimalPlaces: 18), tokenContractAddress: "KT19at7rQUvyjxnZ2fBv7D9zc8rkyG7gAoU8", dexterExchangeAddress: "KT1PDrBE59Zmxnb8vXRgRAG1XmvTMTs5EDHU"),
			Token(icon: #imageLiteral(resourceName: "kUSD"), name: "token_kusd".localized, symbol: "kUSD",
				  tokenType: .fa1_2, balance: TokenAmount.zeroBalance(decimalPlaces: 18), tokenContractAddress: "KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV", dexterExchangeAddress: "KT1AbYeDbjjcAnV1QK7EZUUdqku77CdkTuv6"),
			Token(icon: #imageLiteral(resourceName: "tzBTC"), name: "token_tzbtc".localized, symbol: "tzBTC",
				  tokenType: .fa1_2, balance: TokenAmount.zeroBalance(decimalPlaces: 8), tokenContractAddress: "KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn", dexterExchangeAddress: "KT1BGQR7t4izzKZ7eRodKWTodAsM23P38v7N"),
			Token(icon: #imageLiteral(resourceName: "USDtz"), name: "token_usdtz".localized, symbol: "USDtz",
				  tokenType: .fa1_2, balance: TokenAmount.zeroBalance(decimalPlaces: 6), tokenContractAddress: "KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9", dexterExchangeAddress: "KT1Tr2eG3eVmPRbymrbU2UppUmKjFPXomGG9"),
			Token(icon: #imageLiteral(resourceName: "wxtz"), name: "token_wxtz".localized, symbol: "wXTZ",
				  tokenType: .fa1_2, balance: TokenAmount.zeroBalance(decimalPlaces: 6), tokenContractAddress: "KT1VYsVfmobT7rsMVivvZ4J8i3bPiqz12NaH", dexterExchangeAddress: "KT1D56HQfMmwdopmFLTwNHFJSs6Dsg2didFo")
		]
		
		#endif
	}
	
	func setupTestnetBakers() {
		testnetBakers = [
			"tz1aWXP237BLwNHJcCD4b3DutCevhqq2T1Z9",
			"tz1VpvtSaSxKvykrqajFJTZqCXgoVJ5cKaM1",
			"tz1PirboZKFVqkfE45hVLpkpXaZtLk3mqC17",
			"tz1cXeGHP8Urj2pQRwpAkCdPGbCdqFUPsQwU",
			"tz1YSzTPwEUpMrRxeTTHNo6RVxfo2TMN633b",
			"tz1R55a2HQbXUAzWKJYE5bJp3UvvawwCm9Pr",
			"tz1VWasoyFGAWZt5K2qZRzP3cWzv3z7MMhP8",
			"tz1T8UYSbVuRm6CdhjvwCfXsKXb4yL9ai9Q3",
			"tz1fyYJwgV1ozj6RyjtU1hLTBeoqQvQmRjVv",
			"tz1RR6wETy9BeXG3Fjk25YmkSMGHxTtKkhpX",
			"tz1fj3tzFejSmPyZZ2xsqehBxQE9GGr3rK8d",
			"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5",
			"tz1aCnK45NP7CG31WMkUTP3d3V486mXwzZZt"
		]
	}
	
	func moonPayURL(forWalletAddress address: String?) -> URL? {
		let keys = MagmaWalletKeys()
		
		#if TESTNET
		let moonPayURL = "https://buy-staging.moonpay.com/?apiKey=\(keys.moonPayAPIKey_testnet)&enabledPaymentMethods=credit_debit_card,apple_pay,google_pay,samsung_pay,sepa_bank_transfer,gbp_bank_transfer,gbp_open_banking_payment&currencyCode=eth&colorCode=%23992871&redirectURL=magmawallet://wallet"
		
		#else
		let moonPayURL = "https://buy.moonpay.io/?apiKey=\(keys.moonPayAPIKey)&enabledPaymentMethods=credit_debit_card,apple_pay,google_pay,samsung_pay,sepa_bank_transfer,gbp_bank_transfer,gbp_open_banking_payment&currencyCode=xtz&colorCode=%23992871&redirectURL=magmawallet://wallet"
		#endif
		
		return URL(string: moonPayURL)
	}
	
	func setupErrorCallback() {
		ErrorHandlingService.shared.errorEventClosure = { (error) in
			ErrorMessageService.logSentryEvent(for: error)
		}
	}
	
	func createClientsWithCurrentURLs() {
		guard let mainURL = URL(string: currentMainNodeURL),
			  let parseURL = URL(string: currentParseNodeURL),
			  let inDEXterURL = URL(string: "https://dev.indexter.dexter.exchange/"),
			  let tzktURL = URL(string: currentTzKTURL),
			  let betterCallDevURL = URL(string: currentBetterCallDevURL) else {
			fatalError("Can't create URL's for camlKit clients")
		}
		
		tezosClientConfig = TezosNodeClientConfig(primaryNodeURL: mainURL, parseNodeURL: parseURL, inDEXTerURL: inDEXterURL, tezosChainName: tezosChainName, tzktURL: tzktURL, betterCallDevURL: betterCallDevURL, urlSession: sharedSession, networkType: networkType)
		
		if networkType == .testnet {
			tezosClientConfig.loggingConfig.allOn()
		} else {
			tezosClientConfig.loggingConfig.allOff()
		}
		
		tezosNodeClient = TezosNodeClient(config: tezosClientConfig)
		betterCallDevClient = BetterCallDevClient(networkService: tezosNodeClient.networkService, config: tezosClientConfig)
		tzktClient = TzKTClient(networkService: tezosNodeClient.networkService, config: tezosClientConfig, betterCallDevClient: betterCallDevClient)
	}
}
