//
//  ViewModelResponse.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 09/01/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import camlKit

struct ViewModelResponse {
	
	/**
     A boolean to say whether or not the viewModel update was a success.
     */
    public let success: Bool
    
    /**
     An optional integar to allow the controller to act on different responses.
     */
    public let responseCode: Int?
    
    /**
     A optional display string for the controller to use. For example, as an alert to inform the user
     of an error or an update of some kind.
     */
    public let userDisplayString: String?
	
	/**
	
	*/
	public let errorResponse: ErrorResponse?
}
