//
//  LoadingCell.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 26/02/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import os.log

struct LoadingCellModel: UITableViewCellModel {
	
	static let staticIdentifier = "MVVMLoadingCell"
	let cellId: String = LoadingCellModel.staticIdentifier
	let estimatedRowHeight: CGFloat
	
	let loadingMessage: String
	let backgroundColor: UIColor = .clear
	let activityIndicatorStyle: UIActivityIndicatorView.Style = .white
	
	static func defaultHeight() -> CGFloat {
		return (UIScreen.main.bounds.height * 0.90)
	}
}

class LoadingCell: UITableViewCell, UITableViewCellMVVM {
	
	let label = UILabel()
	let activityIndicator = UIActivityIndicatorView(style: .gray)
	
	func setup(withModel model: UITableViewCellModel) {
		guard let cellModel = model as? LoadingCellModel else {
			os_log(.error, log: .mvvm, "Cell model passed in to 'LoadingCell' did not match required type 'LoadingCellModel', instead found '%@'", String(describing: model.self))
			return
		}
		
		self.backgroundColor = cellModel.backgroundColor
		self.contentView.backgroundColor = cellModel.backgroundColor
		
		activityIndicator.style = cellModel.activityIndicatorStyle
		activityIndicator.translatesAutoresizingMaskIntoConstraints = false
		activityIndicator.startAnimating()
		self.contentView.addSubview(activityIndicator)
		
		label.text = cellModel.loadingMessage
		label.font = UIFont.barlowFont(.regular, withSize: 15)
		label.numberOfLines = 0
		label.textAlignment = .center
		label.translatesAutoresizingMaskIntoConstraints = false
		self.contentView.addSubview(label)
		
		NSLayoutConstraint.activate([
			activityIndicator.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: (cellModel.estimatedRowHeight * 0.4)),
			activityIndicator.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor),
			activityIndicator.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor),
			
			label.topAnchor.constraint(equalTo: activityIndicator.bottomAnchor, constant: 12),
			label.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor),
			label.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 24),
			label.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -24),
			label.bottomAnchor.constraint(lessThanOrEqualTo: self.contentView.bottomAnchor, constant: -40)
		])
		
		self.selectionStyle = .none
	}
}
