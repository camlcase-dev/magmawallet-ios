//
//  ErrorCell.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 26/02/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import os.log

struct ErrorCellModel: UITableViewCellModel {
	
	static let staticIdentifier = "MVVMErrorCell"
	let cellId: String = ErrorCellModel.staticIdentifier
	let estimatedRowHeight: CGFloat
	
	let errorMessage: String
	let backgroundColor: UIColor = .clear
	let labelColor: UIColor = .white
	
	static func defaultHeight() -> CGFloat {
		return (UIScreen.main.bounds.height * 0.90)
	}
}

class ErrorCell: UITableViewCell, UITableViewCellMVVM {
	
	let label = UILabel()
	
	func setup(withModel model: UITableViewCellModel) {
		guard let cellModel = model as? ErrorCellModel else {
			os_log(.error, log: .mvvm, "Cell model passed in to 'ErrorCell' did not match required type 'ErrorCellModel', instead found '%@'", String(describing: model.self))
			return
		}
		
		self.backgroundColor = cellModel.backgroundColor
		self.contentView.backgroundColor = cellModel.backgroundColor
		
		label.text = cellModel.errorMessage
		label.font = UIFont.barlowFont(.regular, withSize: 18)
		label.numberOfLines = 0
		label.textAlignment = .center
		label.textColor = cellModel.labelColor
		label.translatesAutoresizingMaskIntoConstraints = false
		self.contentView.addSubview(label)
		
		NSLayoutConstraint.activate([
			label.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: (cellModel.estimatedRowHeight * 0.4)),
			label.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor),
			label.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 24),
			label.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -24),
			label.bottomAnchor.constraint(lessThanOrEqualTo: self.contentView.bottomAnchor, constant: -40)
		])
		
		self.selectionStyle = .none
	}
}
