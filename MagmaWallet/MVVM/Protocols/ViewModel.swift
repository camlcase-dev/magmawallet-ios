//
//  ViewModel.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 09/01/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation

/**
The purpose of a ViewModel is to abstract away business logic from the controller. ViewModels should trigger networking code, read from databases, disk, ram etc, to find the information needed.
Then format / filter / aggregate / concatenate the data so that it is human readable and provide that to the controller, so that the controller need only assign the correct value to the correct place.

Example:
```
self.myViewModel.update() { response in
	self.currencyLabel.text = myViewModel.purchasePrice
}
```

This implementation is super simple, ViewModels can be classes or structs, simply implement the ViewModel protocol.
Every ViewModel must have a single generic entrypoint, with a consistent return type. This is to make unit testing easier by always knowing how to invoke the business logic and knowing what will be returned.
The implementation of the protocol must fetch the data, process it and store it on itself. The controller can then access thi data once it knows the work is finished.
*/
protocol ViewModel {
	
	/**
	This function will be responsible for doing whatever is necessary to update the data and then translate this into an easy to use format for the controller.
	For example the view model might expose a `fullName` property that adds together a`firstName` and `lastName` coming from the server or exposing a date string rather than a timestamp, and so on.
	This update may include sending a network request, reading data from CoreData / database etc.
	
	- Return: Promise with a `ViewModelResponse` indicating the status of the update.
	*/
	mutating func update(withInput input: [String: Any]?, completion: @escaping (ViewModelResponse) -> Void)
}
