//
//  UITableViewCellMVVM.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 10/01/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation

/**
 This protocol adds a generic `setup` method to each `UITableViewCell` so that we can remove
 boilerplate code in the `UITableView` data source methods, and give a standardised config function
 for unit tests to take advantage of.
 */
protocol UITableViewCellMVVM {
	
    /**
     The setup method is where the `UITableViewCell` subclass will take the model and update its UI.
     Setting text to `UILabels`, `UITextFields` etc. This method will be called automatically inside
     `cellForRowAtIndexPath()`
     
     - Parameter withModel: A type conforming to `UITableViewCellModel` that the cell will use to
     setup its UI.
     */
    func setup(withModel model: UITableViewCellModel)
}
