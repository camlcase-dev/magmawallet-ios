//
//  UITableViewCellModel.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 09/01/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit

/**
 A type conforming to `UITableViewCellModel` must expose a cellId for the `UITableView` to
 be able to dequeue an instance of the required `UITableViewCell`.
 
 The conforming type must then provide the required additional data for the given use case of the cell.
 */
protocol UITableViewCellModel {
    
	/**
	Setting the staticIdentifier to your xib name (if using one) and sharing that identifier with the cellId, allows 2 things.
	
	1. When registering xib's with a tableview, you have a constant for both values, avoiding the need to copy / paste strings or create additional config files.
	2. The MVVM Tableview extension can still use the cellId in a generic context without needing to access the key staticly
	*/
	static var staticIdentifier: String { get }
	
    /**
     A cellId that will be used to dequeue an instance of a `UITableViewCell`.
     This will be called automatically inside `cellForRowAtIndexPath()`.
    */
    var cellId: String { get }
	
	/**
	
	*/
	var estimatedRowHeight: CGFloat { get }
}
