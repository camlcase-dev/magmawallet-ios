//
//  AppDelegate.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 09/01/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import MagmaWalletKit
import CoreData
import Sentry
import Keys
import Toast_Swift
import os.log

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
	
	var window: UIWindow?
	var launchingSystemPopup = false
	var willResignActive = false
	var multitaskingCoverView = GradientView()
	
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
		
		// Setup Sentry crash reporting
		/*do {
			let sentryOptions = try Options(dict: ["dsn": MagmaWalletKeys().sentryDSN])
			sentryOptions.beforeSend = { (event) -> Event? in
				
				// Scrub any identifiable data to keep users anonymous
				event.context?["app"]?.removeValue(forKey: "device_app_hash")
				event.user = nil
				return event
			}
			
			SentrySDK.start(options: sentryOptions)
			
		} catch (let error) {
			os_log(.error, log: .sentry, "Sentry throw an error: %@", "\(error)")
		}*/
		
		
		// Styles consistent for every screen / control
		setGlobalStyles()
		
		
		// Setup dependency manager
		let _ = DependencyManager.shared
		
		
		// process special arguments coming from XCUITest to do things like show keyboard and reset app data
		processXCUITestArguments()
		
		
		// Download and store a list of Tezos bakers
		DependencyManager.shared.tezosBakerService.fetchAndStoreTezosBakers { (success) in
			os_log(.debug, log: .bakers, "Download bakers: %@", success ? "Success" : "Failure")
		}
		
		// Kick off processing contacts, things may have changed while app was in the background
		let contactService = DependencyManager.shared.contactService
		contactService.clearInMemoryContacts()
		contactService.deleteContactsFromDisk()
		contactService.fetchContacts()
		
		return true
	}
	
	func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
		return DependencyManager.shared.deeplinkService.handle(application: app, url: url, andOptions: options)
	}
	
	func applicationWillResignActive(_ application: UIApplication) {
		if !launchingSystemPopup {
			willResignActive = true
			
			multitaskingCoverView = GradientView(frame: UIScreen.main.bounds)
			multitaskingCoverView.light = true
			self.window?.addSubview(multitaskingCoverView)
			
			// Disable 60 second background refresh refresh
			// DependencyManager.shared.homeService.stopPolling()
			
		} else {
			launchingSystemPopup = false
		}
	}
	
	func applicationDidBecomeActive(_ application: UIApplication) {
		if DependencyManager.shared.accountService.getOnboardingStatus() == .onboardingComplete && willResignActive {
			guard let root = self.window?.rootViewController as? UINavigationController, let onscreenController = root.viewControllers.last else {
				os_log(.error, log: .viewLifeCycle, "Can't find root view controller insdie applicationDidBecomeActive")
				return
			}
			
			let loginVC = UIStoryboard.loginViewController()
			loginVC.modalPresentationStyle = .fullScreen
			
			var presenter: UIViewController = onscreenController
			if let presented = onscreenController.presentedViewController {
				presenter = presented
			}
			
			presenter.present(loginVC, animated: false, completion: nil)
			self.willResignActive = false
			
			// Disable 60 second background refresh refresh
			// DependencyManager.shared.homeService.startPolling()
		}
		
		multitaskingCoverView.removeFromSuperview()
	}
	
	func application(_ application: UIApplication, shouldAllowExtensionPointIdentifier extensionPointIdentifier: UIApplication.ExtensionPointIdentifier) -> Bool {
		if extensionPointIdentifier == UIApplication.ExtensionPointIdentifier.keyboard {
			return false
		}
		
		return true
	}
}

extension AppDelegate {
	
	func processXCUITestArguments() {
		let environment = ProcessInfo.processInfo.environment
		
		if environment["XCUITEST-KEYBOARD"] == "true" {
			disconeectHardwareKeyboard()
		}
		
		if environment["XCUITEST-RESET"] == "true" {
			let _ = DependencyManager.shared.accountService.deleteWallet()
			DependencyManager.shared.tezosBakerService.clearInMemoryBakers()
			DependencyManager.shared.tezosBakerService.deleteBakersFromDisk()
		}
		
		if environment["XCUITEST-CLEAR-CONTACTS"] == "true" {
			let contacts = DependencyManager.shared.contactService.getContacts()
			
			for contact in contacts {
				if contact.containsOnlyMagmaData {
					let _ = DependencyManager.shared.contactService.deleteContact(withContactId: contact.contactIdentifier)
					
				} else {
					let _ = DependencyManager.shared.contactService.removeCurrentAddressFromContact(withContactId: contact.contactIdentifier)
				}
			}
		}
	}
	
	func disconeectHardwareKeyboard() {
		#if targetEnvironment(simulator)
		// Disable hardware keyboards.
		let setHardwareLayout = NSSelectorFromString("setHardwareLayout:")
		UITextInputMode.activeInputModes
			// Filter `UIKeyboardInputMode`s.
			.filter({ $0.responds(to: setHardwareLayout) })
			.forEach { $0.perform(setHardwareLayout, with: nil) }
		#endif
	}
	
	func setGlobalStyles() {
		// Navigation Title
		UINavigationBar.appearance().titleTextAttributes = [
			NSAttributedString.Key.font: UIFont.barlowFont(.bold, withSize: 14),
			NSAttributedString.Key.foregroundColor: UIColor.white,
			NSAttributedString.Key.kern: 2.0
		]
		
		// Navigation bar
		UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
		UINavigationBar.appearance().shadowImage = UIImage()
		UINavigationBar.appearance().isTranslucent = true
		
		// Back button
		UINavigationBar.appearance().backIndicatorImage = UIImage(named: "Back")
		UINavigationBar.appearance().backIndicatorTransitionMaskImage = UIImage(named: "Back")
		UINavigationBar.appearance().tintColor = UIColor.white
		
		// Rather than set back button text to " ", on every viewController. Just set the text to clear and very small
		UIBarButtonItem.appearance(whenContainedInInstancesOf: [UINavigationBar.self]).setTitleTextAttributes([
			NSAttributedString.Key.font: UIFont.systemFont(ofSize: 0.1),
			NSAttributedString.Key.foregroundColor: UIColor.clear
		], for: .normal)
		UIBarButtonItem.appearance(whenContainedInInstancesOf: [UINavigationBar.self]).setTitleTextAttributes([
			NSAttributedString.Key.font: UIFont.systemFont(ofSize: 0.1),
			NSAttributedString.Key.foregroundColor: UIColor.clear
		], for: .highlighted)
		
		// Text input
		UITextView.appearance().tintColor = .white
		
		// Tab bar
		UITabBar.appearance().backgroundImage = UIImage()
		UITabBar.appearance().shadowImage = UIImage()
	}
	
	func removeNavigationStyles() {
		UINavigationBar.appearance().setBackgroundImage(nil, for: .default)
		UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.systemBlue]
		UINavigationBar.appearance().tintColor = UIColor.systemBlue
		UIBarButtonItem.appearance(whenContainedInInstancesOf: [UINavigationBar.self]).setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.systemBlue], for: .normal)
	}
}

