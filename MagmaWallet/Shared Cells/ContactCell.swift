//
//  ContactCell.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 02/09/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import MagmaWalletKit
import os.log

struct ContactCellModel: UITableViewCellModel {
	
	static let staticIdentifier = "ContactCell"
	let cellId: String = ContactCellModel.staticIdentifier
	let estimatedRowHeight: CGFloat = 84
	
	let name: String
	let address: String
}

class ContactCell: UITableViewCell, UITableViewCellMVVM {
	
	@IBOutlet weak var nameLabel: UILabel!
	
	func setup(withModel model: UITableViewCellModel) {
		guard let cellModel = model as? ContactCellModel else {
			os_log(.error, log: .mvvm, "Cell model passed in to 'ContactCell' did not match required type 'ContactCellModel', instead found '%@'", String(describing: model.self))
			return
		}
		
		nameLabel.text = cellModel.name
	}
}
