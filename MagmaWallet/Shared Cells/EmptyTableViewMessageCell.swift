//
//  EmptyTableViewMessageCell.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 02/09/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import MagmaWalletKit
import os.log

struct EmptyTableViewMessageCellModel: UITableViewCellModel {
	
	static let staticIdentifier = "EmptyTableViewMessageCell"
	let cellId: String = EmptyTableViewMessageCellModel.staticIdentifier
	let estimatedRowHeight: CGFloat = 52
	
	let message: String
}

class EmptyTableViewMessageCell: UITableViewCell, UITableViewCellMVVM {
	
	@IBOutlet weak var messageBackgroundView: UIView!
	@IBOutlet weak var messageLabel: ThemeLabel!
	
	func setup(withModel model: UITableViewCellModel) {
		guard let cellModel = model as? EmptyTableViewMessageCellModel else {
			os_log(.error, log: .mvvm, "Cell model passed in to 'EmptyTableViewMessageCell' did not match required type 'EmptyTableViewMessageCellModel', instead found '%@'", String(describing: model.self))
			return
		}
		
		messageLabel.text = cellModel.message
		selectionStyle = .none
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		
		messageBackgroundView.cornerRadius = messageBackgroundView.bounds.height / 2
	}
}
