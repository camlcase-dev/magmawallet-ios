//
//  TransactionHistoryExchangeCell.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 15/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import MagmaWalletKit
import os.log

struct TransactionHistoryExchangeCellModel: TransactionHistoryCellModelProtocol, Equatable {
	
	static let staticIdentifier = "TransactionHistoryExchangeCell"
	let cellId: String = TransactionHistoryExchangeCellModel.staticIdentifier
	let estimatedRowHeight: CGFloat = 74
	
	let operationHash: String
	let amountSent: String
	let amountReceived: String
	let exchangeRate: String
	let networkFee: String
}

class TransactionHistoryExchangeCell: UITableViewCell, UITableViewCellMVVM {
	
	@IBOutlet weak var typeLabel: ThemeLabel!
	@IBOutlet weak var amountSentLabel: ThemeLabel!
	@IBOutlet weak var amountReceivedLabel: ThemeLabel!
	@IBOutlet weak var exchangeRateLabel: ThemeLabel!
	@IBOutlet weak var liquidityFeeLabel: ThemeLabel!
	@IBOutlet weak var networkFeeLabel: ThemeLabel!
	
	func setup(withModel model: UITableViewCellModel) {
		guard let cellModel = model as? TransactionHistoryExchangeCellModel else {
			os_log(.error, log: .mvvm, "Cell model passed in to 'TransactionHistoryExchangeCell' did not match required type 'TransactionHistoryExchangeCellModel', instead found '%@'", String(describing: model.self))
			return
		}
		
		typeLabel.text = "act_type_exchange".localized.uppercased()
		liquidityFeeLabel.text = "act_liquidity_fee".localized
		
		amountSentLabel.text = cellModel.amountSent
		amountReceivedLabel.text = cellModel.amountReceived
		exchangeRateLabel.text = cellModel.exchangeRate
		networkFeeLabel.text = cellModel.networkFee
	}
}
