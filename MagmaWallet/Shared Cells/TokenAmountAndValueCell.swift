//
//  TokenAmountAndValueCell.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 05/02/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import camlKit
import MagmaWalletKit
import os.log

struct TokenAmountAndValueCellModel: UITableViewCellModel {
	
	static let staticIdentifier = "TokenAmountAndValueCell"
	let cellId: String = TokenAmountAndValueCellModel.staticIdentifier
	let estimatedRowHeight: CGFloat = 95
	
	let amount: String
	let symbol: String
	let conversionString: String
	let currencyString: String
	let token: Token
	let hideCurrency: Bool
}

class TokenAmountAndValueCell: UITableViewCell, UITableViewCellMVVM {
	
	@IBOutlet weak var blurView: UIVisualEffectView!
	@IBOutlet weak var tokenAmountLabel: ThemeLabel!
	@IBOutlet weak var tokenSymbolLabel: ThemeLabel!
	@IBOutlet weak var conversionRateView: UIView!
	@IBOutlet weak var conversionRateLabel: ThemeLabel!
	@IBOutlet weak var currencyLabel: ThemeLabel!
	
	func setup(withModel model: UITableViewCellModel) {
		guard let cellModel = model as? TokenAmountAndValueCellModel else {
			os_log(.error, log: .mvvm, "Cell model passed in to 'TokenAmountAndValueCell' did not match required type 'TokenAmountAndValueCellModel', instead found '%@'", String(describing: model.self))
			return
		}
		
		tokenAmountLabel.text = cellModel.amount
		tokenSymbolLabel.text = cellModel.symbol
		conversionRateLabel.text = cellModel.conversionString
		currencyLabel.text = cellModel.currencyString
		
		if cellModel.hideCurrency {
			conversionRateView.isHidden = true
			currencyLabel.isHidden = true
		}
	}
}
