//
//  TransactionHistoryFailedCell.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 15/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import MagmaWalletKit
import os.log

struct TransactionHistoryFailedCellModel: TransactionHistoryCellModelProtocol, Equatable {
	
	static let staticIdentifier = "TransactionHistoryFailedCell"
	let cellId: String = TransactionHistoryFailedCellModel.staticIdentifier
	let estimatedRowHeight: CGFloat = 69
	
	let operationHash: String
	let type: String
	let amount: String
	let address: String
	let fee: String
}

class TransactionHistoryFailedCell: UITableViewCell, UITableViewCellMVVM {
	
	@IBOutlet weak var transactionTypeLabel: ThemeLabel!
	@IBOutlet weak var amountLabel: ThemeLabel!
	@IBOutlet weak var addressLabel: ThemeLabel!
	@IBOutlet weak var feeLabel: ThemeLabel!
	
	func setup(withModel model: UITableViewCellModel) {
		guard let cellModel = model as? TransactionHistoryFailedCellModel else {
			os_log(.error, log: .mvvm, "Cell model passed in to 'TransactionHistoryFailedCell' did not match required type 'TransactionHistoryFailedCellModel', instead found '%@'", String(describing: model.self))
			return
		}
		
		transactionTypeLabel.text = cellModel.type
		amountLabel.text = cellModel.amount
		addressLabel.text = cellModel.address
		feeLabel.text = cellModel.fee
	}
}
