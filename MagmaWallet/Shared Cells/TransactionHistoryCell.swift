//
//  TransactionHistoryCell.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 07/04/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import MagmaWalletKit
import os.log

protocol TransactionHistoryCellModelProtocol: UITableViewCellModel {
	
	var operationHash: String { get }
}

struct TransactionHistoryCellModel: TransactionHistoryCellModelProtocol, Equatable {
	
	static let staticIdentifier = "TransactionHistoryCell"
	let cellId: String = TransactionHistoryCellModel.staticIdentifier
	let estimatedRowHeight: CGFloat = 69
	
	let operationHash: String
	let type: String
	let amount: String
	let address: String
	let fee: String
}

class TransactionHistoryCell: UITableViewCell, UITableViewCellMVVM {
	
	@IBOutlet weak var transactionTypeLabel: ThemeLabel!
	@IBOutlet weak var amountLabel: ThemeLabel!
	@IBOutlet weak var addressLabel: ThemeLabel!
	@IBOutlet weak var feeLabel: ThemeLabel!
	
	func setup(withModel model: UITableViewCellModel) {
		guard let cellModel = model as? TransactionHistoryCellModel else {
			os_log(.error, log: .mvvm, "Cell model passed in to 'TransactionHistoryCell' did not match required type 'TransactionHistoryCellModel', instead found '%@'", String(describing: model.self))
			return
		}
		
		transactionTypeLabel.text = cellModel.type
		amountLabel.text = cellModel.amount
		addressLabel.text = cellModel.address
		feeLabel.text = cellModel.fee
	}
}
