//
//  Dictionary+HashableType.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 27/01/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation

// https://stackoverflow.com/questions/42459484/make-a-swift-dictionary-where-the-key-is-type
struct HashableType<T> : Hashable {
	
	static func == (lhs: HashableType, rhs: HashableType) -> Bool {
		return lhs.base == rhs.base
	}
	
	let base: T.Type
	
	init(_ base: T.Type) {
		self.base = base
	}
	
	func hash(into hasher: inout Hasher) {
		hasher.combine(ObjectIdentifier(base))
	}
}

extension Dictionary {
	subscript<T>(key: T.Type) -> Value? where Key == HashableType<T> {
		get { return self[HashableType(key)] }
		set { self[HashableType(key)] = newValue }
	}
}
