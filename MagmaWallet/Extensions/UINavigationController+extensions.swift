//
//  UINavigationController+extensions.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 22/01/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import Sentry
import os.log

extension UINavigationController {
	
	func popToWelcome() {
		guard let vc = self.viewControllers.first(where: { $0 is WelcomeViewController }) as? WelcomeViewController else {
			logSentryWarning(className: String(describing: WelcomeViewController.self), stack: self.viewControllers.map({ String(describing: $0.self) }))
			self.popToRootViewController(animated: true)
			return
		}
		
		self.popToViewController(vc, animated: true)
	}
	
	func popToWallet() {
		guard let vc = self.viewControllers.first(where: { $0 is TabBarController }) as? TabBarController else {
			logSentryWarning(className: String(describing: TabBarController.self), stack: self.viewControllers.map({ String(describing: $0.self) }))
			self.popToRootViewController(animated: true)
			return
		}
		
		vc.selectedIndex = TabBarController.walletIndex
		
		// Potential strange timing issue where users can see the success screen as a modal, over the homepage if they click "Return to Exchange" at the wrong moment on wait screen
		if self.topViewController?.isModal == true {
			self.topViewController?.dismiss(animated: true, completion: nil)
		}
		
		self.popToViewController(vc, animated: true)
	}
	
	func popToExchange() {
		guard let vc = self.viewControllers.first(where: { $0 is TabBarController }) as? TabBarController else {
			logSentryWarning(className: String(describing: TabBarController.self), stack: self.viewControllers.map({ String(describing: $0.self) }))
			self.popToRootViewController(animated: true)
			return
		}
		
		vc.selectedIndex = TabBarController.exchangeIndex
		
		if self.topViewController?.isModal == true {
			self.topViewController?.dismiss(animated: true, completion: nil)
		}
		
		self.popToViewController(vc, animated: true)
	}
	
	func popToChooseAmount() {
		guard let vc = self.viewControllers.first(where: { $0 is ChooseAmountViewController }) as? ChooseAmountViewController else {
			logSentryWarning(className: String(describing: ChooseAmountViewController.self), stack: self.viewControllers.map({ String(describing: $0.self) }))
			self.popToRootViewController(animated: true)
			return
		}
		
		self.popToViewController(vc, animated: true)
	}
	
	func popToSettings() {
		guard let vc = self.viewControllers.first(where: { $0 is TabBarController }) as? TabBarController else {
			logSentryWarning(className: String(describing: TabBarController.self), stack: self.viewControllers.map({ String(describing: $0.self) }))
			self.popToRootViewController(animated: true)
			return
		}
		
		vc.selectedIndex = TabBarController.settingsIndex
		self.popToViewController(vc, animated: true)
	}
	
	func popToDelegate() {
		guard let vc = self.viewControllers.first(where: { $0 is DelegateViewController }) as? DelegateViewController else {
			logSentryWarning(className: String(describing: DelegateViewController.self), stack: self.viewControllers.map({ String(describing: $0.self) }))
			self.popToRootViewController(animated: true)
			return
		}
		
		self.popToViewController(vc, animated: true)
	}
	
	func popToContacts() {
		guard let vc = self.viewControllers.first(where: { $0 is TabBarController }) as? TabBarController else {
			logSentryWarning(className: String(describing: TabBarController.self), stack: self.viewControllers.map({ String(describing: $0.self) }))
			self.popToRootViewController(animated: true)
			return
		}
		
		vc.selectedIndex = TabBarController.contactsIndex
		self.popToViewController(vc, animated: true)
	}
	
	func containsSettingViewController() -> Bool {
		return self.viewControllers.first(where: { $0 is TabBarController }) != nil
	}
	
	func containsViewController<T: UIViewController>(controller: T.Type) -> Bool {
		return self.viewControllers.first(where: { $0.self is T }) != nil
	}
	
	private func logSentryWarning(className: String, stack: [String]) {
		os_log(.error, log: .viewLifeCycle, "Can't find %@ in nav stack", className)
		
		let event = Sentry.Event(level: .warning)
		event.exceptions = [Exception(value: "Can't find \(className) in nav stack", type: "View Lifecycle")]
		event.extra = ["Stack": "\(stack)"]
		
		SentrySDK.capture(event: event)
	}
}
