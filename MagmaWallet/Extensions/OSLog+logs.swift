//
//  OSLog+logs.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 10/01/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import os.log

extension OSLog {
	private static var subsystem = Bundle.main.bundleIdentifier!
	
	static let viewLifeCycle = OSLog(subsystem: subsystem, category: "view lifecycle")
	static let network = OSLog(subsystem: subsystem, category: "network")
	static let currencyRates = OSLog(subsystem: subsystem, category: "currency rates")
	static let tezos = OSLog(subsystem: subsystem, category: "tezos")
	static let mvvm = OSLog(subsystem: subsystem, category: "mvvm")
	static let models = OSLog(subsystem: subsystem, category: "models")
	static let bakers = OSLog(subsystem: subsystem, category: "bakers")
	static let contacts = OSLog(subsystem: subsystem, category: "contacts")
	static let sentry = OSLog(subsystem: subsystem, category: "sentry")
	static let inDEXTer = OSLog(subsystem: subsystem, category: "InDEXTer")
	static let tzkt = OSLog(subsystem: subsystem, category: "TZKT")
	static let mock = OSLog(subsystem: subsystem, category: "mock")
	static let moonpay = OSLog(subsystem: subsystem, category: "moonpay")
}
