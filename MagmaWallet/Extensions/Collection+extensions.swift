//
//  Collection+extensions.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 03/11/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation

extension Collection {
	
	/// Returns the element at the specified index if it is within bounds, otherwise nil.
	subscript (safe index: Index) -> Element? {
		return indices.contains(index) ? self[index] : nil
	}
}
