//
//  UIStoryboard+viewControllers.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 16/01/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import os.log

extension UIStoryboard {
	
	class func receiveViewController() -> ReceiveViewController {
		guard let vc = UIStoryboard(name: "Home", bundle: Bundle.main).instantiateViewController(withIdentifier: "receiveModal") as? ReceiveViewController else {
			os_log(.error, log: .viewLifeCycle, "Can't find or parse ReceiveViewController")
			if Thread.current.isRunningXCTest { fatalError("Can't find ReceiveViewController") }
			
			return ReceiveViewController()
		}
		
		vc.modalPresentationStyle = .overCurrentContext
		
		return vc
	}
	
	class func sendViewController() -> ChooseAddressContainerViewController {
		guard let vc = UIStoryboard(name: "Send", bundle: Bundle.main).instantiateInitialViewController() as? ChooseAddressContainerViewController else {
			os_log(.error, log: .viewLifeCycle, "Can't find or parse ChooseAddressViewController")
			if Thread.current.isRunningXCTest { fatalError("Can't find ChooseAddressViewController") }
			
			return ChooseAddressContainerViewController()
		}
		
		return vc
	}
	
	class func sendChooseTokenViewController() -> ChooseTokenViewController {
		guard let vc = UIStoryboard(name: "Send", bundle: Bundle.main).instantiateViewController(withIdentifier: "chooseToken") as? ChooseTokenViewController else {
			os_log(.error, log: .viewLifeCycle, "Can't find or parse ChooseTokenViewController")
			if Thread.current.isRunningXCTest { fatalError("Can't find ChooseTokenViewController") }
			
			return ChooseTokenViewController()
		}
		
		return vc
	}
	
	class func sendChooseAmountViewController() -> ChooseAmountViewController {
		guard let vc = UIStoryboard(name: "Send", bundle: Bundle.main).instantiateViewController(withIdentifier: "chooseAmount") as? ChooseAmountViewController else {
			os_log(.error, log: .viewLifeCycle, "Can't find or parse ChooseAmountViewController")
			if Thread.current.isRunningXCTest { fatalError("Can't find ChooseAmountViewController") }
			
			return ChooseAmountViewController()
		}
		
		return vc
	}
	
	class func contactPermissionsViewController() -> ContactsPermissionViewController {
		guard let vc = UIStoryboard(name: "Contacts", bundle: Bundle.main).instantiateViewController(withIdentifier: "recoveryPhrasePrompt") as? ContactsPermissionViewController else {
			os_log(.error, log: .viewLifeCycle, "Can't find or parse ContactsPermissionViewController")
			if Thread.current.isRunningXCTest { fatalError("Can't find ContactsPermissionViewController") }
			
			return ContactsPermissionViewController()
		}
		
		return vc
	}
	
	class func addContactViewController() -> AddContactViewController {
		guard let vc = UIStoryboard(name: "Contacts", bundle: Bundle.main).instantiateViewController(withIdentifier: "addContact") as? AddContactViewController else {
			os_log(.error, log: .viewLifeCycle, "Can't find or parse AddContactViewController")
			if Thread.current.isRunningXCTest { fatalError("Can't find AddContactViewController") }
			
			return AddContactViewController()
		}
		
		return vc
	}
	
	class func loginViewController() -> UIViewController {
		return UIStoryboard(name: "Login", bundle: Bundle.main).instantiateInitialViewController() ?? UIViewController()
	}
	
	class func pincodeViewController() -> PinCodeViewController {
		guard let vc = UIStoryboard(name: "Onboarding", bundle: Bundle.main).instantiateViewController(withIdentifier: "pincode") as? PinCodeViewController else {
			os_log(.error, log: .viewLifeCycle, "Can't find or parse PinCodeViewController")
			if Thread.current.isRunningXCTest { fatalError("Can't find PinCodeViewController") }
			
			return PinCodeViewController()
		}
		
		return vc
	}
}
