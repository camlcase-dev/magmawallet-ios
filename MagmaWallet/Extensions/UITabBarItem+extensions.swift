//
//  UITabBarItem+extensions.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 27/04/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit

extension UITabBarItem {
	
	@IBInspectable var localizedTitle: String {
        set(value) {
			self.title = value.localized
        }
        get {
			return self.title ?? ""
        }
    }
}
