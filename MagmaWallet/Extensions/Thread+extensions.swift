//
//  Thread+extensions.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 27/01/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation

extension Thread {
	
	var isRunningXCTest: Bool {
		return NSClassFromString("XCTest") != nil
	}
}
