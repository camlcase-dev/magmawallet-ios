//
//  TokenAmountVlaidator.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 30/04/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import MagmaWalletKit
import camlKit

public struct TokenAmountValidator: Validator {
	
	private let numberFormatter = NumberFormatter()
	
	public var isXTZ: Bool = true
	public var balanceLimit: TokenAmount = TokenAmount.zero()
	public var decimalPlaces: Int = 6
	
	public init () {
		
	}
	
	public init (isXTZ: Bool, balanceLimit: TokenAmount, decimalPlaces: Int = 6) {
		self.isXTZ = isXTZ
		self.balanceLimit = balanceLimit
		self.decimalPlaces = decimalPlaces
	}
	
	public func validate(text: String) -> Bool {
		guard let d = TokenAmount(fromNormalisedAmount: text, decimalPlaces: decimalPlaces) else {
			return false
		}
		
		// no negative numbers
		if d < TokenAmount.zero() {
			return false
		}
		
		// Prevent user entering more than the max number of decimal places the token supports
		if doesAmountExceedMaxDecimalDigits(text: text) {
			return false
		}
		
		// no more than balance
		if d > balanceLimit {
			return false
		}
		
		return true
	}
	
	public func restrictEntryIfInvalid(text: String) -> Bool {
		if doesAmountExceedMaxDecimalDigits(text: text) {
			return true
		}
		
		return false
	}
	
	private func doesAmountExceedMaxDecimalDigits(text: String) -> Bool {
		let localizedText = text.replacingOccurrences(of: ",", with: ".")
		let components = localizedText.components(separatedBy: ".")
		
		// Can't have more than 1 decimal place
		if components.count > 2 {
			return true
		}
		
		// check the number of digits after the decimal place
		if components.count > 1, components.last?.count ?? 0 > decimalPlaces {
			return true
		}
		
		return false
	}
}
