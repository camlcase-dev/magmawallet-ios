//
//  DerivationPathValidator.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 11/03/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import Foundation
import MagmaWalletKit
import camlKit
import WalletCore

public struct DerivationPathValidator: Validator {
	
	let uneditableDerivationPath = "m/44'/1729'/"
	let allowedDerivationPathCharacters = CharacterSet(charactersIn: "0123456789/'")
	let allowedTrailingDerivationPathCharacters = CharacterSet(charactersIn: "0123456789'")
	
	public init () {
		
	}
	
	public func validate(text: String) -> Bool {
		
		// Prevent user from removing uneditablesection
		if text.count <= uneditableDerivationPath.count-1 {
			return false
		}
		
		if text.prefix(uneditableDerivationPath.count) != uneditableDerivationPath {
			return false
		}
		
		// Prevent user from entering invalid cahracters
		let userEnteredText = text.replacingOccurrences(of: uneditableDerivationPath, with: "")
		if userEnteredText.rangeOfCharacter(from: allowedDerivationPathCharacters.inverted) != nil {
			return false
		}
		
		// check last character is number or apostrophe
		return (text.last ?? Character("")).unicodeScalars.contains(where: allowedTrailingDerivationPathCharacters.contains(_:))
	}
	
	public func restrictEntryIfInvalid(text: String) -> Bool {
		
		// the last character must be in this list, but we can't stop them typing otherwise they won't be able to enter a full string
		if text.last == "/" {
			return false
		}
		
		return true
	}
}
