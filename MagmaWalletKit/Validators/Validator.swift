//
//  Validator.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 27/02/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation

public protocol Validator {
	func validate(text: String) -> Bool
	func restrictEntryIfInvalid(text: String) -> Bool
}
