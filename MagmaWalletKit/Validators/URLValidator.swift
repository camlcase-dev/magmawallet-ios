//
//  URLValidator.swift
//  MagmaWalletKit
//
//  Created by Simon Mcloughlin on 21/04/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import Foundation

public struct URLValidator: Validator {
	
	public init() {
		
	}
	
	public func validate(text: String) -> Bool {
		return URL(string: text) != nil
	}
	
	public func restrictEntryIfInvalid(text: String) -> Bool {
		return false
	}
}
