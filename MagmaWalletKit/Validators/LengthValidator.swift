//
//  LengthValidator.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 25/03/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation

public struct LengthValidator: Validator {
	
	private let maxLength: Int
	private let minLength: Int
	
	public init(maxLength: Int, minLength: Int) {
		self.maxLength = maxLength
		self.minLength = minLength
	}
	
	public func validate(text: String) -> Bool {
		return (text.count <= self.maxLength) && (text.count >= self.minLength)
	}
	
	public func restrictEntryIfInvalid(text: String) -> Bool {
		if text.count > maxLength {
			return true
		}
		
		return false
	}
}
