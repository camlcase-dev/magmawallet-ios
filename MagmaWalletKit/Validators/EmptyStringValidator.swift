//
//  EmptyStringValidator.swift
//  MagmaWalletKit
//
//  Created by Simon Mcloughlin on 04/09/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation

public struct EmptyStringValidator: Validator {
	
	public init() {
		
	}
	
	public func validate(text: String) -> Bool {
		return !text.trimmingCharacters(in: CharacterSet.newlines.union(CharacterSet.whitespaces)).isEmpty
	}
	
	public func restrictEntryIfInvalid(text: String) -> Bool {
		return false
	}
}
