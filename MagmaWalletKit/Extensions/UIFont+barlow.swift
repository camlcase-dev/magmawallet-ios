//
//  UIFont+barlow.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 10/01/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import os.log

public enum BarlowFont: String {
	case bold = "Barlow-Bold"
	case light = "Barlow-Light"
	case medium = "Barlow-Medium"
	case regular = "Barlow-Regular"
	case semiBold = "Barlow-SemiBold"
}

extension UIFont {
	
	public class func barlowFont(_ font: BarlowFont, withSize size: CGFloat) -> UIFont {
		guard let customFont = UIFont(name: font.rawValue, size: size) else {
			os_log(OSLogType.error, log: .default, "Requested font not installed or misspelled", font.rawValue)
			
			return UIFont.systemFont(ofSize: size)
		}
		
		// Return a dynamic font type
		return UIFontMetrics.default.scaledFont(for: customFont)
	}
}

