//
//  String+QR.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 27/01/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit

extension String {
	
	// MARK: QR
	// https://www.hackingwithswift.com/example-code/media/how-to-create-a-qr-code
	public func generateQRCode() -> UIImage? {
		let data = self.data(using: String.Encoding.ascii)

		if let filter = CIFilter(name: "CIQRCodeGenerator") {
			filter.setValue(data, forKey: "inputMessage")
			let transform = CGAffineTransform(scaleX: 3, y: 3)

			if let output = filter.outputImage?.transformed(by: transform) {
				return UIImage(ciImage: output)
			}
		}

		return nil
	}

	// MARK: Localisation
    public var localized: String {
        return NSLocalizedString(self, comment: "")
    }
    
    public func localized(_ arguments: CVarArg...) -> String {
        return String(format: self.localized, arguments: arguments)
    }


	// MARK: Formatters
	public func extractNumbers() -> String {
		return filter { "0"..."9" ~= $0 }
	}
	
	public func tezosAddressFormat() -> String {
		if self.count < 36 {
			return self
		}
		
		// Tezos addresses are too long for most of the screens. Most people will only look at the start and the end
		// Implmenting a common "middle truncation" to ensure addresses are always displayed in a similar format
		// throughout the app, to avoid confusion
		let first14Index = self.index(self.startIndex, offsetBy: 14)
		let first14 = self[..<first14Index]
		
		let last6Index = self.index(self.endIndex, offsetBy: -6)
		let last6 = self[last6Index...]
		
		return first14 + "..." + last6
	}
	
	
	// MARK: Regex
	public func matches(forRegex regex: String) -> [String] {
		do {
			let regex = try NSRegularExpression(pattern: regex, options: [])
			let nsString = self as NSString
			let results = regex.matches(in: self, options: [], range: NSMakeRange(0, nsString.length))
			
			return results.map { nsString.substring(with: $0.range)}
			
		} catch let error {
			print("invalid regex: \(error.localizedDescription)")
			return []
		}
	}
	
	// MARK: Boolean
	public var boolValue: Bool {
		return (self as NSString).boolValue
	}
	
	
	// MARK: Sizing
	public func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
		let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
		let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)

		return ceil(boundingBox.height)
	}

	public func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
		let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
		let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)

		return ceil(boundingBox.width)
	}
}
