//
//  MagmaWalletKit.h
//  MagmaWalletKit
//
//  Created by Simon Mcloughlin on 06/07/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for MagmaWalletKit.
FOUNDATION_EXPORT double MagmaWalletKitVersionNumber;

//! Project version string for MagmaWalletKit.
FOUNDATION_EXPORT const unsigned char MagmaWalletKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MagmaWalletKit/PublicHeader.h>


