//
//  ThemeLabel.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 22/04/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit

@IBDesignable open class ThemeLabel: UILabel {
	
	// Needed for custom attributed strings
	public static let seedPhraseWordFont = UIFont.barlowFont(.regular, withSize: 16)
	public static let seedPhraseNumberFont = UIFont.barlowFont(.regular, withSize: 12)
	
	
	// Set localized text via interface builder using a more standard approach
	@IBInspectable public var localizedText: String?
	
	override public init(frame: CGRect) {
		super.init(frame: frame)
		
		updateStyles()
	}
	
	required public init?(coder: NSCoder) {
		super.init(coder: coder)
		
		updateStyles()
	}
	
	open override func awakeFromNib() {
		super.awakeFromNib()
		
		updateStyles()
	}
	
	open override func prepareForInterfaceBuilder() {
		super.prepareForInterfaceBuilder()
		
		updateStyles()
	}
	
	// Used to set via, and update Interface builder
	@IBInspectable public var largeHeading: Bool = false
	@IBInspectable public var smallHeading: Bool = false
	@IBInspectable public var primaryDescriptiveText: Bool = false
	@IBInspectable public var secondaryDescriptiveText: Bool = false
	@IBInspectable public var tertiaryDescriptiveText: Bool = false
	@IBInspectable public var waitingText: Bool = false
	@IBInspectable public var sectionHeading: Bool = false
	@IBInspectable public var sectionEmpty: Bool = false
	@IBInspectable public var activityGrouping: Bool = false
	@IBInspectable public var amount: Bool = false
	@IBInspectable public var heading: Bool = false
	@IBInspectable public var subheading: Bool = false
	@IBInspectable public var inlineError: Bool = false
	
	private func updateStyles() {
		if largeHeading { font = UIFont.barlowFont(.light, withSize: 32) }
		else if smallHeading { font = UIFont.barlowFont(.medium, withSize: 18) }
		else if primaryDescriptiveText { font = UIFont.barlowFont(.regular, withSize: 18) }
		else if secondaryDescriptiveText { font = UIFont.barlowFont(.regular, withSize: 16) }
		else if tertiaryDescriptiveText { font = UIFont.barlowFont(.regular, withSize: 14) }
		else if waitingText { font = UIFont.barlowFont(.regular, withSize: 14) }
		else if sectionHeading { font = UIFont.barlowFont(.bold, withSize: 14) }
		else if sectionEmpty { font = UIFont.barlowFont(.regular, withSize: 12) }
		else if activityGrouping { font = UIFont.barlowFont(.semiBold, withSize: 13) }
		else if amount { font = UIFont.barlowFont(.semiBold, withSize: 16) }
		else if heading { font = UIFont.barlowFont(.semiBold, withSize: 16) }
		else if subheading { font = UIFont.barlowFont(.regular, withSize: 12) }
		else if inlineError { font = UIFont.barlowFont(.semiBold, withSize: 12) }
		
		if let text = localizedText {
			self.text = text.localized
		}
	}
}
