//
//  GradientView.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 21/04/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit

@IBDesignable open class GradientView: UIView {
    
    private var gradientLayer = CAGradientLayer()
	private var colors: [Any] = []
	private var locations: [NSNumber] = []
	
	required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
		self.layer.insertSublayer(gradientLayer, at: 0)
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
	
	@IBInspectable public var light: Bool = false {
		didSet {
			if light {
				self.colors = [
					UIColor(red: 153.0 / 255.0, green: 40.0 / 255.0, blue: 113.0 / 255.0, alpha: 1).cgColor,
					UIColor(red: 190.0 / 255.0, green: 52.0 / 255.0, blue: 103.0 / 255.0, alpha: 1).cgColor,
					UIColor(red: 234.0 / 255.0, green: 89.0 / 255.0, blue: 94.0 / 255.0, alpha: 1).cgColor,
					UIColor(red: 245.0 / 255.0, green: 132.0 / 255.0, blue: 72.0 / 255.0, alpha: 1).cgColor
				]
				self.locations = [0.0, 0.24, 0.57, 1.0]
			}
		}
	}
	
	@IBInspectable public var rich: Bool = false {
		didSet {
			if rich {
				self.colors = [
					UIColor(red: 108.0 / 255.0, green: 28.0 / 255.0, blue: 100.0 / 255.0, alpha: 1).cgColor,
					UIColor(red: 230.0 / 255.0, green: 59.0 / 255.0, blue: 74.0 / 255.0, alpha: 1).cgColor,
					UIColor(red: 246.0 / 255.0, green: 118.0 / 255.0, blue: 46.0 / 255.0, alpha: 1).cgColor
				]
				self.locations = [0.0, 0.5, 1.0]
			}
		}
	}
	
	@IBInspectable public var withTexture: Bool = false {
		didSet {
			if withTexture {
				let backgroundImage = UIImage(named: "Texture", in: Bundle(for: GradientView.self), compatibleWith: self.traitCollection)
				let backgroundTextureImageView = UIImageView(image: backgroundImage)
				backgroundTextureImageView.translatesAutoresizingMaskIntoConstraints = false
				
				self.addSubview(backgroundTextureImageView)
				NSLayoutConstraint.activate([
					backgroundTextureImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
					backgroundTextureImageView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
					backgroundTextureImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0)
				])
			}
		}
	}
	
	@IBInspectable public var diagonalWhite: Bool = false {
		didSet {
			if diagonalWhite {
				self.colors = [
					UIColor(red: 243.0 / 255.0, green: 248.0 / 255.0, blue: 255.0 / 255.0, alpha: 0.12).cgColor,
					UIColor(red: 243.0 / 255.0, green: 248.0 / 255.0, blue: 255.0 / 255.0, alpha: 0.20) // Not passing CGColor on purpose
				]
				self.locations = [0.0, 1.0]
			}
		}
	}
    
	open override func layoutSubviews() {
		super.layoutSubviews()
		
		self.gradientLayer.frame = self.bounds
		self.gradientLayer.colors = self.colors
		self.gradientLayer.locations = self.locations
		
		if diagonalWhite {
			self.gradientLayer.startPoint = CGPoint(x: 1, y: 1)
			self.gradientLayer.endPoint = CGPoint(x: 0, y: 0)
		}
    }
}
