//
//  ThemeTextfield.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 21/01/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import os.log

@objc public protocol ThemeTextfieldProtocol: class {
	@objc func rightButtonTapped(_ textfield: ThemeTextfield)
	func didClear()
	func didTapDone()
	func didBeingEditing(_ textField: UITextField)
	func didEndEditing(_ textField: UITextField)
}

public protocol ThemeTextfieldValidationProtocol: class {
	func validated(_ validated: Bool, textfield: ThemeTextfield, forText text: String)
}

open class ThemeTextfield: UITextField {
	
	private let bottomLine = CALayer()
	private var customRightViewButton: UIButton?
	private var customRightViewLabel: UILabel?
	private var inClearMode = false
	private var showingPassword = false
	
	private static let imageNameClear = "Clear-"
	private static var imageSizeClear: CGSize {
		get {
			return UIScreen.main.scale == 3.0 ? CGSize(width: 20, height: 20) : CGSize(width: 14, height: 14)
		}
	}
	
	private static let imageNameScan = "QR-"
	private static var imageSizeScan: CGSize {
		get {
			return UIScreen.main.scale == 3.0 ? CGSize(width: 40, height: 40) : CGSize(width: 26, height: 26)
		}
	}
	
	private static var imageSizeScanDarkMode: CGSize {
		get {
			return UIScreen.main.scale == 3.0 ? CGSize(width: 21, height: 21) : CGSize(width: 20, height: 20)
		}
	}
	
	private static let imageNameContacts = "Contact-Search"
	private static var imageSizeContacts: CGSize {
		get {
			return UIScreen.main.scale == 3.0 ? CGSize(width: 24, height: 24) : CGSize(width: 20, height: 20)
		}
	}
	
	private static let imageNamePasswordShow = "Password-Show"
	private static let imageNamePasswordHide = "Password-Hide"
	private static var imageSizePassword: CGSize {
		get {
			return UIScreen.main.scale == 3.0 ? CGSize(width: 21, height: 21) : CGSize(width: 18, height: 18)
		}
	}
	
	private static let imageNameLight = "Light"
	private static let imageNameDark = "Dark"
	private static let darkModePadding = CGFloat(12)
	
	@IBInspectable public var pinField: Bool = false {
		didSet {
			if pinField {
				font = UIFont.barlowFont(.regular, withSize: 48)
				keyboardType = .numberPad
				textAlignment = .center
			}
		}
	}
	
	@IBInspectable public var addressField: Bool = false {
		didSet {
			if addressField {
				font = UIFont.barlowFont(.regular, withSize: 14)
				keyboardType = .default
				textAlignment = .left
			}
		}
	}
	
	@IBInspectable public var amountField: Bool = false {
		didSet {
			if amountField {
				font = UIFont.barlowFont(.regular, withSize: 48)
				textAlignment = .center
			}
		}
	}
	
	@IBInspectable public var passphraseField: Bool = false {
		didSet {
			if passphraseField {
				font = UIFont.barlowFont(.regular, withSize: 18)
				keyboardType = .default
				textAlignment = .left
				self.isSecureTextEntry = true
				self.rightMode = .passphrase
			}
		}
	}
	
	@IBInspectable public var darkMode: Bool = false {
		didSet {
			if darkMode {
				backgroundColor = UIColor(red: 240.0 / 255.0, green: 240.0 / 255.0, blue: 240.0 / 255.0, alpha: 1)
			}
		}
	}
	
	@IBInspectable public var rightModeTextFont: UIFont = UIFont.barlowFont(.bold, withSize: 28) {
		didSet {
			setupRightMode()
		}
	}
	
	@IBInspectable public var textRectPadding: Bool = false
	
	@IBInspectable public var bottomBorder: Bool = true
	
	public enum RightMode: Equatable {
		case clearOnly
		case scanAndClear
		case contactSearch
		case passphrase
		case text(String)
	}
	
	public var validator: Validator?
	
	public var rightMode: RightMode? {
		didSet {
			setupRightMode()
		}
	}
	
	public weak var themeTextfieldDelegate: ThemeTextfieldProtocol?
	public weak var themeTextfieldValidationDelegate: ThemeTextfieldValidationProtocol?
	public var isValidated = true
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		setup()
	}
	
	required public init?(coder: NSCoder) {
		super.init(coder: coder)
		
		setup()
	}
	
	func setup() {
		self.delegate = self
	}
	
	open override func layoutSubviews() {
		super.layoutSubviews()
		
		if darkMode {
			tintColor = .black
		} else {
			tintColor = .white
		}
		
		if bottomBorder {
			addBottomBorder()
		}
	}
	
	open override func textRect(forBounds bounds: CGRect) -> CGRect {
		if rightMode != nil || textRectPadding == true {
			return CGRect(x: bounds.origin.x + ThemeTextfield.darkModePadding, y: bounds.origin.y, width: bounds.size.width - (ThemeTextfield.darkModePadding*4), height: bounds.size.height)
		} else {
			return super.textRect(forBounds: bounds)
		}
	}
	
	open override func editingRect(forBounds bounds: CGRect) -> CGRect {
		if rightMode != nil || textRectPadding == true {
			return CGRect(x: bounds.origin.x + ThemeTextfield.darkModePadding, y: bounds.origin.y, width: bounds.size.width - (ThemeTextfield.darkModePadding*4), height: bounds.size.height)
		} else {
			return super.editingRect(forBounds: bounds)
		}
	}
	
	open override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
		if rightMode != nil {
			var textRect = super.rightViewRect(forBounds: bounds)
			textRect.origin.x -= ThemeTextfield.darkModePadding
			return textRect
		} else {
			return super.rightViewRect(forBounds: bounds)
		}
	}
	
	@IBInspectable public var localizedPlaceholder: String {
        set(value) {
			self.placeholder = value.localized
        }
        get {
			return self.placeholder ?? ""
        }
    }
	
	private func clearImageName() -> String {
		return darkMode ? ThemeTextfield.imageNameClear + ThemeTextfield.imageNameDark : ThemeTextfield.imageNameClear + ThemeTextfield.imageNameLight
	}
	
	private func qrImageName() -> String {
		return darkMode ? ThemeTextfield.imageNameScan + ThemeTextfield.imageNameDark : ThemeTextfield.imageNameScan + ThemeTextfield.imageNameLight
	}
	
	func setupRightMode() {
		
		switch rightMode {
			case .clearOnly:
				setupCustomButton(withImageNamed: clearImageName(), size: ThemeTextfield.imageSizeClear)
			
			case .scanAndClear:
				setupCustomButton(withImageNamed: qrImageName(), size: darkMode ? ThemeTextfield.imageSizeScanDarkMode : ThemeTextfield.imageSizeScan)
			
			case .contactSearch:
				setupCustomButton(withImageNamed: ThemeTextfield.imageNameContacts, size: ThemeTextfield.imageSizeContacts)
				
			case .passphrase:
				setupCustomButton(withImageNamed: ThemeTextfield.imageNamePasswordShow, size: ThemeTextfield.imageSizePassword)
			
			case .text(let text):
				setupCustomLabel(withText: text)
			
			case .none:
				rightView = nil
		}
	}
	
	private func setupCustomButton(withImageNamed imageName: String, size: CGSize) {
		customRightViewButton = UIButton(type: .custom)
		customRightViewButton?.setBackgroundImage(UIImage(named: imageName), for: .normal)
		customRightViewButton?.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
		
		if imageName.contains(ThemeTextfield.imageNameClear) {
			inClearMode = true
			customRightViewButton?.removeTarget(self, action: #selector(callDelegateRightButtonTapped), for: .touchUpInside)
			customRightViewButton?.addTarget(self, action: #selector(clear), for: .touchUpInside)
			
		} else if imageName == ThemeTextfield.imageNamePasswordShow || imageName == ThemeTextfield.imageNamePasswordHide {
			customRightViewButton?.addTarget(self, action: #selector(showHidePassword), for: .touchUpInside)
		
		} else {
			inClearMode = false
			customRightViewButton?.removeTarget(self, action: #selector(clear), for: .touchUpInside)
			customRightViewButton?.addTarget(self, action: #selector(callDelegateRightButtonTapped), for: .touchUpInside)
		}
		
		self.clearButtonMode = .never
		self.rightView = customRightViewButton
		self.rightViewMode = .always
	}
	
	@objc private func callDelegateRightButtonTapped() {
		themeTextfieldDelegate?.rightButtonTapped(self)
	}
	
	private func setupCustomLabel(withText text: String) {
		customRightViewLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 30, height: 20))
		customRightViewLabel?.text = text
		
		customRightViewLabel?.font = rightModeTextFont
		customRightViewLabel?.textColor = UIColor.init(named: "Subheading")
		
		customRightViewLabel?.sizeToFit()
		
		self.clearButtonMode = .never
		self.rightView = customRightViewLabel
		self.rightViewMode = .always
	}
	
	public func revalidateTextfield() {
		_ = textField(self, shouldChangeCharactersIn: NSRange(location: (self.text?.count ?? 1)-1, length: 0), replacementString: "")
	}
	
	@objc func clear() {
		self.text = ""
		
		updateRightButton(fullText: "")
		themeTextfieldDelegate?.didClear()
		themeTextfieldValidationDelegate?.validated(false, textfield: self, forText: "")
	}
	
	@objc func showHidePassword() {
		showingPassword = !showingPassword
		self.isSecureTextEntry = !showingPassword
		
		updateRightButton(fullText: "")
	}
	
	private func addBottomBorder() {
		bottomLine.removeFromSuperlayer()
		
		bottomLine.frame = CGRect(x: 0.0, y: frame.height + 5, width: frame.width, height: 1.0)
		bottomLine.backgroundColor = UIColor(named: "TextFieldBorder")?.cgColor
		self.borderStyle = UITextField.BorderStyle.none
		self.layer.addSublayer(bottomLine)
	}
	
	private func updateRightButton(fullText: String) {
		if passphraseField == true {
			setupCustomButton(withImageNamed: showingPassword ? ThemeTextfield.imageNamePasswordHide : ThemeTextfield.imageNamePasswordShow, size: ThemeTextfield.imageSizePassword)
			return
		}
		
		if fullText == "" && inClearMode && clearModeSwitchingAllowed(rightMode: rightMode) {
			setupCustomButton(withImageNamed: qrImageName(), size: darkMode ? ThemeTextfield.imageSizeScanDarkMode : ThemeTextfield.imageSizeScan)
			
		} else if !inClearMode && clearModeSwitchingAllowed(rightMode: rightMode) {
			setupCustomButton(withImageNamed: clearImageName(), size: ThemeTextfield.imageSizeClear)
		}
	}
	
	private func clearModeSwitchingAllowed(rightMode: RightMode?) -> Bool {
		if let mode = rightMode, case RightMode.scanAndClear = mode {
			return true
		}
		
		return false
	}
	
	public func addDoneToolbar(onDone: (target: Any, action: Selector)? = nil, onCancel: (target: Any, action: Selector)? = nil) {
        let onDone = onDone ?? (target: self, action: #selector(doneButtonTapped))

		let toolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        toolbar.barStyle = .default
		
        toolbar.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil),
			UIBarButtonItem(title: "action_done".localized, style: .done, target: onDone.target, action: onDone.action)
        ]
		toolbar.sizeToFit()

        self.inputAccessoryView = toolbar
    }
	
    @objc func doneButtonTapped() {
		self.resignFirstResponder()
		self.themeTextfieldDelegate?.didTapDone()
	}
}

extension ThemeTextfield: UITextFieldDelegate {
	
	public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		
		// If return key is "done", automatically have the textfield resign responder and not type anything
		if self.returnKeyType == .done, (string as NSString).rangeOfCharacter(from: CharacterSet.newlines).location != NSNotFound {
			textField.resignFirstResponder()
			return false
		}
		
		// only continue if a validator is assigned
		guard let validator = self.validator else {
			return true
		}
		
		if string.rangeOfCharacter(from: CharacterSet.newlines) != nil {
			textField.resignFirstResponder()
			return false
		}
		
		if let textFieldString = textField.text, let swtRange = Range(range, in: textFieldString) {
			let fullString = textFieldString.replacingCharacters(in: swtRange, with: string)
			
			updateRightButton(fullText: fullString)
			
			if validator.validate(text: fullString) {
				themeTextfieldValidationDelegate?.validated(true, textfield: self, forText: fullString)
				return true
			} else {
				
				// If restrictions are on, we are going to stop whatever the user types from appearing in the textfield.
				// However that new character will still return inside `fullString` if not prevented.
				// In the case of restrictions turned on, if validation fails we check if the previously entered string will pass.
				//
				// E.g. we are restricting the user to enter no more than 6 decimal places
				// If the user enters 7, we want the textfield to not show the 7th digit and only show the 6 previously entered.
				// However this will return a failed validation, disabling a continue/next button, desptite the fact that what the user sees in the textfield should be fine
				if validator.restrictEntryIfInvalid(text: fullString) {
					themeTextfieldValidationDelegate?.validated(validator.validate(text: textFieldString), textfield: self, forText: textFieldString)
					
				} else {
					themeTextfieldValidationDelegate?.validated(false, textfield: self, forText: fullString)
				}
				
				// If restrict entry turned on, prevent text from being entered until the problem is solved.background				// e.g. meant for cases such as the tokenAmountValidator where we may be restricting the maximum amount of decimal places
				// Not mean for cases such as address validation, where validation will fail until an exact match is entered
				return !validator.restrictEntryIfInvalid(text: fullString)
			}
		}
		
		themeTextfieldValidationDelegate?.validated(false, textfield: self, forText: "")
		return true
	}
	
	public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		self.resignFirstResponder()
		return true
	}
	
	public func textFieldDidBeginEditing(_ textField: UITextField) {
		themeTextfieldDelegate?.didBeingEditing(textField)
	}
	
	public func textFieldDidEndEditing(_ textField: UITextField) {
		themeTextfieldDelegate?.didEndEditing(textField)
	}
}
