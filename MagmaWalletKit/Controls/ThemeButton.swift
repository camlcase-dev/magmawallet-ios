//
//  ThemeButton.swift
//  MagmaWallet
//
//  Created by Simon Mcloughlin on 22/04/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import os.log

@IBDesignable open class ThemeButton: UIButton {
	
	private static let caretImageSize = CGSize(width: 13, height: 7)
	private static let exchangeImageSize = CGSize(width: 17, height: 17)
	private static let customPadding = CGFloat(10)
	
	public static let linkButtonHeight = CGFloat(62)
	
	@IBInspectable var enabledBackgroundColor: UIColor = UIColor.clear
	@IBInspectable var disabledBackgroundColor: UIColor = UIColor.clear
	@IBInspectable var selectedBackgroundColor: UIColor = UIColor.clear
	
	@IBInspectable var enabledTextColor: UIColor = UIColor.white
	@IBInspectable var disabledTextColor: UIColor = UIColor.white
	
	// Only used when both set
	var enabledFont: UIFont? = nil
	var selectedFont: UIFont? = nil
	
	@IBInspectable public var localizedText: String?
	
	@IBInspectable public var primaryButton: Bool = false
	@IBInspectable public var secondaryButton: Bool = false
	@IBInspectable public var dropdownButton: Bool = false
	@IBInspectable public var dropdownStyles: Bool = false
	@IBInspectable public var link: Bool = false
	@IBInspectable public var shareButton: Bool = false
	@IBInspectable public var maxButton: Bool = false
	@IBInspectable public var exchangeRateButton: Bool = false
	@IBInspectable public var slippageButton: Bool = false
	@IBInspectable public var greenActionButton: Bool = false
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		updateStyles()
	}
	
	required public init?(coder: NSCoder) {
		super.init(coder: coder)
		
		updateStyles()
	}
	
	open override func awakeFromNib() {
		super.awakeFromNib()
		
		updateStyles()
	}
	
	open override func prepareForInterfaceBuilder() {
		super.prepareForInterfaceBuilder()
		
		updateStyles()
	}
	
	private func updateStyles() {
		if primaryButton {
			loadStylePrimaryButton()
			
		} else if secondaryButton {
			loadStyleSecondaryButton()
			
		} else if dropdownButton {
			loadStyleDropdownButton()
			
		} else if link {
			loadStylesLinkButton()
			
		} else if shareButton {
			loadStylesShareButton()
			
		} else if maxButton {
			loadStylesMaxButton()
			
		} else if exchangeRateButton {
			loadStylesExchangeRateButton()
			
		} else if slippageButton {
			loadStylesSlippageButton()
			
		} else if greenActionButton {
			loadStylesGreenActionButton()
		}
		
		// These styles can be applied with `dropdownButton` at the same time
		if dropdownStyles {
			loadStyleDropdownStyles()
		}
		
		// Check for selected background colours
		if isSelected {
			backgroundColor = selectedBackgroundColor
			
		} else {
			backgroundColor = isEnabled ? enabledBackgroundColor : disabledBackgroundColor
		}
		
		// Check to see if we need to switch fonts when selected
		if let eFont = enabledFont, let sFont = selectedFont {
			titleLabel?.font = isSelected ? sFont : eFont
		}
		
		
		setTitleColor(enabledTextColor, for: .normal)
		setTitleColor(disabledTextColor, for: .disabled)
		
		if let text = localizedText {
			setTitle(text.localized, for: .normal)
		}
	}
	
	open override var isEnabled: Bool {
		didSet {
			updateStyles()
		}
	}
	
	open override var isSelected: Bool {
		didSet {
			updateStyles()
		}
	}
	
	open override func setTitle(_ title: String?, for state: UIControl.State) {
		if primaryButton {
			let primaryButtonFontAttributes: [NSAttributedString.Key: Any] = [
				NSAttributedString.Key.font: UIFont.barlowFont(.semiBold, withSize: 18),
				NSAttributedString.Key.foregroundColor: UIColor.white,
				NSAttributedString.Key.kern: 1.13
			]
			
			let primaryButtonFontDisabledAttributes: [NSAttributedString.Key: Any] = [
				NSAttributedString.Key.font: UIFont.barlowFont(.semiBold, withSize: 18),
				NSAttributedString.Key.foregroundColor: UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.50),
				NSAttributedString.Key.kern: 1.13
			]
			
			let primaryButtonAttributedString = NSMutableAttributedString(string: "\(title?.uppercased() ?? "")", attributes: primaryButtonFontAttributes)
			let primaryButtonDisabledAttributedString = NSMutableAttributedString(string: "\(title?.uppercased() ?? "")", attributes: primaryButtonFontDisabledAttributes)
			
			setAttributedTitle(primaryButtonAttributedString, for: .normal)
			setAttributedTitle(primaryButtonDisabledAttributedString, for: .disabled)
			
		} else {
			super.setTitle(title, for: state)
		}
	}
	
	open override func titleRect(forContentRect contentRect: CGRect) -> CGRect {
		if dropdownButton {
			return CGRect(
				x: contentRect.origin.x + ThemeButton.customPadding,
				y: contentRect.origin.y,
				width: contentRect.size.width - (ThemeButton.caretImageSize.width + ThemeButton.customPadding*3),
				height: contentRect.size.height
			)
		} else if exchangeRateButton {
			return CGRect(
				x: (ThemeButton.customPadding/2) + ThemeButton.exchangeImageSize.width + (ThemeButton.customPadding),
				y: contentRect.origin.y,
				width: contentRect.size.width - (ThemeButton.exchangeImageSize.width + (ThemeButton.customPadding*1.25)),
				height: contentRect.size.height
			)
		}
		
		return super.titleRect(forContentRect: contentRect)
	}
	
	open override func imageRect(forContentRect contentRect: CGRect) -> CGRect {
		if dropdownButton {
			return CGRect(
				x: contentRect.size.width - (ThemeButton.caretImageSize.width + ThemeButton.customPadding),
				y: (contentRect.size.height/2) - (ThemeButton.caretImageSize.height/2),
				width: ThemeButton.caretImageSize.width,
				height: ThemeButton.caretImageSize.height
			)
		} else if exchangeRateButton {
			return CGRect(
				x: ThemeButton.customPadding/2,
				y: (frame.size.height/2) - (ThemeButton.exchangeImageSize.height/2),
				width: ThemeButton.exchangeImageSize.width,
				height: ThemeButton.exchangeImageSize.height
			)
		}
		
		return super.imageRect(forContentRect: contentRect)
	}
	
	open override func layoutSubviews() {
		if exchangeRateButton {
			
			// Update the width and x, so that the button fits all the text without any truncation
			let width = titleLabel?.text?.width(withConstrainedHeight: self.frame.size.height, font: UIFont.barlowFont(.regular, withSize: 12)) ?? 0
			let totalUpdateWidth = width + ThemeButton.exchangeImageSize.width + (ThemeButton.customPadding*2.5)
			let xOffset = (totalUpdateWidth - frame.size.width)
			frame = CGRect(x: frame.origin.x - xOffset, y: frame.origin.y, width: width + ThemeButton.exchangeImageSize.width + (ThemeButton.customPadding*2.5), height: frame.size.height)
		}

		super.layoutSubviews()
	}
	
	func loadStylePrimaryButton() {
		enabledBackgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.20)
		disabledBackgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.10)
		cornerRadius = 0
		
		// Place image on the opposite side depending on reading direction
		semanticContentAttribute = UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
		
		// Find the image by Bundle, so it will be displayed in IB
		let image = UIImage(named: "Right-Arrow", in: Bundle(for: ThemeButton.self), compatibleWith: self.traitCollection)
		setImage(image, for: .normal)
		tintColor = UIColor.white
		imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
		
		
		// For UITests, if none set, set a default
		if accessibilityIdentifier == nil {
			accessibilityIdentifier = "primary-button"
		}
	}
	
	func loadStyleSecondaryButton() {
		enabledBackgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.20)
		disabledBackgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.10)
		disabledTextColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.50)
		cornerRadius = self.frame.height / 2
		contentEdgeInsets = UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 16)
		
		titleLabel?.font = UIFont.barlowFont(.regular, withSize: 14)
		
		// For UITests, if none set, set a default
		if accessibilityIdentifier == nil {
			accessibilityIdentifier = "secondary-button"
		}
	}
	
	func loadStyleDropdownButton() {
		// Place image on the opposite side depending on reading direction
		semanticContentAttribute = UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
		
		// Align text to leading or trailing depending on semantic
		contentHorizontalAlignment = UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft ? .leading : .trailing
		
		// Find the image by Bundle, so it will be displayed in IB
		let image = UIImage(named: "Caret", in: Bundle(for: ThemeButton.self), compatibleWith: self.traitCollection)
		setImage(image, for: .normal)
		tintColor = UIColor.white
		
		titleLabel?.font = UIFont.barlowFont(.medium, withSize: 18)
		
		// For UITests, if none set, set a default
		if accessibilityIdentifier == nil {
			accessibilityIdentifier = "dropdown-button"
		}
	}
	
	func loadStyleDropdownStyles() {
		enabledBackgroundColor = UIColor(red: 255.0 / 255.0, green: 255.0 / 255.0, blue: 255.0 / 255.0, alpha: 0.15)
		cornerRadius = 4
	}
	
	func loadStylesLinkButton() {
		enabledBackgroundColor = UIColor.clear
		disabledBackgroundColor = UIColor.clear
		disabledTextColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.50)
		cornerRadius = 0
		
		titleLabel?.font = UIFont.barlowFont(.regular, withSize: 16)
		
		// For UITests, if none set, set a default
		if accessibilityIdentifier == nil {
			accessibilityIdentifier = "link-button"
		}
	}
	
	func loadStylesShareButton() {
		enabledBackgroundColor = UIColor(red: 185.0 / 255.0, green: 55.0 / 255.0, blue: 92.0 / 255.0, alpha: 1)
		cornerRadius = 25
		
		titleLabel?.font = UIFont.barlowFont(.semiBold, withSize: 18)
		titleLabel?.textColor = UIColor.white
		
		// For UITests, if none set, set a default
		if accessibilityIdentifier == nil {
			accessibilityIdentifier = "share-button"
		}
	}
	
	func loadStylesMaxButton() {
		enabledBackgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.20)
		disabledBackgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.10)
		disabledTextColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.50)
		cornerRadius = self.frame.height / 2
		contentEdgeInsets = UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 16)
		
		titleLabel?.font = UIFont.barlowFont(.bold, withSize: 9)
		
		// For UITests, if none set, set a default
		if accessibilityIdentifier == nil {
			accessibilityIdentifier = "max-button"
		}
	}
	
	func loadStylesExchangeRateButton() {
		enabledBackgroundColor = UIColor(red: 211.0 / 255.0, green: 79.0 / 255.0, blue: 121.0 / 255.0, alpha: 1)
		cornerRadius = self.frame.height / 2
		
		// Find the image by Bundle, so it will be displayed in IB
		let image = UIImage(named: "Swap", in: Bundle(for: ThemeButton.self), compatibleWith: self.traitCollection)?.withRenderingMode(.alwaysOriginal)
		setImage(image, for: .normal)
		
		titleLabel?.font = UIFont.barlowFont(.regular, withSize: 12)
		
		// For UITests, if none set, set a default
		if accessibilityIdentifier == nil {
			accessibilityIdentifier = "exchangerate-button"
		}
	}
	
	func loadStylesSlippageButton() {
		enabledBackgroundColor = UIColor(red: 36.0 / 255.0, green: 45.0 / 255.0, blue: 60.0 / 255.0, alpha: 0.5)
		selectedBackgroundColor = UIColor(red: 12.0 / 255.0, green: 202.0 / 255.0, blue: 74.0 / 255.0, alpha: 1)
		
		enabledFont = UIFont.barlowFont(.regular, withSize: 14)
		selectedFont = UIFont.barlowFont(.bold, withSize: 14)
		
		cornerRadius = self.frame.height / 2
		contentEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
	}
	
	func loadStylesGreenActionButton() {
		enabledBackgroundColor = UIColor(red: 12.0 / 255.0, green: 202.0 / 255.0, blue: 74.0 / 255.0, alpha: 1)
		cornerRadius = 25
		
		titleLabel?.font = UIFont.barlowFont(.semiBold, withSize: 18)
		titleLabel?.textColor = UIColor.white
		
		// For UITests, if none set, set a default
		if accessibilityIdentifier == nil {
			accessibilityIdentifier = "green-action-button"
		}
	}
}
