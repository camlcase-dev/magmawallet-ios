//
//  ThemeSegmentedControl.swift
//  MagmaWalletKit
//
//  Created by Simon Mcloughlin on 01/09/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import os.log

@IBDesignable open class ThemeSegmentedControl: UISegmentedControl {
	
	@IBInspectable public var normalColor: UIColor = UIColor(red: 255.0 / 255.0, green: 255.0 / 255.0, blue: 255.0 / 255.0, alpha: 0.20)
	@IBInspectable public var selectedColor: UIColor = UIColor(red: 108.0 / 255.0, green: 28.0 / 255.0, blue: 100.0 / 255.0, alpha: 1)
	@IBInspectable public var normalFontColor: UIColor = UIColor.init(white: 1, alpha: 0.75)
	@IBInspectable public var selectedFontColor: UIColor = UIColor.white
	@IBInspectable public var selectedCornerRadius: CGFloat = 0
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		updateStyles()
	}
	
	required public init?(coder: NSCoder) {
		super.init(coder: coder)
		
		updateStyles()
	}
	
	public override init(items: [Any]?) {
		super.init(items: items)
		
		updateStyles()
	}
	
	open override func awakeFromNib() {
		super.awakeFromNib()
		
		updateStyles()
	}
	
	open override func prepareForInterfaceBuilder() {
		super.prepareForInterfaceBuilder()
		
		updateStyles()
	}
	
	private func updateStyles() {
		
		backgroundColor = normalColor
		
		if #available(iOS 13.0, *) {
			selectedSegmentTintColor = selectedColor
			
		} else {
			setBackgroundImage(UIImage(color: .clear), for: .normal, barMetrics: .default)
			setBackgroundImage(UIImage(color: selectedColor), for: .selected, barMetrics: .default)
			setDividerImage(UIImage(color: .clear), forLeftSegmentState: .normal, rightSegmentState: .normal, barMetrics: .default)
			tintColor = normalColor
		}
		
		let titleTextNormalAttributes = [NSAttributedString.Key.foregroundColor: normalFontColor, NSAttributedString.Key.font: UIFont.barlowFont(.regular, withSize: 14)]
		let titleTextSelectedAttributes = [NSAttributedString.Key.foregroundColor: selectedFontColor, NSAttributedString.Key.font: UIFont.barlowFont(.bold, withSize: 14)]
		
		self.setTitleTextAttributes(titleTextNormalAttributes, for: .normal)
		self.setTitleTextAttributes(titleTextSelectedAttributes, for: .selected)
	}
	
	open override func layoutSubviews() {
		super.layoutSubviews()
		
		let cornerRadius = bounds.height / 2
		let maskedCorners: CACornerMask = [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner]
		
		clipsToBounds = true
		layer.masksToBounds = true
		layer.cornerRadius = cornerRadius
		layer.maskedCorners = maskedCorners
		
		if #available(iOS 13.0, *) {
			let foregroundIndex = numberOfSegments
			if subviews.indices.contains(foregroundIndex), let subview = subviews[foregroundIndex] as? UIImageView {
				subview.frame = CGRect(x: subview.frame.origin.x+6, y: subview.frame.origin.y+6, width: subview.frame.size.width-12, height: subview.frame.size.height-12)
				subview.image = UIImage()
				subview.clipsToBounds = true
				subview.layer.masksToBounds = true
				subview.backgroundColor = selectedColor
				
				subview.layer.cornerRadius = (subview.frame.size.height-4) / 2 + 3
				subview.layer.maskedCorners = maskedCorners
			}
			
		} else {
			for subview in subviews {
				subview.frame = CGRect(x: subview.frame.origin.x+2, y: subview.frame.origin.y+2, width: subview.frame.size.width-4, height: subview.frame.size.height-4)
				subview.clipsToBounds = true
				subview.layer.masksToBounds = true
				subview.layer.cornerRadius = (subview.frame.size.height-4) / 2 + 3
				subview.layer.maskedCorners = maskedCorners
			}
		}
	}
}
