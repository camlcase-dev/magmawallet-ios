//
//  HomeServiceTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 27/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
import camlKit
@testable import MagmaWallet

class HomeServiceTests: XCTestCase {
	
	var mockHomeServiceDelegate = MockHomeServiceDelegate()
	static var triggeringError = false
	static var triggeringBalanceOnly = false
	static var expectation: XCTestExpectation? = nil
	
	override func setUp() {
		
	}

	override func tearDown() {
		HomeServiceTests.expectation = nil
	}
	
	func testHomeServiceAll() {
		
		// Clear any lingering data
		URLSessionMockErrorGenerator.shared.clear()
		let _ = DependencyManager.shared.accountService.deleteWallet()
		
		for token in DependencyManager.shared.tokens {
			token.localCurrencyRate = 0
		}
		
		DependencyManager.shared.tzktClient.clearHistory()
		
		
		// Create wallet
		MockSharedHelpers.createWallet()
		
		
		// Test Home service
		HomeServiceTests.triggeringBalanceOnly = false
		mockHomeServiceDelegate = MockHomeServiceDelegate()
		
		let homeService = HomeService()
		homeService.addDelegate(mockHomeServiceDelegate)
		
		HomeServiceTests.triggeringError = false
		HomeServiceTests.expectation = self.expectation(description: "refreshing home service")
		homeService.refreshAllNetworkData()
		
		waitForExpectations(timeout: 30, handler: nil)
		
		
		// Check HomeService delegate array is functional
		XCTAssert(mockHomeServiceDelegate.didBeginWasCalled)
		XCTAssert(mockHomeServiceDelegate.didEndWasCalled)
		
		// Check funds were added to wallet
		XCTAssertFalse(DependencyManager.shared.accountService.isWalletEmpty())
		
		// Check transaction history loaded
		let transactionHistory = DependencyManager.shared.tzktClient.currentTransactionHistory(filterByToken: nil, orFilterByAddress: nil)
		XCTAssert(transactionHistory.keys.count == 7, "\(transactionHistory.keys.count)")
		
		// Check we fetched prices
		XCTAssert(DependencyManager.shared.tokens[0].localCurrencyRate.isEqual(to: Decimal(1.92)), DependencyManager.shared.tokens[0].localCurrencyRate.description)
		
		
		// Clean up
		homeService.removeDelegate(mockHomeServiceDelegate)
		HomeServiceTests.expectation = nil
	}
	
	func testHomeServicePriceseOnly() {
		
		// Clear any lingering data
		URLSessionMockErrorGenerator.shared.clear()
		let _ = DependencyManager.shared.accountService.deleteWallet()
		
		for token in DependencyManager.shared.tokens {
			token.localCurrencyRate = 0
		}
		
		DependencyManager.shared.tzktClient.clearHistory()
		
		
		// Create wallet
		MockSharedHelpers.createWallet()
		
		
		// Test Home service
		HomeServiceTests.triggeringBalanceOnly = true
		mockHomeServiceDelegate = MockHomeServiceDelegate()
		
		let homeService = HomeService()
		homeService.addDelegate(mockHomeServiceDelegate)
		
		HomeServiceTests.triggeringError = false
		HomeServiceTests.expectation = self.expectation(description: "refreshing home service")
		homeService.refreshPricesAndBalancesOnly()
		
		waitForExpectations(timeout: 30, handler: nil)
		
		
		// Check HomeService delegate array is functional
		XCTAssert(mockHomeServiceDelegate.didBeginWasCalled)
		XCTAssert(mockHomeServiceDelegate.didEndWasCalled)
		
		// Check funds were added to wallet
		XCTAssertFalse(DependencyManager.shared.accountService.isWalletEmpty())
		
		// Check transaction history loaded
		let transactionHistory = DependencyManager.shared.tzktClient.currentTransactionHistory(filterByToken: nil, orFilterByAddress: nil)
		XCTAssert(transactionHistory.keys.count == 0)
		
		// Check we fetched prices
		XCTAssert(DependencyManager.shared.tokens[0].localCurrencyRate.isEqual(to: Decimal(1.92)))
		
		
		// Clean up
		homeService.removeDelegate(mockHomeServiceDelegate)
		HomeServiceTests.expectation = nil
	}
	
	func testHomeServiceError() {
		MockSharedHelpers.createWallet()
		
		let homeService = HomeService()
		homeService.addDelegate(mockHomeServiceDelegate)
		
		URLSessionMockErrorGenerator.shared.triggerNoInternetErrorOnWallet()
		
		HomeServiceTests.triggeringError = true
		HomeServiceTests.expectation = self.expectation(description: "refreshing home service")
		homeService.refreshAllNetworkData()
		
		waitForExpectations(timeout: 30, handler: nil)
	}
}

class MockHomeServiceDelegate: HomeServiceDelegate {
	
	var didBeginWasCalled = false
	var didEndWasCalled = false
	
	func didBeginRefreshingData(balancesAndPricesOnly: Bool) {
		didBeginWasCalled = true
		
		XCTAssert(HomeServiceTests.triggeringBalanceOnly == balancesAndPricesOnly, "\(HomeServiceTests.triggeringBalanceOnly), \(balancesAndPricesOnly)")
	}
	
	func didEndRefreshingData(balancesAndPricesOnly: Bool, error: ErrorResponse?) {
		if let err = error, HomeServiceTests.triggeringError == false {
			XCTFail("Encountered when we shouldn't: \(err)")
		}
		
		if let err = error, HomeServiceTests.triggeringError == true {
			XCTAssert(err.errorType == .noInternetConnection, "\(err.errorType)")
		}
		
		didEndWasCalled = true
		
		HomeServiceTests.expectation?.fulfill()
		
		XCTAssert(HomeServiceTests.triggeringBalanceOnly == balancesAndPricesOnly, "\(HomeServiceTests.triggeringBalanceOnly), \(balancesAndPricesOnly)")
	}
}
