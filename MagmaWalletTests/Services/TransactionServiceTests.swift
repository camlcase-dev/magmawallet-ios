//
//  TransactionServiceTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 27/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
import camlKit
@testable import MagmaWallet

class TransactionServiceTests: XCTestCase {
	
	// Test data and objects
	let selectedPrimaryTokenIndex = 0
	let selectedPrimaryToken = DependencyManager.shared.tokens[0]
	let selectedSecondaryTokenIndex = 1
	let selectedSecondaryToken = DependencyManager.shared.tokens[1]
	let amountToSend = XTZAmount(fromNormalisedAmount: 15)
	let tokenAmountToSwap = TokenAmount(fromNormalisedAmount: 15, decimalPlaces: 6)
	
	
	// MARK: - Test Setup
	override func setUp() {
	}

	override func tearDown() {
	}
	
	
	
	// MARK: - Test cases
	func testUpdatingState() {
		MockSharedHelpers.createWallet()
		MockSharedHelpers.updateAllBalancesAndPrices()
		
		let transactionService = DependencyManager.shared.transactionService
		
		// Test recording send data
		transactionService.transactionType = .send
		transactionService.sendData.recipientAddress = MockConstants.recipient
		transactionService.sendData.selectedTokenIndex = selectedPrimaryTokenIndex
		transactionService.sendData.amountToSend = amountToSend
		
		XCTAssert(transactionService.transactionType == .send)
		XCTAssert(transactionService.sendData.recipientAddress == MockConstants.recipient)
		XCTAssert(transactionService.sendData.selectedTokenIndex == selectedPrimaryTokenIndex)
		XCTAssert(transactionService.sendData.amountToSend == amountToSend)
		
		// Test clearing stored data
		transactionService.resetStoredData()
		XCTAssert(transactionService.sendData.recipientAddress != MockConstants.recipient)
		XCTAssert(transactionService.sendData.amountToSend != amountToSend)
		
		// Test recoridng exchange data
		transactionService.resetStoredData()
		transactionService.transactionType = .exchange
		transactionService.exchangeData.slippageLimit = 0.5
		transactionService.exchangeData.selectedPrimaryTokenIndex = selectedPrimaryTokenIndex
		transactionService.exchangeData.selectedSecondaryTokenIndex = selectedSecondaryTokenIndex
		transactionService.updateAmountToExchange(amount: amountToSend)
		
		XCTAssert(transactionService.transactionType == .exchange)
		XCTAssert(transactionService.exchangeData.selectedPrimaryTokenIndex == selectedPrimaryTokenIndex)
		XCTAssert(transactionService.exchangeData.selectedSecondaryTokenIndex == selectedSecondaryTokenIndex)
		XCTAssert(transactionService.exchangeData.conversion.expected == TokenAmount(fromNormalisedAmount: 0.00001521, decimalPlaces: 8), transactionService.exchangeData.conversion.expected.normalisedRepresentation)
		XCTAssert(transactionService.exchangeData.conversion.minimum == TokenAmount(fromNormalisedAmount: 0.00001513, decimalPlaces: 8), transactionService.exchangeData.conversion.minimum.normalisedRepresentation)
		XCTAssert(transactionService.exchangeData.conversion.liquidityFee == TokenAmount(fromNormalisedAmount: 0.045, decimalPlaces: 6), transactionService.exchangeData.conversion.liquidityFee.normalisedRepresentation)
		XCTAssert(transactionService.exchangeData.conversion.displayExchangeRate == Decimal(0.00000101), transactionService.exchangeData.conversion.displayExchangeRate.description)
		XCTAssert(transactionService.exchangeData.conversion.displayPriceImpact == 0.23, transactionService.exchangeData.conversion.displayPriceImpact.description)
		
		transactionService.exchangeData.selectedPrimaryTokenIndex = selectedSecondaryTokenIndex
		transactionService.exchangeData.selectedSecondaryTokenIndex = selectedPrimaryTokenIndex
		transactionService.updateAmountToExchange(amount: tokenAmountToSwap)
		
		XCTAssert(transactionService.exchangeData.conversion.expected == TokenAmount(fromNormalisedAmount: 7563.16416, decimalPlaces: 6), transactionService.exchangeData.conversion.expected.normalisedRepresentation)
		XCTAssert(transactionService.exchangeData.conversion.minimum == TokenAmount(fromNormalisedAmount: 7525.348339, decimalPlaces: 6), transactionService.exchangeData.conversion.minimum.normalisedRepresentation)
		XCTAssert(transactionService.exchangeData.conversion.liquidityFee == TokenAmount(fromNormalisedAmount: 0.045, decimalPlaces: 6), transactionService.exchangeData.conversion.liquidityFee.normalisedRepresentation)
		XCTAssert(transactionService.exchangeData.conversion.displayExchangeRate == Decimal(50421.0944).rounded(scale: 4, roundingMode: .down), transactionService.exchangeData.conversion.displayExchangeRate.description)
		XCTAssert(transactionService.exchangeData.conversion.displayPriceImpact == 94.86, transactionService.exchangeData.conversion.displayPriceImpact.description)
	}
}
