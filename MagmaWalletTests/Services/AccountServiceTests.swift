//
//  AccountServiceTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 26/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
import camlKit
@testable import MagmaWallet

class AccountServiceTests: XCTestCase {
	
	// Test data and objects
	let accountService = DependencyManager.shared.accountService
	
	
	
	// MARK: - Test Setup
	override func setUp() {
		URLSessionMockErrorGenerator.shared.clear()
	}

	override func tearDown() {
		
	}
	
	
	
	// MARK: - Test cases
	func testOnboardingStatus() {
		let status1 = accountService.getOnboardingStatus()
		XCTAssert(status1 == .none)
		
		let _ = accountService.setOnboardingtatus(status: .walletCreationComplete)
		let status2 = accountService.getOnboardingStatus()
		XCTAssert(status2 == .walletCreationComplete)
		
		let _ = accountService.setOnboardingtatus(status: .onboardingComplete)
		let status3 = accountService.getOnboardingStatus()
		XCTAssert(status3 == .onboardingComplete)
		
		accountService.deleteAllOnboardinStatus()
		let status4 = accountService.getOnboardingStatus()
		XCTAssert(status4 == .none)
	}
	
	func testPinCode() {
		accountService.setPinCode(code: "123456", temp: true)
		let trueResult = accountService.validatePinCode(code: "123456", temp: true)
		let falseResult = accountService.validatePinCode(code: "654321", temp: true)
		
		XCTAssert(trueResult)
		XCTAssert(!falseResult)
		
		let hasPinResult1 = accountService.hasPinCodeSet()
		
		accountService.setPinCode(code: "123456", temp: false)
		let trueResult2 = accountService.validatePinCode(code: "123456", temp: false)
		let falseResult2 = accountService.validatePinCode(code: "654321", temp: false)
		let hasPinResult2 = accountService.hasPinCodeSet()
		
		XCTAssert(trueResult2)
		XCTAssert(!falseResult2)
		XCTAssert(!hasPinResult1)
		XCTAssert(hasPinResult2)
		
		
		accountService.deleteAllOnboardinStatus()
		let falseResult3 = accountService.validatePinCode(code: "123456", temp: true)
		let falseResult4 = accountService.validatePinCode(code: "123456", temp: false)
		XCTAssert(!falseResult3)
		XCTAssert(!falseResult4)
	}
	
	func testBiometric() {
		let isCapable = accountService.isBiometricCapable()
		XCTAssert(isCapable)
		
		let availableOption = accountService.availableBiometricType()
		XCTAssert(availableOption != .none)
		
		let currentChoice = accountService.getBiometricChoice()
		XCTAssert(currentChoice == .none)
		
		accountService.setBiometricChoice(type: availableOption)
		let updatedChoice =  accountService.getBiometricChoice()
		XCTAssert(updatedChoice == availableOption)
		
		accountService.deleteAllOnboardinStatus()
		let updatedChoice2 =  accountService.getBiometricChoice()
		XCTAssert(updatedChoice2 == .none)
	}
	
	func testWalletCreate() {
		
		// Make sure disk is clean
		let _ = accountService.deleteWallet()
		XCTAssert(accountService.currentAddress() == nil, accountService.currentAddress() ?? "-")
		
		
		let createResult1 = accountService.createWallet(withPassphrase: "")
		XCTAssert(createResult1)
		XCTAssertNotNil(accountService.walletMnemonic())
		
		let currentWallet1 = accountService.currentWallet()
		XCTAssert(currentWallet1?.address == accountService.currentAddress())
		XCTAssert(currentWallet1?.type == .hd)
		
		
		// Delete and test with passphrase
		let _ = accountService.deleteWallet()
		XCTAssert(accountService.currentAddress() == nil, accountService.currentAddress() ?? "-")
		
		
		let createResult2 = accountService.createWallet(withPassphrase: MockConstants.walletPassphrase)
		XCTAssert(createResult2)
		XCTAssertNotNil(accountService.walletMnemonic())
		
		let currentWallet2 = accountService.currentWallet()
		XCTAssert(currentWallet2?.address == accountService.currentAddress())
		XCTAssert(currentWallet2?.type == .hd)
		XCTAssert(currentWallet2?.address != currentWallet1?.address)
		
		
		// Delete again to clean up
		let deleteResult2 = accountService.deleteWallet()
		XCTAssert(deleteResult2)
		XCTAssert(accountService.currentAddress() == nil, accountService.currentAddress() ?? "-")
	}
	
	func testWalletImport() {
		
		// Make sure disk is clean
		let _ = accountService.deleteWallet()
		XCTAssert(accountService.currentAddress() == nil, accountService.currentAddress() ?? "-")
		
		
		// import linear, no passphrase
		let importResult1 = accountService.importWallet(mnemonic: MockConstants.walletMnemonic, passpharse: "", type: .linear, derivationPath: nil)
		XCTAssert(importResult1)
		
		let currentWallet1 = accountService.currentWallet()
		XCTAssertNotNil(currentWallet1)
		XCTAssert(currentWallet1?.type == .linear)
		XCTAssert(currentWallet1?.mnemonic == MockConstants.walletMnemonic)
		XCTAssert(currentWallet1?.address == MockConstants.walletAddressLinearNoPassphrase, currentWallet1?.address ?? "")
		XCTAssert(accountService.currentAddress() == MockConstants.walletAddressLinearNoPassphrase, accountService.currentAddress() ?? "")
		XCTAssert(accountService.walletMnemonic() == MockConstants.walletMnemonic)
		
		
		// import linear, passphrase
		let importResult2 = accountService.importWallet(mnemonic: MockConstants.walletMnemonic, passpharse: MockConstants.walletPassphrase, type: .linear, derivationPath: nil)
		XCTAssert(importResult2)
		
		let currentWallet2 = accountService.currentWallet()
		XCTAssertNotNil(currentWallet2)
		XCTAssert(currentWallet2?.type == .linear)
		XCTAssert(currentWallet2?.mnemonic == MockConstants.walletMnemonic)
		XCTAssert(currentWallet2?.address == MockConstants.walletAddressLinearPassphrase, currentWallet2?.address ?? "")
		XCTAssert(accountService.currentAddress() == MockConstants.walletAddressLinearPassphrase, accountService.currentAddress() ?? "")
		XCTAssert(accountService.walletMnemonic() == MockConstants.walletMnemonic)
		
		
		// import hd, no passphrase
		let importResult3 = accountService.importWallet(mnemonic: MockConstants.walletMnemonic, passpharse: "", type: .hd, derivationPath: MockConstants.walletDefaultDerivationPath)
		XCTAssert(importResult3)
		
		let currentWallet3 = accountService.currentWallet()
		XCTAssertNotNil(currentWallet3)
		XCTAssert(currentWallet3?.type == .hd)
		XCTAssert(currentWallet3?.mnemonic == MockConstants.walletMnemonic)
		XCTAssert(currentWallet3?.address == MockConstants.walletAddressHdNoPassphrase, currentWallet3?.address ?? "")
		XCTAssert(accountService.currentAddress() == MockConstants.walletAddressHdNoPassphrase, accountService.currentAddress() ?? "")
		XCTAssert(accountService.walletMnemonic() == MockConstants.walletMnemonic)
		
		
		// import hd, passphrase
		let importResult4 = accountService.importWallet(mnemonic: MockConstants.walletMnemonic, passpharse: MockConstants.walletPassphrase, type: .hd, derivationPath: MockConstants.walletDefaultDerivationPath)
		XCTAssert(importResult4)
		
		let currentWallet4 = accountService.currentWallet()
		XCTAssertNotNil(currentWallet4)
		XCTAssert(currentWallet4?.type == .hd)
		XCTAssert(currentWallet4?.mnemonic == MockConstants.walletMnemonic)
		XCTAssert(currentWallet4?.address == MockConstants.walletAddressHdPassphrase, currentWallet4?.address ?? "")
		XCTAssert(accountService.currentAddress() == MockConstants.walletAddressHdPassphrase, accountService.currentAddress() ?? "")
		XCTAssert(accountService.walletMnemonic() == MockConstants.walletMnemonic)
		
		
		// import hd, custom derivation path
		let importResult5 = accountService.importWallet(mnemonic: MockConstants.walletMnemonic, passpharse: "", type: .hd, derivationPath: MockConstants.walletCustomDerivationPath)
		XCTAssert(importResult5)
		
		let currentWallet5 = accountService.currentWallet()
		XCTAssertNotNil(currentWallet5)
		XCTAssert(currentWallet5?.type == .hd)
		XCTAssert(currentWallet5?.mnemonic == MockConstants.walletMnemonic)
		XCTAssert(currentWallet5?.address == MockConstants.walletAddressHdCustomDerivation, currentWallet5?.address ?? "")
		XCTAssert(accountService.currentAddress() == MockConstants.walletAddressHdCustomDerivation, accountService.currentAddress() ?? "")
		XCTAssert(accountService.walletMnemonic() == MockConstants.walletMnemonic)
		
		
		// Delete to clean up
		let deleteResult2 = accountService.deleteWallet()
		XCTAssert(deleteResult2)
		XCTAssert(accountService.currentAddress() == nil, accountService.currentAddress() ?? "-")
	}
	
	func testDelegate() {
		MockSharedHelpers.createWallet()
		accountService.clearDelegate()
		
		let currentDelegate = accountService.currentDelegate()
		XCTAssertNil(currentDelegate)
		
		// Recover delegate from network
		let expectation = self.expectation(description: "refresh delegate")
		accountService.refreshDelegate {
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 3, handler: nil)
		
		
		let refreshedDelegate = accountService.currentDelegate()
		XCTAssertNotNil(refreshedDelegate)
		XCTAssert(refreshedDelegate == DependencyManager.shared.testnetBakers[0], refreshedDelegate ?? "-")
	}
	
	func testBalances() {
		MockSharedHelpers.createWallet()
		
		let beforeBalance = DependencyManager.shared.tokens[0].balance
		XCTAssert(beforeBalance == XTZAmount.zero(), beforeBalance.normalisedRepresentation)
		
		let beforeToken = DependencyManager.shared.tokens[1].balance
		XCTAssert(beforeToken == TokenAmount.zero(), beforeToken.normalisedRepresentation)
		
		
		// Refresh balances
		let expectation = self.expectation(description: "refresh delegate")
		accountService.refreshBalances(forceRefresh: false, completion: { (result) in
			expectation.fulfill()
		})
		waitForExpectations(timeout: 3, handler: nil)
		
		
		let updatedBalance = DependencyManager.shared.tokens[0].balance
		XCTAssert(updatedBalance == MockConstants.walletXTZBalance, updatedBalance.normalisedRepresentation)
		
		let updatedToken = DependencyManager.shared.tokens[1].balance
		XCTAssert(updatedToken > TokenAmount.zero(), updatedToken.normalisedRepresentation)
	}
	
	func testBalancesFail() {
		let _ = accountService.deleteWallet()
		
		let expectation = self.expectation(description: "refesh balance")
		accountService.refreshBalances(forceRefresh: false) { (result) in
			switch result {
				case .success(_):
					XCTFail()
				
				case .failure(let error):
					XCTAssert(error.errorType == .unknownError, error.description)
			}
			
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 3, handler: nil)
		
		let updatedBalance = DependencyManager.shared.tokens[0].balance
		XCTAssert(updatedBalance == XTZAmount.zero(), updatedBalance.normalisedRepresentation)
	}
	
	func testIsWalletEmpty() {
		let _ = accountService.deleteWallet()
		XCTAssert(accountService.isWalletEmpty() == true)
		
		MockSharedHelpers.createWallet()
		XCTAssert(accountService.isWalletEmpty() == true)
		
		MockSharedHelpers.addTokens()
		XCTAssert(accountService.isWalletEmpty() == false)
	}
}
