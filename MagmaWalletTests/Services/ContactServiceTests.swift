//
//  ContactServiceTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 14/09/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
@testable import MagmaWallet

class ContactServiceTests: XCTestCase {
	
	static var expectation: XCTestExpectation? = nil
	
	var mockContactServiceDelegate = MockContactServiceDelegate()
	let mockStore = DependencyManager.shared.contactService.contactStore as? CNContactStoreMock
	
	
	
	// MARK: - Test Setup
	override func setUp() {
		URLSessionMockErrorGenerator.shared.clear()
		DependencyManager.shared.contactService.clearInMemoryContacts()
		DependencyManager.shared.contactService.deleteContactsFromDisk()
		
		mockStore?.resetMockContacts()
	}

	override func tearDown() {
		DependencyManager.shared.contactService.delegate = nil
		ContactServiceTests.expectation = nil
	}
	
	
	
	// MARK: - Test cases
	func testContactsProcessed() {
		let contactService = DependencyManager.shared.contactService
		processContacts()
		
		let contacts = contactService.getContacts()
		XCTAssert(contacts.count == 2)
		XCTAssert(contacts[0].fullName == "Jane Doe")
		XCTAssert(contacts[0].tezosAddress == CNContactStoreMock.mockAddress1)
		XCTAssert(contacts[0].networkProtocol == .carthagenet)
		XCTAssert(contacts[0].containsOnlyMagmaData == false)
		
		XCTAssert(contacts[1].fullName == "Boaty McBoatface")
		XCTAssert(contacts[1].tezosAddress == CNContactStoreMock.mockAddress1)
		XCTAssert(contacts[1].networkProtocol == .carthagenet)
		XCTAssert(contacts[1].containsOnlyMagmaData == true)
	}
	
	func testReadFromDisk() {
		let contactService = DependencyManager.shared.contactService
		processContacts()
		
		let contacts = contactService.readContactsFromDisk()
		XCTAssert(contacts.count == 2)
		XCTAssert(contacts[0].fullName == "Jane Doe")
		XCTAssert(contacts[0].tezosAddress == CNContactStoreMock.mockAddress1)
		XCTAssert(contacts[0].networkProtocol == .carthagenet)
		XCTAssert(contacts[0].containsOnlyMagmaData == false)
		
		XCTAssert(contacts[1].fullName == "Boaty McBoatface")
		XCTAssert(contacts[1].tezosAddress == CNContactStoreMock.mockAddress1)
		XCTAssert(contacts[1].networkProtocol == .carthagenet)
		XCTAssert(contacts[1].containsOnlyMagmaData == true)
	}
	
	func testDelegate() {
		let contactService = DependencyManager.shared.contactService
		XCTAssert(contactService.getContacts().count == 0)
		
		ContactServiceTests.expectation = self.expectation(description: "fetching contacts")
		contactService.delegate = mockContactServiceDelegate
		contactService.fetchContacts()
		XCTAssert(contactService.isFetching())
		
		wait(for: [ContactServiceTests.expectation!], timeout: 10)
		let contcatCount = contactService.getContacts().count
		XCTAssert(contcatCount == 2, "\(contcatCount)")
		ContactServiceTests.expectation = nil
	}
	
	func testGetContact() {
		let contactService = DependencyManager.shared.contactService
		processContacts()
		
		let contact1 = contactService.getContact(atIndex: 0)
		XCTAssert(contact1?.fullName == "Jane Doe")
		XCTAssert(contact1?.tezosAddress == CNContactStoreMock.mockAddress1)
		XCTAssert(contact1?.networkProtocol == .carthagenet)
		XCTAssert(contact1?.containsOnlyMagmaData == false)
		
		let contact2 = contactService.getContact(atIndex: 1)
		XCTAssert(contact2?.fullName == "Boaty McBoatface")
		XCTAssert(contact2?.tezosAddress == CNContactStoreMock.mockAddress1)
		XCTAssert(contact2?.networkProtocol == .carthagenet)
		XCTAssert(contact2?.containsOnlyMagmaData == true)
		
		let contact3 = contactService.getContact(atIndex: 42)
		XCTAssert(contact3 == nil)
	}
	
	func testDisplayStringForAddress() {
		let contactService = DependencyManager.shared.contactService
		processContacts()
		
		let string1 = contactService.displayStringForAddress(CNContactStoreMock.mockAddress1)
		XCTAssert(string1 == "Jane Doe")
		
		let string2 = contactService.displayStringForAddress("not-a-stored-tezos-address")
		XCTAssert(string2 == "not-a-stored-tezos-address")
	}
	
	func testEditContact() {
		let contactService = DependencyManager.shared.contactService
		processContacts()
		
		
		// Try to edit a system contact name and Address. Only the Address should be changed
		let result = contactService.editContact(withContactId: "1", address: CNContactStoreMock.mockAddress2, andName: "blah")
		XCTAssert(result)
		
		processContacts()
		let updatedContact = contactService.getContact(atIndex: 0) // contact has ID "1", but should be in position 0, because the first contact has no address
		
		XCTAssert(updatedContact?.fullName == "Jane Doe", updatedContact?.fullName ?? "")
		XCTAssert(updatedContact?.tezosAddress == CNContactStoreMock.mockAddress2, updatedContact?.tezosAddress ?? "")
		
		
		
		// Try to edit a system contact, to add an address
		let result2 = contactService.editContact(withContactId: "0", address: CNContactStoreMock.mockAddress1, andName: "blah")
		XCTAssert(result2)
		
		processContacts()
		let updatedContact2 = contactService.getContact(atIndex: 0) // contact has ID "1", but should be in position 0, because the first contact has no address
		
		XCTAssert(updatedContact2?.fullName == "John Doe", updatedContact2?.fullName ?? "")
		XCTAssert(updatedContact2?.tezosAddress == CNContactStoreMock.mockAddress1, updatedContact2?.tezosAddress ?? "")
		
		
		
		// Try to edit a magma contact name and address, both should change
		let result3 = contactService.editContact(withContactId: "2", address: CNContactStoreMock.mockAddress2, andName: "blah")
		XCTAssert(result3)
		
		processContacts()
		let updatedContact3 = contactService.getContact(atIndex: 2)
		
		XCTAssert(updatedContact3?.fullName == "blah", updatedContact3?.fullName ?? "")
		XCTAssert(updatedContact3?.tezosAddress == CNContactStoreMock.mockAddress2, updatedContact3?.tezosAddress ?? "")
	}
	
	func testAddContact() {
		let contactService = DependencyManager.shared.contactService
		processContacts()
		
		let contacts = contactService.getContacts()
		XCTAssert(contacts.count == 2, "\(contacts.count)")
		
		
		let result = contactService.addNewContact(name: "Testy McTestface", address: CNContactStoreMock.mockAddress1)
		XCTAssert(result)
		
		
		processContacts()
		let updatedContacts = contactService.getContacts()
		XCTAssert(updatedContacts.count == 3, "\(updatedContacts.count)")
	}
	
	func testDeleteContact() {
		let contactService = DependencyManager.shared.contactService
		processContacts()
		
		
		// Delete system contact should fail
		let result1 = contactService.deleteContact(withContactId: "1")
		XCTAssert(!result1)
		
		
		// Delete magma contact should delete
		let result2 = contactService.deleteContact(withContactId: "2")
		XCTAssert(result2)
		
		
		processContacts()
		XCTAssert(contactService.getContacts().count == 1)
	}
	
	func testRemoveAddressFromContact() {
		let contactService = DependencyManager.shared.contactService
		processContacts()
		
		
		// System contact, should be removed
		let result1 = contactService.removeCurrentAddressFromContact(withContactId: "1")
		XCTAssert(result1)
		
		
		// Magma contact, should be removed
		let result2 = contactService.removeCurrentAddressFromContact(withContactId: "2")
		XCTAssert(result2)
		
		
		processContacts()
		XCTAssert(contactService.getContacts().count == 0)
	}
	
	
	
	// MARk: - Helpers
	func processContacts() {
		let contactService = DependencyManager.shared.contactService
		let internalExpectation = self.expectation(description: "fetching contacts")
		
		contactService.finishedRefreshingCallback = { () in
			internalExpectation.fulfill()
		}
		contactService.fetchContacts()
		
		wait(for: [internalExpectation], timeout: 10)
	}
}

class MockContactServiceDelegate: ContactServiceDelegate {
	
	func finishedFetchingContacts() {
		ContactServiceTests.expectation?.fulfill()
		ContactServiceTests.expectation = nil
	}
}
