//
//  TezosBakerServiceTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 22/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
@testable import MagmaWallet

class TezosBakerServiceTests: XCTestCase {
	
	// Test data and objects
	let errorURL = "https://api.baking-bad.org/v2/bakers?accuracy=precise&timing=stable&health=active"
	
	
	
	// MARK: - Test Setup
	override func setUp() {
		URLSessionMockErrorGenerator.shared.clear()
		DependencyManager.shared.tezosBakerService.clearInMemoryBakers()
		DependencyManager.shared.tezosBakerService.deleteBakersFromDisk()
	}

	override func tearDown() {
		
	}
	
	
	
	// MARK: - Test cases
	func testBakersProcessed() {
		let expectation = self.expectation(description: "fetching bakers")
		DependencyManager.shared.tezosBakerService.fetchAndStoreTezosBakers { (success) in
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 3, handler: nil)
		
		let bakers = DependencyManager.shared.tezosBakerService.allBakers()
		let testnetBackerCount = DependencyManager.shared.testnetBakers.count
		
		// Check total count
		XCTAssert(bakers.count == testnetBackerCount)
		
		// Check attributes
		XCTAssert(bakers[0].address == DependencyManager.shared.testnetBakers[0], "\(bakers[0].address)")
		XCTAssert(bakers[0].name == "Baking Team", "\(bakers[0].name)")
		XCTAssert(bakers[0].estimatedRoi == 0.06121900000000001024, "\(bakers[0].estimatedRoi)")
		XCTAssert(bakers[0].logo == "https://services.tzkt.io/v1/logos/baking_team.png", "\(bakers[0].logo)")
		XCTAssert(bakers[0].logoURL == URL(string:"https://services.tzkt.io/v1/logos/baking_team.png"), "\(String(describing: bakers[0].logoURL))")
		XCTAssert(bakers[0].fee.description == "0", "\(bakers[0].fee.description)")
		XCTAssert(bakers[0].openForDelegation == true)
		
		// Check testnet bakers
		XCTAssert(bakers[0].address == DependencyManager.shared.testnetBakers[0])
		XCTAssert(bakers[testnetBackerCount-1].address == DependencyManager.shared.testnetBakers[testnetBackerCount-1])
		
		// Check fetching name
		let bakerName = DependencyManager.shared.tezosBakerService.bakerNameForAddress(address: DependencyManager.shared.testnetBakers[0])
		XCTAssert(bakerName == "Baking Team", "\(bakerName)")
	}
	
	func testBakerFetchError() {
		URLSessionMockErrorGenerator.shared.trigger(error: NSError(domain: "test", code: 0, userInfo: nil), forURL: URL(string: errorURL)!)
		
		
		let expectation = self.expectation(description: "fetching bakers")
		DependencyManager.shared.tezosBakerService.fetchAndStoreTezosBakers { (success) in
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 3, handler: nil)
		
		
		// Test bakers list is empty
		let bakers = DependencyManager.shared.tezosBakerService.allBakers()
		XCTAssert(bakers.count == 0)
	}
	
	func testBakersPresentAfterError() {
		// re-run first test to fill cache with data
		testBakersProcessed()
		
		
		// Set error up, to ensure an error returned does not delete the cached data
		URLSessionMockErrorGenerator.shared.trigger(error: NSError(domain: "test", code: 0, userInfo: nil), forURL: URL(string: errorURL)!)
		
		let expectation = self.expectation(description: "fetching bakers")
		DependencyManager.shared.tezosBakerService.fetchAndStoreTezosBakers { (success) in
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 3, handler: nil)
		
		
		// Confirm bakers still present after error
		let bakers = DependencyManager.shared.tezosBakerService.allBakers()
		let testnetBackerCount = DependencyManager.shared.testnetBakers.count
		XCTAssert(bakers.count == testnetBackerCount)
	}
	
	func testReadFromDisk() {
		var tezosBakers: [TezosBaker]? = nil
		
		let expectation = self.expectation(description: "fetching bakers")
		DependencyManager.shared.tezosBakerService.fetchAndStoreTezosBakers { (success) in
			tezosBakers = TezosBakerService().readBakersFromDisk()
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 3, handler: nil)
		
		let testnetBackerCount = DependencyManager.shared.testnetBakers.count
		
		XCTAssert(tezosBakers != nil)
		XCTAssert(tezosBakers?.count == testnetBackerCount)
		
		XCTAssert(tezosBakers?[0].name == "Baking Team", "\(tezosBakers?[0].name ?? "")")
	}
}
