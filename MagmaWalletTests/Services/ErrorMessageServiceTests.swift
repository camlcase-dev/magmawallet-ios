//
//  ErrorMessageServiceTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 29/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
import camlKit
@testable import MagmaWallet

class ErrorMessageServiceTests: XCTestCase {
	
	override func setUp() {
		
	}

	override func tearDown() {
		
	}
	
	func testParseTezosKitErrors() {
		
		let unknownError = ErrorResponse.unknownError()
		let insufficeintFundsError = ErrorResponse.error(string: "", errorType: .insufficientFunds)
		let networkError = ErrorResponse(requestURL: nil, requestJSON: nil, responseJSON: nil, httpStatusCode: 404, errorObject: nil, errorString: "", errorType: .invalidAddress)
		
		let unknownErrorDetails = ErrorMessageService.errorTitleAndMessage(for: unknownError)
		XCTAssert(unknownErrorDetails.title == "Uh oh!", unknownErrorDetails.title)
		XCTAssert(unknownErrorDetails.message == "Something went wrong.", unknownErrorDetails.message)
		
		let insufficeintFundsErrorDetails = ErrorMessageService.errorTitleAndMessage(for: insufficeintFundsError)
		XCTAssert(insufficeintFundsErrorDetails.title == "Insufficient XTZ balance", insufficeintFundsErrorDetails.title)
		XCTAssert(insufficeintFundsErrorDetails.message == "We could not complete this transaction due to insufficient XTZ wallet balance.", insufficeintFundsErrorDetails.message)
		
		let networkErrorDetails = ErrorMessageService.errorTitleAndMessage(for: networkError)
		XCTAssert(networkErrorDetails.title == "Invalid wallet address", networkErrorDetails.title)
		XCTAssert(networkErrorDetails.message == "Please enter a valid wallet address and try again.", networkErrorDetails.message)
	}
}
