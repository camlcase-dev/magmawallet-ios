//
//  MoonPayServiceTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 17/02/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import XCTest
import camlKit
@testable import MagmaWallet

class MoonPayServiceTests: XCTestCase {
	
	override func setUp() {
		
	}
	
	override func tearDown() {
		
	}
	
	func testIsAvailable() {
		DependencyManager.shared.moonPayService.clearMoonPayData()
		
		let expectation1 = self.expectation(description: "fetching currencies")
		DependencyManager.shared.moonPayService.fetchSupportedCountriesIfNeeded(completion: {
			expectation1.fulfill()
		})
		
		let expectation2 = self.expectation(description: "fetching countries")
		DependencyManager.shared.moonPayService.fetchSupportedInUSAIfNeeded {
			expectation2.fulfill()
		}
		
		self.wait(for: [expectation1, expectation2], timeout: 3)
		
		
		let result1 = DependencyManager.shared.moonPayService.isMoonPayAvailable(forLocale: Locale(identifier: "en_IE"))
		XCTAssert(result1)
		
		let result2 = DependencyManager.shared.moonPayService.isMoonPayAvailable(forLocale: Locale(identifier: "en_GB"))
		XCTAssert(result2)
		
		let result3 = DependencyManager.shared.moonPayService.isMoonPayAvailable(forLocale: Locale(identifier: "en_US"))
		XCTAssert(!result3)
	}
}
