//
//  TokenPriceServiceTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 23/04/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
import camlKit
@testable import MagmaWallet

struct tokenToXTZ: Codable {
	let xtz_pool: String
	let token_pool: String
	let token_in: String
	let xtz_out: String
	let slippage: String
}

struct xtzToToken: Codable {
	let xtz_pool: String
	let token_pool: String
	let xtz_in: String
	let token_out: String
	let slippage: String
}

class TokenPriceServiceTests: XCTestCase {
	
	// Test data and objects
	let coinGeckoURL = "https://api.coingecko.com/....."
	
	let testXTZAmounts = [
		XTZAmount(fromNormalisedAmount: 0),
		XTZAmount(fromNormalisedAmount: 1),
		XTZAmount(fromNormalisedAmount: 7.03),
		XTZAmount(fromNormalisedAmount: 10.00),
		XTZAmount(fromNormalisedAmount: 14.000000),
		XTZAmount(fromNormalisedAmount: 1_000_000.000000),
		XTZAmount(fromRpcAmount: "1000000")!,
		XTZAmount(fromNormalisedAmount: 1_000_000_000_000),
		XTZAmount(fromNormalisedAmount: 1_000.000001)
	]
	let testDecimalCurrencyFormatsEUR = ["€0.00", "€1.00", "€7.03", "€10.00", "€14.00", "€1,000,000.00", "€1.00", "€1,000,000,000,000.00", "€1,000.00"]
	let testDecimalCurrencyFormatsUSD = ["$0.00", "$1.00", "$7.03", "$10.00", "$14.00", "$1,000,000.00", "$1.00", "$1,000,000,000,000.00", "$1,000.00"]

	
	
	// MARK: - Test Setup
    override func setUp() {
		removeDefaultToUSD()
		URLSessionMockErrorGenerator.shared.clear()
    }

    override func tearDown() {
		
    }
	
	
	
	// MARK: - Helpers
	func defaultLocaleToUSD() {
		let expectation = self.expectation(description: "fetching prices")
		DependencyManager.shared.tokenPriceService.updateSelectedCurrency(code: "USD") { _ in
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 3, handler: nil)
	}
	
	func removeDefaultToUSD() {
		let expectation = self.expectation(description: "fetching prices")
		DependencyManager.shared.tokenPriceService.updateSelectedCurrency(code: "EUR") { _ in
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 3, handler: nil)
	}
	
	
	
	// MARK: - Test cases
	func testPrices() {
		let expectation = self.expectation(description: "fetching prices")
		DependencyManager.shared.tokenPriceService.updateAllPrices { _ in
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 3, handler: nil)
		
		
		// Take XTZ and the first FA1.2 token
		let tokenXTZ = DependencyManager.shared.balances[0]
		let tokenFA = DependencyManager.shared.balances[1]
		
		// Test the download dollar price was parsed correctly
		XCTAssert(tokenXTZ.localCurrencyRate.isEqual(to: Decimal(1.92)), tokenXTZ.localCurrencyRate.description)
		
		// Take the FA1.2 tokens dollar price (round to 18 decimal digits to avoid floating point issues)
		let tokenDollarPrice = tokenFA.localCurrencyRate.rounded(scale: 8, roundingMode: .down)
		let targetDollarPrice = Decimal(1884133.522167974).rounded(scale: 8, roundingMode: .down)
		let targetXTZPool = XTZAmount(fromNormalisedAmount: 7974.114286)
		let targetTokenPool = TokenAmount(fromNormalisedAmount: 0.00812591, decimalPlaces: 8)
		
		// Check the values add up
		XCTAssert(tokenDollarPrice.description == targetDollarPrice.description, "\(tokenDollarPrice.description) != \(targetDollarPrice.description)")
		XCTAssert(tokenFA.dexterXTZPool == targetXTZPool, tokenFA.dexterXTZPool.normalisedRepresentation)
		XCTAssert(tokenFA.dexterTokenPool == targetTokenPool, tokenFA.dexterTokenPool.normalisedRepresentation)
		
		// Check Local currency (EUR) downloaded correctly
		let localCurrencyRate = DependencyManager.shared.tokenPriceService.selectedCurrencyMultiplier
		let targetCurrencyRate = Decimal(1.92)
		
		XCTAssert(localCurrencyRate.isEqual(to: targetCurrencyRate), "\(localCurrencyRate) != \(targetCurrencyRate)")
		
		// Check XTZ token has picked up the local curreny rate and applied it correctly
		let localCurrencyXTZPrice = tokenXTZ.localCurrencyRate.rounded(scale: 5, roundingMode: .down)
		let targetLocalCurrencyXTZPrice = Decimal(1.92).rounded(scale: 5, roundingMode: .down)
		
		XCTAssert(localCurrencyXTZPrice.isEqual(to: targetLocalCurrencyXTZPrice), "\(localCurrencyXTZPrice) != \(targetLocalCurrencyXTZPrice)")
		
		
		// Check FA token has picked up the local curreny rate and applied it correctly
		let localCurrencyFAPrice = tokenFA.localCurrencyRate.rounded(scale: 5, roundingMode: .down)
		let targetLocalCurrencyFAPrice = Decimal(1884133.522163).rounded(scale: 5, roundingMode: .down)
		
		XCTAssert(localCurrencyFAPrice.description == targetLocalCurrencyFAPrice.description, "\(localCurrencyFAPrice.description) != \(targetLocalCurrencyFAPrice.description)")
	}
	
	func testPricesFailure() {
		URLSessionMockErrorGenerator.shared.trigger(error: URLSessionMockErrorGenerator.noInternetConnectionError(), forURL: URL(string: coinGeckoURL)!)
		
		let expectation = self.expectation(description: "fetching prices")
		DependencyManager.shared.tokenPriceService.updateAllPrices { _ in
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 3, handler: nil)
		
		let tokenXTZ = DependencyManager.shared.balances[0]
		XCTAssert(tokenXTZ.localCurrencyRate.isEqual(to: Decimal(1.92)), tokenXTZ.localCurrencyRate.description)
	}
	
	func testSharedNumberFormatter() {
		let defaultLocale = DependencyManager.shared.tokenPriceService.sharedNumberFormatter()
		XCTAssert(defaultLocale.locale == Locale.current)
	}
	
	func testCurrencyCode() {
		let expectation = self.expectation(description: "fetching prices")
		DependencyManager.shared.tokenPriceService.updateAllPrices { _ in
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 3, handler: nil)
		
		let notDefaulted = DependencyManager.shared.tokenPriceService.selectedCurrency
		XCTAssert(notDefaulted == "eur", notDefaulted ?? "")
	}
	
	func testPlaceHolderString() {
		let expectation = self.expectation(description: "fetching prices")
		DependencyManager.shared.tokenPriceService.updateAllPrices { _ in
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 3, handler: nil)
		
		let notDefaulted = DependencyManager.shared.tokenPriceService.placeholderCurrencyString()
		XCTAssert(notDefaulted == "€0.00")
	}
	
	func testFormatDecimalToCurrencyString() {
		var formats: [String] = []
		
		for amount in testXTZAmounts {
			formats.append(DependencyManager.shared.tokenPriceService.format(decimal: amount.toNormalisedDecimal() ?? 0, numberStyle: .currency))
		}
		
		for (index, formatString) in formats.enumerated() {
			let isEqual = formatString == testDecimalCurrencyFormatsEUR[index]
			
			if !isEqual {
				// Log every failure, not just the last one via XCTAsset's message
				print("Error: \(formatString) not equal to \(testDecimalCurrencyFormatsEUR[index])")
			}
			
			XCTAssert(isEqual)
		}
		
		
		defaultLocaleToUSD()
		formats = []
		
		for amount in testXTZAmounts {
			formats.append(DependencyManager.shared.tokenPriceService.format(decimal: amount.toNormalisedDecimal() ?? 0, numberStyle: .currency))
		}
		
		for (index, formatString) in formats.enumerated() {
			let isEqual = formatString == testDecimalCurrencyFormatsUSD[index]
			
			if !isEqual {
				// Log every failure, not just the last one via XCTAsset's message
				print("Error: \(formatString) not equal to \(testDecimalCurrencyFormatsUSD[index])")
			}
			
			XCTAssert(isEqual, "\(formatString) != \(testDecimalCurrencyFormatsUSD[index])")
		}
		
		removeDefaultToUSD()
	}
}
