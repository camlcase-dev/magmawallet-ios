//
//  UIViewController+extensions.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 27/01/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
@testable import MagmaWallet

class UIVIewControllerTests: XCTestCase {
	
	
	// MARK: - Test Setup
    override func setUp() {
		
    }

    override func tearDown() {
		
    }
	
	
	
	// MARK: - Test cases
	func testAlertErrorWithMessage() {
		let vc =  UIViewController()
		UIApplication.shared.keyWindow?.rootViewController? = vc
		
		vc.alert(errorWithMessage: "Mock")
		
		let expectation = self.expectation(description: "launching alert")
		DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
			
			XCTAssert(vc.presentedViewController != nil)
			
			let alertViewController = vc.presentedViewController as! UIAlertController
			XCTAssert(alertViewController.title == "Error")
			XCTAssert(alertViewController.message == "Mock")
			
			
			expectation.fulfill()
		})
		wait(for: [expectation], timeout: 1.5)
	}
	
	func testAlertTitleWithMessage() {
		let vc =  UIViewController()
		UIApplication.shared.keyWindow?.rootViewController? = vc
		
		vc.alert(withTitle: "Title", andMessage: "Message")
		
		let expectation = self.expectation(description: "launching alert")
		DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
			
			XCTAssert(vc.presentedViewController != nil)
			
			let alertViewController = vc.presentedViewController as! UIAlertController
			XCTAssert(alertViewController.title == "Title")
			XCTAssert(alertViewController.message == "Message")
			
			
			expectation.fulfill()
		})
		wait(for: [expectation], timeout: 1.5)
	}
	
	func testAlertTitleWithMessageAndActions() {
		let vc =  UIViewController()
		UIApplication.shared.keyWindow?.rootViewController? = vc
		
		vc.alert(withTitle: "Title", andMessage: "Message", okText: "OK-Test", okAction: { (action) in }, cancelText: "CANCEL-Test", cancelAction: nil)
		
		let expectation = self.expectation(description: "launching alert")
		DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
			
			XCTAssert(vc.presentedViewController != nil)
			
			let alertViewController = vc.presentedViewController as! UIAlertController
			XCTAssert(alertViewController.title == "Title")
			XCTAssert(alertViewController.message == "Message")
			XCTAssert(alertViewController.actions.count == 2)
			XCTAssert(alertViewController.actions[0].title == "OK-Test")
			XCTAssert(alertViewController.actions[1].title == "CANCEL-Test")
			
			expectation.fulfill()
		})
		wait(for: [expectation], timeout: 1.5)
	}
	
	func testIsModal() {
		let vc = UIViewController()
		let vc2 = UIViewController()
		let _ = UINavigationController(rootViewController: vc)
		
		vc.present(vc2, animated: false) {
			
			XCTAssert(vc2.isModal == true)
		}
	}
	
	func testShowHideSubviews() {
		let vc = UIViewController()
		let label = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 40))
		
		vc.view.addSubview(label)
		XCTAssert(label.isHidden == false)
		
		vc.hideAllSubviews()
		XCTAssert(label.isHidden == true)
		
		vc.showAllSubviews()
		XCTAssert(label.isHidden == false)
	}
	
	func testShowHideActivity() {
		let vc = UIViewController()
		vc.showActivity()
		
		let activityView = vc.view.subviews[0]
		XCTAssert(activityView.bounds == vc.view.bounds)
		XCTAssert(activityView.backgroundColor == .clear)
		
		vc.hideActivity()
		XCTAssert(vc.view.subviews.count == 0)
	}
}
