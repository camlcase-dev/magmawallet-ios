//
//  UINavigationControllerTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 27/01/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
@testable import MagmaWallet

class UINavigationControllerTests: XCTestCase {
	
	
	// MARK: - Test Setup
	
    override func setUp() {
		
    }

    override func tearDown() {
		
    }
	
	
	
	// MARK: - Test cases
	
	func testPopToWelcome() {
		let welcome = WelcomeViewController()
		let settings = SettingsViewController()
		let nav = UINavigationController(rootViewController: welcome)
		nav.pushViewController(settings, animated: false)
		
		XCTAssert(nav.topViewController is SettingsViewController)
		
		nav.popToWelcome()
		XCTAssert(nav.topViewController is WelcomeViewController)
	}
	
	func testPopToTabbarScreens() {
		let pinCodeScreen = PinCodeViewController()
		let nav = createNavigationControllerWithTabbar()
		
		// Push pincode on to stack, to test popping
		nav.pushViewController(pinCodeScreen, animated: false)
		XCTAssert(nav.topViewController is PinCodeViewController)
		
		nav.popToWallet()
		XCTAssert(nav.topViewController is TabBarController)
		XCTAssert((nav.topViewController as! TabBarController).selectedIndex == TabBarController.walletIndex)
	}
	
	func testPopToChooseAmount() {
		let choose = ChooseAmountViewController()
		let settings = SettingsViewController()
		let nav = UINavigationController(rootViewController: choose)
		nav.pushViewController(settings, animated: false)
		
		XCTAssert(nav.topViewController is SettingsViewController)
		
		nav.popToChooseAmount()
		XCTAssert(nav.topViewController is ChooseAmountViewController)
	}
	
	func testPopToDelegate() {
		let delegateVc = DelegateViewController()
		let settings = SettingsViewController()
		let nav = UINavigationController(rootViewController: delegateVc)
		nav.pushViewController(settings, animated: false)
		
		XCTAssert(nav.topViewController is SettingsViewController)
		
		nav.popToDelegate()
		XCTAssert(nav.topViewController is DelegateViewController)
	}
	
	func testContainsSettings() {
		let welcome = WelcomeViewController()
		let nav = UINavigationController(rootViewController: welcome)
		
		XCTAssert(nav.containsSettingViewController() == false)
		
		
		// Test with Tabbar
		let navWithTabbar = createNavigationControllerWithTabbar()
		XCTAssert(navWithTabbar.containsSettingViewController() == true)
	}
	
	
	// MARK: - Helpers
	func createNavigationControllerWithTabbar() -> UINavigationController {
		let home = HomeViewContoller()
		let exchange = ExchangeViewController()
		let activity = ActivityViewController()
		let settings = SettingsViewController()
		let tabbar = TabBarController()
		tabbar.viewControllers = [home, exchange, activity, settings]
		let nav = UINavigationController(rootViewController: tabbar)
		
		return nav
	}
}
