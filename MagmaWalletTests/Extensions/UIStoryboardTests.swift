//
//  UIStoryboardTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 27/01/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
@testable import MagmaWallet

class UIStoryboardTest: XCTestCase {
	
	
	// MARK: - Test Setup
    override func setUp() {
		
    }

    override func tearDown() {
		
    }
	
	
	
	// MARK: - Test cases
	func testReceiveViewController() {
		let vc = UIStoryboard.receiveViewController() // Will throw fatal error if can't find
		XCTAssert(vc.title == nil)
	}
	
	func testSendViewController() {
		let vc = UIStoryboard.sendViewController() // Will throw fatal error if can't find
		XCTAssert(vc.title == nil)
	}
	
	func testLoginViewController() {
		let vc = UIStoryboard.loginViewController() // Will throw fatal error if can't find
		XCTAssert(vc.title == nil)
	}
	
	func testPinCodeViewController() {
		let vc = UIStoryboard.pincodeViewController() // Will throw fatal error if can't find
		XCTAssert(vc.title == nil)
	}
}
