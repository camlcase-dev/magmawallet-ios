//
//  UIViewTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 23/06/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
@testable import MagmaWallet

class UIViewTests: XCTestCase {
	
	
	// MARK: - Test Setup
    override func setUp() {
		
    }

    override func tearDown() {
		
    }
	
	
	
	// MARK: - Test cases
	func testStyles() {
		let view = UIView()
		view.borderWidth = 5
		view.borderColor = UIColor.black
		view.cornerRadius = 14
		view.maskToBounds = true
		
		XCTAssertTrue(view.layer.borderWidth == 5)
		XCTAssertTrue(view.layer.borderColor == UIColor.black.cgColor)
		XCTAssertTrue(view.layer.cornerRadius == 14)
		XCTAssertTrue(view.layer.masksToBounds == true)
		
		XCTAssertTrue(view.borderWidth == 5)
		XCTAssertTrue(view.borderColor == UIColor.black)
		XCTAssertTrue(view.cornerRadius == 14)
		XCTAssertTrue(view.maskToBounds == true)
	}
	
	func testRoundCorners() {
		let view = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
		view.roundCorners(corners: [.topLeft, .topRight], radius: 5)
		view.layoutSubviews()
		
		let view2 = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
		let path = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 5, height: 5))
		let mask = CAShapeLayer()
		mask.path = path.cgPath
		view2.layer.mask = mask
		
		XCTAssertTrue((view.layer.mask as? CAShapeLayer)?.path == (view2.layer.mask as? CAShapeLayer)?.path)
	}
}
