//
//  UITabrbarItemTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 23/06/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
@testable import MagmaWallet

class UITabbarItemTests: XCTestCase {
	
	
	// MARK: - Test Setup
    override func setUp() {
		
    }

    override func tearDown() {
		
    }
	
	
	
	// MARK: - Test cases
	func testLocalized() {
		let tabbarItem = UITabBarItem(title: "TEST", image: nil, selectedImage: nil)
		XCTAssert(tabbarItem.title == "TEST")
		
		tabbarItem.localizedTitle = "onb_splash_bottom_text"
		XCTAssert(tabbarItem.title == "TEZOS WALLET")
	}
}
