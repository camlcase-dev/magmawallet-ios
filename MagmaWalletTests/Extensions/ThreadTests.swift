//
//  ThreadTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 27/01/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
@testable import MagmaWallet

class ThreadTests: XCTestCase {
	
    override func setUp() {
    }

    override func tearDown() {
    }

	func testGlobalSettingsAction() {
		XCTAssertTrue(Thread.current.isRunningXCTest)
	}
}
