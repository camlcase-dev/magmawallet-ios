//
//  UIFontTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 23/06/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
@testable import MagmaWallet

class UIFontTests: XCTestCase {
	
	
	// MARK: - Test Setup
    override func setUp() {
		
    }

    override func tearDown() {
		
    }
	
	
	
	// MARK: - Test cases
	func testBarlowFonts() {
		let font1 = UIFont.barlowFont(.bold, withSize: 10)
		let font2 = UIFont.barlowFont(.light, withSize: 10)
		let font3 = UIFont.barlowFont(.medium, withSize: 10)
		let font4 = UIFont.barlowFont(.regular, withSize: 10)
		let font5 = UIFont.barlowFont(.semiBold, withSize: 10)
		
		XCTAssert(font1.fontName == "Barlow-Bold")
		XCTAssert(font2.fontName == "Barlow-Light")
		XCTAssert(font3.fontName == "Barlow-Medium")
		XCTAssert(font4.fontName == "Barlow-Regular")
		XCTAssert(font5.fontName == "Barlow-SemiBold")
	}
}
