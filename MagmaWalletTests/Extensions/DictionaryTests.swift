//
//  DictionaryTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 23/06/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
@testable import MagmaWallet

class DictionaryTests: XCTestCase {
	
	
	// MARK: - Test Setup
    override func setUp() {
		
    }

    override func tearDown() {
		
    }
	
	
	
	// MARK: - Test cases
	func testHashableKeys() {
		let key1 = HomeViewContoller()
		let val1 = "Home"
		
		let key2 = WelcomeViewController()
		let val2 = "Welcome"
		
		let key3 = PinCodeViewController()
		let val3 = "PinCode"
		
		let dict = [key1: val1, key2: val2, key3: val3]
		
		XCTAssert(dict[key1] == val1)
		XCTAssert(dict[key2] == val2)
		XCTAssert(dict[key3] == val3)
	}
}
