//
//  MockConstants.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 17/06/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import camlKit
@testable import MagmaWallet

class MockConstants {
	
	static let walletMnemonic = "else topic alcohol cargo session else razor remain horn object dignity help"
	static let walletAddressLinearNoPassphrase = "tz1e4hAp7xpjekmXnYe4677ELGA3UxR79EFb"
	static let walletAddressLinearPassphrase = "tz1Rv7raQPzRKhURwc9QUNQxJPDHydUs31j4"
	static let walletAddressHdNoPassphrase = "tz1X2yA7evDKputSBthrwuGpxpAYHkDUkVCN"
	static let walletAddressHdPassphrase = "tz1gSqYLR1VUVwbxWu5xPLo1MrwRVsDKaQcC"
	static let walletAddressHdCustomDerivation = "tz1Q2EyseSzuz3xJLfMcssTo4fpaZ53pe5ep"
	
	static let walletDefaultDerivationPath = "m/44'/1729'/0'/0'"
	static let walletCustomDerivationPath = "m/44'/1729'/0'/1'"
	static let walletPassphrase = "blah blah"
	
	static let walletAddressFormatted = "tz1e4hAp7xpjek...R79EFb"
	static let contactName = "Testy McTestface"
	static let contactAddress = "tz1RKLWbGm7T4mnxDZHWazkbnvaryKsxxZTF"
	static let contactlessAddress = "tz1RKLWbGm7T4mnxDZHWazkbnvaryKsxxZT2"
	static let contactlessAddressFormatted = "tz1RKLWbGm7T4m...sxxZT2"
	static let myWalletName = "My Wallet"
	static let operationHash = "onkGZ6eFRrQ8Zv5PXfKmxVMfkHXYWc8kMhPXc5J4cXa8yp2p4nT"
	static let blockId = "BMUERgvraycEAgAa9xX6HKEZ6LMBbsRn65XDmapnzwPWd7etdH9"
	
	static let recipient = "tz1RKLWbGm7T4mnxDZHWazkbnvaryKsxxZTF"
	static let recipientFormatted = "tz1RKLWbGm7T4m...sxxZTF"
	
	static let totalWalletBalanceFormatted = "€34,505.20"
	static let walletXTZBalance = XTZAmount(fromNormalisedAmount: 2023.233806)
	static let walletXTZLowBalance = XTZAmount(fromNormalisedAmount: 0.000100)
	static let walletTezBalanceFormatted = "2,023.233806 XTZ"
	static let walletTezBalanceFormattedWithoutSymbol = "2,023.233806"
	static let walletTezConversionFormatted = "€1.92"
	static let walletTezDetailsConversionFormatted = "€2.472915"
	static let walletTezCurrencyFormatted = "€3,884.60"
	
	static let walletTokenDecimal0Balance = Decimal(812591)
	static let walletTokenDecimal0BalanceFormatted = "812,591"
	static let walletTokenDecimal0ConversionFormatted = "€0.024267"
	static let walletTokenDecimal0CurrencyFormatted = "€15,310.29"
	
	static let walletTokenDecimal2Balance = Decimal(8125.91)
	static let walletTokenDecimal2BalanceFormatted = "8,125.91"
	static let walletTokenDecimal2ConversionFormatted = "€2.426720"
	static let walletTokenDecimal2CurrencyFormatted = "€15,310.29"
	
	static let walletTokenDecimal3Balance = Decimal(812.591)
	static let walletTokenDecimal3BalanceFormatted = "812.591"
	static let walletTokenDecimal3ConversionFormatted = "€24.267204"
	static let walletTokenDecimal3CurrencyFormatted = "€15,310.29"
	
	static let walletTokenDecimal4Balance = Decimal(81.2591)
	static let walletTokenDecimal4BalanceFormatted = "81.2591"
	static let walletTokenDecimal4ConversionFormatted = "€242.67204"
	static let walletTokenDecimal4CurrencyFormatted = "€15,310.29"
	
	static let walletTokenDecimal6Balance = Decimal(0.812591)
	static let walletTokenDecimal6BalanceFormatted = "0.812591"
	static let walletTokenDecimal6ConversionFormatted = "€2,4267.204"
	static let walletTokenDecimal6CurrencyFormatted = "€15,310.29"
	
	static let walletTokenDecimal8Balance = Decimal(0.00812591).rounded(scale: 8, roundingMode: .down)
	static let walletTokenDecimal8BalanceFormatted = "0.00812591"
	static let walletTokenDecimal8BalanceFormattedAndTruncated = "0.008125"
	static let walletTokenDecimal8ConversionFormatted = "€1,884,133.52"
	static let walletTokenDecimal8CurrencyFormatted = "€15,310.29"
	
	static let walletTokenDecimal18Balance = Decimal(0.00812591).rounded(scale: 18, roundingMode: .down)
	static let walletTokenDecimal18BalanceFormatted = "0.000000000000812591"
	static let walletTokenDecimal18BalanceFormattedAndTruncated = "0"
	static let walletTokenDecimal18ConversionFormatted = "€2,426,720.44"
	static let walletTokenDecimal18CurrencyFormatted = "€15,310.29"
	
	static let transactionHistoryGroupData: [ [TzKTTransaction.TransactionSubType] ] = [
		[.nativeTokenReceive],
		[.exchangeXTZToToken],
		[.exchangeTokenToXTZ, .nativeTokenSend],
		[.send, .reveal],
		[.receive],
		[.delegation],
		[.nativeTokenReceive]
	]
	
	static let transactionHistoryParsed: [UITableViewSectionModel] = [
		UITableViewSectionModel(withHeader: "Nov 03 2020", footer: nil, andCellModels: [
			MagmaWallet.TransactionHistoryCellModel(operationHash: "oofvCjEbyNjZB9AuQi6BptzMqpTTwgEtxun9hizNWmaWSUN16tw", type: "RECEIVE", amount: "+0.00000001 tzBTC", address: "From: Testy McTestface", fee: "Network fee: N/A")
		]),
		UITableViewSectionModel(withHeader: "Oct 28 2020", footer: nil, andCellModels: [
			MagmaWallet.TransactionHistoryExchangeCellModel(operationHash: "ooVkQGcqMdtYbbrNAmE1Ht1v46LD4NQVnDCLCviL5QQ9HukUJUJ", amountSent: "-10 XTZ", amountReceived: "+0.0001146 tzBTC", exchangeRate: "1 XTZ → 0.000011 tzBTC", networkFee: "Network fee: 0.045359 XTZ")
		]),
		UITableViewSectionModel(withHeader: "Oct 27 2020", footer: nil, andCellModels: [
			MagmaWallet.TransactionHistoryExchangeCellModel(operationHash: "ooRXrm9wNiguRhJSAfy58ta2ZE89W3aW5ZPprAhSWojwNQFdwb9", amountSent: "-0.000005 tzBTC", amountReceived: "+29.787038 XTZ", exchangeRate: "1 tzBTC → 5957407.6 XTZ", networkFee: "Network fee: 0.0876 XTZ"),
			MagmaWallet.TransactionHistoryCellModel(operationHash: "ooapauxHDiruwLnPMBCi7TjrEvFpB9M2Yf1M84GxLkzG9c446Ua", type: "SEND", amount: "-0.0000001 tzBTC", address: "To: Testy McTestface", fee: "Network fee: 0.080302 XTZ")
		]),
		UITableViewSectionModel(withHeader: "Oct 21 2020", footer: nil, andCellModels: [
			MagmaWallet.TransactionHistoryCellModel(operationHash: "ooNikcHDL1DUsfuh7N8DkJwbcHPBhgyij2FxQvA99NtuRWTzcnk", type: "SEND", amount: "-10 XTZ", address: "To: tz1SqjMCv8XBEe...EoNiEV", fee: "Network fee: 0.259415 XTZ"),
			MagmaWallet.TransactionHistoryCellModel(operationHash: "ooNikcHDL1DUsfuh7N8DkJwbcHPBhgyij2FxQvA99NtuRWTzcnk", type: "PUBLIC KEY REVEAL", amount: "N/A", address: "My Wallet", fee: "Network fee: 0.001268 XTZ")
		]),
		UITableViewSectionModel(withHeader: "Oct 16 2020", footer: nil, andCellModels: [
			MagmaWallet.TransactionHistoryCellModel(operationHash: "opND5nyS1GWgxvp3xhFwi9heLjUvgvbTCSLsgRcSU3xghk1hCYd", type: "RECEIVE", amount: "+25 XTZ", address: "From: Testy McTestface", fee: "Network fee: N/A")
		]),
		UITableViewSectionModel(withHeader: "Jul 23 2020", footer: nil, andCellModels: [
			MagmaWallet.TransactionHistoryCellModel(operationHash: "ooPWdDR8L1zz6uAmdCPFLf5rVwba7HrStZX1PxKc8vKuS9y1tBh", type: "DELEGATION", amount: "N/A", address: "To: tz1YH2LE6p7Sj1...AZYHnX", fee: "Network fee: 0.00138 XTZ")
		]),
		UITableViewSectionModel(withHeader: "Jul 16 2020", footer: nil, andCellModels: [
			MagmaWallet.TransactionHistoryCellModel(operationHash: "oor4MquMKJoovQ92Tw9vJdN3uaKFL588D6bzYqGmYqCGGTwDbCs", type: "RECEIVE", amount: "+1,000 ?", address: "From: Testy McTestface", fee: "Network fee: N/A")
		])
	]
	
	static let filteredTransactionHistoryGroupData: [ [TzKTTransaction.TransactionSubType] ] = [
		[.exchangeXTZToToken],
		[.exchangeTokenToXTZ],
		[.send],
		[.receive]
	]
	
	static let filteredTransactionHistoryParsed: [UITableViewSectionModel] = [
		UITableViewSectionModel(withHeader: "Oct 28 2020", footer: nil, andCellModels: [
			MagmaWallet.TransactionHistoryExchangeCellModel(operationHash: "ooVkQGcqMdtYbbrNAmE1Ht1v46LD4NQVnDCLCviL5QQ9HukUJUJ", amountSent: "-10 XTZ", amountReceived: "+0.0001146 tzBTC", exchangeRate: "1 XTZ → 0.000011 tzBTC", networkFee: "Network fee: 0.045359 XTZ")
		]),
		UITableViewSectionModel(withHeader: "Oct 27 2020", footer: nil, andCellModels: [
			MagmaWallet.TransactionHistoryExchangeCellModel(operationHash: "ooRXrm9wNiguRhJSAfy58ta2ZE89W3aW5ZPprAhSWojwNQFdwb9", amountSent: "-0.000005 tzBTC", amountReceived: "+29.787038 XTZ", exchangeRate: "1 tzBTC → 5957407.6 XTZ", networkFee: "Network fee: 0.0876 XTZ")
		]),
		UITableViewSectionModel(withHeader: "Oct 21 2020", footer: nil, andCellModels: [
			MagmaWallet.TransactionHistoryCellModel(operationHash: "ooNikcHDL1DUsfuh7N8DkJwbcHPBhgyij2FxQvA99NtuRWTzcnk", type: "SEND", amount: "-10 XTZ", address: "To: tz1SqjMCv8XBEe...EoNiEV", fee: "Network fee: 0.259415 XTZ")
		]),
		UITableViewSectionModel(withHeader: "Oct 16 2020", footer: nil, andCellModels: [
			MagmaWallet.TransactionHistoryCellModel(operationHash: "opND5nyS1GWgxvp3xhFwi9heLjUvgvbTCSLsgRcSU3xghk1hCYd", type: "RECEIVE", amount: "+25 XTZ", address: "From: Testy McTestface", fee: "Network fee: N/A")
		])
	]
}
