//
//  MockSharedHelpers.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 18/06/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
import Keys
@testable import MagmaWallet

class MockSharedHelpers: XCTestCase {
	
	private static let shared = MockSharedHelpers()
	
	
	// MARK: - Wallet and balance
	class func createWallet() {
		let _ = DependencyManager.shared.accountService.deleteWallet()
		let _ = DependencyManager.shared.accountService.importWallet(mnemonic: MockConstants.walletMnemonic, passpharse: "", type: .hd, derivationPath: MockConstants.walletDefaultDerivationPath)
	}
	
	class func addTokens() {
		let expectation = shared.expectation(description: "refesh balances")
		DependencyManager.shared.accountService.refreshBalances(forceRefresh: false) { (result) in
			switch result {
				case .success(_):
					
					if URLSessionMock.shared.triggerLowBalance {
						XCTAssert(DependencyManager.shared.tokens[0].balance == MockConstants.walletXTZLowBalance, DependencyManager.shared.tokens[0].balance.normalisedRepresentation)
					} else {
						XCTAssert(DependencyManager.shared.tokens[0].balance == MockConstants.walletXTZBalance, DependencyManager.shared.tokens[0].balance.normalisedRepresentation)
					}
					
				case .failure(let error):
					XCTFail("Encountered error: \(error)")
			}
			
			expectation.fulfill()
		}
		
		shared.waitForExpectations(timeout: 3, handler: nil)
	}
	
	class func updateAllBalancesAndPrices() {
		let expectation = shared.expectation(description: "refesh balances")
		
		let defualtCurrencyCode = Locale.current.currencyCode?.lowercased() ?? "eur"
		DependencyManager.shared.tokenPriceService.updateSelectedCurrency(code: defualtCurrencyCode) {
			DependencyManager.shared.homeService.executeBalanceAndPriceQueries(completion: { (error) in
				XCTAssert(error == nil)
				expectation.fulfill()
				
			})
		}
		
		shared.waitForExpectations(timeout: 3, handler: nil)
	}
	
	class func updateDelegate() {
		let expectation = shared.expectation(description: "refesh wallet delegate")
		DependencyManager.shared.accountService.refreshDelegate {
			expectation.fulfill()
		}
		
		shared.waitForExpectations(timeout: 3, handler: nil)
	}
	
	class func createWalletWithEverything() {
		MockSharedHelpers.createWallet()
		MockSharedHelpers.updateAllBalancesAndPrices()
		MockSharedHelpers.updateDelegate()
	}
	
	class func deleteWalletAndBalances() {
		let _ = DependencyManager.shared.accountService.deleteWallet()
		DependencyManager.shared.tokenPriceService.deleteRatesFromDisk()
	}
	
	
	
	// MARK: - Bakers
	
	class func fetchBakers() {
		let expectation = shared.expectation(description: "refresh bakers")
		DependencyManager.shared.tezosBakerService.fetchAndStoreTezosBakers { (success) in
			expectation.fulfill()
		}
		
		shared.waitForExpectations(timeout: 20, handler: nil)
	}
	
	
	
	// MARK: - Transaction history
	
	class func fetchTransactionHistory() {
		let expectation = shared.expectation(description: "refresh transaction history")
		DependencyManager.shared.tzktClient.refreshTransactionHistory(forAddress: MockConstants.walletAddressHdNoPassphrase, andSupportedTokens: DependencyManager.shared.tokens, completion: {
			expectation.fulfill()
		})
		
		shared.waitForExpectations(timeout: 20, handler: nil)
	}
	
	class func deleteTransactionHistory() {
		DependencyManager.shared.tzktClient.clearHistory()
	}
	
	
	
	// MARK: - Everything
	
	class func updateEverything() {
		MockSharedHelpers.deleteWalletAndBalances()
		MockSharedHelpers.deleteTransactionHistory()
		
		MockSharedHelpers.createWalletWithEverything()
		MockSharedHelpers.fetchTransactionHistory()
	}
}
