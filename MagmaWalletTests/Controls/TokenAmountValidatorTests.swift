//
//  TokenAmountValidatorTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 25/02/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import XCTest
import camlKit
@testable import MagmaWallet

class TokenAmountValidatorTests: XCTestCase {

	// MARK: - Test Setup
	
	override func setUp() {
		URLSessionMockErrorGenerator.shared.clear()
	}
	
	override func tearDown() {
		
	}
	
	
	
	// MARK: - Test cases

	func testTokenAmountValidator() {
		let testString1 = "15"
		let testString2 = "15.000001"
		let testString3 = "16"
		
		let validatorXTZ = TokenAmountValidator(isXTZ: true, balanceLimit: TokenAmount(fromNormalisedAmount: 15, decimalPlaces: 6) )
		XCTAssertTrue(validatorXTZ.validate(text: testString1))
		XCTAssertFalse(validatorXTZ.validate(text: testString2))
		XCTAssertFalse(validatorXTZ.validate(text: testString3))
		
		XCTAssertFalse(validatorXTZ.restrictEntryIfInvalid(text: testString1))
		XCTAssertFalse(validatorXTZ.restrictEntryIfInvalid(text: testString2))
		
		let validatorToken = TokenAmountValidator(isXTZ: false, balanceLimit: TokenAmount(fromNormalisedAmount: 15, decimalPlaces: 6) )
		XCTAssertTrue(validatorToken.validate(text: testString1))
		XCTAssertFalse(validatorToken.validate(text: testString2))
		XCTAssertFalse(validatorToken.validate(text: testString3))
		
		XCTAssertFalse(validatorToken.restrictEntryIfInvalid(text: testString1))
		XCTAssertFalse(validatorToken.restrictEntryIfInvalid(text: testString3))
	}

}
