//
//  ContactsViewModelTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 16/09/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
@testable import MagmaWallet

class ContactsViewModelTests: XCTestCase {
	
	let mockStore = DependencyManager.shared.contactService.contactStore as? CNContactStoreMock
	
	
	// MARK: - Test Setup
    override func setUp() {
		URLSessionMockErrorGenerator.shared.clear()
		mockStore?.resetMockContacts()
    }

    override func tearDown() {
		
    }
	
	
	// MARK: - Test cases
	func testContacts() {
		ContactServiceTests().processContacts()
		
		let viewModel = ContactsViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(response.success)
			XCTAssert(viewModel.tableViewData.count == 1)
			XCTAssert(viewModel.tableViewData[0].cellModels.count == 2)
			
			let cellModel1 = viewModel.tableViewData[0].cellModels[0] as? ContactCellModel
			XCTAssert(cellModel1?.name == "Jane Doe")
			XCTAssert(cellModel1?.address == CNContactStoreMock.mockAddress1)
			
			let cellModel2 = viewModel.tableViewData[0].cellModels[1] as? ContactCellModel
			XCTAssert(cellModel2?.name == "Boaty McBoatface")
			XCTAssert(cellModel2?.address == CNContactStoreMock.mockAddress1)
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 3, handler: nil)
	}
}
