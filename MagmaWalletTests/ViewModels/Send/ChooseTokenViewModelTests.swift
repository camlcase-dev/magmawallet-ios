//
//  ChooseTokenViewModelTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 18/06/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
@testable import MagmaWallet

class ChooseTokenViewModelTests: XCTestCase {
	
	// MARK: - Test Setup
	
    override func setUp() {
		URLSessionMockErrorGenerator.shared.clear()
    }

    override func tearDown() {
		
    }
	
	
	
	// MARK: - Test cases
	
	func testChooseToken() {
		MockSharedHelpers.createWalletWithEverything()
		
		let viewModel = ChooseTokenViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { [weak self] (response) in
			
			XCTAssert(viewModel.tableViewData.count == 1)
			XCTAssert(viewModel.tableViewData[0].cellModels.count == DependencyManager.shared.tokens.count, "\(viewModel.tableViewData[0].cellModels.count)")
			
			// Check XTZ
			self?.checkXTZ(cellModel: viewModel.tableViewData[0].cellModels[0])
			
			// Check Tokens
			let cellModels = viewModel.tableViewData[0].cellModels
			self?.checkTokens(cellModels: [cellModels[1], cellModels[2]])
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 3, handler: nil)
	}
	
	
	
	// MARK: - Helpers
	
	func checkXTZ(cellModel: UITableViewCellModel) {
		let model = cellModel as! ChooseTokenCellModel
		XCTAssert(model.symbol == DependencyManager.shared.tokens[0].symbol)
		XCTAssert(model.amount == MockConstants.walletTezBalanceFormattedWithoutSymbol, "\(model.amount) != \(MockConstants.walletTezBalanceFormattedWithoutSymbol)")
		XCTAssert(model.currencyString == MockConstants.walletTezCurrencyFormatted, "\(model.currencyString) != \(MockConstants.walletTezCurrencyFormatted)")
	}
	
	func checkTokens(cellModels: [UITableViewCellModel]) {
		for (index, model) in cellModels.enumerated() {
			let m = model as! ChooseTokenCellModel
			let token = DependencyManager.shared.tokens[index+1]
			
			XCTAssert(m.symbol == token.symbol)
			
			if token.decimalPlaces == 8 {
				XCTAssert(m.amount == MockConstants.walletTokenDecimal8BalanceFormatted, "\(m.amount)")
				XCTAssert(m.currencyString == MockConstants.walletTokenDecimal8CurrencyFormatted, "\(m.currencyString)")
						   
			} else if token.decimalPlaces == 6 {
				XCTAssert(m.amount == MockConstants.walletTokenDecimal6BalanceFormatted, "\(m.amount)")
				XCTAssert(m.currencyString == MockConstants.walletTokenDecimal6CurrencyFormatted, "\(m.currencyString)")
				
			} else {
				XCTAssert(m.amount == MockConstants.walletTokenDecimal18BalanceFormatted, "\(m.amount)")
				XCTAssert(m.currencyString == MockConstants.walletTokenDecimal18CurrencyFormatted, "\(m.currencyString)")
			}
		}
	}
}
