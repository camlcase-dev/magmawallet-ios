//
//  SendWaitViewModelTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 18/06/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
import camlKit
@testable import MagmaWallet

class SendWaitViewModelTests: XCTestCase {
	
	// MARK: - Test Setup
	
	override class func setUp() {
		ConfirmSendViewModelTests.setUp()
	}
	
    override func setUp() {
		URLSessionMockErrorGenerator.shared.clear()
    }

    override func tearDown() {
		
    }
	
	
	
	// MARK: - Test cases
	
	func testViewModelSuccess() {
		
		// Use test to fill TransactionService state with Mock operation object
		ConfirmSendViewModelTests().testSendXTZ()
		
		let viewModel = SendWaitViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(response.success)
			XCTAssert(DependencyManager.shared.transactionService.sendData.inProgressOpHash == MockConstants.operationHash)
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 20, handler: nil)
	}
	
	func testViewModelFailureGas() {
		
		// Use test to fill TransactionService state with Mock operation object
		ConfirmSendViewModelTests().testSendXTZ()
		
		URLSessionMockErrorGenerator.shared.triggerGasExhaustedError()
		
		let viewModel = SendWaitViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(response.success == false)
			XCTAssert(response.errorResponse?.errorType == .unknownError, "\(response.errorResponse?.errorType.rawValue ?? "")")
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 20, handler: nil)
	}
}
