//
//  ConfirmSendViewModelTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 17/02/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
import camlKit
@testable import MagmaWallet

class ConfirmSendViewModelTests: XCTestCase {
	
	// MARK: - Test Setup
	
	override class func setUp() {
		MockSharedHelpers.createWalletWithEverything()
		MockSharedHelpers.updateAllBalancesAndPrices()
	}
	
    override func setUp() {
		URLSessionMockErrorGenerator.shared.clear()
    }

    override func tearDown() {
		
    }
	
	
	// MARK: - Test cases
	
	func testSendXTZ() {
		DependencyManager.shared.transactionService.transactionType = .send
		DependencyManager.shared.transactionService.sendData.amountToSend = XTZAmount(fromNormalisedAmount: 27)
		DependencyManager.shared.transactionService.sendData.recipientAddress = MockConstants.recipient
		DependencyManager.shared.transactionService.sendData.selectedTokenIndex = 0
		
		
		let viewModel = ConfirmSendViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(viewModel.sendTokenString == "27 XTZ", viewModel.sendTokenString)
			XCTAssert(viewModel.sendCurrencyString == "€51.84", viewModel.sendCurrencyString)
			XCTAssert(viewModel.networkFeeTokenString == "0.001296 XTZ", viewModel.networkFeeTokenString)
			XCTAssert(viewModel.networkFeeCurrencyString == "€0.00", viewModel.networkFeeCurrencyString)
			XCTAssert(viewModel.totalTokenString == "27.001296 XTZ", viewModel.totalTokenString)
			XCTAssert(viewModel.totalCurrencyString == "€51.84", "\(viewModel.totalCurrencyString)")
			XCTAssert(viewModel.addressString == MockConstants.recipient)
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 30, handler: nil)
	}
	
	func testSendXTZErrorInvalidAddress() {
		DependencyManager.shared.transactionService.transactionType = .send
		DependencyManager.shared.transactionService.sendData.amountToSend = XTZAmount(fromNormalisedAmount: 27)
		DependencyManager.shared.transactionService.sendData.recipientAddress = MockConstants.recipient
		DependencyManager.shared.transactionService.sendData.selectedTokenIndex = 0
		
		URLSessionMockErrorGenerator.shared.triggerInvalidAddressError()
		
		let viewModel = ConfirmSendViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(response.success == false)
			XCTAssert(response.errorResponse?.errorType == .invalidAddress, "\(response.errorResponse?.errorType.rawValue ?? "")")
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 30, handler: nil)
	}
	
	func testSendXTZErrorFunds() {
		DependencyManager.shared.transactionService.transactionType = .send
		DependencyManager.shared.transactionService.sendData.amountToSend = XTZAmount(fromNormalisedAmount: 27)
		DependencyManager.shared.transactionService.sendData.recipientAddress = MockConstants.recipient
		DependencyManager.shared.transactionService.sendData.selectedTokenIndex = 0
		
		URLSessionMockErrorGenerator.shared.triggerInvalidAddressError()
		
		let viewModel = ConfirmSendViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(response.success == false)
			XCTAssert(response.errorResponse?.errorType == .invalidAddress, "\(response.errorResponse?.errorType.rawValue ?? "")")
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 30, handler: nil)
	}
	
	func testSendXTZErrorSupressed() {
		DependencyManager.shared.transactionService.transactionType = .send
		DependencyManager.shared.transactionService.sendData.amountToSend = XTZAmount(fromNormalisedAmount: 27)
		DependencyManager.shared.transactionService.sendData.recipientAddress = MockConstants.recipient
		DependencyManager.shared.transactionService.sendData.selectedTokenIndex = 0
		
		URLSessionMockErrorGenerator.shared.triggerBalanceTooLowError()
		
		let viewModel = ConfirmSendViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			// Insufficient funds error on a normal send are now surpressed, because its only possible to know about some fees after estimation.
			// Process is to always deuct the fees from the sending amount if sending max
			XCTAssert(response.success == true)
			
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 30, handler: nil)
	}
	
	func testSendToken() {
		DependencyManager.shared.transactionService.transactionType = .send
		DependencyManager.shared.transactionService.sendData.amountToSend = TokenAmount(fromNormalisedAmount: 4, decimalPlaces: DependencyManager.shared.tokens[1].decimalPlaces)
		DependencyManager.shared.transactionService.sendData.recipientAddress = MockConstants.recipient
		DependencyManager.shared.transactionService.sendData.selectedTokenIndex = 1
		
		
		let viewModel = ConfirmSendViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(viewModel.sendTokenString == "4 tzBTC", "\(viewModel.sendTokenString)")
			XCTAssert(viewModel.sendCurrencyString == "€7,536,534.08", "\(viewModel.sendCurrencyString)")
			XCTAssert(viewModel.networkFeeTokenString == "0.001296 XTZ", "\(viewModel.networkFeeTokenString)")
			XCTAssert(viewModel.networkFeeCurrencyString == "€0.00")
			XCTAssert(viewModel.totalTokenString == "4 tzBTC\n + 0.001296 XTZ", "\(viewModel.totalTokenString)")
			XCTAssert(viewModel.totalCurrencyString == "€7,536,534.09", "\(viewModel.totalCurrencyString)")
			XCTAssert(viewModel.addressString == MockConstants.recipient)
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 30, handler: nil)
	}
	
	func testSendTokenErrorInvalidAddress() {
		DependencyManager.shared.transactionService.transactionType = .send
		DependencyManager.shared.transactionService.sendData.amountToSend = TokenAmount(fromNormalisedAmount: 942, decimalPlaces: DependencyManager.shared.tokens[1].decimalPlaces)
		DependencyManager.shared.transactionService.sendData.recipientAddress = MockConstants.recipient
		DependencyManager.shared.transactionService.sendData.selectedTokenIndex = 1
		
		URLSessionMockErrorGenerator.shared.triggerInvalidAddressError()
		
		let viewModel = ConfirmSendViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(response.success == false)
			XCTAssert(response.errorResponse?.errorType == .invalidAddress, "\(response.errorResponse?.errorType.rawValue ?? "")")
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 30, handler: nil)
	}
	
	func testSendTokenErrorFunds() {
		DependencyManager.shared.transactionService.transactionType = .send
		DependencyManager.shared.transactionService.sendData.amountToSend = TokenAmount(fromNormalisedAmount: 942, decimalPlaces: DependencyManager.shared.tokens[1].decimalPlaces)
		DependencyManager.shared.transactionService.sendData.recipientAddress = MockConstants.recipient
		DependencyManager.shared.transactionService.sendData.selectedTokenIndex = 1
		
		URLSessionMockErrorGenerator.shared.triggerBalanceTooLowError()
		
		let viewModel = ConfirmSendViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(response.success == false)
			XCTAssert(response.errorResponse?.errorType == .insufficientFunds, "\(response.errorResponse?.errorType.rawValue ?? "")")
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 30, handler: nil)
	}
}
