//
//  ImportBalanceViewModelTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 24/03/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import XCTest
@testable import MagmaWallet

class ImpactBalanceViewModelTests: XCTestCase {
	
	// MARK: - Test Setup
	
	override func setUp() {
		URLSessionMockErrorGenerator.shared.clear()
	}
	
	override func tearDown() {
		
	}
	
	
	
	// MARK: - Test cases
	
	func testBalances() {
		MockSharedHelpers.createWalletWithEverything()
		
		DependencyManager.shared.transactionService.walletCreationData.importlinearWalletAddress = MockConstants.walletAddressLinearNoPassphrase
		DependencyManager.shared.transactionService.walletCreationData.importHdWalletAddress = MockConstants.walletAddressHdNoPassphrase
		
		let viewModel = ImportBalanceViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(viewModel.tableViewData.count == 2, "\(viewModel.tableViewData.count)")
			XCTAssert(viewModel.tableViewData[0].cellModels.count == 1, "\(viewModel.tableViewData[0].cellModels.count)")
			XCTAssert(viewModel.tableViewData[0].header == "HD wallet (BIP-44)", viewModel.tableViewData[0].header ?? "")
			XCTAssert(viewModel.tableViewData[1].cellModels.count == 1, "\(viewModel.tableViewData[1].cellModels.count)")
			XCTAssert(viewModel.tableViewData[1].header == "Non-HD wallet", viewModel.tableViewData[1].header ?? "")
			
			let firstCell = viewModel.tableViewData[0].cellModels[0] as? ImportBalanceCellModel
			let secondCell = viewModel.tableViewData[1].cellModels[0] as? ImportBalanceCellModel
			
			XCTAssert(firstCell?.address == "tz1h63ojTT6uiSuKqNJqAU9ezwmB719kMARK", firstCell?.address ?? "")
			XCTAssert(firstCell?.balance == "0.000001 XTZ", firstCell?.balance ?? "")
			XCTAssert(firstCell?.otherTokens == true)
			
			XCTAssert(secondCell?.address == "tz1h63ojTT6uiSuKqNJqAU9ezwmB719kMARK", secondCell?.address ?? "")
			XCTAssert(secondCell?.balance == "0.000001 XTZ", secondCell?.balance ?? "")
			XCTAssert(secondCell?.otherTokens == true)
			
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 3, handler: nil)
	}
	
	func testLoadingCell() {
		let tableViewData = ImportBalanceViewModel().loadingCell()
		
		XCTAssert(tableViewData.count == 1)
		XCTAssert(tableViewData[0].cellModels.count == 1)
		XCTAssert(tableViewData[0].cellModels[0] is LoadingCellModel)
	}
	
	func testErrorCell() {
		let tableViewData = ImportBalanceViewModel().errorCell()
		
		XCTAssert(tableViewData.count == 1)
		XCTAssert(tableViewData[0].cellModels.count == 1)
		XCTAssert(tableViewData[0].cellModels[0] is ErrorCellModel)
	}
}
