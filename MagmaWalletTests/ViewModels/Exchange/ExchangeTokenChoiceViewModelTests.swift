//
//  ExchangeTokenChoiceViewModelTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 18/06/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
@testable import MagmaWallet

class ExchangeTokenChoiceViewModelTests: XCTestCase {
	
	// MARK: - Test Setup
	
    override func setUp() {
		URLSessionMockErrorGenerator.shared.clear()
    }

    override func tearDown() {
		
    }
	
	
	
	// MARK: - Test cases
	
	func testListXTZToTokenChoosingPrimary() {
		MockSharedHelpers.createWalletWithEverything()
		
		DependencyManager.shared.transactionService.transactionType = .exchange
		DependencyManager.shared.transactionService.exchangeData.tokenSelectionInProgress = .primary
		DependencyManager.shared.transactionService.exchangeData.selectedPrimaryTokenIndex = 0
		DependencyManager.shared.transactionService.exchangeData.selectedSecondaryTokenIndex = 1
		
		let viewModel = ExchangeTokenChoiceViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(viewModel.tableViewData.count == 1)
			XCTAssert(viewModel.tableViewData[0].cellModels.count == DependencyManager.shared.tokens.count, "\(viewModel.tableViewData[0].cellModels.count)")
			
			let cellModel1 = viewModel.tableViewData[0].cellModels[0] as! TokenChoiceCellModel
			XCTAssert(cellModel1.symbol == DependencyManager.shared.tokens[0].symbol, cellModel1.symbol)
			XCTAssert(cellModel1.balance == MockConstants.walletTezBalanceFormattedWithoutSymbol, cellModel1.balance)
			
			let cellModel2 = viewModel.tableViewData[0].cellModels[1] as! TokenChoiceCellModel
			XCTAssert(cellModel2.symbol == DependencyManager.shared.tokens[1].symbol, cellModel2.symbol)
			XCTAssert(cellModel2.balance == MockConstants.walletTokenDecimal8BalanceFormatted, cellModel2.balance)
			
			let cellModel3 = viewModel.tableViewData[0].cellModels[2] as! TokenChoiceCellModel
			XCTAssert(cellModel3.symbol == DependencyManager.shared.tokens[2].symbol, cellModel3.symbol)
			XCTAssert(cellModel3.balance == MockConstants.walletTokenDecimal6BalanceFormatted, cellModel3.balance)
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 3, handler: nil)
	}
	
	func testListXTZToTokenChoosingSecondary() {
		MockSharedHelpers.createWalletWithEverything()
		
		DependencyManager.shared.transactionService.transactionType = .exchange
		DependencyManager.shared.transactionService.exchangeData.tokenSelectionInProgress = .secondary
		DependencyManager.shared.transactionService.exchangeData.selectedPrimaryTokenIndex = 0
		DependencyManager.shared.transactionService.exchangeData.selectedSecondaryTokenIndex = 1
		
		let viewModel = ExchangeTokenChoiceViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(viewModel.tableViewData.count == 1)
			XCTAssert(viewModel.tableViewData[0].cellModels.count == DependencyManager.shared.tokens.count-2, "\(viewModel.tableViewData[0].cellModels.count)")
			
			let cellModel1 = viewModel.tableViewData[0].cellModels[0] as! TokenChoiceCellModel
			XCTAssert(cellModel1.symbol == DependencyManager.shared.tokens[2].symbol)
			XCTAssert(cellModel1.balance == "", "\(cellModel1.balance)")
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 3, handler: nil)
	}
	
	func testListTokenToXTZChoosingPrimary() {
		MockSharedHelpers.createWalletWithEverything()
		
		DependencyManager.shared.transactionService.transactionType = .exchange
		DependencyManager.shared.transactionService.exchangeData.tokenSelectionInProgress = .primary
		DependencyManager.shared.transactionService.exchangeData.selectedPrimaryTokenIndex = 1
		DependencyManager.shared.transactionService.exchangeData.selectedSecondaryTokenIndex = 0
		
		let viewModel = ExchangeTokenChoiceViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(viewModel.tableViewData.count == 1)
			XCTAssert(viewModel.tableViewData[0].cellModels.count == DependencyManager.shared.tokens.count, "\(viewModel.tableViewData[0].cellModels.count)")
			
			let cellModel1 = viewModel.tableViewData[0].cellModels[0] as! TokenChoiceCellModel
			XCTAssert(cellModel1.symbol == DependencyManager.shared.tokens[0].symbol)
			XCTAssert(cellModel1.balance == MockConstants.walletTezBalanceFormattedWithoutSymbol)
			
			let cellModel2 = viewModel.tableViewData[0].cellModels[1] as! TokenChoiceCellModel
			XCTAssert(cellModel2.symbol == DependencyManager.shared.tokens[1].symbol, cellModel2.symbol)
			XCTAssert(cellModel2.balance == MockConstants.walletTokenDecimal8BalanceFormatted, cellModel2.balance)
			
			let cellModel3 = viewModel.tableViewData[0].cellModels[2] as! TokenChoiceCellModel
			XCTAssert(cellModel3.symbol == DependencyManager.shared.tokens[2].symbol, cellModel3.symbol)
			XCTAssert(cellModel3.balance == MockConstants.walletTokenDecimal6BalanceFormatted, cellModel3.balance)
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 3, handler: nil)
	}
	
	func testListTokenToXTZChoosingSecondary() {
		MockSharedHelpers.createWalletWithEverything()
		
		DependencyManager.shared.transactionService.transactionType = .exchange
		DependencyManager.shared.transactionService.exchangeData.tokenSelectionInProgress = .secondary
		DependencyManager.shared.transactionService.exchangeData.selectedPrimaryTokenIndex = 1
		DependencyManager.shared.transactionService.exchangeData.selectedSecondaryTokenIndex = 0
		
		let viewModel = ExchangeTokenChoiceViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(viewModel.tableViewData.count == 1)
			XCTAssert(viewModel.tableViewData[0].cellModels.count == DependencyManager.shared.tokens.count-2, "\(viewModel.tableViewData[0].cellModels.count)")
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 3, handler: nil)
	}
}
