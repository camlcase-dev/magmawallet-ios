//
//  ExchangeWaitViewModelTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 18/06/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
@testable import MagmaWallet

class ExchangeWaitViewModelTests: XCTestCase {
	
	// MARK: - Test Setup
	
	override class func setUp() {
		ExchangeConfirmViewModelTests.setUp()
	}
	
    override func setUp() {
		URLSessionMockErrorGenerator.shared.clear()
    }

    override func tearDown() {
		
    }
	
	
	
	// MARK: - Test cases
	
	func testXTZToTokenSuccess() {
		
		// Use test to fill TransactionService state with Mock operation object
		ExchangeConfirmViewModelTests().testXTZToToken()
		
		let viewModel = ExchangeWaitViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(response.success)
			XCTAssert(DependencyManager.shared.transactionService.exchangeData.inProgressOpHash == MockConstants.operationHash)
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 30, handler: nil)
	}
	
	func testXTZToTokenFailure() {
		
		// Use test to fill TransactionService state with Mock operation object
		ExchangeConfirmViewModelTests().testXTZToToken()
		
		URLSessionMockErrorGenerator.shared.triggerGasExhaustedError()
		
		let viewModel = ExchangeWaitViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(response.success == false)
			XCTAssert(response.errorResponse?.errorType == .unknownError, "\(response.errorResponse?.errorType.rawValue ?? "")")
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 30, handler: nil)
	}
	
	func testTokenToXTZSuccess() {
		
		// Use test to fill TransactionService state with Mock operation object
		ExchangeConfirmViewModelTests().testTokenToXTZ()
		
		let viewModel = ExchangeWaitViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(response.success)
			XCTAssert(DependencyManager.shared.transactionService.exchangeData.inProgressOpHash == MockConstants.operationHash)
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 30, handler: nil)
	}
	
	func testTokenToXTZFailure() {
		
		// Use test to fill TransactionService state with Mock operation object
		ExchangeConfirmViewModelTests().testTokenToXTZ()
		
		URLSessionMockErrorGenerator.shared.triggerGasExhaustedError()
		
		let viewModel = ExchangeWaitViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(response.success == false)
			XCTAssert(response.errorResponse?.errorType == .unknownError, "Returned: \(response.errorResponse?.errorType.rawValue ?? "")")
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 30, handler: nil)
	}
}
