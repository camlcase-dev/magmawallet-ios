//
//  ExchangeViewModelTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 18/06/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
import camlKit
@testable import MagmaWallet

class ExchangeViewModelTests: XCTestCase {
	
	// MARK: - Test Setup
	
    override func setUp() {
		URLSessionMockErrorGenerator.shared.clear()
    }

    override func tearDown() {
		
    }
	
	
	
	// MARK: - Test cases
	
	func testViewModelEmpty() {
		MockSharedHelpers.createWalletWithEverything()
		DependencyManager.shared.transactionService.resetStoredData()
		
		let viewModel = ExchangeViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(viewModel.textFieldValidator.isXTZ == true)
			XCTAssert(viewModel.textFieldValidator.balanceLimit.description == "2023.233806")
			XCTAssert(viewModel.fromButtonString == DependencyManager.shared.tokens[0].symbol)
			XCTAssert(viewModel.toButtonString == DependencyManager.shared.tokens[1].symbol)
			XCTAssert(viewModel.fromTokenBalance == MockConstants.walletTezBalanceFormattedWithoutSymbol, viewModel.fromTokenBalance)
			XCTAssert(viewModel.conversionRateString == "1 XTZ = 0.00000102 tzBTC", viewModel.conversionRateString)
			XCTAssert(viewModel.fromCurrencyString == "€0.00", viewModel.fromCurrencyString)
			XCTAssert(viewModel.toAmountString == "0")
			XCTAssert(viewModel.toCurrencyString == "€0.00")
			XCTAssert(viewModel.isMaxEnabled == true)
			XCTAssert(viewModel.isUIDisabled == false)
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 3, handler: nil)
	}
	
	func testViewModelSwapXTZ() {
		MockSharedHelpers.createWalletWithEverything()
		
		DependencyManager.shared.transactionService.transactionType = .exchange
		DependencyManager.shared.transactionService.exchangeData.selectedPrimaryTokenIndex = 0
		DependencyManager.shared.transactionService.exchangeData.selectedSecondaryTokenIndex = 1
		DependencyManager.shared.transactionService.updateAmountToExchange(amount: TokenAmount(fromNormalisedAmount: 42, decimalPlaces: 6) )
		
		let viewModel = ExchangeViewModel()
		viewModel.fromButtonString = "XTZ" // skip viewmodel's "first time" state
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(viewModel.textFieldValidator.isXTZ == true)
			XCTAssert(viewModel.textFieldValidator.balanceLimit.description == "2023.233806")
			XCTAssert(viewModel.fromButtonString == DependencyManager.shared.tokens[0].symbol)
			XCTAssert(viewModel.toButtonString == DependencyManager.shared.tokens[1].symbol)
			XCTAssert(viewModel.fromTokenBalance == MockConstants.walletTezBalanceFormattedWithoutSymbol, viewModel.fromTokenBalance)
			XCTAssert(viewModel.conversionRateString == "1 XTZ = 0.00000101 tzBTC", viewModel.conversionRateString)
			XCTAssert(viewModel.fromCurrencyString == "€80.64", viewModel.fromCurrencyString)
			XCTAssert(viewModel.toAmountString == "0.00004244", viewModel.toAmountString)
			XCTAssert(viewModel.toCurrencyString == "€79.96", viewModel.toCurrencyString)
			XCTAssert(viewModel.isMaxEnabled == true)
			XCTAssert(viewModel.isUIDisabled == false)
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 3, handler: nil)
	}
	
	func testViewModelSwapXTZMax() {
		MockSharedHelpers.createWalletWithEverything()
		
		DependencyManager.shared.transactionService.transactionType = .exchange
		DependencyManager.shared.transactionService.exchangeData.selectedPrimaryTokenIndex = 0
		DependencyManager.shared.transactionService.exchangeData.selectedSecondaryTokenIndex = 1
		
		let viewModel = ExchangeViewModel()
		viewModel.fromButtonString = "XTZ" // skip viewmodel's "first time" state
		
		let maxValueString = viewModel.maxValue()
		let maxValueBalance = DependencyManager.shared.tokens[0].balance
		XCTAssert(maxValueString == "2023.233806")
		XCTAssert(maxValueString == maxValueBalance.normalisedRepresentation)
		
		DependencyManager.shared.transactionService.updateAmountToExchange(amount: maxValueBalance)
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(viewModel.textFieldValidator.isXTZ == true)
			XCTAssert(viewModel.textFieldValidator.balanceLimit.description == "2023.233806")
			XCTAssert(viewModel.fromButtonString == DependencyManager.shared.tokens[0].symbol)
			XCTAssert(viewModel.toButtonString == DependencyManager.shared.tokens[1].symbol)
			XCTAssert(viewModel.fromTokenBalance == MockConstants.walletTezBalanceFormattedWithoutSymbol, viewModel.fromTokenBalance)
			XCTAssert(viewModel.conversionRateString == "1 XTZ = 0.00000081 tzBTC", viewModel.conversionRateString)
			XCTAssert(viewModel.fromCurrencyString == "€3,884.60", viewModel.fromCurrencyString)
			XCTAssert(viewModel.toAmountString == "0.00164056", viewModel.toAmountString)
			XCTAssert(viewModel.toCurrencyString == "€3,091.03", viewModel.toCurrencyString)
			XCTAssert(viewModel.isMaxEnabled == false)
			XCTAssert(viewModel.isUIDisabled == false)
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 3, handler: nil)
	}
	
	func testViewModelSwapToken() {
		MockSharedHelpers.createWalletWithEverything()
		
		DependencyManager.shared.transactionService.transactionType = .exchange
		DependencyManager.shared.transactionService.exchangeData.selectedPrimaryTokenIndex = 1
		DependencyManager.shared.transactionService.exchangeData.selectedSecondaryTokenIndex = 0
		DependencyManager.shared.transactionService.updateAmountToExchange(amount: TokenAmount(fromNormalisedAmount: 0.005, decimalPlaces: DependencyManager.shared.tokens[1].decimalPlaces))
		
		let viewModel = ExchangeViewModel()
		viewModel.fromButtonString = "XTZ" // skip viewmodel's "first time" state
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(viewModel.textFieldValidator.isXTZ == false)
			XCTAssert(viewModel.textFieldValidator.balanceLimit.description == "0.00812591", viewModel.textFieldValidator.balanceLimit.description)
			XCTAssert(viewModel.fromButtonString == DependencyManager.shared.tokens[1].symbol)
			XCTAssert(viewModel.toButtonString == DependencyManager.shared.tokens[0].symbol)
			XCTAssert(viewModel.fromTokenBalance == MockConstants.walletTokenDecimal8BalanceFormatted, viewModel.fromTokenBalance)
			XCTAssert(viewModel.conversionRateString == "1 tzBTC = 606379.873 XTZ", viewModel.conversionRateString)
			XCTAssert(viewModel.fromCurrencyString == "€9,420.66", viewModel.fromCurrencyString)
			XCTAssert(viewModel.toAmountString == "3,031.899365", viewModel.toAmountString)
			XCTAssert(viewModel.toCurrencyString == "€5,821.24", viewModel.toCurrencyString)
			XCTAssert(viewModel.isMaxEnabled == true)
			XCTAssert(viewModel.isUIDisabled == false)
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 3, handler: nil)
	}
	
	func testViewModelSwapTokenMax() {
		MockSharedHelpers.createWalletWithEverything()
		
		DependencyManager.shared.transactionService.transactionType = .exchange
		DependencyManager.shared.transactionService.exchangeData.selectedPrimaryTokenIndex = 1
		DependencyManager.shared.transactionService.exchangeData.selectedSecondaryTokenIndex = 0
		
		let viewModel = ExchangeViewModel()
		viewModel.fromButtonString = "XTZ" // skip viewmodel's "first time" state
		
		let maxValueString = viewModel.maxValue()
		let maxValueBalance = DependencyManager.shared.tokens[1].balance
		XCTAssert(maxValueString == "0.00812591", maxValueString)
		XCTAssert(maxValueString == maxValueBalance.normalisedRepresentation)
		
		DependencyManager.shared.transactionService.updateAmountToExchange(amount: maxValueBalance)
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(viewModel.textFieldValidator.isXTZ == false)
			XCTAssert(viewModel.textFieldValidator.balanceLimit.description == "0.00812591", viewModel.textFieldValidator.balanceLimit.description)
			XCTAssert(viewModel.fromButtonString == DependencyManager.shared.tokens[1].symbol)
			XCTAssert(viewModel.toButtonString == DependencyManager.shared.tokens[0].symbol)
			XCTAssert(viewModel.fromTokenBalance == MockConstants.walletTokenDecimal8BalanceFormatted, viewModel.fromTokenBalance)
			XCTAssert(viewModel.conversionRateString == "1 tzBTC = 489922.67598337 XTZ", viewModel.conversionRateString)
			XCTAssert(viewModel.fromCurrencyString == "€15,310.29", viewModel.fromCurrencyString)
			XCTAssert(viewModel.toAmountString == "3,981.067572")
			XCTAssert(viewModel.toCurrencyString == "€7,643.64", viewModel.toCurrencyString)
			XCTAssert(viewModel.isMaxEnabled == false)
			XCTAssert(viewModel.isUIDisabled == false)
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 3, handler: nil)
	}
}
