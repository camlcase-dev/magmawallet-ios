//
//  LiquidityFeesViewModelTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 24/03/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import XCTest
import camlKit
@testable import MagmaWallet

class LiquidityFeesViewModelTests: XCTestCase {
	
	// MARK: - Test Setup
	
	override func setUp() {
		URLSessionMockErrorGenerator.shared.clear()
	}
	
	override func tearDown() {
		
	}
	
	
	
	// MARK: - Test cases
	
	func testLiquidityFee() {
		MockSharedHelpers.createWalletWithEverything()
		
		DependencyManager.shared.transactionService.transactionType = .exchange
		DependencyManager.shared.transactionService.exchangeData.tokenSelectionInProgress = .primary
		DependencyManager.shared.transactionService.exchangeData.selectedPrimaryTokenIndex = 0
		DependencyManager.shared.transactionService.exchangeData.selectedSecondaryTokenIndex = 1
		DependencyManager.shared.transactionService.updateAmountToExchange(amount: TokenAmount(fromNormalisedAmount: 100, decimalPlaces: 6))
		
		let viewModel = LiquidityFeeViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(viewModel.liquidityFeeAmount == "0.3 XTZ", viewModel.liquidityFeeAmount)
			XCTAssert(viewModel.liquidityFeeCurrency == "€0.57", viewModel.liquidityFeeCurrency)
			
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 3, handler: nil)
	}
}
