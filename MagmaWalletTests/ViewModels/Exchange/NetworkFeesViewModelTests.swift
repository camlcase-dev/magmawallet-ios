//
//  NetworkFeesViewModelTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 24/03/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import XCTest
import camlKit
@testable import MagmaWallet

class NetworkFeesViewModelTests: XCTestCase {
	
	// MARK: - Test Setup
	
	override func setUp() {
		URLSessionMockErrorGenerator.shared.clear()
	}
	
	override func tearDown() {
		
	}
	
	
	
	// MARK: - Test cases
	
	func testLiquidityFee() {
		MockSharedHelpers.createWalletWithEverything()
		ExchangeConfirmViewModelTests().testXTZToToken()
		
		let viewModel = NetworkFeeViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(viewModel.tableViewData.count == 1, "\(viewModel.tableViewData.count)")
			XCTAssert(viewModel.tableViewData[0].cellModels.count == 2, "\(viewModel.tableViewData[0].cellModels.count)")
			
			let firstCell = viewModel.tableViewData[0].cellModels[0] as? NetworkFeeCellModel
			let secondCell = viewModel.tableViewData[0].cellModels[1] as? NetworkFeeCellModel
			
			XCTAssert(firstCell?.title == "Baker fee:", firstCell?.title ?? "")
			XCTAssert(firstCell?.amount == "0.001296 XTZ", firstCell?.amount ?? "")
			XCTAssert(firstCell?.currency == "€0.00", firstCell?.currency ?? "")
			
			XCTAssert(secondCell?.title == "Total:", secondCell?.title ?? "")
			XCTAssert(secondCell?.amount == "0.001296 XTZ", secondCell?.amount ?? "")
			XCTAssert(secondCell?.currency == "€0.00", secondCell?.currency ?? "")
			
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 3, handler: nil)
	}
}
