//
//  ExchangeConfirmViewModelTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 18/06/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
import camlKit
@testable import MagmaWallet

class ExchangeConfirmViewModelTests: XCTestCase {
	
	// MARK: - Test Setup
	
	override class func setUp() {
		MockSharedHelpers.createWalletWithEverything()
	}
	
    override func setUp() {
		URLSessionMockErrorGenerator.shared.clear()
    }

    override func tearDown() {
		
    }
	
	
	
	// MARK: - Test cases
	
	func testXTZToToken() {
		DependencyManager.shared.transactionService.transactionType = .exchange
		DependencyManager.shared.transactionService.exchangeData.selectedPrimaryTokenIndex = 0
		DependencyManager.shared.transactionService.exchangeData.selectedSecondaryTokenIndex = 1
		DependencyManager.shared.transactionService.updateAmountToExchange(amount: XTZAmount(fromNormalisedAmount: 18))
		
		let viewModel = ExchangeConfirmViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(viewModel.tokenToSendString == "18 XTZ")
			XCTAssert(viewModel.tokenToSendCurrencyString == "€34.56", viewModel.tokenToSendCurrencyString)
			XCTAssert(viewModel.tokenToReceiveString == "0.00001824 tzBTC", viewModel.tokenToReceiveString)
			XCTAssert(viewModel.tokenToReceiveCurrencyString == "€34.36", viewModel.tokenToReceiveCurrencyString)
			XCTAssert(viewModel.liquidityFeeString == "0.054 XTZ", viewModel.liquidityFeeString)
			XCTAssert(viewModel.liquidityFeeCurrencyString == "€0.10", viewModel.liquidityFeeCurrencyString)
			XCTAssert(viewModel.networkFeeString == "0.001296 XTZ", viewModel.networkFeeString)
			XCTAssert(viewModel.networkFeeCurrencyString == "€0.00", viewModel.networkFeeCurrencyString)
			XCTAssert(viewModel.priceImpactString == "Price impact: 0.23%", viewModel.priceImpactString)
			XCTAssert(viewModel.conversionRateString == "1 XTZ = 0.00000101 tzBTC", viewModel.conversionRateString)
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 20, handler: nil)
	}
	
	func testXTZToTokenMax() {
		DependencyManager.shared.transactionService.transactionType = .exchange
		DependencyManager.shared.transactionService.exchangeData.selectedPrimaryTokenIndex = 0
		DependencyManager.shared.transactionService.exchangeData.selectedSecondaryTokenIndex = 1
		
		let viewModel = ExchangeConfirmViewModel()
		let maxValueString = ExchangeViewModel().maxValue()
		let maxValueBalance = DependencyManager.shared.tokens[0].balance
		XCTAssert(maxValueString == maxValueBalance.normalisedRepresentation, "\(maxValueString) != \(maxValueBalance.normalisedRepresentation)")
		
		DependencyManager.shared.transactionService.updateAmountToExchange(amount: maxValueBalance)
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(viewModel.tokenToSendString == "2,023.232509 XTZ", viewModel.tokenToSendString)
			XCTAssert(viewModel.tokenToSendCurrencyString == "€3,884.60", viewModel.tokenToSendCurrencyString)
			XCTAssert(viewModel.tokenToReceiveString == "0.00164056 tzBTC", viewModel.tokenToReceiveString)
			XCTAssert(viewModel.tokenToReceiveCurrencyString == "€3,091.03", viewModel.tokenToReceiveCurrencyString)
			XCTAssert(viewModel.liquidityFeeString == "6.069697 XTZ", viewModel.liquidityFeeString)
			XCTAssert(viewModel.liquidityFeeCurrencyString == "€11.65", viewModel.liquidityFeeCurrencyString)
			XCTAssert(viewModel.networkFeeString == "0.001296 XTZ", viewModel.networkFeeString)
			XCTAssert(viewModel.networkFeeCurrencyString == "€0.00", viewModel.networkFeeCurrencyString)
			XCTAssert(viewModel.priceImpactString == "Price impact: 20.24%", viewModel.priceImpactString)
			XCTAssert(viewModel.conversionRateString == "1 XTZ = 0.00000081 tzBTC", viewModel.conversionRateString)
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 20, handler: nil)
	}
	
	func testXTZToTokenErrorInvalid() {
		DependencyManager.shared.transactionService.transactionType = .exchange
		DependencyManager.shared.transactionService.exchangeData.selectedPrimaryTokenIndex = 0
		DependencyManager.shared.transactionService.exchangeData.selectedSecondaryTokenIndex = 1
		DependencyManager.shared.transactionService.updateAmountToExchange(amount: XTZAmount(fromNormalisedAmount: 18))
		
		URLSessionMockErrorGenerator.shared.triggerInvalidDexterRequestError()
		
		let viewModel = ExchangeConfirmViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(response.success == false)
			XCTAssert(response.errorResponse?.errorType == .unknownError, "\(response.errorResponse?.errorType.rawValue ?? "")")
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 20, handler: nil)
	}
	
	func testTokenToXTZ() {
		DependencyManager.shared.transactionService.transactionType = .exchange
		DependencyManager.shared.transactionService.exchangeData.selectedPrimaryTokenIndex = 1
		DependencyManager.shared.transactionService.exchangeData.selectedSecondaryTokenIndex = 0
		DependencyManager.shared.transactionService.updateAmountToExchange(amount: TokenAmount(fromNormalisedAmount: 157, decimalPlaces: DependencyManager.shared.tokens[1].decimalPlaces))
		
		let viewModel = ExchangeConfirmViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(response.success == true, response.errorResponse?.description ?? "-")
			XCTAssert(viewModel.tokenToSendString == "157 tzBTC", viewModel.tokenToSendString)
			XCTAssert(viewModel.tokenToSendCurrencyString == "€295,808,962.98", viewModel.tokenToSendCurrencyString)
			XCTAssert(viewModel.tokenToReceiveString == "7,973.700346 XTZ", viewModel.tokenToReceiveString)
			XCTAssert(viewModel.tokenToReceiveCurrencyString == "€15,309.50", viewModel.tokenToReceiveCurrencyString)
			XCTAssert(viewModel.liquidityFeeString == "0.471 tzBTC", viewModel.liquidityFeeString)
			XCTAssert(viewModel.liquidityFeeCurrencyString == "€887,426.88", viewModel.liquidityFeeCurrencyString)
			XCTAssert(viewModel.networkFeeString == "0.002592 XTZ", viewModel.networkFeeString)
			XCTAssert(viewModel.networkFeeCurrencyString == "€0.00", viewModel.networkFeeCurrencyString)
			XCTAssert(viewModel.priceImpactString == "Price impact: 99.99%", viewModel.priceImpactString)
			XCTAssert(viewModel.conversionRateString == "1 tzBTC = 50.78790029 XTZ", viewModel.conversionRateString)
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 20, handler: nil)
	}
	
	func testTokenToXTZMax() {
		DependencyManager.shared.transactionService.transactionType = .exchange
		DependencyManager.shared.transactionService.exchangeData.selectedPrimaryTokenIndex = 1
		DependencyManager.shared.transactionService.exchangeData.selectedSecondaryTokenIndex = 0
		
		let viewModel = ExchangeConfirmViewModel()
		let maxValueString = ExchangeViewModel().maxValue()
		let maxValueBalance = DependencyManager.shared.tokens[1].balance
		XCTAssert(maxValueString == maxValueBalance.normalisedRepresentation, "\(maxValueString) != \(maxValueBalance.normalisedRepresentation)")
		
		DependencyManager.shared.transactionService.updateAmountToExchange(amount: maxValueBalance)
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(response.success == true, response.errorResponse?.description ?? "-")
			XCTAssert(viewModel.tokenToSendString == "0.00812591 tzBTC", viewModel.tokenToSendString)
			XCTAssert(viewModel.tokenToSendCurrencyString == "€15,310.29", viewModel.tokenToSendCurrencyString)
			XCTAssert(viewModel.tokenToReceiveString == "3,981.067572 XTZ", viewModel.tokenToReceiveString)
			XCTAssert(viewModel.tokenToReceiveCurrencyString == "€7,643.64", viewModel.tokenToReceiveCurrencyString)
			XCTAssert(viewModel.liquidityFeeString == "0.00002437 tzBTC", viewModel.liquidityFeeString)
			XCTAssert(viewModel.liquidityFeeCurrencyString == "€45.91", viewModel.liquidityFeeCurrencyString)
			XCTAssert(viewModel.networkFeeString == "0.002592 XTZ", viewModel.networkFeeString)
			XCTAssert(viewModel.networkFeeCurrencyString == "€0.00", viewModel.networkFeeCurrencyString)
			XCTAssert(viewModel.priceImpactString == "Price impact: 50.0%", viewModel.priceImpactString)
			XCTAssert(viewModel.conversionRateString == "1 tzBTC = 489922.67598337 XTZ", viewModel.conversionRateString)
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 20, handler: nil)
	}
	
	func testTokenToXTZErrorInvalid() {
		DependencyManager.shared.transactionService.transactionType = .exchange
		DependencyManager.shared.transactionService.exchangeData.selectedPrimaryTokenIndex = 0
		DependencyManager.shared.transactionService.exchangeData.selectedSecondaryTokenIndex = 1
		DependencyManager.shared.transactionService.updateAmountToExchange(amount: TokenAmount(fromNormalisedAmount: 157, decimalPlaces: DependencyManager.shared.tokens[0].decimalPlaces))
		
		URLSessionMockErrorGenerator.shared.triggerInvalidDexterRequestError()
		
		let viewModel = ExchangeConfirmViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(response.success == false)
			XCTAssert(response.errorResponse?.errorType == .unknownError, "\(response.errorResponse?.errorType.rawValue ?? "")")
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 20, handler: nil)
	}
}
