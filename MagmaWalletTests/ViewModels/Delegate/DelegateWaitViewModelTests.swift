//
//  DelegateWaitViewModelTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 18/06/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
@testable import MagmaWallet

class DelegateWaitViewModelTests: XCTestCase {
	
	// MARK: - Test Setup
	
    override func setUp() {
		URLSessionMockErrorGenerator.shared.clear()
    }

    override func tearDown() {
		
    }
	
	
	
	// MARK: - Test cases
	
	func testViewModelSuccess() {
		
		DependencyManager.shared.transactionService.delegateData.selectedBakerAddress = DependencyManager.shared.testnetBakers[0]
		
		let viewModel = DelegateWaitViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(response.success)
			XCTAssert(DependencyManager.shared.transactionService.delegateData.inProgressOpHash == MockConstants.operationHash)
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 20, handler: nil)
	}
	
	func testViewModelFailure() {
		
		DependencyManager.shared.transactionService.delegateData.selectedBakerAddress = DependencyManager.shared.testnetBakers[0]
		
		URLSessionMockErrorGenerator.shared.triggerBalanceTooLowError()
		
		let viewModel = DelegateWaitViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(response.success == false)
			XCTAssert(response.errorResponse?.errorType == .insufficientFundsDelegation, "\(response.errorResponse?.errorType.rawValue ?? "")")
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 20, handler: nil)
	}
}
