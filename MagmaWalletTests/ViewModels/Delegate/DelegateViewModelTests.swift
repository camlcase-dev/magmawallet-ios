//
//  DelegateViewModelTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 18/06/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
@testable import MagmaWallet

class DelegateViewModelTests: XCTestCase {
	
	// MARK: - Test Setup
	
    override func setUp() {
		URLSessionMockErrorGenerator.shared.clear()
    }

    override func tearDown() {
		
    }
	
	
	
	// MARK: - Test cases
	
	func testViewModelEmpty() {
		MockSharedHelpers.deleteWalletAndBalances()
		DependencyManager.shared.accountService.clearDelegate()
		
		let viewModel = DelegateViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(viewModel.selectedBakerName == nil, viewModel.selectedBakerName ?? "-")
			XCTAssert(viewModel.selectedBakerAddress == nil, viewModel.selectedBakerAddress ?? "-")
			XCTAssert(viewModel.infoString == "Delegating a baker will incur a network fee of approximately 0.001257 XTZ")
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 3, handler: nil)
	}
	
	func testViewModelWithDelegate() {
		MockSharedHelpers.createWalletWithEverything()
		
		let viewModel = DelegateViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(viewModel.selectedBakerName == "Baking Team", "\(viewModel.selectedBakerName ?? "")")
			XCTAssert(viewModel.selectedBakerAddress == nil)
			XCTAssert(viewModel.infoString == "Delegating a baker will incur a network fee of approximately 0.001257 XTZ")
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 3, handler: nil)
	}
}
