//
//  BakerListViewModelTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 18/06/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
@testable import MagmaWallet

class BakerListViewModelTests: XCTestCase {
	
	// MARK: - Test Setup
	
    override func setUp() {
		URLSessionMockErrorGenerator.shared.clear()
    }

    override func tearDown() {
		
    }
	
	
	
	// MARK: - Test cases
	
	func testList() {
		DependencyManager.shared.tezosBakerService.clearInMemoryBakers()
		DependencyManager.shared.tezosBakerService.deleteBakersFromDisk()
		MockSharedHelpers.updateDelegate()
		MockSharedHelpers.fetchBakers()
		
		let viewModel = BakerListViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(viewModel.tableViewData.count == 1)
			XCTAssert(viewModel.tableViewData[0].cellModels.count == DependencyManager.shared.testnetBakers.count)
			
			for (index, baker) in viewModel.tableViewData[0].cellModels.enumerated() {
				if index == 0 {
					let cellModel = baker as? BakerCellSelectedModel
					XCTAssert(cellModel?.bakerAddress == DependencyManager.shared.testnetBakers[index])
					
				} else {
					let cellModel = baker as? BakerCellModel
					XCTAssert(cellModel?.bakerAddress == DependencyManager.shared.testnetBakers[index])
				}
			}
			
			let firstBaker = viewModel.tableViewData[0].cellModels[0] as? BakerCellSelectedModel
			XCTAssert(firstBaker?.bakerName == "Baking Team", firstBaker?.bakerName ?? "-")
			XCTAssert(firstBaker?.imageCacheKey == "https://services.tzkt.io/v1/logos/baking_team.png", firstBaker?.imageCacheKey ?? "-")
			
			let fifthBaker = viewModel.tableViewData[0].cellModels[4] as? BakerCellModel
			XCTAssert(fifthBaker?.bakerName == "TzNode", fifthBaker?.bakerName ?? "-")
			XCTAssert(fifthBaker?.imageCacheKey == "https://services.tzkt.io/v1/logos/tznode.png", fifthBaker?.imageCacheKey ?? "-")
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 3, handler: nil)
	}
}
