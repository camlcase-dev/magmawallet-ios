//
//  SettingsViewModelTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 18/06/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
@testable import MagmaWallet

class SettingsViewModelTests: XCTestCase {
	
	// MARK: - Test Setup
	
    override func setUp() {
		URLSessionMockErrorGenerator.shared.clear()
    }

    override func tearDown() {
		
    }
	
	
	
	// MARK: - Test cases
	
	func testViewModelSuccess() {
		MockSharedHelpers.createWalletWithEverything()
		
		let viewModel = SettingsViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(viewModel.tableViewData.count == 4)
			
			XCTAssert(viewModel.tableViewData[0].cellModels.count == 1)
			let cell1 = viewModel.tableViewData[0].cellModels[0] as? SettingTitleSubTitleCellModel
			XCTAssert(cell1?.cellId == SettingTitleSubTitleCellModel.staticIdentifier)
			XCTAssert(cell1?.title == "set_baker".localized)
			XCTAssert(cell1?.subTitle == "Baking Team", "\(cell1?.subTitle ?? "")")
			XCTAssert(cell1?.tappable == true)
			
			
			XCTAssert(viewModel.tableViewData[1].cellModels.count == 3)
			let cell2 = viewModel.tableViewData[1].cellModels[0] as? SettingToggleCellModel
			XCTAssert(cell2?.cellId == SettingToggleCellModel.staticIdentifier)
			XCTAssert(cell2?.delegate != nil)
			XCTAssert((cell2?.title.contains("Face"))! || (cell2?.title.contains("Touch"))!)
			
			let cell3 = viewModel.tableViewData[1].cellModels[1] as? SettingTitleCellModel
			XCTAssert(cell3?.cellId == SettingTitleCellModel.staticIdentifier)
			XCTAssert(cell3?.title == "set_edit_pin".localized)
			
			let cell4 = viewModel.tableViewData[1].cellModels[2] as? SettingTitleCellModel
			XCTAssert(cell4?.cellId == SettingTitleCellModel.staticIdentifier)
			XCTAssert(cell4?.title == "set_recovery".localized)
			
			
			XCTAssert(viewModel.tableViewData[2].cellModels.count == 6)
			let cell5 = viewModel.tableViewData[2].cellModels[0] as? SettingTitleImageCellModel
			XCTAssert(cell5?.cellId == SettingTitleImageCellModel.staticIdentifier)
			XCTAssert(cell5?.title == "set_share_feedback".localized)
			
			let cell6 = viewModel.tableViewData[2].cellModels[1] as? SettingTitleImageCellModel
			XCTAssert(cell6?.cellId == SettingTitleImageCellModel.staticIdentifier)
			XCTAssert(cell6?.title == "set_about".localized)
			
			let cell7 = viewModel.tableViewData[2].cellModels[2] as? SettingTitleImageCellModel
			XCTAssert(cell7?.cellId == SettingTitleImageCellModel.staticIdentifier)
			XCTAssert(cell7?.title == "set_privacy".localized)
			
			let cell8 = viewModel.tableViewData[2].cellModels[3] as? SettingTitleImageCellModel
			XCTAssert(cell8?.cellId == SettingTitleImageCellModel.staticIdentifier)
			XCTAssert(cell8?.title == "set_terms".localized)
			
			let cell9 = viewModel.tableViewData[2].cellModels[4] as? SettingTitleImageCellModel
			XCTAssert(cell9?.cellId == SettingTitleImageCellModel.staticIdentifier)
			XCTAssert(cell9?.title == "set_newsletter".localized)
			
			let cell10 = viewModel.tableViewData[2].cellModels[5] as? SettingTitleCellModel
			XCTAssert(cell10?.cellId == SettingTitleCellModel.staticIdentifier)
			XCTAssert(cell10?.title == "set_attributions_title".localized)
			
			XCTAssert(viewModel.tableViewData[3].cellModels.count == 3)
			let cell11 = viewModel.tableViewData[3].cellModels[0] as? SettingTitleSubTitleCellModel
			XCTAssert(cell11?.cellId == SettingTitleSubTitleCellModel.staticIdentifier)
			XCTAssert(cell11?.title == "set_currency".localized)
			XCTAssert(cell11?.subTitle == "EUR", "\(cell11?.subTitle ?? "")")
			
			let cell13 = viewModel.tableViewData[3].cellModels[1] as? SettingTitleSubTitleCellModel
			XCTAssert(cell13?.cellId == SettingTitleSubTitleCellModel.staticIdentifier)
			
			let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
			let buildNumber = Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? ""
			XCTAssert(cell13?.title == "set_version".localized("\(appVersion).\(buildNumber)"))
			XCTAssert(cell13?.subTitle == "Testnet")
			XCTAssert(cell13?.tappable == false)
			
			let cell14 = viewModel.tableViewData[3].cellModels[2] as? SettingUnpairCellModel
			XCTAssert(cell14?.cellId == SettingUnpairCellModel.staticIdentifier)
			
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 3, handler: nil)
	}
	
	func testViewModelEmpty() {
		MockSharedHelpers.deleteWalletAndBalances()
		MockSharedHelpers.createWallet()
		
		let viewModel = SettingsViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(viewModel.tableViewData.count == 4)
			
			XCTAssert(viewModel.tableViewData[0].cellModels.count == 1)
			let cell1 = viewModel.tableViewData[0].cellModels[0] as? SettingTitleSubTitleCellModel
			XCTAssert(cell1?.cellId == SettingTitleSubTitleCellModel.staticIdentifier)
			XCTAssert(cell1?.title == "set_baker".localized)
			XCTAssert(cell1?.tappable == true)
			
			
			XCTAssert(viewModel.tableViewData[1].cellModels.count == 3)
			let cell2 = viewModel.tableViewData[1].cellModels[0] as? SettingToggleCellModel
			XCTAssert(cell2?.cellId == SettingToggleCellModel.staticIdentifier)
			XCTAssert(cell2?.delegate != nil)
			XCTAssert((cell2?.title.contains("Face"))! || (cell2?.title.contains("Touch"))!)
			
			let cell3 = viewModel.tableViewData[1].cellModels[1] as? SettingTitleCellModel
			XCTAssert(cell3?.cellId == SettingTitleCellModel.staticIdentifier)
			XCTAssert(cell3?.title == "set_edit_pin".localized)
			
			let cell4 = viewModel.tableViewData[1].cellModels[2] as? SettingTitleCellModel
			XCTAssert(cell4?.cellId == SettingTitleCellModel.staticIdentifier)
			XCTAssert(cell4?.title == "set_recovery".localized)
			
			
			XCTAssert(viewModel.tableViewData[2].cellModels.count == 6)
			let cell5 = viewModel.tableViewData[2].cellModels[0] as? SettingTitleImageCellModel
			XCTAssert(cell5?.cellId == SettingTitleImageCellModel.staticIdentifier)
			XCTAssert(cell5?.title == "set_share_feedback".localized)
			
			let cell6 = viewModel.tableViewData[2].cellModels[1] as? SettingTitleImageCellModel
			XCTAssert(cell6?.cellId == SettingTitleImageCellModel.staticIdentifier)
			XCTAssert(cell6?.title == "set_about".localized)
			
			let cell7 = viewModel.tableViewData[2].cellModels[2] as? SettingTitleImageCellModel
			XCTAssert(cell7?.cellId == SettingTitleImageCellModel.staticIdentifier)
			XCTAssert(cell7?.title == "set_privacy".localized)
			
			let cell8 = viewModel.tableViewData[2].cellModels[3] as? SettingTitleImageCellModel
			XCTAssert(cell8?.cellId == SettingTitleImageCellModel.staticIdentifier)
			XCTAssert(cell8?.title == "set_terms".localized)
			
			let cell9 = viewModel.tableViewData[2].cellModels[4] as? SettingTitleImageCellModel
			XCTAssert(cell9?.cellId == SettingTitleImageCellModel.staticIdentifier)
			XCTAssert(cell9?.title == "set_newsletter".localized)
			
			let cell10 = viewModel.tableViewData[2].cellModels[5] as? SettingTitleCellModel
			XCTAssert(cell10?.cellId == SettingTitleCellModel.staticIdentifier)
			XCTAssert(cell10?.title == "set_attributions_title".localized)
			
			
			XCTAssert(viewModel.tableViewData[3].cellModels.count == 3)
			let cell11 = viewModel.tableViewData[3].cellModels[0] as? SettingTitleSubTitleCellModel
			XCTAssert(cell11?.cellId == SettingTitleSubTitleCellModel.staticIdentifier)
			XCTAssert(cell11?.title == "set_currency".localized)
			
			let cell13 = viewModel.tableViewData[3].cellModels[1] as? SettingTitleSubTitleCellModel
			XCTAssert(cell13?.cellId == SettingTitleSubTitleCellModel.staticIdentifier)
			
			let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
			let buildNumber = Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? ""
			XCTAssert(cell13?.title == "set_version".localized("\(appVersion).\(buildNumber)"))
			XCTAssert(cell13?.subTitle == "Testnet")
			XCTAssert(cell13?.tappable == false)
			
			let cell14 = viewModel.tableViewData[3].cellModels[2] as? SettingUnpairCellModel
			XCTAssert(cell14?.cellId == SettingUnpairCellModel.staticIdentifier)
			
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 3, handler: nil)
	}
	
	func testToggleBiometric() {
		let viewModel = SettingsViewModel()
		let availableOption = DependencyManager.shared.accountService.availableBiometricType()
		
		viewModel.toggleChanged(cellTitle: "", selected: true)
		XCTAssert(DependencyManager.shared.accountService.getBiometricChoice() == availableOption)
		
		viewModel.toggleChanged(cellTitle: "", selected: false)
		XCTAssert(DependencyManager.shared.accountService.getBiometricChoice() == .none)
	}
}
