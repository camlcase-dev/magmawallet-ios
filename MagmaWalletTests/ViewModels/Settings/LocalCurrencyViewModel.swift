//
//  LocalCurrencyViewModel.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 24/03/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import XCTest
@testable import MagmaWallet

class LocalCurrencyViewModelTests: XCTestCase {
	
	let viewModel = LocalCurrencyViewModel()
	
	// MARK: - Test Setup
	
	override func setUp() {
		URLSessionMockErrorGenerator.shared.clear()
	}
	
	override func tearDown() {
		
	}
	
	
	
	// MARK: - Test cases
	
	func testBalances() {
		MockSharedHelpers.createWalletWithEverything()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { [weak self] (response) in
			
			XCTAssert(self?.viewModel.tableViewData.count == 2, "\(self?.viewModel.tableViewData.count ?? -1)")
			XCTAssert(self?.viewModel.tableViewData[0].cellModels.count == 8, "\(self?.viewModel.tableViewData[0].cellModels.count ?? -1)")
			XCTAssert(self?.viewModel.tableViewData[1].cellModels.count != 0, "\(self?.viewModel.tableViewData[1].cellModels.count ?? -1)")
			
			let firstCell = self?.viewModel.tableViewData[0].cellModels[0] as? CurrencyChoiceCellModel
			XCTAssert(firstCell?.currencyCode == "USD", firstCell?.currencyCode ?? "")
			XCTAssert(firstCell?.longName == "US Dollar", firstCell?.longName ?? "")
			
			let secondCell = self?.viewModel.tableViewData[0].cellModels[1] as? CurrencyChoiceCellModel
			XCTAssert(secondCell?.currencyCode == "EUR", secondCell?.currencyCode ?? "")
			XCTAssert(secondCell?.longName == "Euro", secondCell?.longName ?? "")
			
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 3, handler: nil)
	}
	
	func testChangeCurrency() {
		testBalances()
		
		let expectation = self.expectation(description: "testChangeCurrency 1")
		
		viewModel.changeCurrency(indexPath: IndexPath(row: 0, section: 0)) {
			XCTAssert(DependencyManager.shared.tokenPriceService.selectedCurrency == "usd", DependencyManager.shared.tokenPriceService.selectedCurrency ?? "")
			expectation.fulfill()
		}
		waitForExpectations(timeout: 3, handler: nil)
		
		
		let expectation2 = self.expectation(description: "testChangeCurrency 2")
		
		viewModel.changeCurrency(indexPath: IndexPath(row: 1, section: 0)) {
			XCTAssert(DependencyManager.shared.tokenPriceService.selectedCurrency == "eur", DependencyManager.shared.tokenPriceService.selectedCurrency ?? "")
			expectation2.fulfill()
		}
		waitForExpectations(timeout: 3, handler: nil)
	}
}
