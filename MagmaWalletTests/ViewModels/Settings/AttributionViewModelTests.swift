//
//  AttributionViewModelTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 24/03/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import XCTest
@testable import MagmaWallet

class AttributionViewModelTests: XCTestCase {
	
	// MARK: - Test Setup
	
	override func setUp() {
		URLSessionMockErrorGenerator.shared.clear()
	}
	
	override func tearDown() {
		
	}
	
	
	
	// MARK: - Test cases
	
	func testBalances() {
		MockSharedHelpers.createWalletWithEverything()
		
		let viewModel = AttributionViewModel()
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(viewModel.tableViewData.count == 1, "\(viewModel.tableViewData.count)")
			XCTAssert(viewModel.tableViewData[0].cellModels.count == 5, "\(viewModel.tableViewData[0].cellModels.count)")
			
			let cell1 = viewModel.tableViewData[0].cellModels[0] as? AttributionCellModel
			XCTAssert(cell1?.title == "Baking Bad", cell1?.title ?? "")
			XCTAssert(cell1?.subtitle == "Baker data", cell1?.subtitle ?? "")
			XCTAssert(cell1?.urlSnippet == "baking-bad.org", cell1?.urlSnippet ?? "")
			XCTAssert(cell1?.url.absoluteString == "https://baking-bad.org/", cell1?.url.absoluteString ?? "")
			
			let cell2 = viewModel.tableViewData[0].cellModels[1] as? AttributionCellModel
			XCTAssert(cell2?.title == "Better Call Dev", cell2?.title ?? "")
			XCTAssert(cell2?.subtitle == "Operations data", cell2?.subtitle ?? "")
			XCTAssert(cell2?.urlSnippet == "better-call.dev", cell2?.urlSnippet ?? "")
			XCTAssert(cell2?.url.absoluteString == "https://better-call.dev/", cell2?.url.absoluteString ?? "")
			
			let cell3 = viewModel.tableViewData[0].cellModels[2] as? AttributionCellModel
			XCTAssert(cell3?.title == "CoinGecko", cell3?.title ?? "")
			XCTAssert(cell3?.subtitle == "Pricing data", cell3?.subtitle ?? "")
			XCTAssert(cell3?.urlSnippet == "coingecko.com", cell3?.urlSnippet ?? "")
			XCTAssert(cell3?.url.absoluteString == "https://coingecko.com/", cell3?.url.absoluteString ?? "")
			
			let cell4 = viewModel.tableViewData[0].cellModels[3] as? AttributionCellModel
			XCTAssert(cell4?.title == "Lokalise", cell4?.title ?? "")
			XCTAssert(cell4?.subtitle == "String localization", cell4?.subtitle ?? "")
			XCTAssert(cell4?.urlSnippet == "lokalise.com", cell4?.urlSnippet ?? "")
			XCTAssert(cell4?.url.absoluteString == "https://lokalise.com/", cell4?.url.absoluteString ?? "")
			
			let cell5 = viewModel.tableViewData[0].cellModels[4] as? AttributionCellModel
			XCTAssert(cell5?.title == "TzKT", cell5?.title ?? "")
			XCTAssert(cell5?.subtitle == "Operations data", cell5?.subtitle ?? "")
			XCTAssert(cell5?.urlSnippet == "tzkt.io", cell5?.urlSnippet ?? "")
			XCTAssert(cell5?.url.absoluteString == "https://tzkt.io/", cell5?.url.absoluteString ?? "")
			
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 3, handler: nil)
	}
}
