//
//  TokenDetailsHeaderViewModelTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 18/06/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
@testable import MagmaWallet

class TokenDetailsHeaderViewModelTests: XCTestCase {
	
	// MARK: - Test Setup
	
    override func setUp() {
		URLSessionMockErrorGenerator.shared.clear()
    }

    override func tearDown() {
		
    }
	
	
	
	// MARK: - Test cases
	
	func testViewModelXTZ() {
		MockSharedHelpers.createWalletWithEverything()
		
		let viewModel = TokenDetailsHeaderViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update(withInput: [TokenDetailsHeaderViewModel.TokenDetailsHeaderViewModelInputKeys.token.rawValue: DependencyManager.shared.tokens[0]]) { (response) in
			
			XCTAssert(viewModel.token?.symbol == DependencyManager.shared.tokens[0].symbol)
			
			XCTAssert(viewModel.tokenAmountString == MockConstants.walletTezBalanceFormattedWithoutSymbol, "\(viewModel.tokenAmountString)")
			XCTAssert(viewModel.tokenConversionRateString == MockConstants.walletTezConversionFormatted, "\(viewModel.tokenConversionRateString)")
			XCTAssert(viewModel.tokenCurrencyAmountString == MockConstants.walletTezCurrencyFormatted)
			
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 3, handler: nil)
	}
	
	func testViewModelToken() {
		MockSharedHelpers.createWalletWithEverything()
		
		let viewModel = TokenDetailsHeaderViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update(withInput: [TokenDetailsHeaderViewModel.TokenDetailsHeaderViewModelInputKeys.token.rawValue: DependencyManager.shared.tokens[1]]) { (response) in
			
			XCTAssert(viewModel.token?.symbol == DependencyManager.shared.tokens[1].symbol)
			
			XCTAssert(viewModel.tokenAmountString == MockConstants.walletTokenDecimal8BalanceFormatted, "\(viewModel.tokenAmountString)")
			XCTAssert(viewModel.tokenSymbolString == DependencyManager.shared.tokens[1].symbol, DependencyManager.shared.tokens[1].symbol)
			XCTAssert(viewModel.tokenConversionRateString == MockConstants.walletTokenDecimal8ConversionFormatted, "\(viewModel.tokenConversionRateString)")
			XCTAssert(viewModel.tokenCurrencyAmountString == MockConstants.walletTokenDecimal8CurrencyFormatted, "\(viewModel.tokenCurrencyAmountString)")
			
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 3, handler: nil)
	}
	
	func testViewModelNoInput() {
		let viewModel = TokenDetailsHeaderViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(response.success == false)
			
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 3, handler: nil)
	}
	
	func testSetupExchangeDetailsXTZ() {
		let viewModel = TokenDetailsHeaderViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update(withInput: [TokenDetailsHeaderViewModel.TokenDetailsHeaderViewModelInputKeys.token.rawValue: DependencyManager.shared.tokens[0]]) { (response) in
			
			XCTAssert(viewModel.token?.symbol == DependencyManager.shared.tokens[0].symbol)
			viewModel.setupExchangeDetails()
			
			let exchangeData = DependencyManager.shared.transactionService.exchangeData
			XCTAssert(DependencyManager.shared.transactionService.transactionType == .exchange)
			XCTAssert(exchangeData.selectedPrimaryTokenIndex == 0, "primary is: \(exchangeData.selectedPrimaryTokenIndex)")
			XCTAssert(exchangeData.selectedSecondaryTokenIndex == 1, "secondary is: \(exchangeData.selectedPrimaryTokenIndex)")
			
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 3, handler: nil)
	}
	
	func testSetupExchangeDetailsToken() {
		let viewModel = TokenDetailsHeaderViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update(withInput: [TokenDetailsHeaderViewModel.TokenDetailsHeaderViewModelInputKeys.token.rawValue: DependencyManager.shared.tokens[1]]) { (response) in
			
			XCTAssert(viewModel.token?.symbol == DependencyManager.shared.tokens[1].symbol)
			viewModel.setupExchangeDetails()
			
			let exchangeData = DependencyManager.shared.transactionService.exchangeData
			XCTAssert(DependencyManager.shared.transactionService.transactionType == .exchange)
			XCTAssert(exchangeData.selectedPrimaryTokenIndex == 1, "primary is: \(exchangeData.selectedPrimaryTokenIndex)")
			XCTAssert(exchangeData.selectedSecondaryTokenIndex == 0, "secondary is: \(exchangeData.selectedPrimaryTokenIndex)")
			
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 3, handler: nil)
	}
}
