//
//  HomeViewModelTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 17/02/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
@testable import MagmaWallet

class HomeViewModelTests: XCTestCase {
	
	// MARK: - Test Setup
	
    override func setUp() {
		URLSessionMockErrorGenerator.shared.clear()
    }

    override func tearDown() {
		
    }
	
	
	
	// MARK: - Test cases
	
	func testHomeViewModelSuccess() {
		MockSharedHelpers.createWalletWithEverything()
		
		let viewModel = HomeViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(viewModel.isWalletEmpty == false)
			
			XCTAssert(viewModel.headerData.totalBalance == MockConstants.totalWalletBalanceFormatted, "\(viewModel.headerData.totalBalance) != \(MockConstants.totalWalletBalanceFormatted)")
			XCTAssert(viewModel.headerData.tezAmount == MockConstants.walletTezBalanceFormatted, "\(viewModel.headerData.tezAmount) != \(MockConstants.walletTezBalanceFormatted)")
			XCTAssert(viewModel.headerData.tezConversionString == MockConstants.walletTezConversionFormatted, "\(viewModel.headerData.tezConversionString) != \(MockConstants.walletTezConversionFormatted)")
			XCTAssert(viewModel.headerData.tezCurrencyAmount == MockConstants.walletTezCurrencyFormatted, "\(viewModel.headerData.tezCurrencyAmount) != \(MockConstants.walletTezCurrencyFormatted)")
			
			XCTAssert(viewModel.tableViewData.count == 1)
			XCTAssert(viewModel.tableViewData[0].cellModels.count == 2)
			
			for (index, cellModel) in viewModel.tableViewData[0].cellModels.enumerated() {
				XCTAssert(cellModel is TokenAmountAndValueCellModel)
				XCTAssert(cellModel.cellId == TokenAmountAndValueCellModel.staticIdentifier)
				
				let model = cellModel as? TokenAmountAndValueCellModel
				XCTAssert(cellModel is TokenAmountAndValueCellModel, "\(cellModel.self)")
				
				
				let token = DependencyManager.shared.tokens[index+1]
				XCTAssert(model?.symbol == token.symbol)
				
				if token.decimalPlaces == 6 {
					XCTAssert(model?.amount == MockConstants.walletTokenDecimal6BalanceFormatted, "\(model?.amount ?? "")")
					XCTAssert(model?.currencyString == MockConstants.walletTokenDecimal6CurrencyFormatted, "\(model?.currencyString ?? "")")
								   
				} else if token.decimalPlaces == 8 {
					XCTAssert(model?.amount == MockConstants.walletTokenDecimal8BalanceFormatted, "\(model?.amount ?? "")")
					XCTAssert(model?.currencyString == MockConstants.walletTokenDecimal8CurrencyFormatted, "\(model?.currencyString ?? "")")
				}
			}
			
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 3, handler: nil)
	}
	
	func testHomeViewModelEmptySuccess() {
		MockSharedHelpers.deleteWalletAndBalances()
		
		let viewModel = HomeViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(viewModel.isWalletEmpty == true)
			
			XCTAssert(viewModel.headerData.totalBalance == "€0.00", "\(viewModel.headerData.totalBalance) != €0.00")
			XCTAssert(viewModel.headerData.tezAmount == "0 XTZ", "\(viewModel.headerData.tezAmount) != 0 XTZ")
			XCTAssert(viewModel.headerData.tezCurrencyAmount == "€0.00", "\(viewModel.headerData.tezCurrencyAmount) != €0.00")
			
			XCTAssert(viewModel.tableViewData.count == 1)
			XCTAssert(viewModel.tableViewData[0].cellModels.count == 1)
			XCTAssert(viewModel.tableViewData[0].cellModels[0] is EmptyWalletCellModel)
			
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 3, handler: nil)
	}
	
	func testLoadingCell() {
		let cellData = HomeViewModel().loadingCell()
		
		XCTAssert(cellData.count == 1)
		XCTAssert(cellData[0].cellModels.count == 1)
		XCTAssert(cellData[0].cellModels[0] is LoadingCellModel)
		
		let cellModel = cellData[0].cellModels[0] as! LoadingCellModel
		XCTAssert(cellModel.estimatedRowHeight > 0)
		XCTAssert(cellModel.loadingMessage == "")
	}
	
	func testErrorCell() {
		let cellData = HomeViewModel().errorCell()
		
		XCTAssert(cellData.count == 1)
		XCTAssert(cellData[0].cellModels.count == 1)
		XCTAssert(cellData[0].cellModels[0] is ErrorCellModel)
		
		let cellModel = cellData[0].cellModels[0] as! ErrorCellModel
		XCTAssert(cellModel.estimatedRowHeight > 0)
		XCTAssert(cellModel.errorMessage == "error_wallet_balance".localized)
	}
	
	func testTezHeaderCellModelData() {
		MockSharedHelpers.createWalletWithEverything()
		
		let viewModel = HomeViewModel()
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			let cellModel = viewModel.tezHeaderDataAsCellModel()
			
			XCTAssert(cellModel.amount == MockConstants.walletTezBalanceFormatted, "\(cellModel.amount) != \(MockConstants.walletTezBalanceFormatted)")
			XCTAssert(cellModel.symbol == "XTZ")
			XCTAssert(cellModel.conversionString == MockConstants.walletTezConversionFormatted, "\(cellModel.conversionString) != \(MockConstants.walletTezConversionFormatted)")
			XCTAssert(cellModel.currencyString == MockConstants.walletTezCurrencyFormatted, "\(cellModel.currencyString) != \(MockConstants.walletTezCurrencyFormatted)")
			XCTAssert(cellModel.token.symbol == DependencyManager.shared.tokens[0].symbol, "\(cellModel.token.symbol)")
			
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 3, handler: nil)
	}
}
