//
//  ActivityViewModelTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 18/06/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
import camlKit
@testable import MagmaWallet

class ActivityViewModelTests: XCTestCase {
	
	// MARK: - Test Setup
	
	// Only once for the entire class
	override class func setUp() {
		URLSessionMockErrorGenerator.shared.clear()
		ContactServiceTests().processContacts()
		let contactResult = DependencyManager.shared.contactService.addNewContact(name: "Testy McTestface", address: "tz1RKLWbGm7T4mnxDZHWazkbnvaryKsxxZTF")
		ContactServiceTests().processContacts()
		
		XCTAssert(contactResult, "Failed to add contact")
	}
	
    override func setUp() {
    }

    override func tearDown() {
		
    }
	
	
	// MARK: - Test cases
	
	func testActivity() {
		TezosBakerServiceTests().setUp()
		TezosBakerServiceTests().testBakersProcessed()
		MockSharedHelpers.updateEverything()
		
		let viewModel = ActivityViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(viewModel.tableViewData.count == MockConstants.transactionHistoryParsed.count)
			
			for (index, section) in viewModel.tableViewData.enumerated() {
				
				// Cheack each section has the right number of transactions
				let correctCount = (section.cellModels.count == MockConstants.transactionHistoryParsed[index].cellModels.count)
				XCTAssert(correctCount, "\(index) count: \(section.cellModels.count), does not match \(MockConstants.transactionHistoryParsed[index].cellModels.count)")
				XCTAssert(section.header == MockConstants.transactionHistoryParsed[index].header, "\(section.header ?? "") != \(MockConstants.transactionHistoryParsed[index].header ?? "")")
				
				// Check each transaction has been parsed correctly
				for (innerIndex, cellModel) in section.cellModels.enumerated() {
					
					if let model = cellModel as? TransactionHistoryCellModel {
						let mockModel = MockConstants.transactionHistoryParsed[index].cellModels[innerIndex] as! TransactionHistoryCellModel
						if model != mockModel {
							print("\nmodel outer: \(index) - inner: \(innerIndex): \(model)")
							print("mockModel outer: \(index) - inner: \(innerIndex): \(mockModel)")
						}
						XCTAssert(model == mockModel, "outerIndex: \(index), innerIndex: \(innerIndex) not matching")
						
					} else if let model = cellModel as? TransactionHistoryFailedCellModel {
						let mockModel = MockConstants.transactionHistoryParsed[index].cellModels[innerIndex] as! TransactionHistoryFailedCellModel
						if model != mockModel {
							print("\nmodel outer: \(index) - inner: \(innerIndex): \(model)")
							print("mockModel outer: \(index) - inner: \(innerIndex): \(mockModel)")
						}
						XCTAssert(model == mockModel, "innerIndex: \(innerIndex) not matching")
						
					} else if let model = cellModel as? TransactionHistoryExchangeCellModel {
						let mockModel = MockConstants.transactionHistoryParsed[index].cellModels[innerIndex] as! TransactionHistoryExchangeCellModel
						if model != mockModel {
							print("\nmodel outer: \(index) - inner: \(innerIndex): \(model)")
							print("mockModel outer: \(index) - inner: \(innerIndex): \(mockModel)")
						}
						XCTAssert(model == mockModel, "innerIndex: \(innerIndex) not matching")
						
					} else {
						XCTFail("Unhanled type: \(cellModel)")
					}
				}
			}
			
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 3, handler: nil)
	}
	
	func testActivityFiltered() {
		TezosBakerServiceTests().setUp()
		TezosBakerServiceTests().testBakersProcessed()
		MockSharedHelpers.updateEverything()
		
		let viewModel = ActivityViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update(withInput: [ActivityViewModel.ActivityViewModelInputKeys.token.rawValue: DependencyManager.shared.tokens[0]]) { (response) in
			
			XCTAssert(viewModel.tableViewData.count == MockConstants.filteredTransactionHistoryParsed.count)
			
			for (index, section) in viewModel.tableViewData.enumerated() {
				
				// Cheack each section has the right number of transactions
				let correctCount = (section.cellModels.count == MockConstants.filteredTransactionHistoryParsed[index].cellModels.count)
				XCTAssert(correctCount, "\(index) count: \(section.cellModels.count), does not match \(MockConstants.filteredTransactionHistoryParsed[index].cellModels.count)")
				XCTAssert(section.header == MockConstants.filteredTransactionHistoryParsed[index].header, "\(section.header ?? "") != \(MockConstants.filteredTransactionHistoryParsed[index].header ?? "")")
				
				// Check each transaction has been parsed correctly
				for (innerIndex, cellModel) in section.cellModels.enumerated() {
					
					if let model = cellModel as? TransactionHistoryCellModel {
						let mockModel = MockConstants.filteredTransactionHistoryParsed[index].cellModels[innerIndex] as! TransactionHistoryCellModel
						if model != mockModel {
							print("\nmodel outer: \(index) - inner: \(innerIndex): \(model)")
							print("mockModel outer: \(index) - inner: \(innerIndex): \(mockModel)")
						}
						XCTAssert(model == mockModel, "outerIndex: \(index), innerIndex: \(innerIndex) not matching")
						
					} else if let model = cellModel as? TransactionHistoryFailedCellModel {
						let mockModel = MockConstants.filteredTransactionHistoryParsed[index].cellModels[innerIndex] as! TransactionHistoryFailedCellModel
						if model != mockModel {
							print("\nmodel outer: \(index) - inner: \(innerIndex): \(model)")
							print("mockModel outer: \(index) - inner: \(innerIndex): \(mockModel)")
						}
						XCTAssert(model == mockModel, "innerIndex: \(innerIndex) not matching")
						
					} else if let model = cellModel as? TransactionHistoryExchangeCellModel {
						let mockModel = MockConstants.filteredTransactionHistoryParsed[index].cellModels[innerIndex] as! TransactionHistoryExchangeCellModel
						if model != mockModel {
							print("\nmodel outer: \(index) - inner: \(innerIndex): \(model)")
							print("mockModel outer: \(index) - inner: \(innerIndex): \(mockModel)")
						}
						XCTAssert(model == mockModel, "innerIndex: \(innerIndex) not matching")
						
					} else {
						XCTFail("Unhabled type: \(cellModel)")
					}
				}
			}
			
			expectation.fulfill()
		}
		
		waitForExpectations(timeout: 3, handler: nil)
	}
	
	func testActivityEmpty() {
		MockSharedHelpers.createWalletWithEverything()
		MockSharedHelpers.deleteTransactionHistory()
		
		let viewModel = ActivityViewModel()
		
		let expectation = self.expectation(description: "viewModel update")
		viewModel.update() { (response) in
			
			XCTAssert(viewModel.tableViewData.count == 1)
			XCTAssert(viewModel.tableViewData[0].cellModels.count == 1)
			XCTAssert(viewModel.tableViewData[0].cellModels[0] is EmptyActivityCellModel)
			
			expectation.fulfill()
		}
			
		waitForExpectations(timeout: 3, handler: nil)
	}
	
	func testLoadingCell() {
		let cellData = ActivityViewModel().loadingCell()
		
		XCTAssert(cellData.count == 1)
		XCTAssert(cellData[0].cellModels.count == 1)
		XCTAssert(cellData[0].cellModels[0] is LoadingCellModel)
		
		let cellModel = cellData[0].cellModels[0] as! LoadingCellModel
		XCTAssert(cellModel.estimatedRowHeight > 0)
		XCTAssert(cellModel.loadingMessage == "")
	}
	
	func testErrorCell() {
		let cellData = ActivityViewModel().errorCell()
		
		XCTAssert(cellData.count == 1)
		XCTAssert(cellData[0].cellModels.count == 1)
		XCTAssert(cellData[0].cellModels[0] is ErrorCellModel)
		
		let cellModel = cellData[0].cellModels[0] as! ErrorCellModel
		XCTAssert(cellModel.estimatedRowHeight > 0)
		XCTAssert(cellModel.errorMessage == "error_wallet_balance".localized)
	}
	
	func testCreateSendTransactionModel() {
		let me = TzKTTransaction.TransactionLocation(alias: "", address: MockConstants.walletAddressHdNoPassphrase)
		let userWithoutContact = TzKTTransaction.TransactionLocation(alias: "", address: MockConstants.contactlessAddress)
		let userWithContact = TzKTTransaction.TransactionLocation(alias: "", address: MockConstants.contactAddress)
		let tokenTarget = TzKTTransaction.TransactionLocation(alias: "", address: DependencyManager.shared.tokens[1].tokenContractAddress ?? "")
		let tokenParameter = TzKTTransaction.TransactionParameter(entrypoint: "transfer", value: ["to": MockConstants.contactAddress, "from": MockConstants.walletAddressHdNoPassphrase, "value": "19000"])
		
		
		let stubTransactionXTZWithoutContact = TzKTTransaction(type: .transaction, id: 0, level: 0, timestamp: "", hash: "", counter: 0, initiater: nil, sender: me, bakerFee: XTZAmount(fromNormalisedAmount: 0.000144), storageFee: XTZAmount.zero(), allocationFee: XTZAmount.zero(), target: userWithoutContact, prevDelegate: nil, newDelegate: nil, amount: XTZAmount(fromNormalisedAmount: 17.000000), parameter: nil, status: .applied)
		stubTransactionXTZWithoutContact.augmentTransaction(withUsersAddress: MockConstants.walletAddressHdNoPassphrase, andTokens: DependencyManager.shared.tokens)
		
		let stubTransactionXTZWithContact = TzKTTransaction(type: .transaction, id: 0, level: 0, timestamp: "", hash: "", counter: 0, initiater: nil, sender: me, bakerFee: XTZAmount(fromNormalisedAmount: 0.000144), storageFee: XTZAmount.zero(), allocationFee: XTZAmount.zero(), target: userWithContact, prevDelegate: nil, newDelegate: nil, amount: XTZAmount(fromNormalisedAmount: 17.000000), parameter: nil, status: .applied)
		stubTransactionXTZWithContact.augmentTransaction(withUsersAddress: MockConstants.walletAddressHdNoPassphrase, andTokens: DependencyManager.shared.tokens)
		
		let stubTransactionToken = TzKTTransaction(type: .transaction, id: 0, level: 0, timestamp: "", hash: "", counter: 0, initiater: nil, sender: me, bakerFee: XTZAmount(fromNormalisedAmount: 0.000144), storageFee: XTZAmount.zero(), allocationFee: XTZAmount.zero(), target: tokenTarget, prevDelegate: nil, newDelegate: nil, amount: XTZAmount.zero(), parameter: tokenParameter, status: .applied)
		stubTransactionToken.augmentTransaction(withUsersAddress: MockConstants.walletAddressHdNoPassphrase, andTokens: DependencyManager.shared.tokens)
		
		let stubTransactionFailed = TzKTTransaction(type: .transaction, id: 0, level: 0, timestamp: "", hash: "", counter: 0, initiater: nil, sender: me, bakerFee: XTZAmount(fromNormalisedAmount: 0.000144), storageFee: XTZAmount.zero(), allocationFee: XTZAmount.zero(), target: userWithContact, prevDelegate: nil, newDelegate: nil, amount: XTZAmount(fromNormalisedAmount: 21.000000), parameter: nil, status: .failed)
		stubTransactionFailed.augmentTransaction(withUsersAddress: MockConstants.walletAddressHdNoPassphrase, andTokens: DependencyManager.shared.tokens)
		
		
		
		let resultXTZWithoutContact = ActivityViewModel().createSendTransactionModel(transaction: stubTransactionXTZWithoutContact) as! TransactionHistoryCellModel
		XCTAssert(resultXTZWithoutContact.address == "To: " + MockConstants.contactlessAddressFormatted, "\(resultXTZWithoutContact.address)")
		XCTAssert(resultXTZWithoutContact.fee == "Network fee: 0.000144 XTZ", "\(resultXTZWithoutContact.fee)")
		XCTAssert(resultXTZWithoutContact.amount == "-17 XTZ", "\(resultXTZWithoutContact.amount)")
		XCTAssert(resultXTZWithoutContact.type == "act_type_send".localized.uppercased(), "\(resultXTZWithoutContact.type)")
		
		let resultXTZWithContact = ActivityViewModel().createSendTransactionModel(transaction: stubTransactionXTZWithContact) as! TransactionHistoryCellModel
		XCTAssert(resultXTZWithContact.address == "To: " + MockConstants.contactName, "\(resultXTZWithContact.address)")
		XCTAssert(resultXTZWithContact.fee == "Network fee: 0.000144 XTZ", "\(resultXTZWithContact.fee)")
		XCTAssert(resultXTZWithContact.amount == "-17 XTZ", "\(resultXTZWithContact.amount)")
		XCTAssert(resultXTZWithContact.type == "act_type_send".localized.uppercased(), "\(resultXTZWithContact.type)")
		
		let resultToken = ActivityViewModel().createSendTransactionModel(transaction: stubTransactionToken) as! TransactionHistoryCellModel
		XCTAssert(resultToken.address == "To: " + MockConstants.contactName, "\(resultToken.address)")
		XCTAssert(resultToken.fee == "Network fee: 0.000144 XTZ", "\(resultToken.fee)")
		XCTAssert(resultToken.amount == "-0.00019 tzBTC", "\(resultToken.amount)")
		XCTAssert(resultToken.type == "act_type_send".localized.uppercased(), "\(resultToken.type)")
		
		let resultFailed = ActivityViewModel().createSendTransactionModel(transaction: stubTransactionFailed) as! TransactionHistoryFailedCellModel
		XCTAssert(resultFailed.address == "To: " + MockConstants.contactName, "\(resultFailed.address)")
		XCTAssert(resultFailed.fee == "Network fee: N/A", "\(resultFailed.fee)")
		XCTAssert(resultFailed.amount == "N/A", "\(resultFailed.amount)")
		XCTAssert(resultFailed.type == "act_type_failed".localized("act_type_send".localized).uppercased(), "\(resultFailed.type)")
	}
	
	func testCreateReceiveTransactionModel() {
		MockSharedHelpers.updateDelegate()
		
		let me = TzKTTransaction.TransactionLocation(alias: "", address: MockConstants.walletAddressHdNoPassphrase)
		let userWithoutContact = TzKTTransaction.TransactionLocation(alias: "", address: MockConstants.contactlessAddress)
		let userWithContact = TzKTTransaction.TransactionLocation(alias: "", address: MockConstants.contactAddress)
		let baker = TzKTTransaction.TransactionLocation(alias: "", address: DependencyManager.shared.testnetBakers[0])
		let tokenTarget = TzKTTransaction.TransactionLocation(alias: "", address: DependencyManager.shared.tokens[1].tokenContractAddress ?? "")
		let tokenParameter = TzKTTransaction.TransactionParameter(entrypoint: "transfer", value: ["to": MockConstants.walletAddressHdNoPassphrase, "from": MockConstants.contactAddress, "value": "19000"])
		
		
		let stubTransactionXTZWithoutContact = TzKTTransaction(type: .transaction, id: 0, level: 0, timestamp: "", hash: "", counter: 0, initiater: nil, sender: userWithoutContact, bakerFee: XTZAmount(fromNormalisedAmount: 0.000144), storageFee: XTZAmount.zero(), allocationFee: XTZAmount.zero(), target: me, prevDelegate: nil, newDelegate: nil, amount: XTZAmount(fromNormalisedAmount: 17.000000), parameter: nil, status: .applied)
		stubTransactionXTZWithoutContact.augmentTransaction(withUsersAddress: MockConstants.walletAddressHdNoPassphrase, andTokens: DependencyManager.shared.tokens)
		
		let stubTransactionXTZWithContact = TzKTTransaction(type: .transaction, id: 0, level: 0, timestamp: "", hash: "", counter: 0, initiater: nil, sender: userWithContact, bakerFee: XTZAmount(fromNormalisedAmount: 0.000144), storageFee: XTZAmount.zero(), allocationFee: XTZAmount.zero(), target: me, prevDelegate: nil, newDelegate: nil, amount: XTZAmount(fromNormalisedAmount: 17.000000), parameter: nil, status: .applied)
		stubTransactionXTZWithContact.augmentTransaction(withUsersAddress: MockConstants.walletAddressHdNoPassphrase, andTokens: DependencyManager.shared.tokens)
		
		let stubTransactionXTZBakingReward = TzKTTransaction(type: .transaction, id: 0, level: 0, timestamp: "", hash: "", counter: 0, initiater: nil, sender: baker, bakerFee: XTZAmount(fromNormalisedAmount: 0.000144), storageFee: XTZAmount.zero(), allocationFee: XTZAmount.zero(), target: me, prevDelegate: nil, newDelegate: nil, amount: XTZAmount(fromNormalisedAmount: 17.000000), parameter: nil, status: .applied)
		stubTransactionXTZBakingReward.augmentTransaction(withUsersAddress: MockConstants.walletAddressHdNoPassphrase, andTokens: DependencyManager.shared.tokens)
		
		let stubTransactionToken = TzKTTransaction(type: .transaction, id: 0, level: 0, timestamp: "", hash: "", counter: 0, initiater: nil, sender: userWithContact, bakerFee: XTZAmount(fromNormalisedAmount: 0.000144), storageFee: XTZAmount.zero(), allocationFee: XTZAmount.zero(), target: tokenTarget, prevDelegate: nil, newDelegate: nil, amount: XTZAmount.zero(), parameter: tokenParameter, status: .applied)
		stubTransactionToken.augmentTransaction(withUsersAddress: MockConstants.walletAddressHdNoPassphrase, andTokens: DependencyManager.shared.tokens)
		
		let stubTransactionFailed = TzKTTransaction(type: .transaction, id: 0, level: 0, timestamp: "", hash: "", counter: 0, initiater: nil, sender: userWithContact, bakerFee: XTZAmount(fromNormalisedAmount: 0.000144), storageFee: XTZAmount.zero(), allocationFee: XTZAmount.zero(), target: me, prevDelegate: nil, newDelegate: nil, amount: XTZAmount(fromNormalisedAmount: 21.000000), parameter: nil, status: .failed)
		stubTransactionFailed.augmentTransaction(withUsersAddress: MockConstants.walletAddressHdNoPassphrase, andTokens: DependencyManager.shared.tokens)
		
		
		
		let resultXTZWithoutContact = ActivityViewModel().createReceiveTransactionModel(transaction: stubTransactionXTZWithoutContact) as! TransactionHistoryCellModel
		XCTAssert(resultXTZWithoutContact.address == "From: " + MockConstants.contactlessAddressFormatted, "\(resultXTZWithoutContact.address)")
		XCTAssert(resultXTZWithoutContact.fee == "Network fee: N/A", "\(resultXTZWithoutContact.fee)")
		XCTAssert(resultXTZWithoutContact.amount == "+17 XTZ", "\(resultXTZWithoutContact.amount)")
		XCTAssert(resultXTZWithoutContact.type == "act_type_receive".localized.uppercased(), "\(resultXTZWithoutContact.type)")
		
		let resultXTZWithContact = ActivityViewModel().createReceiveTransactionModel(transaction: stubTransactionXTZWithContact) as! TransactionHistoryCellModel
		XCTAssert(resultXTZWithContact.address == "From: " + MockConstants.contactName, "\(resultXTZWithContact.address)")
		XCTAssert(resultXTZWithContact.fee == "Network fee: N/A", "\(resultXTZWithContact.fee)")
		XCTAssert(resultXTZWithContact.amount == "+17 XTZ", "\(resultXTZWithContact.amount)")
		XCTAssert(resultXTZWithContact.type == "act_type_receive".localized.uppercased(), "\(resultXTZWithContact.type)")
		
		let resultXTZBakingReward = ActivityViewModel().createReceiveTransactionModel(transaction: stubTransactionXTZBakingReward) as! TransactionHistoryCellModel
		XCTAssert(resultXTZBakingReward.address == "From: Baking Team", "\(resultXTZBakingReward.address)")
		XCTAssert(resultXTZBakingReward.fee == "Network fee: N/A", "\(resultXTZBakingReward.fee)")
		XCTAssert(resultXTZBakingReward.amount == "+17 XTZ", "\(resultXTZBakingReward.amount)")
		XCTAssert(resultXTZBakingReward.type == "act_type_reward".localized.uppercased(), "\(resultXTZBakingReward.type)")
		
		let resultToken = ActivityViewModel().createReceiveTransactionModel(transaction: stubTransactionToken) as! TransactionHistoryCellModel
		XCTAssert(resultToken.address == "From: " + MockConstants.contactName, "\(resultToken.address)")
		XCTAssert(resultToken.fee == "Network fee: N/A", "\(resultToken.fee)")
		XCTAssert(resultToken.amount == "+0.00019 tzBTC", "\(resultToken.amount)")
		XCTAssert(resultToken.type == "act_type_receive".localized.uppercased(), "\(resultToken.type)")
		
		let resultFailed = ActivityViewModel().createReceiveTransactionModel(transaction: stubTransactionFailed) as! TransactionHistoryFailedCellModel
		XCTAssert(resultFailed.address == "From: " + MockConstants.contactName, "\(resultFailed.address)")
		XCTAssert(resultFailed.fee == "Network fee: N/A", "\(resultFailed.fee)")
		XCTAssert(resultFailed.amount == "N/A", "\(resultFailed.amount)")
		XCTAssert(resultFailed.type == "act_type_failed".localized("act_type_receive".localized).uppercased(), "\(resultFailed.type)")
	}
	
	func testCreateExchangeTransactionModel() {
		let me = TzKTTransaction.TransactionLocation(alias: "", address: MockConstants.walletAddressHdNoPassphrase)
		let xtzToTokenTarget = TzKTTransaction.TransactionLocation(alias: "", address: DependencyManager.shared.tokens[1].dexterExchangeAddress ?? "")
		let xtzToTokenParameter = TzKTTransaction.TransactionParameter(entrypoint: "xtzToToken", value: ["to": MockConstants.walletAddressHdNoPassphrase, "deadline": "2020-10-28T12:02:40Z", "minTokensBought": "100"])
		
		let tokenToXTZTarget = TzKTTransaction.TransactionLocation(alias: "", address: DependencyManager.shared.tokens[1].dexterExchangeAddress ?? "")
		let tokenToXTZParameter = TzKTTransaction.TransactionParameter(entrypoint: "tokenToXtz", value: ["to": MockConstants.walletAddressHdNoPassphrase, "owner": MockConstants.walletAddressHdNoPassphrase, "deadline": "2020-10-28T12:02:40Z", "tokenSold": "", "minXtzBought": "17000000"])
		
		
		let stubTransactionXtzToToken = TzKTTransaction(type: .transaction, id: 0, level: 0, timestamp: "", hash: "", counter: 0, initiater: nil, sender: me, bakerFee: XTZAmount(fromNormalisedAmount: 0.000144), storageFee: XTZAmount.zero(), allocationFee: XTZAmount.zero(), target: xtzToTokenTarget, prevDelegate: nil, newDelegate: nil, amount: XTZAmount(fromNormalisedAmount: 17.000000), parameter: xtzToTokenParameter, status: .applied)
		stubTransactionXtzToToken.augmentTransaction(withUsersAddress: MockConstants.walletAddressHdNoPassphrase, andTokens: DependencyManager.shared.tokens)
		stubTransactionXtzToToken.secondaryToken = DependencyManager.shared.tokens[1]
		stubTransactionXtzToToken.secondaryAmount = TokenAmount(fromNormalisedAmount: 0.00000100, decimalPlaces: DependencyManager.shared.tokens[1].decimalPlaces)
		
		let stubTransactionXtzToTokenFailed = TzKTTransaction(type: .transaction, id: 0, level: 0, timestamp: "", hash: "", counter: 0, initiater: nil, sender: me, bakerFee: XTZAmount(fromNormalisedAmount: 0.000144), storageFee: XTZAmount.zero(), allocationFee: XTZAmount.zero(), target: xtzToTokenTarget, prevDelegate: nil, newDelegate: nil, amount: XTZAmount(fromNormalisedAmount: 21.000000), parameter: xtzToTokenParameter, status: .failed)
		stubTransactionXtzToTokenFailed.augmentTransaction(withUsersAddress: MockConstants.walletAddressHdNoPassphrase, andTokens: DependencyManager.shared.tokens)
		stubTransactionXtzToTokenFailed.secondaryToken = DependencyManager.shared.tokens[1]
		stubTransactionXtzToTokenFailed.secondaryAmount = TokenAmount(fromNormalisedAmount: 0.00000110, decimalPlaces: DependencyManager.shared.tokens[2].decimalPlaces)
		
		let stubTransactionTokenToXTZ = TzKTTransaction(type: .transaction, id: 0, level: 0, timestamp: "", hash: "", counter: 0, initiater: nil, sender: me, bakerFee: XTZAmount(fromNormalisedAmount: 0.000144), storageFee: XTZAmount.zero(), allocationFee: XTZAmount.zero(), target: tokenToXTZTarget, prevDelegate: nil, newDelegate: nil, amount: TokenAmount.zero(), parameter: tokenToXTZParameter, status: .applied)
		stubTransactionTokenToXTZ.augmentTransaction(withUsersAddress: MockConstants.walletAddressHdNoPassphrase, andTokens: DependencyManager.shared.tokens)
		stubTransactionTokenToXTZ.token = DependencyManager.shared.tokens[1]
		stubTransactionTokenToXTZ.amount = TokenAmount(fromNormalisedAmount: 17.00000000, decimalPlaces: DependencyManager.shared.tokens[1].decimalPlaces)
		stubTransactionTokenToXTZ.secondaryToken = DependencyManager.shared.tokens[0]
		stubTransactionTokenToXTZ.secondaryAmount = TokenAmount(fromNormalisedAmount: 0.00000100, decimalPlaces: DependencyManager.shared.tokens[0].decimalPlaces)
		
		let stubTransactionTokenToXTZFailed = TzKTTransaction(type: .transaction, id: 0, level: 0, timestamp: "", hash: "", counter: 0, initiater: nil, sender: me, bakerFee: XTZAmount(fromNormalisedAmount: 0.000144), storageFee: XTZAmount.zero(), allocationFee: XTZAmount.zero(), target: tokenToXTZTarget, prevDelegate: nil, newDelegate: nil, amount: TokenAmount.zero(), parameter: tokenToXTZParameter, status: .failed)
		stubTransactionTokenToXTZFailed.augmentTransaction(withUsersAddress: MockConstants.walletAddressHdNoPassphrase, andTokens: DependencyManager.shared.tokens)
		stubTransactionTokenToXTZFailed.token = DependencyManager.shared.tokens[1]
		stubTransactionTokenToXTZFailed.amount = TokenAmount(fromNormalisedAmount: 21.00000000, decimalPlaces: DependencyManager.shared.tokens[1].decimalPlaces)
		stubTransactionTokenToXTZFailed.secondaryToken = DependencyManager.shared.tokens[0]
		stubTransactionTokenToXTZFailed.secondaryAmount = TokenAmount(fromNormalisedAmount: 0.00000110, decimalPlaces: DependencyManager.shared.tokens[0].decimalPlaces)
		
		
		
		let resultXTZToToken = ActivityViewModel().createExchangeTransactionModel(transaction: stubTransactionXtzToToken) as! TransactionHistoryExchangeCellModel
		XCTAssert(resultXTZToToken.amountReceived == "+0.000001 tzBTC", "\(resultXTZToToken.amountReceived)")
		XCTAssert(resultXTZToToken.amountSent == "-17 XTZ", "\(resultXTZToToken.amountSent)")
		XCTAssert(resultXTZToToken.exchangeRate == "1 XTZ → 0 tzBTC", "\(resultXTZToToken.exchangeRate)")
		XCTAssert(resultXTZToToken.networkFee == "Network fee: 0.000144 XTZ", "\(resultXTZToToken.networkFee)")
		
		let resultXTZToTokenFailed = ActivityViewModel().createExchangeTransactionModel(transaction: stubTransactionXtzToTokenFailed) as! TransactionHistoryFailedCellModel
		XCTAssert(resultXTZToTokenFailed.address == "21 XTZ → 0.000001 tzBTC", "\(resultXTZToTokenFailed.address)")
		XCTAssert(resultXTZToTokenFailed.fee == "Network fee: N/A", "\(resultXTZToTokenFailed.fee)")
		XCTAssert(resultXTZToTokenFailed.amount == "N/A", "\(resultXTZToTokenFailed.amount)")
		XCTAssert(resultXTZToTokenFailed.type == "EXCHANGE FAILED", "\(resultXTZToTokenFailed.type)")
		
		let resultTokenToXTZ = ActivityViewModel().createExchangeTransactionModel(transaction: stubTransactionTokenToXTZ) as! TransactionHistoryExchangeCellModel
		XCTAssert(resultTokenToXTZ.amountReceived == "+0.000001 XTZ", "\(resultTokenToXTZ.amountReceived)")
		XCTAssert(resultTokenToXTZ.amountSent == "-17 tzBTC", "\(resultTokenToXTZ.amountSent)")
		XCTAssert(resultTokenToXTZ.exchangeRate == "1 tzBTC → 0 XTZ", "\(resultTokenToXTZ.exchangeRate)")
		XCTAssert(resultTokenToXTZ.networkFee == "Network fee: 0.000144 XTZ", "\(resultTokenToXTZ.networkFee)")
		
		let resultTokenToXTZFailed = ActivityViewModel().createExchangeTransactionModel(transaction: stubTransactionTokenToXTZFailed) as! TransactionHistoryFailedCellModel
		XCTAssert(resultTokenToXTZFailed.address == "21 tzBTC → 0.000001 XTZ", "\(resultTokenToXTZFailed.address)")
		XCTAssert(resultTokenToXTZFailed.fee == "Network fee: N/A", "\(resultTokenToXTZFailed.fee)")
		XCTAssert(resultTokenToXTZFailed.amount == "N/A", "\(resultTokenToXTZFailed.amount)")
		XCTAssert(resultTokenToXTZFailed.type == "EXCHANGE FAILED", "\(resultTokenToXTZFailed.type)")
	}
	
	func testCreateDelegationTransactionModel() {
		let me = TzKTTransaction.TransactionLocation(alias: "", address: MockConstants.walletAddressHdNoPassphrase)
		let baker = TzKTTransaction.TransactionLocation(alias: "", address: MockConstants.recipient)
		
		let stubTransaction = TzKTTransaction(type: .delegation, id: 0, level: 0, timestamp: "", hash: "", counter: 0, initiater: nil, sender: me, bakerFee: XTZAmount(fromNormalisedAmount: 0.000144), storageFee: XTZAmount.zero(), allocationFee: XTZAmount.zero(), target: nil, prevDelegate: baker, newDelegate: baker, amount: XTZAmount.zero(), parameter: nil, status: .applied)
		stubTransaction.augmentTransaction(withUsersAddress: MockConstants.walletAddressHdNoPassphrase, andTokens: DependencyManager.shared.tokens)
		
		let stubTransactionFailed = TzKTTransaction(type: .delegation, id: 0, level: 0, timestamp: "", hash: "", counter: 0, initiater: nil, sender: me, bakerFee: XTZAmount(fromNormalisedAmount: 0.000144), storageFee: XTZAmount.zero(), allocationFee: XTZAmount.zero(), target: nil, prevDelegate: baker, newDelegate: baker, amount: XTZAmount.zero(), parameter: nil, status: .failed)
		stubTransactionFailed.augmentTransaction(withUsersAddress: MockConstants.walletAddressHdNoPassphrase, andTokens: DependencyManager.shared.tokens)
		
		
		let resultXTZ = ActivityViewModel().createDelegationTransactionModel(transaction: stubTransaction) as! TransactionHistoryCellModel
		XCTAssert(resultXTZ.address == "To: " + MockConstants.recipientFormatted, "\(resultXTZ.address)")
		XCTAssert(resultXTZ.fee == "Network fee: 0.000144 XTZ", "\(resultXTZ.fee)")
		XCTAssert(resultXTZ.amount == "N/A", "\(resultXTZ.amount)")
		XCTAssert(resultXTZ.type == "act_type_delegation".localized.uppercased(), "\(resultXTZ.type)")
		
		let resultFailed = ActivityViewModel().createDelegationTransactionModel(transaction: stubTransactionFailed) as! TransactionHistoryFailedCellModel
		XCTAssert(resultFailed.address == "To: " + MockConstants.recipientFormatted, "\(resultFailed.address)")
		XCTAssert(resultFailed.fee == "Network fee: N/A", "\(resultFailed.fee)")
		XCTAssert(resultFailed.amount == "N/A", "\(resultFailed.amount)")
		XCTAssert(resultFailed.type == "act_type_failed".localized("act_type_delegation".localized).uppercased(), "\(resultFailed.type)")
	}
	
	func testCreateRevealTransactionModel() {
		let me = TzKTTransaction.TransactionLocation(alias: "", address: MockConstants.walletAddressHdNoPassphrase)
		
		let stubTransaction = TzKTTransaction(type: .reveal, id: 0, level: 0, timestamp: "", hash: "", counter: 0, initiater: nil, sender: me, bakerFee: XTZAmount(fromNormalisedAmount: 0.000144), storageFee: XTZAmount.zero(), allocationFee: XTZAmount.zero(), target: nil, prevDelegate: nil, newDelegate: nil, amount: XTZAmount.zero(), parameter: nil, status: .applied)
		stubTransaction.augmentTransaction(withUsersAddress: MockConstants.walletAddressHdNoPassphrase, andTokens: DependencyManager.shared.tokens)
		
		let stubTransactionFailed = TzKTTransaction(type: .reveal, id: 0, level: 0, timestamp: "", hash: "", counter: 0, initiater: nil, sender: me, bakerFee: XTZAmount(fromNormalisedAmount: 0.000144), storageFee: XTZAmount.zero(), allocationFee: XTZAmount.zero(), target: nil, prevDelegate: nil, newDelegate: nil, amount: XTZAmount.zero(), parameter: nil, status: .failed)
		stubTransactionFailed.augmentTransaction(withUsersAddress: MockConstants.walletAddressHdNoPassphrase, andTokens: DependencyManager.shared.tokens)
		
		
		let resultXTZ = ActivityViewModel().createRevealTransactionModel(transaction: stubTransaction) as! TransactionHistoryCellModel
		XCTAssert(resultXTZ.address == MockConstants.myWalletName, "\(resultXTZ.address)")
		XCTAssert(resultXTZ.fee == "Network fee: 0.000144 XTZ", "\(resultXTZ.fee)")
		XCTAssert(resultXTZ.amount == "N/A", "\(resultXTZ.amount)")
		XCTAssert(resultXTZ.type == "act_type_reveal".localized.uppercased(), "\(resultXTZ.type)")
		
		let resultFailed = ActivityViewModel().createRevealTransactionModel(transaction: stubTransactionFailed) as! TransactionHistoryFailedCellModel
		XCTAssert(resultFailed.address == MockConstants.myWalletName, "\(resultFailed.address)")
		XCTAssert(resultFailed.fee == "Network fee: N/A", "\(resultFailed.fee)")
		XCTAssert(resultFailed.amount == "N/A", "\(resultFailed.amount)")
		XCTAssert(resultFailed.type == "act_type_failed".localized("act_type_reveal".localized).uppercased(), "\(resultFailed.type)")
	}
	
	func testeCreateUnknownTransactionModel() {
		let me = TzKTTransaction.TransactionLocation(alias: "", address: MockConstants.walletAddressHdNoPassphrase)
		let contact = TzKTTransaction.TransactionLocation(alias: "", address: MockConstants.contactAddress)
		
		let stubTransaction = TzKTTransaction(type: .unknown, id: 0, level: 0, timestamp: "", hash: "", counter: 0, initiater: nil, sender: me, bakerFee: XTZAmount(fromNormalisedAmount: 0.000144), storageFee: XTZAmount.zero(), allocationFee: XTZAmount.zero(), target: contact, prevDelegate: nil, newDelegate: nil, amount: XTZAmount.zero(), parameter: nil, status: .applied)
		stubTransaction.augmentTransaction(withUsersAddress: MockConstants.walletAddressHdNoPassphrase, andTokens: DependencyManager.shared.tokens)
		
		let stubTransactionFailed = TzKTTransaction(type: .unknown, id: 0, level: 0, timestamp: "", hash: "", counter: 0, initiater: nil, sender: me, bakerFee: XTZAmount(fromNormalisedAmount: 0.000144), storageFee: XTZAmount.zero(), allocationFee: XTZAmount.zero(), target: contact, prevDelegate: nil, newDelegate: nil, amount: XTZAmount.zero(), parameter: nil, status: .failed)
		stubTransactionFailed.augmentTransaction(withUsersAddress: MockConstants.walletAddressHdNoPassphrase, andTokens: DependencyManager.shared.tokens)
		
		let resultXTZ = ActivityViewModel().createUnknownTransactionModel(transaction: stubTransaction) as! TransactionHistoryCellModel
		XCTAssert(resultXTZ.address == "To: " + MockConstants.contactName, "\(resultXTZ.address)")
		XCTAssert(resultXTZ.fee == "Network fee: 0.000144 XTZ", "\(resultXTZ.fee)")
		XCTAssert(resultXTZ.amount == "N/A", "\(resultXTZ.amount)")
		XCTAssert(resultXTZ.type == "act_type_unknown".localized.uppercased(), "\(resultXTZ.type)")
		
		let resultFailed = ActivityViewModel().createUnknownTransactionModel(transaction: stubTransactionFailed) as! TransactionHistoryFailedCellModel
		XCTAssert(resultFailed.address == "To: " + MockConstants.contactName, "\(resultFailed.address)")
		XCTAssert(resultFailed.fee == "Network fee: N/A", "\(resultFailed.fee)")
		XCTAssert(resultFailed.amount == "N/A", "\(resultFailed.amount)")
		XCTAssert(resultFailed.type == "act_type_failed".localized("act_type_unknown".localized).uppercased(), "\(resultFailed.type)")
	}
}
