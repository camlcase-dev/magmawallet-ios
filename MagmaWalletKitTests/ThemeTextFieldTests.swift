//
//  ThemeTextFieldTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 22/06/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
import camlKit
@testable import MagmaWalletKit

class ThemeTestFieldTests: XCTestCase {
	
	var validationShouldFail = false
	
	
	// MARK: - Test Setup
    override func setUp() {
		
    }

    override func tearDown() {
		
    }
	
	
	
	// MARK: - Test cases
	func testTypes() {
		let textField1 = ThemeTextfield(frame: CGRect(x: 0, y: 0, width: 100, height: 40))
		textField1.pinField = true
		
		XCTAssertTrue(textField1.font == UIFont.barlowFont(.regular, withSize: 48))
		XCTAssertTrue(textField1.keyboardType == .numberPad)
		XCTAssertTrue(textField1.textAlignment == .center)
		
		
		let textField2 = ThemeTextfield(frame: CGRect(x: 0, y: 0, width: 100, height: 40))
		textField2.addressField = true
		
		XCTAssertTrue(textField2.font == UIFont.barlowFont(.regular, withSize: 14))
		XCTAssertTrue(textField2.keyboardType == .default)
		XCTAssertTrue(textField2.textAlignment == .left)
		
		
		let textField3 = ThemeTextfield(frame: CGRect(x: 0, y: 0, width: 100, height: 40))
		textField3.amountField = true
		
		XCTAssertTrue(textField3.font == UIFont.barlowFont(.regular, withSize: 48))
		XCTAssertTrue(textField3.textAlignment == .center)
		
		
		let textField4 = ThemeTextfield(frame: CGRect(x: 0, y: 0, width: 100, height: 40))
		textField4.darkMode = true
		
		XCTAssertTrue(textField4.backgroundColor == UIColor(red: 240.0 / 255.0, green: 240.0 / 255.0, blue: 240.0 / 255.0, alpha: 1))
	}
	
	func testLocalizedPlaceholder() {
		let textField = ThemeTextfield(frame: CGRect(x: 0, y: 0, width: 100, height: 40))
		textField.localizedPlaceholder = "onb_splash_bottom_text"
		
		textField.awakeFromNib()
		
		XCTAssertTrue(textField.placeholder == "TEZOS WALLET")
	}
	
	func testRightMode() {
		let textField = ThemeTextfield(frame: CGRect(x: 0, y: 0, width: 100, height: 40))
		textField.rightMode = .none
		XCTAssertNil(textField.rightView)
		
		
		textField.rightMode = .clearOnly
		textField.layoutSubviews()
		XCTAssertNotNil(textField.rightView)
		XCTAssert(textField.clearButtonMode == .never)
		XCTAssert(textField.rightViewMode == .always)
		
		
		textField.rightMode = .none
		textField.rightView = nil
		textField.layoutSubviews()
		XCTAssertNil(textField.rightView)
		
		
		textField.rightMode = .scanAndClear
		textField.layoutSubviews()
		XCTAssertNotNil(textField.rightView)
		XCTAssert(textField.clearButtonMode == .never)
		XCTAssert(textField.rightViewMode == .always)
		
		
		textField.rightMode = .none
		textField.rightView = nil
		textField.layoutSubviews()
		XCTAssertNil(textField.rightView)
		
		
		textField.rightMode = .text("MOCK")
		textField.layoutSubviews()
		XCTAssertNotNil(textField.rightView)
		XCTAssert(textField.clearButtonMode == .never)
		XCTAssert(textField.rightViewMode == .always)
		XCTAssertTrue((textField.rightView as! UILabel).text == "MOCK")
	}
	
	func testValidatorTextProcessing() {
		let textField = ThemeTextfield(frame: CGRect(x: 0, y: 0, width: 100, height: 40))
		textField.validator = LengthValidator(maxLength: 10, minLength: 4)
		textField.themeTextfieldValidationDelegate = self
		
		validationShouldFail = false
		textField.text = "abcdef"
		textField.revalidateTextfield()
		
		validationShouldFail = true
		textField.text = "abcdefghijklmnop"
		textField.revalidateTextfield()
		
		validationShouldFail = false
		textField.text = "1234567"
		textField.revalidateTextfield()
		
		validationShouldFail = true
		textField.text = "12"
		textField.revalidateTextfield()
	}
}

extension ThemeTestFieldTests: ThemeTextfieldValidationProtocol {
	
	func validated(_ validated: Bool, textfield: ThemeTextfield, forText text: String) {
		XCTAssertTrue(validated == !validationShouldFail, "forText: \(text)")
	}
}
