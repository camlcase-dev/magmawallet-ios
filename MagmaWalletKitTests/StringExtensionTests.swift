//
//  StringExtensionTests.swift
//  MagmaWalletKitTests
//
//  Created by Simon Mcloughlin on 20/11/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
@testable import MagmaWalletKit

class StringExtensionTests: XCTestCase {

    override func setUpWithError() throws {
    }

    override func tearDownWithError() throws {
    }

	
	// MARK: Tests
	
	func testQRCode() {
		let qr = "hello".generateQRCode()
		
		XCTAssertTrue(qr != nil)
	}
	
	func testLocalized() {
		let string = "onb_splash_bottom_text".localized
		XCTAssert(string == "TEZOS WALLET", string)
		
		let stringWithFormat = "onb_biometric_prompt_yes".localized("MOCK")
		XCTAssert(stringWithFormat == "Yes, use MOCK")
	}
	
	func testExtractNumbers() {
		let string = "abc123def"
		XCTAssert(string.extractNumbers() == "123")
	}
	
	func testTezosAddressFromat() {
		let string = MockConstants.walletAddress
		XCTAssert(string.tezosAddressFormat() == MockConstants.walletAddressFormatted)
	}
	
	func testRegex() {
		let michelsonString = "Pair (Pair (Pair \"tz1e4hAp7xpjekmXnYe4677ELGA3UxR79EFb\" \"tz1e4hAp7xpjekmXnYe4677ELGA3UxR79EFb\") (Pair 100 3972987)) \"2020-04-08T14:24:54Z\""
		let matches = michelsonString.matches(forRegex: "\\((.*?)\\)")
		XCTAssert(matches.count == 2)
		XCTAssert(matches[0] == "(Pair (Pair \"tz1e4hAp7xpjekmXnYe4677ELGA3UxR79EFb\" \"tz1e4hAp7xpjekmXnYe4677ELGA3UxR79EFb\")")
		XCTAssert(matches[1] == "(Pair 100 3972987)")
	}
	
	func testBoolValue() {
		let string1 = "true"
		let string2 = "false"
		let string3 = "1"
		let string4 = "0"
		let string5 = "blah"
		
		XCTAssertTrue(string1.boolValue)
		XCTAssertFalse(string2.boolValue)
		XCTAssertTrue(string3.boolValue)
		XCTAssertFalse(string4.boolValue)
		XCTAssertFalse(string5.boolValue)
	}
	
	func testHeightAndWidth() {
		let string = "this is a string"
		let height = string.height(withConstrainedWidth: 100, font: UIFont.systemFont(ofSize: 10))
		let width = string.width(withConstrainedHeight: 40, font: UIFont.systemFont(ofSize: 10))
		
		XCTAssert(height == 12.0, "\(height)")
		XCTAssert(width == 68.0, "\(width)")
	}
}
