//
//  GradientViewTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 22/06/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
@testable import MagmaWalletKit

class GradientViewTests: XCTestCase {
	
	// MARK: - Test Setup
    override func setUp() {
		
    }

    override func tearDown() {
		
    }
	
	
	
	// MARK: - Test cases
	func testLight() {
		let gradientView = GradientView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
		gradientView.light = true
		gradientView.layoutSubviews()
		
		let gradientLayer = gradientView.layer.sublayers?[0] as! CAGradientLayer
		XCTAssert(gradientLayer.colors?.count == 4)
		XCTAssert(gradientLayer.locations?.count == 4)
	}
	
	func testRich() {
		let gradientView = GradientView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
		gradientView.rich = true
		gradientView.layoutSubviews()
		
		let gradientLayer = gradientView.layer.sublayers?[0] as! CAGradientLayer
		XCTAssert(gradientLayer.colors?.count == 3)
		XCTAssert(gradientLayer.locations?.count == 3)
	}
	
	func testDiagonalWhite() {
		let gradientView = GradientView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
		gradientView.diagonalWhite = true
		gradientView.layoutSubviews()
		
		let gradientLayer = gradientView.layer.sublayers?[0] as! CAGradientLayer
		XCTAssert(gradientLayer.colors?.count == 2)
		XCTAssert(gradientLayer.locations?.count == 2)
	}
}
