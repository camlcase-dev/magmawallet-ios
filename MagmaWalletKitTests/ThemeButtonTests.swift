//
//  ThemeButtonTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 22/06/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
@testable import MagmaWalletKit

class ThemeButtonTests: XCTestCase {
	
	// MARK: - Test Setup
    override func setUp() {
		
    }

    override func tearDown() {
		
    }
	
	
	
	// MARK: - Test cases
	func testUpdateStyles() {
		let button1 = ThemeButton(frame: CGRect(x: 0, y: 0, width: 100, height: 40))
		button1.primaryButton = true
		button1.prepareForInterfaceBuilder()
		checkForPrimaryButtonTheme(button: button1)
		
		let button2 = ThemeButton(frame: CGRect(x: 0, y: 0, width: 100, height: 40))
		button2.link = true
		button2.awakeFromNib()
		checkForLinkTheme(button: button2)
		
		let button3 = ThemeButton(frame: CGRect(x: 0, y: 0, width: 100, height: 40))
		button3.secondaryButton = true
		button3.prepareForInterfaceBuilder()
		checkForSecondaryButtonTheme(button: button3)
		
		let button4 = ThemeButton(frame: CGRect(x: 0, y: 0, width: 100, height: 40))
		button4.dropdownButton = true
		button4.prepareForInterfaceBuilder()
		checkForDropdownButtonTheme(button: button4)
		
		let button5 = ThemeButton(frame: CGRect(x: 0, y: 0, width: 100, height: 40))
		button5.shareButton = true
		button5.prepareForInterfaceBuilder()
		checkForShareButtonTheme(button: button5)
		
		let button6 = ThemeButton(frame: CGRect(x: 0, y: 0, width: 100, height: 40))
		button6.maxButton = true
		button6.prepareForInterfaceBuilder()
		checkForMaxButtonTheme(button: button6)
		
		let button7 = ThemeButton(frame: CGRect(x: 0, y: 0, width: 100, height: 40))
		button7.exchangeRateButton = true
		button7.prepareForInterfaceBuilder()
		checkForExchangeRateButtonTheme(button: button7)
		
		let button8 = ThemeButton(frame: CGRect(x: 0, y: 0, width: 100, height: 40))
		button8.slippageButton = true
		button8.prepareForInterfaceBuilder()
		checkForSlippageButtonTheme(button: button8)
	}
	
	func testSetTitle() {
		let button1 = ThemeButton(frame: CGRect(x: 0, y: 0, width: 100, height: 40))
		button1.primaryButton = true
		button1.setTitle("test", for: .normal)
		button1.setTitle("test", for: .disabled)
		button1.prepareForInterfaceBuilder()
		
		let button2 = ThemeButton(frame: CGRect(x: 0, y: 0, width: 100, height: 40))
		button2.link = true
		button2.setTitle("test", for: .normal)
		button2.prepareForInterfaceBuilder()
		
		XCTAssert(button1.title(for: .normal) == nil)
		XCTAssert(button1.attributedTitle(for: .normal)?.string == "TEST")
		XCTAssert(button1.attributedTitle(for: .disabled)?.string == "TEST")
		XCTAssert(button2.title(for: .normal) == "test")
	}
	
	func testLocalizedText() {
		let button = ThemeButton(frame: CGRect(x: 0, y: 0, width: 100, height: 40))
		button.localizedText = "onb_splash_bottom_text"
		
		button.awakeFromNib()
		
		XCTAssertTrue(button.title(for: .normal) == "TEZOS WALLET")
	}
	
	func testBackground() {
		let button = ThemeButton(frame: CGRect(x: 0, y: 0, width: 100, height: 40))
		button.enabledTextColor = UIColor.black
		button.enabledBackgroundColor = UIColor.red
		button.disabledBackgroundColor = UIColor.blue
		button.disabledTextColor = UIColor.white
		
		button.awakeFromNib()
		
		button.isEnabled = true
		XCTAssertTrue(button.titleColor(for: .normal) == UIColor.black)
		XCTAssertTrue(button.backgroundColor == UIColor.red)
		
		button.isEnabled = false
		XCTAssertTrue(button.titleColor(for: .disabled) == UIColor.white)
		XCTAssertTrue(button.backgroundColor == UIColor.blue)
	}
	
	
	
	
	// MARK: - Helpers
	func checkForPrimaryButtonTheme(button: ThemeButton) {
		XCTAssertTrue(button.enabledBackgroundColor == UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.20))
		XCTAssertTrue(button.disabledBackgroundColor == UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.10))
		XCTAssertTrue(button.cornerRadius == 0)
		XCTAssertTrue(button.semanticContentAttribute == .forceRightToLeft)
		XCTAssertNotNil(button.image(for: .normal))
		XCTAssertTrue(button.tintColor == UIColor.white)
		XCTAssertTrue(button.imageEdgeInsets == UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0))
	}
	
	func checkForLinkTheme(button: ThemeButton) {
		XCTAssertTrue(button.enabledBackgroundColor == UIColor.clear)
		XCTAssertTrue(button.disabledBackgroundColor == UIColor.clear)
		XCTAssertTrue(button.disabledTextColor == UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.50))
		XCTAssertTrue(button.cornerRadius == 0)
		XCTAssertNil(button.image(for: .normal))
		XCTAssertTrue(button.titleLabel?.font == UIFont.barlowFont(.regular, withSize: 16))
	}
	
	func checkForSecondaryButtonTheme(button: ThemeButton) {
		XCTAssertTrue(button.enabledBackgroundColor == UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.20))
		XCTAssertTrue(button.disabledBackgroundColor == UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.10))
		XCTAssertTrue(button.disabledTextColor == UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.50))
		XCTAssertTrue(button.cornerRadius == button.frame.height / 2)
		XCTAssertNil(button.image(for: .normal))
		XCTAssertTrue(button.contentEdgeInsets == UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 16))
		XCTAssertTrue(button.titleLabel?.font == UIFont.barlowFont(.regular, withSize: 14))
	}
	
	func checkForDropdownButtonTheme(button: ThemeButton) {
		XCTAssertTrue(button.image(for: .normal) != nil)
		XCTAssertTrue(button.tintColor == UIColor.white)
		XCTAssertTrue(button.titleLabel?.font == UIFont.barlowFont(.medium, withSize: 18))
	}
	
	func checkForShareButtonTheme(button: ThemeButton) {
		XCTAssertTrue(button.enabledBackgroundColor == UIColor(red: 185.0 / 255.0, green: 55.0 / 255.0, blue: 92.0 / 255.0, alpha: 1))
		XCTAssertTrue(button.titleLabel?.font == UIFont.barlowFont(.semiBold, withSize: 18))
		XCTAssertTrue(button.titleLabel?.textColor == UIColor.white)
	}
	
	func checkForMaxButtonTheme(button: ThemeButton) {
		XCTAssertTrue(button.enabledBackgroundColor == UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.20))
		XCTAssertTrue(button.disabledBackgroundColor == UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.10))
		XCTAssertTrue(button.disabledTextColor == UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.50))
		XCTAssertTrue(button.cornerRadius == button.frame.height / 2)
		XCTAssertTrue(button.titleLabel?.font == UIFont.barlowFont(.bold, withSize: 9))
	}
	
	func checkForExchangeRateButtonTheme(button: ThemeButton) {
		XCTAssertTrue(button.enabledBackgroundColor == UIColor(red: 211.0 / 255.0, green: 79.0 / 255.0, blue: 121.0 / 255.0, alpha: 1))
		XCTAssertTrue(button.cornerRadius == button.frame.height / 2)
		XCTAssertNotNil(button.image(for: .normal))
		XCTAssertTrue(button.titleLabel?.font == UIFont.barlowFont(.regular, withSize: 12))
	}
	
	func checkForSlippageButtonTheme(button: ThemeButton) {
		XCTAssertTrue(button.enabledBackgroundColor == UIColor(red: 36.0 / 255.0, green: 45.0 / 255.0, blue: 60.0 / 255.0, alpha: 0.5))
		XCTAssertTrue(button.selectedBackgroundColor == UIColor(red: 12.0 / 255.0, green: 202.0 / 255.0, blue: 74.0 / 255.0, alpha: 1))
		XCTAssertTrue(button.enabledFont == UIFont.barlowFont(.regular, withSize: 14))
		XCTAssertTrue(button.selectedFont == UIFont.barlowFont(.bold, withSize: 14))
	}
}
