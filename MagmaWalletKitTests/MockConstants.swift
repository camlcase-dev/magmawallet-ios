//
//  MockConstants.swift
//  MagmaWalletKitTests
//
//  Created by Simon Mcloughlin on 07/07/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
@testable import MagmaWallet

class MockConstants {
	static let walletAddress = "tz1e4hAp7xpjekmXnYe4677ELGA3UxR79EFb"
	static let walletAddressFormatted = "tz1e4hAp7xpjek...R79EFb"
	
	static let recipient = "tz1RKLWbGm7T4mnxDZHWazkbnvaryKsxxZTF"
	static let recipientFormatted = "tz1RKLWbGm7T4m...sxxZTF"
}
