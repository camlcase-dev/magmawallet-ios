//
//  ThemeLabelTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 22/06/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
@testable import MagmaWalletKit

class ThemeLabelTests: XCTestCase {
	
	// MARK: - Test Setup
    override func setUp() {
		
    }

    override func tearDown() {
		
    }
	
	
	
	// MARK: - Test cases
	func testUpdateStyles() {
		let label = ThemeLabel(frame: CGRect(x: 0, y: 0, width: 100, height: 25))
		label.font = UIFont.systemFont(ofSize: 10)
		
		var previousFont = label.font
		
		label.largeHeading = true
		label.prepareForInterfaceBuilder()
		
		XCTAssertFalse(previousFont == label.font)
		previousFont = label.font
		
		label.largeHeading = false
		label.smallHeading = true
		label.prepareForInterfaceBuilder()
		
		XCTAssertFalse(previousFont == label.font)
		previousFont = label.font
		
		label.smallHeading = false
		label.primaryDescriptiveText = true
		label.prepareForInterfaceBuilder()
		
		XCTAssertFalse(previousFont == label.font)
		previousFont = label.font
		
		label.primaryDescriptiveText = false
		label.secondaryDescriptiveText = true
		label.prepareForInterfaceBuilder()
		
		XCTAssertFalse(previousFont == label.font)
	}
	
	func testLocalizableText() {
		let label = ThemeLabel(frame: CGRect(x: 0, y: 0, width: 100, height: 25))
		label.localizedText = "onb_splash_bottom_text"
		
		label.awakeFromNib()
		
		XCTAssertTrue(label.text == "TEZOS WALLET")
	}
}
