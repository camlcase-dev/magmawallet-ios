//
//  ThemeSegmentedControlTests.swift
//  MagmaWalletKitTests
//
//  Created by Simon Mcloughlin on 23/03/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import XCTest
@testable import MagmaWalletKit

class ThemeSegmentedControlTests: XCTestCase {
	
	// MARK: - Test Setup
	override func setUp() {
		
	}
	
	override func tearDown() {
		
	}
	
	
	
	// MARK: - Test cases
	func testUpdateStyles() {
		let button1 = ThemeSegmentedControl(frame: CGRect(x: 0, y: 0, width: 100, height: 50))
		button1.prepareForInterfaceBuilder()
		button1.layoutSubviews()
		checkStyles(button: button1)
		
		let button2 = ThemeSegmentedControl(items: ["yes", "no"])
		button2.prepareForInterfaceBuilder()
		button2.awakeFromNib()
		checkStyles(button: button2)
	}
	
	func checkStyles(button: ThemeSegmentedControl) {
		let normalAttributes = (button.titleTextAttributes(for: .normal) as? [NSAttributedString.Key: NSObject]) ?? [:]
		let selectedAttributes = (button.titleTextAttributes(for: .selected) as? [NSAttributedString.Key: NSObject]) ?? [:]
		
		let titleTextNormalAttributes: [NSAttributedString.Key: NSObject] = [NSAttributedString.Key.foregroundColor: UIColor.init(white: 1, alpha: 0.75), NSAttributedString.Key.font: UIFont.barlowFont(.regular, withSize: 14)]
		let titleTextSelectedAttributes: [NSAttributedString.Key: NSObject] = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.barlowFont(.bold, withSize: 14)]
		
		XCTAssert(normalAttributes == titleTextNormalAttributes)
		XCTAssert(selectedAttributes == titleTextSelectedAttributes)
		
		if #available(iOS 13.0, *) {
			XCTAssert(button.selectedSegmentTintColor == UIColor(red: 108.0 / 255.0, green: 28.0 / 255.0, blue: 100.0 / 255.0, alpha: 1))
		}
	}
}
