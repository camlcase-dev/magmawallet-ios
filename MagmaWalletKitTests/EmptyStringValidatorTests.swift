//
//  EmptyStringValidatorTests.swift
//  MagmaWalletKitTests
//
//  Created by Simon Mcloughlin on 20/11/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
@testable import MagmaWalletKit

class EmptyStringValidatorTests: XCTestCase {

    override func setUpWithError() throws {
    }

    override func tearDownWithError() throws {
    }
	
	
	
	// MARK: Tests
	
	func testValidate() {
		let validator = EmptyStringValidator()
		
		XCTAssertFalse(validator.validate(text: ""))
		XCTAssertFalse(validator.validate(text: " "))
		XCTAssertFalse(validator.validate(text: "\n"))
		XCTAssertFalse(validator.validate(text: "\t"))
		
		XCTAssertTrue(validator.validate(text: "abc"))
	}
	
	func testRestrictEntryIfInvalid() {
		let validator = EmptyStringValidator()
		
		XCTAssertFalse(validator.restrictEntryIfInvalid(text: ""))
		XCTAssertFalse(validator.restrictEntryIfInvalid(text: " "))
		XCTAssertFalse(validator.restrictEntryIfInvalid(text: "\n"))
		XCTAssertFalse(validator.restrictEntryIfInvalid(text: "\t"))
		XCTAssertFalse(validator.restrictEntryIfInvalid(text: "abc"))
	}
}
