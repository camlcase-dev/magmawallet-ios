//
//  ValidatorTests.swift
//  MagmaWalletTests
//
//  Created by Simon Mcloughlin on 22/06/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
import camlKit
@testable import MagmaWalletKit

class ValidatorTests: XCTestCase {
	
	// MARK: - Test Setup
    override func setUp() {
		
    }

    override func tearDown() {
		
    }
	
	
	
	// MARK: - Test cases
	func testLengthValidator() {
		let testString1 = "abc"
		let testString2 = "abcdef"
		let testString3 = "abcdefghi"
		
		let validator = LengthValidator(maxLength: 6, minLength: 4)
		
		XCTAssertFalse(validator.validate(text: testString1))
		XCTAssertTrue(validator.validate(text: testString2))
		XCTAssertFalse(validator.validate(text: testString3))
		
		XCTAssertFalse(validator.restrictEntryIfInvalid(text: testString2))
		XCTAssertTrue(validator.restrictEntryIfInvalid(text: testString3))
	}
	
	func testTokenAddressValidator() {
		let testString1 = "tz1e4hAp7xpjekmXnYe4677ELGA3UxR79EF2"
		let testString2 = MockConstants.walletAddress
		let testString3 = MockConstants.walletAddressFormatted
		let testString4 = MockConstants.recipient
		let testString5 = MockConstants.recipientFormatted
		
		let validator = TezosAddressValidator(ownAddress: MockConstants.walletAddress)
		
		XCTAssertTrue(validator.validate(text: testString1))
		XCTAssertFalse(validator.validate(text: testString2)) // When entering addresses to the app, entering your own address is a problem
		XCTAssertFalse(validator.validate(text: testString3))
		XCTAssertTrue(validator.validate(text: testString4))
		XCTAssertFalse(validator.validate(text: testString5))
	}
	
	func testNumberValidator() {
		let testString1 = "abc"
		let testString2 = "5"
		let testString3 = "5.5"
		let testString4 = "5.500005"
		let testString5 = "7"
		let testString6 = "5.00"
		let testString7 = "5.123"
		
		let validator = NumberValidator(min: 5, max: 6, decimalPlaces: 6)
		let validatorDecimal = NumberValidator(min: 5, max: 6, decimalPlaces: 2)
		let validatorNoDecimal = NumberValidator(min: 5, max: 6, decimalPlaces: 0)
		
		XCTAssertFalse(validator.validate(text: testString1))
		XCTAssertTrue(validator.validate(text: testString2))
		XCTAssertTrue(validator.validate(text: testString3))
		XCTAssertTrue(validator.validate(text: testString4))
		XCTAssertFalse(validator.validate(text: testString5))
		XCTAssertTrue(validatorDecimal.validate(text: testString6))
		XCTAssertFalse(validatorDecimal.validate(text: testString7))
		
		XCTAssertTrue(validatorDecimal.restrictEntryIfInvalid(text: testString4))
		XCTAssertTrue(validatorNoDecimal.restrictEntryIfInvalid(text: testString3))
		XCTAssertFalse(validator.restrictEntryIfInvalid(text: testString1))
	}
}
