//
//  LaunchPerformanceTest.swift
//  MagmaWalletUITests
//
//  Created by Simon Mcloughlin on 11/05/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest

class LaunchPerformanceTest: XCTestCase {

	
	// MARK: - Setup
    override func setUpWithError() throws {
        continueAfterFailure = false
    }

    override func tearDownWithError() throws {
    }
	
	
	
	// MARK: - Test cases
    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTOSSignpostMetric.applicationLaunch]) {
                XCUIApplication().launch()
            }
        }
    }
}
