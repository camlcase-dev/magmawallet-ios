//
//  UITest04_ExchangeTests.swift
//  MagmaWalletUITests
//
//  Created by Simon Mcloughlin on 12/10/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest

class UITest04_ExchangeTests: XCTestCase {
	
	// MARK: - Setup
    override func setUpWithError() throws {
        continueAfterFailure = false

		SharedHelpers.shared.application().launch()
    }

    override func tearDownWithError() throws {
		
    }
	
	
	// MARK: - Test cases
	
	// These tests need to be named alphabetically to ensure they are run in order.
	// These tests are running on the live tesetnet outside of our control. We do not have the ability to automatically fill these accounts with tokens.
	// Accounts have been filled and need to send, and send back to ensure the run smoothly for a long period of time
	func test01_XTZToToken() {
		SharedHelpers.shared.makeSureWeAreLoggedInto(address: SharedHelpers.address1_linear, withMnemonic: SharedHelpers.mnemonic1, hd: false, passphrase: nil, derivationPath: nil)
		
		let app = XCUIApplication()
		let randomAmount = Int.random(in: 1 ..< 10000)
		let exchangeAmountString = "3.\(randomAmount)"
		let exchangeAmount = Decimal(string: exchangeAmountString) ?? 0
		
		// check the home screen to see is there an amount of tzBTC already present, if so store it for later
		var previousTokenAmount = "0"
		let firstCell = app.tables.cells.element(boundBy: 0)
		let tokenTypeLabel = firstCell.staticTexts["token-type-label"].label
		
		if tokenTypeLabel == "tzBTC" {
			previousTokenAmount = firstCell.staticTexts["token-amount-label"].label
		}
		
		
		// Go to exchange
		app.tabBars.children(matching: .button).element(boundBy: 1).tap()
		verifyTokenDropdownWorks(withApplication: app)
		
		
		// Enter an amount
		let textField = app.textFields["enter-amount"]
		textField.tap()
		sleep(2)
		
		textField.typeText(exchangeAmount.description)
		SharedHelpers.shared.tapToolBarDoneButton()
		SharedHelpers.shared.tapPrimaryButton()
		
		
		// If get a warning about price impact, just ignore and move on
		if app.staticTexts["Price impact notice"].exists || app.staticTexts["Price impact warning"].exists {
			SharedHelpers.shared.tapPrimaryButton()
		}
		
		
		// Check confirm details
		SharedHelpers.shared.waitForStaticText("Confirm details", inApp: app, delay: 30)
		
		let xtzLabel = app.staticTexts["primary-amount"].label
		let tokenLabel = app.staticTexts["secondary-amount"].label.components(separatedBy: " ").first ?? "0"
		
		XCTAssert(xtzLabel == "\(exchangeAmount) XTZ", "\(xtzLabel) != \(exchangeAmount) XTZ")
		
		
		// Check both screens contain info
		verifyNetworkFeeAndLiquidityFeeDetails(withApplication: app)
		
		
		// Confirm the trade
		SharedHelpers.shared.tapPrimaryButton()
		
		
		// Wait for "wait" screen to show
		SharedHelpers.shared.waitForStaticText("This transaction will continue to process in the background if you navigate away from this screen.", inApp: app, delay: 30)
		
		
		// Wait for success screen
		SharedHelpers.shared.waitForStaticText("Success!", inApp: app, delay: 600)
		SharedHelpers.shared.tapLinkButton()
		sleep(2)
		
		
		// Grab new token amount from home screen
		let updatedTokenAmount = firstCell.staticTexts["token-amount-label"].label
		
		XCTAssert(updatedTokenAmount != "0")
		XCTAssert(tokenLabel != "0")
		
		let previousAmountAsDecimal = Decimal(string: previousTokenAmount.replacingOccurrences(of: ",", with: "")) ?? 0
		let updatedAmountAsDecimal = Decimal(string: updatedTokenAmount.replacingOccurrences(of: ",", with: "")) ?? 0
		let confirmationAmountAsDecimal = Decimal(string: tokenLabel.replacingOccurrences(of: ",", with: "")) ?? 0
		
		XCTAssert(updatedAmountAsDecimal != 0)
		XCTAssert(confirmationAmountAsDecimal != 0)
		
		let expected = (previousAmountAsDecimal + confirmationAmountAsDecimal)
		
		// Its fine if we get back more than expected (due to exchange rates), not ok to get less
		XCTAssert(updatedAmountAsDecimal >= expected, "\(updatedAmountAsDecimal) is not >= \(expected)")
	}
	
	func test02_TokenToXTZ() {
		SharedHelpers.shared.makeSureWeAreLoggedInto(address: SharedHelpers.address1_linear, withMnemonic: SharedHelpers.mnemonic1, hd: false, passphrase: nil, derivationPath: nil)
		
		let app = XCUIApplication()
		
		// check the home screen to see is there an amount of tzBTC already present, if so store it for later
		var previousTokenAmount = "0"
		var exchangeAmount = "0"
		var exchangeAmountAsDecimal: Decimal = 0
		
		let firstCell = app.tables.cells.element(boundBy: 0)
		let tokenTypeLabel = firstCell.staticTexts["token-type-label"].label
		
		if tokenTypeLabel == "tzBTC" {
			previousTokenAmount = firstCell.staticTexts["token-amount-label"].label
			let previousAmountAsDecimal = Decimal(string: previousTokenAmount.replacingOccurrences(of: ",", with: "")) ?? 0
			
			exchangeAmountAsDecimal = (previousAmountAsDecimal - 0.00000256) // transfer evertyhing except 0.00000256
			print("previousTokenAmount: \(previousTokenAmount)")
			print("previousAmountAsDecimal: \(previousAmountAsDecimal)")
			print("exchangeAmountAsDecimal: \(exchangeAmountAsDecimal)")
			
			XCTAssert(exchangeAmountAsDecimal > 0)
			
			exchangeAmount = "\(exchangeAmountAsDecimal)"
		}
		
		
		// Go to exchange, switch tokens and enter amount
		app.tabBars.children(matching: .button).element(boundBy: 1).tap()
		SharedHelpers.shared.tapExchangeRateButton()
		
		let topDropdownText = app.buttons["exchange-from-token-dropdown"].label
		let bottomDropdownText = app.buttons["exchange-to-token-dropdown"].label
		
		XCTAssert(topDropdownText == "tzBTC")
		XCTAssert(bottomDropdownText == "XTZ")
		
		let textField = app.textFields["enter-amount"]
		textField.tap()
		sleep(2)
		
		textField.typeText(exchangeAmount)
		SharedHelpers.shared.tapToolBarDoneButton()
		SharedHelpers.shared.tapPrimaryButton()
		
		if app.staticTexts["Price impact notice"].exists || app.staticTexts["Price impact warning"].exists {
			SharedHelpers.shared.tapPrimaryButton()
		}
		
		
		// Check confirm details
		SharedHelpers.shared.waitForStaticText("Confirm details", inApp: app, delay: 30)
		
		let tokenLabel = app.staticTexts["primary-amount"].label
		
		XCTAssert(tokenLabel == "\(exchangeAmount) tzBTC", "\(tokenLabel) != \(exchangeAmount) tzBTC")
		SharedHelpers.shared.tapPrimaryButton()
		
		
		// Wait for "wait" screen to show
		SharedHelpers.shared.waitForStaticText("This transaction will continue to process in the background if you navigate away from this screen.", inApp: app, delay: 30)
		
		
		// Wait for success screen
		SharedHelpers.shared.waitForStaticText("Success!", inApp: app, delay: 600)
		SharedHelpers.shared.tapLinkButton()
		sleep(10)
		
		
		// Grab new token amount from home screen
		let updatedTokenAmount = firstCell.staticTexts["token-amount-label"].label
		
		XCTAssert(updatedTokenAmount != "0")
		XCTAssert(tokenLabel != "0")
		
		let previousAmountAsDecimal = Decimal(string: previousTokenAmount.replacingOccurrences(of: ",", with: "")) ?? 0
		let updatedAmountAsDecimal = Decimal(string: updatedTokenAmount.replacingOccurrences(of: ",", with: "")) ?? 0
		let confirmationAmountAsDecimal = Decimal(string: tokenLabel.replacingOccurrences(of: ",", with: "")) ?? 0
		
		XCTAssert(updatedAmountAsDecimal != 0)
		XCTAssert(confirmationAmountAsDecimal != 0)
		
		let expected = (previousAmountAsDecimal - confirmationAmountAsDecimal)
		
		// Its fine if we get back more than expected (due to exchange rates), not ok to get less
		XCTAssert(updatedAmountAsDecimal >= expected, "\(updatedAmountAsDecimal) is not >= \(expected)")
	}
	
	func test03_AdvancedOptions() {
		SharedHelpers.shared.makeSureWeAreLoggedInto(address: SharedHelpers.address1_linear, withMnemonic: SharedHelpers.mnemonic1, hd: false, passphrase: nil, derivationPath: nil)
		
		let app = XCUIApplication()
		
		// Go to exchange, tap advanced options
		app.tabBars.children(matching: .button).element(boundBy: 1).tap()
		SharedHelpers.shared.tapLinkButton()
		
		let slippageButton1 = app.buttons["slippage-button-1"]
		let slippageButton2 = app.buttons["slippage-button-2"]
		var isFirstSelected = false
		
		// Change the slippage value, via buttons
		if !slippageButton1.isSelected {
			isFirstSelected = true
			slippageButton1.tap()
			
		} else if !slippageButton2.isSelected {
			isFirstSelected = false
			slippageButton2.tap()
		}
		app.buttons["close-button"].tap()
		sleep(2)
		
		
		// Check the same option is still selected
		SharedHelpers.shared.tapLinkButton()
		
		if isFirstSelected {
			XCTAssert(slippageButton1.isSelected)
		} else {
			XCTAssert(slippageButton2.isSelected)
		}
		
		
		// Change the slippage value, via textfield
		let slippageTextfield = app.textFields["slippage-textfield"]
		slippageTextfield.tap()
		
		app.keyboards.keys["Delete"].tap()
		app.keyboards.keys["Delete"].tap()
		app.keyboards.keys["Delete"].tap()
		
		slippageTextfield.typeText("0.3")
		SharedHelpers.shared.tapToolBarDoneButton()
		app.buttons["close-button"].tap()
		sleep(2)
		
		
		// slippage in textfield remain present
		SharedHelpers.shared.tapLinkButton()
		
		XCTAssert(!slippageButton1.isSelected)
		XCTAssert(!slippageButton2.isSelected)
		XCTAssert(slippageTextfield.value as? String == "0.3")
		
		
		// Change the timeout
		let timeoutTextfield = app.textFields["timeout-textfield"]
		timeoutTextfield.tap()
		
		app.keyboards.keys["Delete"].tap()
		app.keyboards.keys["Delete"].tap()
		
		let random = Int.random(in: 5 ..< 30)
		let newTimeout = "\(random)"
		
		timeoutTextfield.typeText(newTimeout)
		SharedHelpers.shared.tapToolBarDoneButton()
		app.buttons["close-button"].tap()
		sleep(2)
		
		SharedHelpers.shared.tapLinkButton()
		
		// Verify the value has been saved
		let enteredValue = timeoutTextfield.value as? String
		XCTAssert((enteredValue ?? "") == newTimeout, "\(String(describing: enteredValue)) != \(newTimeout)")
	}
	
	func test04_priceImpact() {
		SharedHelpers.shared.makeSureWeAreLoggedInto(address: SharedHelpers.address1_linear, withMnemonic: SharedHelpers.mnemonic1, hd: false, passphrase: nil, derivationPath: nil)
		
		let app = XCUIApplication()
		
		// Go to exchange
		app.tabBars.children(matching: .button).element(boundBy: 1).tap()
		
		
		// Enter an amount
		SharedHelpers.shared.tapMaxButton()
		SharedHelpers.shared.tapPrimaryButton()
		
		SharedHelpers.shared.waitForStaticText("price-impact-title", inApp: app, delay: 5)
	}
	
	
	
	// MARK: Helper functions -
	
	func verifyTokenDropdownWorks(withApplication app: XCUIApplication) {
		app.buttons["exchange-to-token-dropdown"].tap()
		sleep(2)
		
		XCTAssert(app.staticTexts["Select token"].exists)
		XCTAssert(app.staticTexts["USDtz"].exists)
		
		app.buttons["modal-close-button"].tap()
		sleep(2)
	}
	
	func verifyNetworkFeeAndLiquidityFeeDetails(withApplication app: XCUIApplication) {
		
		// Go into Network fee details
		app.buttons["network-fee-button"].tap()
		sleep(2)
		
		// Check that at least baker fee and total are present
		XCTAssert(app.staticTexts["Baker fee:"].exists)
		XCTAssert(app.staticTexts["Total:"].exists)
		
		// Check each amount is greater than 0
		let feeLabelQuery = app.staticTexts["fee-amount-label"].children(matching: .staticText)
		for i in 0..<feeLabelQuery.count {
			let feeAmount = feeLabelQuery.element(boundBy: i).label
			let feeAmountAsDecimal = Decimal(string: feeAmount.components(separatedBy: " ").first ?? "0") ?? 0
			
			XCTAssert(feeAmountAsDecimal > 0, "Index \(i) failed")
		}
		
		// Bring up the modal and check that at least the title is displayed
		app.otherElements["fee-info-view"].tap()
		sleep(2)
		
		XCTAssert(app.staticTexts["Tezos network fees"].exists)
		app.buttons["modal-close-button"].tap()
		
		// Go back to confirm details
		app.navigationBars.firstMatch.buttons["Back"].tap()
		SharedHelpers.shared.waitForStaticText("Confirm details", inApp: app, delay: 30)
		
		
		
		// Now do the same for liquidity
		app.buttons["liquidity-fee-button"].tap()
		sleep(2)
		
		XCTAssert(app.staticTexts["Total:"].exists)
		let feeAmount2 = app.staticTexts["fee-amount-label"].label
		let feeAmountAsDecimal2 = Decimal(string: feeAmount2.components(separatedBy: " ").first ?? "0") ?? 0
		
		XCTAssert(feeAmountAsDecimal2 > 0)
		
		// Bring up the modal and check that at least the title is displayed
		app.otherElements["fee-info-view"].tap()
		sleep(2)
		
		XCTAssert(app.staticTexts["Exchange liquidity"].exists)
		app.buttons["modal-close-button"].tap()
		
		// Go back to confirm details
		app.navigationBars.firstMatch.buttons["Back"].tap()
		SharedHelpers.shared.waitForStaticText("Confirm details", inApp: app, delay: 30)
	}
}
