//
//  UITest01_OnboardingTests.swift
//  MagmaWalletUITests
//
//  Created by Simon Mcloughlin on 24/06/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest

class UITest01_OnboardingTests: XCTestCase {
	
	// MARK: - Setup
    override func setUpWithError() throws {
        continueAfterFailure = false
		
		SharedHelpers.shared.application().launch()
    }

    override func tearDownWithError() throws {
		
    }
	
	
	
	// MARK: - Test cases
	
	func test01_CreateNewWallet() {
		try! SharedHelpers.shared.unpairWalletIfPaired()
		
		
		// Check if we are on the welcome screen, or on the choose type after the warning screen
		let app = XCUIApplication()
		if app.buttons["CREATE NEW WALLET"].exists {
			// Create new wallet
			SharedHelpers.shared.tapPrimaryButton()
		}
		
		
		// Check modal popup
		app.navigationBars["MagmaWallet.WalletTypeChoiceView"].buttons["Info"].tap()
		sleep(2)
		
		XCTAssert(app.staticTexts["Why use (or not use) a passphrase?"].exists)
		app.buttons["close-button"].tap()
		sleep(2)
		
		
		// Choose without password
		app.buttons["without-password"].tap()
		sleep(2)
		
		
		// Check recovery phrase confirmation and pin are successful
		handleRecoveryPhraseAndPin(app: app, withPassphrase: false)
		verifyHomeEmpty(app: app)
	}
	
	func test02_CreateNewWalletWithPassphrase() {
		try! SharedHelpers.shared.unpairWalletIfPaired()
		
		
		// Create new wallet
		SharedHelpers.shared.tapPrimaryButton()
		
		let app = XCUIApplication()
		app.buttons["with-password"].tap()
		sleep(2)
		
		// Enter password
		let textfield = app.secureTextFields["new-wallet-passphrase"]
		textfield.tap()
		sleep(2)
		
		textfield.typeText(SharedHelpers.passphrase)
		SharedHelpers.shared.tapKeyboardDoneButton()
		sleep(2)
		
		SharedHelpers.shared.tapPrimaryButton()
		
		
		// Check recovery phrase confirmation and pin are successful
		handleRecoveryPhraseAndPin(app: app, withPassphrase: true)
		verifyHomeEmpty(app: app)
	}
	
	func test03_CreateNewWalletAfterWarning() {
		let app = XCUIApplication()
		goBackToWelcome(app: app)
		
		// Test attempting to create new wallet
		SharedHelpers.shared.tapPrimaryButton()
		XCTAssertTrue(app.staticTexts["Your device is already paired with a wallet on the network."].exists)
		
		let yesContinueButton = app.buttons["YES, CONTINUE"]
		yesContinueButton.tap()
		sleep(1)
		
		let alert = app.alerts["Are you sure?"]
		alert.buttons["Cancel"].tap()
		sleep(1)
		
		SharedHelpers.shared.tapPrimaryButton()
		yesContinueButton.tap()
		alert.buttons["Yes, Continue"].tap()
		sleep(1)
		
		test01_CreateNewWallet()
	}
	
	func test04_ImportLinearWallet() {
		try! SharedHelpers.shared.unpairWalletIfPaired()
		let app = XCUIApplication()
		
		importWalletAndVerifyAddress(app: app, mnemonic: SharedHelpers.mnemonic1, expectedAddress: SharedHelpers.address1_linear, hd: false, passphrase: nil, derivationPath: nil)
	}
	
	func test05_ImportHDWalletWithPassphrase() {
		try! SharedHelpers.shared.unpairWalletIfPaired()
		let app = XCUIApplication()
		
		importWalletAndVerifyAddress(app: app, mnemonic: SharedHelpers.mnemonic1, expectedAddress: SharedHelpers.address1_hd, hd: true, passphrase: SharedHelpers.passphrase, derivationPath: nil)
	}
	
	func test06_ImportHDWalletWithPassphraseAndDerivationPath() {
		try! SharedHelpers.shared.unpairWalletIfPaired()
		let app = XCUIApplication()
		
		importWalletAndVerifyAddress(app: app, mnemonic: SharedHelpers.mnemonic2, expectedAddress: SharedHelpers.address2_hd, hd: true, passphrase: SharedHelpers.passphrase, derivationPath: SharedHelpers.derivationPath)
	}
	
	func test07_ImportLinearWalletAfterWarning() {
		let app = XCUIApplication()
		goBackToWelcome(app: app)
		
		
		// Test attempting to import wallet
		SharedHelpers.shared.tapLinkButton()
		XCTAssertTrue(app.staticTexts["Your device is already paired with a wallet on the network."].exists)
		
		let yesContinueButton = app.buttons["YES, CONTINUE"]
		yesContinueButton.tap()
		sleep(1)
		
		let alert = app.alerts["Are you sure?"]
		alert.buttons["Cancel"].tap()
		sleep(1)
		
		SharedHelpers.shared.tapLinkButton()
		yesContinueButton.tap()
		alert.buttons["Yes, Continue"].tap()
		sleep(1)
		
		
		// Check modal popup
		app.navigationBars["MagmaWallet.RecoveryPhraseEntryView"].buttons["Info"].tap()
		sleep(2)
		
		XCTAssert(app.staticTexts["Import / Recovery help"].exists)
		app.buttons["close-button"].tap()
		sleep(2)
		
		
		// Continue with import
		importWalletAndVerifyAddress(app: app, mnemonic: SharedHelpers.mnemonic1, expectedAddress: SharedHelpers.address1_linear, hd: false, passphrase: nil, derivationPath: nil)
		sleep(2)
		
		
		// Check token details functionality, while logged into a funded account
		verifyTokenDetails(app: app)
	}
	
	
	
	// MARK: - Flow functions
	
	func handleRecoveryPhraseAndPin(app: XCUIApplication, withPassphrase: Bool) {
		// Show recovery phrase
		SharedHelpers.shared.tapPrimaryButton()
		
		
		// Capture recovery phrase
		var recoveryPhrase = ""
		for index in 1...12 {
			let mnemonicWord = app.staticTexts["mnemonic-\(index)"].label
			let components = mnemonicWord.components(separatedBy: " ") // get the word after the number and space
			recoveryPhrase.append(components.last ?? "")
			
			if index != 12 {
				recoveryPhrase.append(" ")
			}
		}
		
		
		// Continue to confirmation
		SharedHelpers.shared.tapPrimaryButton()
		
		let textview = app.textViews["import-textview"]
		textview.tap()
		sleep(2)
		
		textview.typeText(recoveryPhrase)
		SharedHelpers.shared.tapKeyboardDoneButton()
		sleep(2)
		
		
		// Check if the passphrase needs to be entered
		if withPassphrase {
			let textfield = app.secureTextFields["import-passphrase"]
			textfield.tap()
			sleep(2)
			
			textfield.typeText(SharedHelpers.passphrase)
			SharedHelpers.shared.tapKeyboardDoneButton()
			sleep(2)
		}
		
		
		// Continue to pin prompt
		SharedHelpers.shared.tapPrimaryButton()
		
		createNewPin(app: app)
	}
	
	func createNewPin(app: XCUIApplication) {
		SharedHelpers.shared.tapPrimaryButton()
		
		
		// Enter pincode
		SharedHelpers.shared.enterPinCode()
		
		
		// Confirm pincode
		SharedHelpers.shared.enterPinCode()
		
		
		// Skip biometric
		app.buttons["Maybe later"].tap()
		sleep(1)
		
		app.buttons["TAKE ME TO MY WALLET"].tap()
		sleep(1)
	}
	
	func verifyHomeEmpty(app: XCUIApplication) {
		let text = "Your wallet is empty"
		SharedHelpers.shared.waitForStaticText(text, inApp: app, delay: 35)
		XCTAssertTrue(app.tables.cells.staticTexts[text].exists)
	}
	
	func verifyHomeAndUsingAddress(app: XCUIApplication, address: String) {
		let text = "Your wallet is empty"
		SharedHelpers.shared.waitForStaticText(text, inApp: app, delay: 35)
		XCTAssertTrue(app.tables.cells.staticTexts[text].exists)
	}
	
	func goBackToWelcome(app: XCUIApplication) {
		app.buttons["Back"].tap()
		
		sleep(3)
		XCTAssertTrue(app.buttons["Login"].exists)
	}
	
	func importWalletAndVerifyAddress(app: XCUIApplication, mnemonic: String, expectedAddress: String, hd: Bool, passphrase: String?, derivationPath: String?) {
		
		if app.buttons["Import or recover a wallet"].exists {
			// Import wallet from welcome page
			SharedHelpers.shared.tapLinkButton()
		}
		
		
		// Enter mnemonic
		let textview = app.textViews["import-textview"]
		textview.tap()
		sleep(2)
		
		textview.typeText(mnemonic)
		SharedHelpers.shared.tapKeyboardDoneButton()
		
		
		// Check if we need to enter passphrase
		if let pass = passphrase {
			let textfield = app.secureTextFields["import-passphrase"]
			textfield.tap()
			sleep(2)
			
			textfield.typeText(pass)
			SharedHelpers.shared.tapKeyboardDoneButton()
		}
		
		
		// Check if we need to enter derivation path
		if let path = derivationPath {
			
			app.buttons["advanced-options-button"].tap()
			sleep(1)
			
			let textfield = app.textFields["derivation-path-textfield"]
			textfield.tap()
			sleep(2)
			
			
			let userSideOfDerivation = path.components(separatedBy: "1729'/").last ?? ""
			
			let deleteKey = app.keyboards.keys["delete"]
			deleteKey.tap()
			deleteKey.tap()
			deleteKey.tap()
			deleteKey.tap()
			deleteKey.tap()
			
			textfield.typeText(userSideOfDerivation)
			SharedHelpers.shared.tapKeyboardDoneButton()
		}
		
		
		// Tap continue
		SharedHelpers.shared.tapPrimaryButton()
		
		
		if derivationPath == nil {
			// wait for balance screen
			SharedHelpers.shared.waitForStaticText("import-balance-address", inApp: app, delay: 35)
			
			app.tables.cells.element(boundBy: hd ? 0 : 1).tap()
			sleep(2)
			
		} else {
			sleep(2)
		}
		
		SharedHelpers.shared.tapPrimaryButton()
		
		createNewPin(app: app)
		
		let walletAddress = SharedHelpers.shared.getCurrentWalletAddressFromWalletScreen()
		XCTAssert(walletAddress == expectedAddress, walletAddress)
	}
	
	func verifyTokenDetails(app: XCUIApplication) {
		
		// Check homepage has an XTZ balance
		let homeTotal = app.staticTexts[SharedHelpers.IdentifierConstants.home_totalXTZ]
		let balanceString = homeTotal.label
		let xtzAmountAsNumber = Decimal(string: balanceString.components(separatedBy: " ").first ?? "0") ?? 0
		XCTAssert(xtzAmountAsNumber > 0, "\(xtzAmountAsNumber)")
		
		
		// Tap on XTZ to open XTZ details
		homeTotal.tap()
		sleep(2)
		
		
		// Verify header content is present
		let tokenDetailsTotal = app.staticTexts[SharedHelpers.IdentifierConstants.tokenDetails_tokenBalance]
		let tokenDetailsSymbol = app.staticTexts[SharedHelpers.IdentifierConstants.tokenDetails_tokenSymbol]
		
		let tokenAmountAsNumber = Decimal(string: tokenDetailsTotal.label) ?? 0
		XCTAssert(tokenAmountAsNumber > 0, "\(tokenAmountAsNumber)")
		XCTAssert(tokenDetailsSymbol.label == "XTZ", tokenDetailsSymbol.label)
		
		
		// Verify that there is at least 1 valid transaction history item displayed
		let tablesQuery = app.tables
		let typeLabels = tablesQuery.staticTexts.matching(identifier: SharedHelpers.IdentifierConstants.activityCell_type)
		var foundValidTransaction = false
		
		for i in 0...typeLabels.count {
			let transactionLabel = typeLabels.element(boundBy: i)
			
			if (transactionLabel.label.contains("RECEIVE") || transactionLabel.label.contains("SEND") || transactionLabel.label.contains("EXCHANGE") || transactionLabel.label == "BAKING REWARD") {
				foundValidTransaction = true
				break
			}
		}
		XCTAssert(foundValidTransaction)
		
		
		// Back out
		app.navigationBars["Tezos"].buttons["Back"].tap()
		sleep(2)
		
		
		// Tap on tzBTC to view details
		tablesQuery.staticTexts["tzBTC"].tap()
		sleep(2)
		
		
		// Verify header content is present
		let tokenDetailsTotal2 = app.staticTexts[SharedHelpers.IdentifierConstants.tokenDetails_tokenBalance]
		let tokenDetailsSymbol2 = app.staticTexts[SharedHelpers.IdentifierConstants.tokenDetails_tokenSymbol]
		
		let tokenAmountAsNumber2 = Decimal(string: tokenDetailsTotal2.label) ?? 0
		XCTAssert(tokenAmountAsNumber2 > 0, "\(tokenAmountAsNumber2)")
		XCTAssert(tokenDetailsSymbol2.label == "tzBTC", tokenDetailsSymbol2.label)
		
		
		// Verify that there is at least 1 valid transaction history item displayed
		let typeLabels2 = tablesQuery.staticTexts.matching(identifier: SharedHelpers.IdentifierConstants.activityCell_type)
		foundValidTransaction = false
		
		for i in 0...typeLabels2.count {
			let transactionLabel = typeLabels2.element(boundBy: i)
			
			if (transactionLabel.label.contains("RECEIVE") || transactionLabel.label.contains("SEND") || transactionLabel.label.contains("EXCHANGE") || transactionLabel.label == "BAKING REWARD") {
				foundValidTransaction = true
				break
			}
		}
		XCTAssert(foundValidTransaction)
	}
}
