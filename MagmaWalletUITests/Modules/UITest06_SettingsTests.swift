//
//  UITest06_SettingsTests.swift
//  MagmaWalletUITests
//
//  Created by Simon Mcloughlin on 05/08/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest

class UITest06_SettingsTests: XCTestCase {
	
	// MARK: - Setup
    override func setUpWithError() throws {
        continueAfterFailure = false
		
		SharedHelpers.shared.application().launch()
    }

    override func tearDownWithError() throws {
		
    }
	
	
	
	// MARK: - Test cases
	func test01_EditPin() {
		SharedHelpers.shared.makeSureWeAreLoggedInto(address: SharedHelpers.address1_linear, withMnemonic: SharedHelpers.mnemonic1, hd: false, passphrase: nil, derivationPath: nil)
		
		let app = XCUIApplication()
		app.tabBars.children(matching: .button).element(boundBy: 4).tap()
		sleep(2)
		
		
		// Enter current pin and set to new pin
		app.tables.staticTexts["Edit PIN"].tap()
		sleep(2)
		
		SharedHelpers.shared.enterPinCode()
		SharedHelpers.shared.enterUpdatedPinCode()
		SharedHelpers.shared.enterUpdatedPinCode()
		
		
		// Reset pin back to old pin
		app.tables.staticTexts["Edit PIN"].tap()
		sleep(2)
		
		SharedHelpers.shared.enterUpdatedPinCode()
		SharedHelpers.shared.enterPinCode()
		SharedHelpers.shared.enterPinCode()
	}
	
	func test02_ExternalLinks() {
		let app = XCUIApplication()
		let safari = XCUIApplication(bundleIdentifier: "com.apple.mobilesafari")
		
		if app.frame.size.width <= 320 {
			// Hack to detect iOS 12 simulator. iOS 12 can't see to get the URL from safari and crashes the test. Skipping in that case
			XCTAssert(app.frame.size.width <= 320)
			return
		}
		
		SharedHelpers.shared.makeSureWeAreLoggedInto(address: SharedHelpers.address1_linear, withMnemonic: SharedHelpers.mnemonic1, hd: false, passphrase: nil, derivationPath: nil)
		
		
		// Go to settings
		app.tabBars.children(matching: .button).element(boundBy: 4).tap()
		
		
		let tablesQuery = app.tables
		let urlTextField = safari.textFields["URL"]
		
		
		// Check About goes to correct web page
		tablesQuery.staticTexts["About Magma"].tap()
		sleep(2)
		
		safari.buttons["URL"].tap()
		urlTextField.tap()
		
		var url = urlTextField.value as! String 
		XCTAssert(url == "https://magmawallet.io/about/", url)
		
		app.activate()
		sleep(2)
		SharedHelpers.shared.enterPinCodeWithoutContinueButton()
		
		
		
		// Check Share feedback goes to correct web page
		tablesQuery.staticTexts["Support & Feedback"].tap()
		sleep(2)
		
		safari.buttons["URL"].tap()
		urlTextField.tap()
		
		url = urlTextField.value as! String
		XCTAssert(url == "https://docs.google.com/forms/d/e/1FAIpQLSdynaHhnvnJ_lu_00O8bUYBjpvgNMMc3-AarfG9uBKULOCPrQ/viewform", url)
		
		app.activate()
		sleep(2)
		SharedHelpers.shared.enterPinCodeWithoutContinueButton()
		
		
		
		// Check Privacy Policy goes to correct web page
		tablesQuery.staticTexts["Privacy Policy"].tap()
		sleep(2)
		
		safari.buttons["URL"].tap()
		urlTextField.tap()
		
		url = urlTextField.value as! String
		XCTAssert(url == "https://magmawallet.io/ios/privacy-policy/", url)
		
		app.activate()
		sleep(2)
		SharedHelpers.shared.enterPinCodeWithoutContinueButton()
		
		
		
		// Check Terms and conditions goes to correct web page
		tablesQuery.staticTexts["Terms & Conditions"].tap()
		sleep(2)
		
		safari.buttons["URL"].tap()
		urlTextField.tap()
		
		url = urlTextField.value as! String
		XCTAssert(url == "https://magmawallet.io/ios/terms/", url)
		
		app.activate()
		sleep(2)
		SharedHelpers.shared.enterPinCodeWithoutContinueButton()
	}
	
	func test03_ShowRecoveryPhrase() {
		SharedHelpers.shared.makeSureWeAreLoggedInto(address: SharedHelpers.address1_linear, withMnemonic: SharedHelpers.mnemonic1, hd: false, passphrase: nil, derivationPath: nil)
		
		let app = XCUIApplication()
		app.tabBars.children(matching: .button).element(boundBy: 4).tap()
		sleep(2)
		
		// Go to recovery phrase
		app.tables.staticTexts["Show Recovery Phrase"].tap()
		sleep(2)
		
		// Make sure the title is on the screen
		XCTAssert(app.staticTexts["Recovery Phrase"].exists)
		
		// Wait for the timeout and make sure it out returns
		sleep(35)
		XCTAssert(app.navigationBars["SETTINGS"].exists)
	}
	
	func test04_Delegation() {
		SharedHelpers.shared.makeSureWeAreLoggedInto(address: SharedHelpers.address1_linear, withMnemonic: SharedHelpers.mnemonic1, hd: false, passphrase: nil, derivationPath: nil)
		
		let app = XCUIApplication()
		app.tabBars.children(matching: .button).element(boundBy: 4).tap()
		sleep(2)
		
		let tablesQuery = app.tables
		tablesQuery.staticTexts["Baker Delegation"].tap()
		sleep(2)
		
		
		// Open bakers list
		app.buttons["dropdown-button"].tap()
		
		
		// Check if we are already delegated and undelegate if so
		if tablesQuery.buttons["undelegate-button"].exists {
			undelegateFlow(withApplication: app)
		}
		
		
		// Tap first baker
		tablesQuery.cells.firstMatch.tap()
		SharedHelpers.shared.tapPrimaryButton()
		
		
		// If we get minimum delegation notice, just move on
		if app.staticTexts["delegation-min-notice-title"].exists {
			SharedHelpers.shared.tapPrimaryButton()
		}
		
		
		// Wait for the loading screen
		SharedHelpers.shared.waitForStaticText("This transaction will continue to process in the background if you navigate away from this screen.", inApp: app, delay: 60)
		
		
		// Wait for the success screen
		SharedHelpers.shared.waitForStaticText("Baker added!", inApp: app, delay: 600)
		SharedHelpers.shared.tapPrimaryButton()
		
		
		// Verify we are back on settings
		XCTAssert(app.navigationBars["SETTINGS"].exists)
	}
	
	func undelegateFlow(withApplication app: XCUIApplication) {
		app.tables.buttons["undelegate-button"].tap()
		sleep(1)
		
		app.buttons["modal-close-button"].tap()
		SharedHelpers.shared.tapPrimaryButton()
		
		
		// If we get minimum delegation notice, just move on
		if app.staticTexts["delegation-min-notice-title"].exists {
			SharedHelpers.shared.tapPrimaryButton()
		}
		
		
		// Wait for the loading screen
		SharedHelpers.shared.waitForStaticText("This transaction will continue to process in the background if you navigate away from this screen.", inApp: app, delay: 60)
		
		
		// Wait for the success screen
		SharedHelpers.shared.waitForStaticText("Baker removed!", inApp: app, delay: 600)
		SharedHelpers.shared.tapPrimaryButton()
		
		
		// Verify we are back on settings
		XCTAssert(app.navigationBars["SETTINGS"].exists)
		
		
		// Go back in to delegation and open list
		app.tables.staticTexts["Baker Delegation"].tap()
		app.buttons["dropdown-button"].tap()
	}
	
	func test05_DelegationError() {
		SharedHelpers.shared.makeSureWeAreLoggedInto(address: SharedHelpers.address1_linear, withMnemonic: SharedHelpers.mnemonic1, hd: false, passphrase: nil, derivationPath: nil)
		
		let app = XCUIApplication()
		app.tabBars.children(matching: .button).element(boundBy: 4).tap()
		sleep(2)
		
		let tablesQuery = app.tables
		tablesQuery.staticTexts["Baker Delegation"].tap()
		sleep(2)
		
		
		// Open bakers list
		app.buttons["dropdown-button"].tap()
		sleep(2)
		
		
		let textfield = app.textFields["baker-address-textfield"]
		textfield.tap()
		sleep(2)
		
		textfield.typeText("tz1abcdefghijklmnopqrstuvwxyz1234567")
		sleep(1)
		
		app.buttons["modal-close-button"].tap()
		SharedHelpers.shared.tapPrimaryButton()
		
		
		SharedHelpers.shared.waitForStaticText("Invalid baker address", inApp: app, delay: 30)
		SharedHelpers.shared.tapPrimaryButton()
	}
	
	func test06_Attribution() {
		SharedHelpers.shared.makeSureWeAreLoggedInto(address: SharedHelpers.address1_linear, withMnemonic: SharedHelpers.mnemonic1, hd: false, passphrase: nil, derivationPath: nil)
		
		let app = XCUIApplication()
		app.tabBars.children(matching: .button).element(boundBy: 4).tap()
		sleep(2)
		
		let tablesQuery = app.tables
		tablesQuery.staticTexts["Attributions"].tap()
		sleep(2)
		
		XCTAssert(app.staticTexts["Baking Bad"].exists)
		XCTAssert(app.staticTexts["Better Call Dev"].exists)
		XCTAssert(app.staticTexts["CoinGecko"].exists)
		XCTAssert(app.staticTexts["TzKT"].exists)
	}
	
	func test07_CurrencyChange() {
		SharedHelpers.shared.makeSureWeAreLoggedInto(address: SharedHelpers.address1_linear, withMnemonic: SharedHelpers.mnemonic1, hd: false, passphrase: nil, derivationPath: nil)
		
		let app = XCUIApplication()
		app.tabBars.children(matching: .button).element(boundBy: 4).tap()
		sleep(2)
		
		let tablesQuery = app.tables
		tablesQuery.staticTexts["Local currency"].tap()
		sleep(2)
		
		
		// Select USD and check it updated
		tablesQuery.children(matching: .cell).element(boundBy: 0).tap()
		sleep(10)
		
		app.tabBars.children(matching: .button).firstMatch.tap()
		sleep(2)
		
		let totalCurrencyAmount = app.staticTexts["home-total-currency-amount"].label
		let symbol = String(totalCurrencyAmount.prefix(1))
		XCTAssert(symbol == "$")
		
		
		// Switch back to EUR and check it updated
		app.tabBars.children(matching: .button).element(boundBy: 4).tap()
		sleep(2)
		
		tablesQuery.staticTexts["Local currency"].tap()
		sleep(2)
		
		tablesQuery.children(matching: .cell).element(boundBy: 1).tap()
		sleep(10)
		
		app.tabBars.children(matching: .button).firstMatch.tap()
		sleep(2)
		
		let totalCurrencyAmount2 = app.staticTexts["home-total-currency-amount"].label
		let symbol2 = String(totalCurrencyAmount2.prefix(1))
		XCTAssert(symbol2 == "€")
	}
}
