//
//  UITest03_SendTests.swift
//  MagmaWalletUITests
//
//  Created by Simon Mcloughlin on 25/06/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest

class UITest03_SendTests: XCTestCase {
	
	// MARK: - Setup
    override func setUpWithError() throws {
        continueAfterFailure = false
		
		SharedHelpers.shared.application().launch()
    }

    override func tearDownWithError() throws {
		
    }
	
	
	
	// MARK: - Test cases
	
	// These tests need to be named alphabetically to ensure they are run in order.
	// These tests are running on the live tesetnet outside of our control. We do not have the ability to automatically fill these accounts with tokens.
	// Accounts have been filled and need to send, and send back to ensure the run smoothly for a long period of time
	func test01_SendXTZ() {
		SharedHelpers.shared.makeSureWeAreLoggedInto(address: SharedHelpers.address1_linear, withMnemonic: SharedHelpers.mnemonic1, hd: false, passphrase: nil, derivationPath: nil)
		
		
		let randomDecimal = Int.random(in: 1 ..< 10000)
		let sendingAmount = "1.\(randomDecimal)"
		var networkFee = ""
		
		let app = XCUIApplication()
		let currentBalanceLabel = app.staticTexts[SharedHelpers.IdentifierConstants.home_totalXTZ].label
		let currentBalance = currentBalanceLabel.components(separatedBy: " ").first ?? "0"
		
		
		
		// Open send screen
		app.buttons["Send Enabled"].tap()
		sleep(2)
		
		
		// Enter address
		let addressTextField = app.textFields.containing(.button, identifier:"QR Light").element
		addressTextField.tap()
		addressTextField.typeText(SharedHelpers.address1_hd)
		SharedHelpers.shared.tapKeyboardDoneButton()
		SharedHelpers.shared.tapPrimaryButton()
		sleep(2)
		
		
		// Choose XTZ
		app.tables.staticTexts["XTZ"].tap()
		sleep(2)
		
		
		// Enter amount
		let amountTextField = app.textFields["0"]
		amountTextField.tap()
		amountTextField.typeText(sendingAmount)
		SharedHelpers.shared.tapPrimaryButton()
		sleep(2)
		
		
		// Check confirm details
		SharedHelpers.shared.waitForStaticText("Confirm details", inApp: app, delay: 30)
		
		let amountToSendLabelText = app.staticTexts["amount-to-send-label"].label
		let sendingAmountAsDecimal = Decimal(string: sendingAmount.replacingOccurrences(of: ",", with: "")) ?? 0
		XCTAssert(amountToSendLabelText == (sendingAmountAsDecimal.description + " XTZ"))
		
		let networkFeeLabelText = app.staticTexts["network-fee-label"].label
		networkFee = networkFeeLabelText.components(separatedBy: " ").first ?? "0"
		
		SharedHelpers.shared.tapPrimaryButton()
		
		
		// Wait for "wait" screen to show
		SharedHelpers.shared.waitForStaticText("This transaction will continue to process in the background if you navigate away from this screen.", inApp: app, delay: 30)
		
		
		// Wait for success screen
		SharedHelpers.shared.waitForStaticText("Success!", inApp: app, delay: 600)
		SharedHelpers.shared.tapPrimaryButton()
		sleep(2)
		
		
		// Compare with whats on screen now
		let homeXTZBalanceLabelText = app.staticTexts[SharedHelpers.IdentifierConstants.home_totalXTZ].label
		let updatedHomeBalance = homeXTZBalanceLabelText.components(separatedBy: " ").first ?? "0"
		
		
		// Check we have all the data
		XCTAssert(currentBalance != "0")
		XCTAssert(updatedHomeBalance != "0")
		XCTAssert(networkFee != "0")
		
		
		// Check converting to decimals works ok
		let currentBalanceAsDecimal = Decimal(string: currentBalance.replacingOccurrences(of: ",", with: "")) ?? 0
		let updatedHomeBalanceAsDecimal = Decimal(string: updatedHomeBalance.replacingOccurrences(of: ",", with: "")) ?? 0
		let networkFeeAsDecimal = Decimal(string: networkFee.replacingOccurrences(of: ",", with: "")) ?? 0
		
		XCTAssert(currentBalanceAsDecimal != 0)
		XCTAssert(updatedHomeBalanceAsDecimal != 0)
		XCTAssert(sendingAmountAsDecimal != 0)
		XCTAssert(networkFeeAsDecimal != 0)
		
		
		// Check homepage has the expected balance displayed
		let expected = (currentBalanceAsDecimal - (sendingAmountAsDecimal + networkFeeAsDecimal))
		XCTAssert(expected.description == updatedHomeBalanceAsDecimal.description, "\(expected.description) != \(updatedHomeBalanceAsDecimal.description)")
		
		
		// Verify event shows up in activity
		verifyActivityContains(amountString: "-\(sendingAmountAsDecimal) XTZ", inApp: app)
	}
	
	func test02_SendToken() {
		SharedHelpers.shared.makeSureWeAreLoggedInto(address: SharedHelpers.address1_linear, withMnemonic: SharedHelpers.mnemonic1, hd: false, passphrase: nil, derivationPath: nil)
		
		
		let randomDecimal = Int.random(in: 1 ..< 100)
		let sendingAmount = "0.000000\(randomDecimal)"
		var networkFee = ""
		
		let app = XCUIApplication()
		let currentXTZBalanceLabel = app.staticTexts[SharedHelpers.IdentifierConstants.home_totalXTZ].label
		let currentXTZBalance = currentXTZBalanceLabel.components(separatedBy: " ").first ?? "0"
		let currentTokenBalance = app.tables.staticTexts["token-amount-label"].label
		
		
		
		// Open send screen
		app.buttons["Send Enabled"].tap()
		sleep(2)
		
		
		// Enter address
		let addressTextField = app.textFields.containing(.button, identifier:"QR Light").element
		addressTextField.tap()
		addressTextField.typeText(SharedHelpers.address1_hd)
		SharedHelpers.shared.tapKeyboardDoneButton()
		SharedHelpers.shared.tapPrimaryButton()
		sleep(2)
		
		
		// Choose Token
		app.tables.staticTexts["tzBTC"].tap()
		sleep(2)
		
		
		// Enter amount
		let amountTextField = app.textFields["0"]
		amountTextField.tap()
		amountTextField.typeText(sendingAmount)
		SharedHelpers.shared.tapPrimaryButton()
		sleep(2)
		
		
		// Check confirm details
		SharedHelpers.shared.waitForStaticText("Confirm details", inApp: app, delay: 30)
		
		let amountToSendLabelText = app.staticTexts["amount-to-send-label"].label
		let sendingAmountAsDecimal = Decimal(string: sendingAmount.replacingOccurrences(of: ",", with: "")) ?? 0
		XCTAssert(amountToSendLabelText == (sendingAmountAsDecimal.description + " tzBTC"))
		
		let networkFeeLabelText = app.staticTexts["network-fee-label"].label
		networkFee = networkFeeLabelText.components(separatedBy: " ").first ?? "0"
		
		SharedHelpers.shared.tapPrimaryButton()
		
		
		// Wait for "wait" screen to show
		SharedHelpers.shared.waitForStaticText("This transaction will continue to process in the background if you navigate away from this screen.", inApp: app, delay: 30)
		
		
		// Wait for success screen
		SharedHelpers.shared.waitForStaticText("Success!", inApp: app, delay: 600)
		SharedHelpers.shared.tapPrimaryButton()
		sleep(2)
		
		
		// Compare with whats on screen now
		let homeXTZBalanceLabelText = app.staticTexts[SharedHelpers.IdentifierConstants.home_totalXTZ].label
		let updatedHomeXTZBalance = homeXTZBalanceLabelText.components(separatedBy: " ").first ?? "0"
		let updatedHomeTokenBalance = app.tables.staticTexts["token-amount-label"].label
		
		
		// Check we have all the data
		XCTAssert(currentXTZBalance != "0")
		XCTAssert(currentTokenBalance != "0")
		XCTAssert(updatedHomeXTZBalance != "0")
		XCTAssert(updatedHomeTokenBalance != "0")
		XCTAssert(networkFee != "0")
		
		
		// Check converting to decimals works ok
		let currentXTZBalanceAsDecimal = Decimal(string: currentXTZBalance.replacingOccurrences(of: ",", with: "")) ?? 0
		let currentTokenBalanceAsDecimal = Decimal(string: currentTokenBalance.replacingOccurrences(of: ",", with: "")) ?? 0
		let updatedHomeXTZBalanceAsDecimal = Decimal(string: updatedHomeXTZBalance.replacingOccurrences(of: ",", with: "")) ?? 0
		let updatedHomeTokenBalanceAsDecimal = Decimal(string: updatedHomeTokenBalance.replacingOccurrences(of: ",", with: "")) ?? 0
		let networkFeeAsDecimal = Decimal(string: networkFee.replacingOccurrences(of: ",", with: "")) ?? 0
		
		XCTAssert(currentXTZBalanceAsDecimal != 0)
		XCTAssert(currentTokenBalanceAsDecimal != 0)
		XCTAssert(updatedHomeXTZBalanceAsDecimal != 0)
		XCTAssert(updatedHomeTokenBalanceAsDecimal != 0)
		XCTAssert(sendingAmountAsDecimal != 0)
		XCTAssert(networkFeeAsDecimal != 0)
		
		
		// Check homepage has the expected balance displayed
		let expected1 = (currentXTZBalanceAsDecimal - networkFeeAsDecimal)
		let expected2 = (currentTokenBalanceAsDecimal - sendingAmountAsDecimal)
		XCTAssert(expected1.description == updatedHomeXTZBalanceAsDecimal.description, "\(expected1.description) != \(updatedHomeXTZBalanceAsDecimal.description)")
		XCTAssert(expected2.description == updatedHomeTokenBalanceAsDecimal.description, "\(expected2.description) != \(updatedHomeTokenBalanceAsDecimal.description)")
		
		
		// Verify event shows up in activity
		verifyActivityContains(amountString: "-\(sendingAmountAsDecimal) tzBTC", inApp: app)
	}
	
	func test03_testMoonPayOpens() {
		SharedHelpers.shared.makeSureWeAreLoggedInto(address: SharedHelpers.address1_linear, withMnemonic: SharedHelpers.mnemonic1, hd: false, passphrase: nil, derivationPath: nil)
		let app = XCUIApplication()
		
		
		// Open send screen
		app.buttons["moonpay-button"].tap()
		sleep(2)
		
		XCTAssert(app.staticTexts["moonpay-modal-title"].exists)
		
		app.buttons["open-moonpay-button"].tap()
		sleep(5)
		
		app.buttons["Done"].tap()
	}
	
	func test04_sendErrorScreen() {
		SharedHelpers.shared.makeSureWeAreLoggedInto(address: SharedHelpers.address1_linear, withMnemonic: SharedHelpers.mnemonic1, hd: false, passphrase: nil, derivationPath: nil)
		let app = XCUIApplication()
		
		// Open send screen
		app.buttons["Send Enabled"].tap()
		sleep(2)
		
		
		// Enter fake address
		let addressTextField = app.textFields.containing(.button, identifier:"QR Light").element
		addressTextField.tap()
		addressTextField.typeText("tz1abcdefghijklmnopqrstuvwxyz1234567")
		SharedHelpers.shared.tapKeyboardDoneButton()
		SharedHelpers.shared.tapPrimaryButton()
		sleep(2)
		
		
		// Choose XTZ
		app.tables.staticTexts["XTZ"].tap()
		sleep(2)
		
		
		// Enter amount
		let amountTextField = app.textFields["0"]
		amountTextField.tap()
		amountTextField.typeText("1")
		SharedHelpers.shared.tapPrimaryButton()
		
		SharedHelpers.shared.waitForStaticText("Invalid wallet address", inApp: app, delay: 30)
		SharedHelpers.shared.tapPrimaryButton()
	}
	
	func test05_VerifyReceivedAndSendBackMax() {
		SharedHelpers.shared.makeSureWeAreLoggedInto(address: SharedHelpers.address1_hd, withMnemonic: SharedHelpers.mnemonic1, hd: true, passphrase: SharedHelpers.passphrase, derivationPath: nil)
		
		let app = XCUIApplication()
		let currentXTZBalanceLabel = app.staticTexts[SharedHelpers.IdentifierConstants.home_totalXTZ].label
		let currentXTZBalance = currentXTZBalanceLabel.components(separatedBy: " ").first ?? "0"
		let currentTokenBalance = app.tables.staticTexts["token-amount-label"].label
		
		let currentXTZBalanceAsDecimal = Decimal(string: currentXTZBalance.replacingOccurrences(of: ",", with: "")) ?? 0
		let currentTokenBalanceAsDecimal = Decimal(string: currentTokenBalance.replacingOccurrences(of: ",", with: "")) ?? 0
		
		// Verify this previously empty account, has XTZ and tzBTC in it
		XCTAssert(currentXTZBalanceAsDecimal > 0)
		XCTAssert(currentTokenBalanceAsDecimal > 0)
		
		
		// Send everything back to first account. Note, we have to send token first, so we have XTZ to pay the fees
		sendMaxToken(app: app)
		sendMaxTez(app: app)
	}
	
	
	
	// MARK: - Helpers
	func sendMaxToken(app: XCUIApplication) {
		
		// Open send screen
		app.buttons["Send Enabled"].tap()
		sleep(2)
		
		
		// Enter address
		let addressTextField = app.textFields.containing(.button, identifier:"QR Light").element
		addressTextField.tap()
		addressTextField.typeText(SharedHelpers.address1_linear)
		SharedHelpers.shared.tapKeyboardDoneButton()
		SharedHelpers.shared.tapPrimaryButton()
		sleep(2)
		
		
		// Choose XTZ
		app.tables.staticTexts["tzBTC"].tap()
		sleep(2)
		
		
		// Tap send all
		SharedHelpers.shared.tapSecondaryButton()
		SharedHelpers.shared.tapPrimaryButton()
		sleep(2)
		
		
		// Check confirm details
		SharedHelpers.shared.waitForStaticText("Confirm details", inApp: app, delay: 30)
		SharedHelpers.shared.tapPrimaryButton()
		
		
		// Wait for "wait" screen to show
		SharedHelpers.shared.waitForStaticText("This transaction will continue to process in the background if you navigate away from this screen.", inApp: app, delay: 30)
		
		
		// Wait for success screen
		SharedHelpers.shared.waitForStaticText("Success!", inApp: app, delay: 600)
		SharedHelpers.shared.tapPrimaryButton()
		sleep(2)
		
		
		// Verify all sent, and no token is display on the wallet screen
		XCTAssert(!app.tables.staticTexts["token-amount-label"].exists)
	}
	
	func sendMaxTez(app: XCUIApplication) {
		
		// Open send screen
		app.buttons["Send Enabled"].tap()
		sleep(2)
		
		
		// Enter address
		let addressTextField = app.textFields.containing(.button, identifier:"QR Light").element
		addressTextField.tap()
		addressTextField.typeText(SharedHelpers.address1_linear)
		SharedHelpers.shared.tapKeyboardDoneButton()
		SharedHelpers.shared.tapPrimaryButton()
		sleep(2)
		
		// Tap send all
		SharedHelpers.shared.tapSecondaryButton()
		SharedHelpers.shared.tapPrimaryButton()
		sleep(2)
		
		
		// Check confirm details
		SharedHelpers.shared.waitForStaticText("Confirm details", inApp: app, delay: 30)
		SharedHelpers.shared.tapPrimaryButton()
		
		
		// Wait for "wait" screen to show
		SharedHelpers.shared.waitForStaticText("This transaction will continue to process in the background if you navigate away from this screen.", inApp: app, delay: 30)
		
		
		// Wait for success screen
		SharedHelpers.shared.waitForStaticText("Success!", inApp: app, delay: 600)
		SharedHelpers.shared.tapPrimaryButton()
		sleep(2)
		
		
		// Verify all sent
		let xtzBalanceLabel = app.staticTexts[SharedHelpers.IdentifierConstants.home_totalXTZ].label
		let xtzBalance = xtzBalanceLabel.components(separatedBy: " ").first ?? "0"
		let xtzBalanceAsDecimal = Decimal(string: xtzBalance.replacingOccurrences(of: ",", with: "")) ?? 0
		
		XCTAssert(xtzBalanceAsDecimal == 0.000001)
	}
	
	func verifyActivityContains(amountString: String, inApp app: XCUIApplication) {
		
		// Navigate to activity screen (assuming we are on the root tabController)
		app.tabBars.children(matching: .button).element(boundBy: 3).tap()
		sleep(2)
		
		let topMostCell = app.tables.cells.element(boundBy: 0)
		let typeText = topMostCell.staticTexts[SharedHelpers.IdentifierConstants.activityCell_type].label
		let amountText = topMostCell.staticTexts[SharedHelpers.IdentifierConstants.activityCell_amount].label
		
		XCTAssert(typeText == "SEND", typeText)
		XCTAssert(amountText == amountString, "On activity screen: \"\(amountText)\", what code expected: \"\(amountString)\"")
	}
}
