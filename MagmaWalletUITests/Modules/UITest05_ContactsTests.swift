//
//  UITest05_ContactsTests.swift
//  MagmaWalletUITests
//
//  Created by Simon Mcloughlin on 17/09/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest

class UITest05_ContactsTests: XCTestCase {
	
	let testUserName = "Test user 1"
	let testUserNameEdited = "Test user 2"
	let testUsernameAddress = "tz1abcdefghijklmnopqrstuvwxyz1234567"
	let systemContactName = "John Appleseed"
	
	
	// MARK: - Setup
    override func setUpWithError() throws {
        continueAfterFailure = false
    }

    override func tearDownWithError() throws {
		
    }
	
	
	
	// MARK: - Test cases
	func test00_EnsureCleanState() {
		
		// In case a test fails on a previous run, ensure state permissions state is defaulted
		let settings = XCUIApplication(bundleIdentifier: "com.apple.Preferences")
		settings.launch()
		sleep(2)
		
		if settings.tables.cells["Magma"].firstMatch.exists {
			settings.tables.cells["Magma"].firstMatch.tap()
			sleep(2)
			
			let contactsCell = settings.tables.cells["Contacts"]
			let contactsSwitch = settings.tables.switches["Contacts"]
			if !SharedHelpers.shared.isCellWithSwitchOn(cell: contactsCell) {
				contactsSwitch.tap()
			}
			
			// Then launch shared app to make sure any lingering contacts get cleaned up
			SharedHelpers.shared.application(clearContacts: true).launch()
			
			// Wait to ensure theres enough time to process contacts
			sleep(20)
		}
	}
	
	func test01_CreateContactFromTabbar() {
		SharedHelpers.shared.application().launch()
		SharedHelpers.shared.makeSureWeAreLoggedInto(address: SharedHelpers.address1_linear, withMnemonic: SharedHelpers.mnemonic1, hd: false, passphrase: nil, derivationPath: nil)
		
		// Go to contacts tab and check its empty
		let app = XCUIApplication()
		app.tabBars.children(matching: .button).element(boundBy: 2).tap()
		sleep(1)
		
		let tablesQuery = app.tables
		XCTAssert(tablesQuery.cells.staticTexts["You have not added any contacts"].exists)
		
		// Add a contact
		SharedHelpers.shared.tapTopRightButton()
		sleep(2)
		
		
		// Check we are presented with permissions screen
		if app.buttons["APPROVE ACCESS"].exists {
			
			SharedHelpers.shared.tapPrimaryButton()
			sleep(2)
			
			
			// Test can't seem to access permission alert directly from magma, need to call out to springboard
			let springboard = XCUIApplication(bundleIdentifier: "com.apple.springboard")
			let alertAllowButton = springboard.buttons.element(boundBy: 1)
			if alertAllowButton.waitForExistence(timeout: 5) {
				alertAllowButton.tap()
			}
			
			sleep(10)
		}
		
		
		// Enter Name
		let nameTextfield = app.textFields.element(boundBy: 0)
		nameTextfield.tap()
		sleep(2)
		nameTextfield.typeText(testUserName)
		
		// Enter Address
		let addressTextField = app.textFields.element(boundBy: 1)
		addressTextField.tap()
		addressTextField.typeText(testUsernameAddress)
		app.keyboards.buttons["Done"].tap()
		SharedHelpers.shared.tapPrimaryButton()
		
		// Check user is now in list
		sleep(4)
		XCTAssert(tablesQuery.cells.staticTexts[testUserName].exists)
		
		
		
		// Go back to add contacts
		SharedHelpers.shared.tapTopRightButton()
		app.buttons["Contact Search"].tap()
		sleep(2)
		
		app.tables.cells.staticTexts[systemContactName].tap()
		sleep(3)
		
		
		// Enter Address
		addressTextField.tap()
		addressTextField.typeText(SharedHelpers.address1_hd)
		app.keyboards.buttons["Done"].tap()
		SharedHelpers.shared.tapPrimaryButton()
		
		// Check user is now in list
		sleep(4)
		XCTAssert(tablesQuery.cells.staticTexts[systemContactName].exists)
		
		
		
		// Try to add another address to same contact, should cause error
		SharedHelpers.shared.tapTopRightButton()
		app.buttons["Contact Search"].tap()
		sleep(2)
		
		app.tables.cells.staticTexts[systemContactName].tap()
		sleep(2)
		
		app.alerts["Error"].buttons["OK"].tap()
	}
	
	func test02_ContactDetails() {
		SharedHelpers.shared.application().launch()
		SharedHelpers.shared.enterPinCodeWithoutContinueButton()
		
		let app = XCUIApplication()
		app.tabBars.children(matching: .button).element(boundBy: 2).tap()
		app.tables.cells.staticTexts[testUserName].tap()
		sleep(2)
		
		
		XCTAssert(app.staticTexts[testUserName].exists)
		
		// iOS 12 and iOS 13 treat this element differently, despite having "static text" selected as the type
		var addressElement: XCUIElement? = nil
		if app.staticTexts.matching(identifier: "address-button").count == 0 {
			addressElement = app.buttons["address-button"]
		} else {
			addressElement = app.staticTexts["address-button"]
		}
		
		let buttonText1 = addressElement?.label ?? ""
		XCTAssert(buttonText1.prefix(10) == testUsernameAddress.prefix(10))
		
		checkHistory(forFirstContact: true, withApp: app)
		checkEdit(forFirstContact: true, withApp: app)
		sleep(2)
		
		app.tables.cells.staticTexts[systemContactName].tap()
		sleep(2)
		
		XCTAssert(app.staticTexts[systemContactName].exists)
		
		// iOS 12 and iOS 13 treat this element differently, despite having "static text" selected as the type
		if app.staticTexts.matching(identifier: "address-button").count == 0 {
			addressElement = app.buttons["address-button"]
		} else {
			addressElement = app.staticTexts["address-button"]
		}
		
		let buttonText2 = addressElement?.label ?? ""
		XCTAssert(buttonText2.prefix(10) == SharedHelpers.address1_hd.prefix(10))
		
		
		checkHistory(forFirstContact: false, withApp: app)
		checkEdit(forFirstContact: false, withApp: app)
		checkSend(withApp: app)
	}
	
	func checkHistory(forFirstContact: Bool, withApp app: XCUIApplication) {
		app.tables.cells.staticTexts["Transaction history"].tap()
		
		if forFirstContact {
			XCTAssert(app.tables.cells.staticTexts["You have no transaction history to display."].exists)
			
		} else {
			XCTAssert(app.tables.cells.containing(.staticText, identifier: "RECEIVE").staticTexts["From: John Appleseed"].exists)
			XCTAssert(app.tables.cells.containing(.staticText, identifier: "SEND").staticTexts["To: John Appleseed"].exists)
		}
		
		app.navigationBars["MagmaWallet.ContactTransactionHistoryView"].buttons["Back"].tap()
	}
	
	func checkEdit(forFirstContact: Bool, withApp app: XCUIApplication) {
		app.tables.cells.staticTexts["Edit"].tap()
		
		if forFirstContact {
			let nameTextfield = app.textFields.element(boundBy: 0)
			nameTextfield.tap()
			sleep(2)
			
			app.keyboards.keys["delete"].tap()
			app.keyboards.keys["more"].tap()
			app.keyboards.keys["2"].tap()
			app.keyboards.buttons["Done"].tap()
			SharedHelpers.shared.tapPrimaryButton()
			sleep(4)
			
			XCTAssert(app.tables.cells.staticTexts[testUserNameEdited].exists)
			
		} else {
			app.textFields.element(boundBy: 0).tap()
			sleep(2)
			
			XCTAssert(!app.keyboards.buttons["Done"].exists) // Textfield should be disabled so no keyboard should show
			XCTAssert(!app.buttons["Contact Search"].exists) // Contact search button should be removed
			
			app.navigationBars["MagmaWallet.AddContactView"].buttons["Back"].tap()
		}
	}
	
	func checkSend(withApp app: XCUIApplication) {
		app.tables.cells.staticTexts["Send to contact"].tap()
		app.tables.cells.staticTexts["XTZ"].tap()
		let textfield = app.textFields["0"]
		textfield.tap()
		sleep(2)
		
		textfield.typeText("1")
		SharedHelpers.shared.tapPrimaryButton()
		
		// Check confirm details
		SharedHelpers.shared.waitForStaticText("Confirm details", inApp: app, delay: 30)
		XCTAssert(app.staticTexts["To John Appleseed:"].exists)
		XCTAssert(app.staticTexts[SharedHelpers.address1_hd].exists)
	}
	
	func test03_SendWithContact() {
		SharedHelpers.shared.application().launch()
		SharedHelpers.shared.enterPinCodeWithoutContinueButton()
		
		let app = XCUIApplication()
		app.buttons["Send Enabled"].tap()
		app.segmentedControls.buttons["Select contact"].tap()
		app.tables.cells.staticTexts["John Appleseed"].tap()
		app.tables.cells.staticTexts["XTZ"].tap()
		let textfield = app.textFields["0"]
		textfield.tap()
		sleep(2)
		
		textfield.typeText("1")
		SharedHelpers.shared.tapPrimaryButton()
		
		// Check confirm details
		SharedHelpers.shared.waitForStaticText("Confirm details", inApp: app, delay: 30)
		XCTAssert(app.staticTexts["To John Appleseed:"].exists)
		XCTAssert(app.staticTexts[SharedHelpers.address1_hd].exists)
	}
	
	func test04_Delete() {
		SharedHelpers.shared.application().launch()
		SharedHelpers.shared.enterPinCodeWithoutContinueButton()
		
		let app = XCUIApplication()
		app.tabBars.children(matching: .button).element(boundBy: 2).tap()
		
		let tablesQuery = app.tables
		tablesQuery.cells.staticTexts["Test user 2"].tap()
		tablesQuery.cells.staticTexts["Delete"].tap()
		sleep(2)
		
		app.alerts.element(boundBy: 0).buttons["Confirm"].tap()
		sleep(4)
		
		
		tablesQuery.cells.staticTexts["John Appleseed"].tap()
		tablesQuery.cells.staticTexts["Delete"].tap()
		sleep(2)
		
		app.alerts.element(boundBy: 0).buttons["Confirm"].tap()
		sleep(4)
		
		XCTAssert(tablesQuery.cells.staticTexts["You have not added any contacts"].exists)
	}
	
	func test05_SendAddContact() {
		SharedHelpers.shared.application().launch()
		SharedHelpers.shared.enterPinCodeWithoutContinueButton()
		
		let app = XCUIApplication()
		app.buttons["Send Enabled"].tap()
		let textfield = app.textFields["tz1..."]
		textfield.tap()
		sleep(2)
		
		textfield.typeText(SharedHelpers.address1_hd)
		app.keyboards.buttons["Done"].tap()
		sleep(2)
		SharedHelpers.shared.tapSecondaryButton()
		
		let nameTextfield = app.textFields.element(boundBy: 0)
		nameTextfield.tap()
		sleep(2)
		
		nameTextfield.typeText(testUserName)
		app.keyboards.buttons["Done"].tap()
		sleep(2)
		
		SharedHelpers.shared.tapPrimaryButton()
		
		// Choose asset and wait for confirmation
		app.tables.cells.staticTexts["XTZ"].tap()
		let amountTextField = app.textFields["0"]
		amountTextField.tap()
		sleep(2)
		
		amountTextField.typeText("1")
		SharedHelpers.shared.tapPrimaryButton()
		
		// Check confirm details
		SharedHelpers.shared.waitForStaticText("Confirm details", inApp: app, delay: 30)
		XCTAssert(app.staticTexts["To \(testUserName):"].exists)
		XCTAssert(app.staticTexts[SharedHelpers.address1_hd].exists)
	}
	
	func test06_SendContactListAddContact() {
		SharedHelpers.shared.application().launch()
		SharedHelpers.shared.enterPinCodeWithoutContinueButton()
		
		let app = XCUIApplication()
		app.buttons["Send Enabled"].tap()
		app.segmentedControls.buttons["Select contact"].tap()
		app.buttons["Add Contact"].tap()
		sleep(2)
		
		// Enter Name
		let nameTextfield = app.textFields.element(boundBy: 0)
		nameTextfield.tap()
		sleep(2)
		
		nameTextfield.typeText(testUserName)
		app.keyboards.buttons["Done"].tap()
		sleep(2)
		
		
		// Enter Address
		let addressTextField = app.textFields.element(boundBy: 1)
		addressTextField.tap()
		addressTextField.typeText(SharedHelpers.address1_hd)
		app.keyboards.buttons["Done"].tap()
		SharedHelpers.shared.tapPrimaryButton()
		sleep(2)
		
		// Choose asset and wait for confirmation
		if app.staticTexts["Select asset"].exists {
			app.tables.cells.element(boundBy: 0).tap()
			sleep(2)
		}
		
		let amountTextField = app.textFields["0"]
		amountTextField.tap()
		sleep(2)
		
		amountTextField.typeText("1")
		SharedHelpers.shared.tapPrimaryButton()
		
		// Check confirm details
		SharedHelpers.shared.waitForStaticText("Confirm details", inApp: app, delay: 30)
		XCTAssert(app.staticTexts["To \(testUserName):"].exists)
		XCTAssert(app.staticTexts[SharedHelpers.address1_hd].exists)
		
		
		// Delete all contacts
		app.navigationBars["MagmaWallet.ConfirmSendView"].buttons["Back"].tap()
		sleep(1)
		
		app.navigationBars["MagmaWallet.ChooseAmountView"].buttons["Back"].tap()
		sleep(1)
		
		app.navigationBars["MagmaWallet.ChooseTokenView"].buttons["Back"].tap()
		sleep(1)
		
		app.navigationBars["MagmaWallet.AddContactView"].buttons["Back"].tap()
		sleep(1)
		
		app.navigationBars["MagmaWallet.ChooseAddressContainerView"].buttons["Back"].tap()
		sleep(1)
		
		app.tabBars.children(matching: .button).element(boundBy: 2).tap()
		
		var contactCount = app.tables.cells.count
		while contactCount > 0 {
			app.tables.cells.element(boundBy: 0).tap()
			app.tables.cells.staticTexts["Delete"].tap()
			sleep(2)
			
			app.alerts["Delete contact"].buttons["Confirm"].tap()
			sleep(4)
			
			contactCount -= 1
		}
		
		XCTAssert(app.tables.cells.staticTexts["You have not added any contacts"].exists)
	}
	
	func test07_SystemContactStillExists() {
		let contacts = XCUIApplication(bundleIdentifier: "com.apple.MobileAddressBook")
		contacts.launch()
		
		// Confrim that deleting system contacts in Magma, doesn't delete the contact record
		XCTAssert(contacts.tables.cells.staticTexts[systemContactName].exists)
	}
	
	func test08_RemovePermission() {
		let settings = XCUIApplication(bundleIdentifier: "com.apple.Preferences")
		settings.launch()
		sleep(1)
		
		settings.tables.cells["Magma"].tap()
		sleep(1)
		
		let contactsCell = settings.tables.cells["Contacts"]
		let contactsSwitch = settings.tables.switches["Contacts"]
		XCTAssert(SharedHelpers.shared.isCellWithSwitchOn(cell: contactsCell))
		
		contactsSwitch.tap()
		XCTAssert(!SharedHelpers.shared.isCellWithSwitchOn(cell: contactsCell))
		
		
		// Launch Magma
		SharedHelpers.shared.application().launch()
		SharedHelpers.shared.enterPinCodeWithoutContinueButton()
		let app = XCUIApplication()
		
		app.buttons["Send Enabled"].tap()
		
		// Check the add contacts button on the enter screen opens permission screen
		let tz1TextField = app.textFields["tz1..."]
		tz1TextField.tap()
		sleep(2)
		
		tz1TextField.typeText(SharedHelpers.address1_hd)
		app.keyboards.buttons["Done"].tap()
		sleep(2)
		
		SharedHelpers.shared.tapSecondaryButton()
		XCTAssert(app.buttons["APPROVE ACCESS"].exists)
		
		
		// Check add contact button on send contact list opens permission
		let backButton = app.navigationBars["MagmaWallet.ContactsPermissionView"].buttons["Back"]
		backButton.tap()
		app.segmentedControls.buttons["Select contact"].tap()
		app.buttons["Add Contact"].tap()
		XCTAssert(app.buttons["APPROVE ACCESS"].exists)
		
		
		// Check contacts tab add button, opens permission screen
		backButton.tap()
		app.navigationBars["MagmaWallet.ChooseAddressContainerView"].buttons["Back"].tap()
		app.tabBars.children(matching: .button).element(boundBy: 2).tap()
		SharedHelpers.shared.tapTopRightButton()
		XCTAssert(app.buttons["APPROVE ACCESS"].exists)
		
		
		// Rest permissions for next run
		SharedHelpers.shared.tapPrimaryButton()
		
		XCTAssert(!SharedHelpers.shared.isCellWithSwitchOn(cell: contactsCell))
		
		contactsSwitch.tap()
		XCTAssert(SharedHelpers.shared.isCellWithSwitchOn(cell: contactsCell))
	}
}
