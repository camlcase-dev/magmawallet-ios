//
//  UITest02_LoginTests.swift
//  MagmaWalletUITests
//
//  Created by Simon Mcloughlin on 25/06/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest

class UITest02_LoginTests: XCTestCase {
	
	// MARK: - Setup
    override func setUpWithError() throws {
        continueAfterFailure = false
		
        SharedHelpers.shared.application().launch()
    }

    override func tearDownWithError() throws {
		
    }
	
	
	
	// MARK: - Test cases
	func testPinCodeRequiredOnForeground() {
		let app = XCUIApplication()
		
		if !SharedHelpers.shared.didLaunchOnPinCode() {
			// If we don't start on the pincode, create a wallet
			UITest01_OnboardingTests().test01_CreateNewWallet()
			
		} else if SharedHelpers.shared.didLaunchOnPinCode() {
			// Else if we are presented with a pinCode screen on launch, login
			SharedHelpers.shared.enterPinCodeWithoutContinueButton()
		}
		
		// Press back button
		XCUIDevice.shared.press(.home)
		
		sleep(2)
		app.launch()
		
		sleep(2)
		XCTAssertTrue(app.staticTexts["Please enter your PIN code"].exists)
	}
	
	func testUserCanLoginAfterPressingBack() {
		let app = XCUIApplication()
		
		if !SharedHelpers.shared.didLaunchOnPinCode() {
			// If we don't start on the pincode, create a wallet
			UITest01_OnboardingTests().test01_CreateNewWallet()
			
			// Press back button and launch again
			XCUIDevice.shared.press(.home)
			
			sleep(2)
			app.launch()
		}
		
		sleep(2)
		app.buttons["Back"].tap()
		app.buttons["Login"].tap()
		
		sleep(2)
		SharedHelpers.shared.enterPinCodeWithoutContinueButton()
		
		sleep(3)
		XCTAssertTrue(app.tabBars.children(matching: .button).element(boundBy: 3).exists) // verify settings exists
	}
}
