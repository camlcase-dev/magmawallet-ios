//
//  SharedHelpers.swift
//  MagmaWalletUITests
//
//  Created by Simon Mcloughlin on 24/06/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import XCTest
import Keys

class SharedHelpers: XCTestCase {
	
	static let shared = SharedHelpers()
	
	static let mnemonic1 = MagmaWalletKeys().uitest_mnemonic1
	static let address1_linear = MagmaWalletKeys().uitest_address1_linear
	static let address1_hd = MagmaWalletKeys().uitest_address1_hd
	static let mnemonic2 = MagmaWalletKeys().uitest_mnemonic2
	static let address2_linear = MagmaWalletKeys().uitest_address2_linear
	static let address2_hd = MagmaWalletKeys().uitest_address2_hd
	static let passphrase = MagmaWalletKeys().uitest_passphrase
	static let derivationPath = MagmaWalletKeys().uitest_derivationPath
	
	static let settingsTabIndex = 4
	
	private let sharedApplication = XCUIApplication()
	private var launchCount = 0
	
	struct IdentifierConstants {
		static let welcome_login = "welcome-login-button"
		
		static let home_totalXTZ = "home-total-xtz"
		
		static let tokenDetails_tokenBalance = "token-details-balance"
		static let tokenDetails_tokenSymbol = "token-details-token-symbol"
		
		static let activityCell_type = "activity-cell-type"
		static let activityCell_amount = "activity-cell-amount"
		static let activityCell_secondAmount = "activity-cell-second-amount" // Only for type = Exchange
	}
	
	
	
	// MARK: - Setup
    override func setUpWithError() throws {
        continueAfterFailure = false
    }

    override func tearDownWithError() throws {
    }
	
	
	// MARK: - Helper functions
	func application(clearContacts: Bool = false) -> XCUIApplication {
		sharedApplication.launchEnvironment = ["XCUITEST-KEYBOARD": "true"]
		
		if launchCount == 0 {
			sharedApplication.launchEnvironment["XCUITEST-RESET"] = "true"
			launchCount += 1
		}
		
		if clearContacts {
			sharedApplication.launchEnvironment["XCUITEST-CLEAR-CONTACTS"] = "true"
		}
		
		return sharedApplication
	}
	
	func waitForStaticText(_ string: String, inApp app: XCUIApplication, delay: TimeInterval) {
		let obj = app.staticTexts[string]
		let exists = NSPredicate(format: "exists == 1")

		expectation(for: exists, evaluatedWith: obj, handler: nil)
		waitForExpectations(timeout: delay, handler: nil)
	}
	
	func waitForLabelToLoadData(accessibilityId: String, inApp app: XCUIApplication, delay: TimeInterval) {
		let obj = app.staticTexts[accessibilityId]
		let exists = NSPredicate(format: "(exists == 1) AND (label.length > 1)")
		
		expectation(for: exists, evaluatedWith: obj, handler: nil)
		waitForExpectations(timeout: delay, handler: nil)
	}
	
	func didLaunchOnPinCode() -> Bool {
		sleep(2) // Short delay to account for animations
		
		return application().staticTexts["Please enter your PIN code"].exists
	}
	
	func didLaunchOnWelcome() -> Bool {
		sleep(2) // Short delay to account for animations
		
		return application().staticTexts["CREATE NEW WALLET"].exists
	}
	
	func enterPinCode() {
		enterPinCodeWithoutContinueButton(code: "000000")
		tapPrimaryButton()
	}
	
	func enterUpdatedPinCode() {
		enterPinCodeWithoutContinueButton(code: "000001")
		tapPrimaryButton()
	}
	
	func enterPinCodeWithoutContinueButton(code: String = "000000") {
		let app = application()
		
		sleep(2) // wait for keyboard to show
		let inputField = app.secureTextFields["pincode-field"]
		inputField.tap()
		
		sleep(2)
		inputField.typeText(code)
	}
	
	func unpairWalletIfPaired() throws {
		if didLaunchOnPinCode() {
			enterPinCodeWithoutContinueButton()
			
			sleep(2)
			unpairWallet()
		}
	}
	
	func unpairWallet() {
		sharedApplication.tabBars.children(matching: .button).element(boundBy: SharedHelpers.settingsTabIndex).tap()
		sharedApplication.tables.staticTexts["Your funds will not be lost, but you will need to import your recovery phrase to this or any other device to restore access."].tap()
		sharedApplication.alerts["Wait!"].buttons["OK"].tap()
		
		sleep(5)
	}
	
	func getCurrentWalletAddressFromWalletScreen() -> String {
		sharedApplication.buttons["Receive"].tap()
		sleep(2)
		
		let address = sharedApplication.staticTexts["current-address-label"].label
		sharedApplication.buttons["close-button"].tap()
		sleep(2)
		
		return address
	}
	
	
	// MARK: - Tapping buttons
	func tapPrimaryButton() {
		sharedApplication.buttons["primary-button"].tap()
		sleep(2)
	}
	
	func tapSecondaryButton() {
		sharedApplication.buttons["secondary-button"].tap()
		sleep(2)
	}
	
	func tapDropdownButton() {
		sharedApplication.buttons["dropdown-button"].tap()
		sleep(2)
	}
	
	func tapLinkButton() {
		sharedApplication.buttons["link-button"].tap()
		sleep(2)
	}
	
	func tapShareButton() {
		sharedApplication.buttons["share-button"].tap()
		sleep(2)
	}
	
	func tapMaxButton() {
		sharedApplication.buttons["max-button"].tap()
		sleep(2)
	}
	
	func tapExchangeRateButton() {
		sharedApplication.buttons["exchangerate-button"].tap()
		sleep(2)
	}
	
	func tapToolBarDoneButton() {
		sharedApplication.toolbars["Toolbar"].buttons["Done"].tap()
		sleep(2)
	}
	
	func tapKeyboardDoneButton() {
		sharedApplication.keyboards.buttons["Done"].tap()
		sleep(2)
	}
	
	
	// MARK: - Account functions
	
	func makeSureWeAreLoggedInto(address: String, withMnemonic mnemonic: String, hd: Bool, passphrase: String?, derivationPath: String?) {
		if !SharedHelpers.shared.didLaunchOnPinCode() {
			
			// If we have not imported any account, import the account we want
			UITest01_OnboardingTests().importWalletAndVerifyAddress(app: sharedApplication, mnemonic: mnemonic, expectedAddress: address, hd: hd, passphrase: passphrase, derivationPath: derivationPath)
			
		} else {
			
			// If we have already imported, login, check its the one we want
			SharedHelpers.shared.enterPinCodeWithoutContinueButton()
			sleep(2)
			
			// If not the correct account, logout and import
			if SharedHelpers.shared.getCurrentWalletAddressFromWalletScreen() != address {
				SharedHelpers.shared.unpairWallet()
				UITest01_OnboardingTests().importWalletAndVerifyAddress(app: sharedApplication, mnemonic: mnemonic, expectedAddress: address, hd: hd, passphrase: passphrase, derivationPath: derivationPath)
			}
			
			SharedHelpers.shared.waitForLabelToLoadData(accessibilityId: "home-total-xtz", inApp: SharedHelpers.shared.application(), delay: 60)
		}
	}
	
	
	// MARK: - Misc
	func tapCoordinate(x: CGFloat, y: CGFloat) {
		let normalized = sharedApplication.coordinate(withNormalizedOffset: CGVector(dx: 0, dy: 0))
		let coordinate = normalized.withOffset(CGVector(dx: x, dy: y))
		coordinate.tap()
	}
	
	func tapTopRightButton() {
		let right = sharedApplication.frame.size.width
		
		if right <= 320 {
			tapCoordinate(x: right-30, y: 40)
			
		} else {
			tapCoordinate(x: right-40, y: 65)
		}
		sleep(2)
	}
	
	// Issue in iOS 12 fetching the `value` of a switch, but the answer is present in the debug description
	func isCellWithSwitchOn(cell: XCUIElement) -> Bool {
		return cell.debugDescription.contains("value: 1")
	}
}
