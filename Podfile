$minSupportedOS = '12.0'

platform :ios, $minSupportedOS

use_frameworks!
inhibit_all_warnings!

# Temporary workaround to CDN offline, breaking builds
#source 'https://github.com/CocoaPods/Specs.git'
#source 'https://cdn.cocoapods.org/'


def shared_pods
  #pod 'camlKit', '~> 0.3.17'
  pod 'camlKit', :git => 'https://gitlab.com/camlcase-dev/camlkit.git', :tag => '0.3.17' # Remove delay from trunk updates to push up a bug fix
  #pod 'camlKit', :path => '~/Documents/projects/camlKit'
end

target 'MagmaWallet' do
  shared_pods
  pod 'KeychainSwift', '19.0'
  pod 'Sentry', '6.0.10'
  pod 'Kingfisher', '5.13.1'
  pod 'Toast-Swift', '~> 5.0.1'
  pod 'lottie-ios', '3.1.8'
end

target 'MagmaWalletTests' do
  shared_pods
end

target 'MagmaWalletUITests' do
  shared_pods
end

target 'MagmaWalletKitTests' do
  shared_pods
end


# Plugins
plugin 'cocoapods-keys', {
  :project => "MagmaWallet",
  :target => ["MagmaWallet", "MagmaWalletUITests"],
  :keys => [
    "sentryDSN",
    "tezosNodeURL",
    "tezosNodeURL_parse",
    "tezosNodeURL_credentials",
    "tezosNodeURL_testnet",
    "tezosNodeURL_testnet_parse",
    "tezosNodeURL_testnet_credentials",
    "tzktURL",
    "tzktURL_testnet",
    "moonPayAPIKey",
    "moonPayAPIKey_testnet",
    "uitest_mnemonic1",
    "uitest_address1_linear",
    "uitest_address1_hd",
    "uitest_mnemonic2",
    "uitest_address2_linear",
    "uitest_address2_hd",
    "uitest_passphrase",
    "uitest_derivationPath"
  ]
}


# Hide all warnings from cocoapods
post_install do |installer|

  installer.pods_project.targets.each do |target|
    target.build_configurations.each do |config|

        # Tell Xcode to inhibit all warnings
        config.build_settings['GCC_WARN_INHIBIT_ALL_WARNINGS'] = "YES"

        # Stop warning about overriding always embed setting
        config.build_settings['ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES'] = '$(inherited)'

        # Stop a app store connect error "Too many symbols"
        config.build_settings['IPHONEOS_DEPLOYMENT_TARGET'] = $minSupportedOS

        # Xcode 12 error forcing pods to build for different archs
        config.build_settings["EXCLUDED_ARCHS[sdk=iphonesimulator*]"] = "arm64"
    end
  end

  # Pod-Framework
  installer.pods_project.targets.each do |target|
      if target.name == 'Pod-Framework'
          target.build_configurations.each do |config|
              config.build_settings['WARNING_CFLAGS'] ||= ['"-Wno-nullability-completeness"']
          end
      end
  end

  # This removes the warning about swift conversion, hopefuly forever!
  installer.pods_project.root_object.attributes['LastSwiftMigration'] = 9999
  installer.pods_project.root_object.attributes['LastSwiftUpdateCheck'] = 9999
  installer.pods_project.root_object.attributes['LastUpgradeCheck'] = 9999
end
